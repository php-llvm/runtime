//===--- box_access.cpp ---------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implements access to universal type values.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include <iostream>
#include <assert.h>
#include "box.h"
#include "functions.h"
#include "log.h"
//------------------------------------------------------------------------------

namespace phprt {


// box accessors functions for type hints
PHPRT_API bool checked_get_bool_strict(const Box& v) {
  if (v.is_bool())
    return v.get_bool();
  
  TypeError("expected boolean value");
  return false;
}

PHPRT_API bool checked_get_bool_weak(const Box& v) {
  if (v.is_string())
    return v.to_bool();

  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
    return v.to_bool();
  default:
    break;
  }

  TypeError("expected boolean value");
  return false;
}


PHPRT_API intvalue_t checked_get_integer_strict(const Box& v) {
  if (v.is_integer())
    return v.get_integer();

  TypeError("expected integer value");
  return 0;
}

PHPRT_API intvalue_t checked_get_integer_weak(const Box& v) {
  if (v.is_string() && starts_with_number(v.get_string()))
    return v.to_integer();

  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
    return v.to_integer();

  default:
    break;
  }

  TypeError("expected integer value");
  return 0;
}


PHPRT_API double checked_get_double_strict(const Box& v) {
  if (v.is_double() || v.is_integer())
    return v.to_double();
  
  TypeError("expected double value");
  return 0;
}

PHPRT_API double checked_get_double_weak(const Box& v) {
  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
    return v.to_double();

  case vkConstString:
    if (starts_with_number(v.get_string()))
      return v.to_double();

    // Fall through
  default:
    break;
  }
  
  TypeError("expected double value");
  return 0;
}


PHPRT_API StringBox checked_get_string_strict(const Box& v) {
  if (v.is_string())
    return (StringBox)v;
  
  TypeError("expected string value");
  return STRING_REF("");
}

PHPRT_API StringBox checked_get_string_weak(const Box& v) {
  if (v.is_string())
    return (StringBox&)v;

  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
    return v.to_string();

  default:
    break;
  }

  TypeError("expected string value");
  return STRING_REF("");
}


PHPRT_API ArrayBox checked_get_array(const Box& v) {
  if (v.is_array()) 
    return v.get_array();

  TypeError("expected array value");
  return ArrayBox();
}


PHPRT_API RefBox<bool> checked_get_bool_ref_strict(Box& v) {
  if (!v.is_bool()) {
    TypeError("expected boolean value");
    v.set_false();
  }
  
  v.make_reference();
  return (Boxed<bool>&)v;
}

PHPRT_API RefBox<bool> checked_get_bool_ref_weak(Box& v) {
  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
  case vkConstString:
    v.set_bool(v.to_bool());
    break;

  default:
    TypeError("expected boolean value");
    v.set_false();
    break;
  }

  v.make_reference();
  return (Boxed<bool>&)v;
}


PHPRT_API RefBox<intvalue_t> checked_get_integer_ref_strict(Box& v) {
  if (!v.is_integer()) {
    TypeError("expected integer value");
    v.set_integer(0);
  }
  v.make_reference();
  return (Boxed<intvalue_t>&)v;
}


PHPRT_API RefBox<intvalue_t> checked_get_integer_ref_weak(Box& v) {
  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
    v.set_integer(v.to_integer());
    break;

  case vkConstString:
    if (starts_with_number(v.get_string())) {
      v.set_integer(v.to_integer());
      break;
    }

    // Fall through
  default:
    TypeError("expected integer value");
    v.set_integer(0);
    break;
  }

  v.make_reference();
  return (Boxed<intvalue_t>&)v;
}


PHPRT_API RefBox<double> checked_get_double_ref_strict(Box& v) {
  if (!v.is_double()) {
    TypeError("expected double value");
    v.set_double(0);
  }
  v.make_reference();
  return (Boxed<double>&)v;
}

PHPRT_API RefBox<double> checked_get_double_ref_weak(Box& v) {
  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
    v.set_double(v.to_double());
    break;

  case vkConstString:
    if (starts_with_number(v.get_string())) {
      v.set_double(v.to_double());
      break;
    }
    // Fall through
  default:
    TypeError("expected double value");
    v.set_double(0);
    break;
  }
  v.make_reference();
  return (Boxed<double>&)v;
}


PHPRT_API RefBox<StringBox> checked_get_string_ref_strict(Box& v) {
  if (!v.is_string()) {
    TypeError("expected string value");
    v.set_str("");
  }
  v.make_reference();
  return (Boxed<StringBox>&)v;
}

PHPRT_API RefBox<StringBox> checked_get_string_ref_weak(Box& v) {
  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
  case vkConstString:
    v.copy_value(v.to_string());
    break;
  default:
    TypeError("expected string value");
    v.set_str("");
    break;
  }
  v.make_reference();
  return (Boxed<StringBox>&)v;
}


PHPRT_API RefBox<ArrayBox> checked_get_array_ref(Box& v) {
  if (v.is_array())
    return (Boxed<ArrayBox>&)v;

  TypeError("expected array type");
  //TODO: v.set_array();
  return (Boxed<ArrayBox>&)v;
}


PHPRT_API bool &checked_access_bool_strict(Box& v) {
  if (!v.is_bool()) {
    TypeError("expected boolean value");
    v.set_false();
  }
  return v.access_bool();
}

PHPRT_API bool &checked_access_bool_weak(Box& v) {
  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
  case vkConstString:
    v.set_bool(v.to_bool());
    break;
  default:
    TypeError("expected boolean value");
    v.set_false();
    break;
  }
  return v.access_bool();
}


PHPRT_API intvalue_t &checked_access_integer_strict(Box& v) {
  if (!v.is_integer()) {
    TypeError("expected integer value");
    v.set_integer(0);
  }
  return v.access_integer();
}


PHPRT_API intvalue_t &checked_access_integer_weak(Box& v) {
  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
    v.set_integer(v.to_integer());
    break;

  case vkConstString:
    if (starts_with_number(v.get_string())) {
      v.set_integer(v.to_integer());
      break;
    }

    // Fall through
  default:
    TypeError("expected integer value");
    v.set_integer(0);
    break;
  }
  return v.access_integer();
}


PHPRT_API double &checked_access_double_strict(Box& v) {
  if (!v.is_double()) {
    TypeError("expected double value");
    v.set_double(0);
  }
  return v.access_double();
}

PHPRT_API double &checked_access_double_weak(Box& v) {
  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
    v.set_double(v.to_double());
    break;

  case vkConstString:
    if (starts_with_number(v.get_string())) {
      v.set_double(v.to_double());
      break;
    }

    // Fall through
  default:
    TypeError("expected double value");
    v.set_double(0);
    break;
  }
  return v.access_double();
}


PHPRT_API StringBox &checked_access_string_strict(Box& v) {
  if (!v.is_string()) {
    TypeError("expected string value");
    v.set_str("");
  }
  return static_cast<StringBox&>(v);
}

PHPRT_API StringBox &checked_access_string_weak(Box& v) {
  switch (v.get_target_type()) {
  case vkBool:
  case vkInteger:
  case vkDouble:
  case vkConstString:
    v.copy_value(v.to_string());
    break;

  default:
    TypeError("expected string value");
    v.set_str("");
    break;
  }
  return static_cast<StringBox&>(v);
}


PHPRT_API IArray *checked_access_array(Box& v) {
  if (v.is_array())
    return v.get_array();

  TypeError("expected array value");
  //TODO: v.set_array();
  return v.get_array();
}

} //namespace phprt