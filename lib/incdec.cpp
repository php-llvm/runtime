//===--- incdec.cpp -------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Increment/decrement function implementation.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "functions.h"
#include "log.h"
#include "conversion/number_recognizer.h"
#include <limits>
//------------------------------------------------------------------------------

namespace phprt {

static unsigned get_increment_extra(string_ref s) {
  assert(!s.empty());
  for (int i = s.len - 1; i >= 0; --i) {
    char c = s[i];
    if (c == 'Z' || c == 'z' || c == '9')
      continue;
    return 0;
  }
  return 1;
}

static void increment_string(char *str, unsigned size, unsigned extra) {
  assert(size != 0);
  assert(extra == get_increment_extra(string_ref::make(str, size)));

  if (extra)
    memmove(str + extra, str, size * sizeof(char));

  bool overflow = false;
  enum { lower = 'a', upper = 'A', numeric = '1' } last = lower;
  for (int i = extra + size - 1; i >= (int)extra; --i) {
    char c = str[i];
    if ('a' <= c && c <= 'z') {
      overflow = (c == 'z');
      if (overflow)
        str[i] = 'a';
      else
        ++str[i];
      last = lower;
    }
    else if ('A' <= c && c <= 'Z') {
      overflow = (c == 'Z');
      if (overflow)
        str[i] = 'A';
      else
        ++str[i];
      last = upper;
    }
    else if ('0' <= c && c <= '9') {
      overflow = (c == '9');
      if (overflow)
        str[i] = '0';
      else
        ++str[i];
      last = numeric;
    } else {
      overflow = false;
      break;
    }
    if (!overflow)
      break;
  }
  if (overflow)
    str[0] = last;
}


PHPRT_API void increment_string(Box &v) {
  string_ref str = v.get_string();
  if (str.empty()) {
    v.set_str("1");
    return;
  }

  conversion::NumberRecognizer<> Recog;
  Recog.recognize(str.data, str.len);
  if (Recog.success()) {
    if (Recog.is_float())
      v.set_double(Recog.to<double>() + 1.0);
    else {
      intvalue_t val;
      auto st = Recog.read(val);
      if (st == conversion::IntOverflow ||
          val == std::numeric_limits<intvalue_t>::max()) {
        v.set_double(Recog.to<double>() + 1.0);
      } else {
        v.set_integer(Recog.to<intvalue_t>() + 1);
      }
    }
    return;
  }

  unsigned extra = get_increment_extra(str);
  char *buf = v.get_string_buffer(extra);
  increment_string(buf, str.len, extra);
  return;
}


PHPRT_API void decrement_string(Box &v) {
  string_ref str = v.get_string();
  if (str.empty()) {
    v.set_integer(-1);
    return;
  }

  conversion::NumberRecognizer<> Recog;
  Recog.recognize(str.data, str.len);
  if (Recog.success()) {
    if (Recog.is_float())
      v.set_double(Recog.to<double>() - 1.0);
    else {
      intvalue_t val;
      auto st = Recog.read(val);
      if (st == conversion::IntOverflowNegative ||
          val == std::numeric_limits<intvalue_t>::min()) {
        v.set_double(Recog.to<double>() - 1.0);
      } else {
        v.set_integer(Recog.to<intvalue_t>() - 1);
      }
    }
    return;
  }

  TypeWarning("string decrement is not allowed");
}


PHPRT_API Box prefix_increment(box &v) {
  if (v.is_integer()) {
    auto val = v.get_integer();
    if (val == std::numeric_limits<intvalue_t>::max())
      v.set_double(val + 1.0);
    else
      v.set_integer(val + 1);
    return (Box&)v;
  }
  
  if (v.is_double())
    return prefix_increment_double(v.access_double());

  if (v.is_string()) {
    increment_string((Box&)v);
    return (Box)v;
  }

  if (v.is_object()) {
    assert(0 && "object increment is not implemented yet");
  }
  TypeError("prefix increment is not allowed for %0 type") << v.get_type_name();
  return (Box&)v;
}


PHPRT_API Box postfix_increment(box &v) {
  Box result = (Box&)v;

  if (v.is_integer()) {
    auto val = v.get_integer();
    if (val == std::numeric_limits<intvalue_t>::max())
      v.set_double(val + 1.0);
    else
      v.set_integer(val + 1);
    return result;
  }

  if (v.is_double())
    return postfix_increment_double(v.access_double());

  if (v.is_string()) {
    increment_string((Box&)v);
    return result;
  }

  if (v.is_object()) {
    assert(0 && "object increment is not implemented yet");
  }
  TypeError("postfix increment is not allowed for %0 type") << v.get_type_name();
  return (Box&)v;
}


PHPRT_API Box prefix_decrement(box &v) {
  if (v.is_integer()) {
    auto val = v.get_integer();
    if (val == std::numeric_limits<intvalue_t>::min())
      v.set_double(val - 1.0);
    else
      v.set_integer(val - 1);
    return (Box&)v;
  }

  if (v.is_double())
    return prefix_decrement_double(v.access_double());

  if (v.is_string()) {
    decrement_string((Box&)v);
    return (Box&)v;
  }

  if (v.is_object()) {
    assert(0 && "object decrement is not implemented yet");
  }
  TypeError("prefix decrement is not allowed for %0 type") << v.get_type_name();
  return (Box&)v;
}


PHPRT_API Box postfix_decrement(box &v) {
  Box result = (Box&)v;
  if (v.is_integer()) {
    auto val = v.get_integer();
    if (val == std::numeric_limits<intvalue_t>::min())
      v.set_double(val - 1.0);
    else
      v.set_integer(val - 1);
    return result;
  }

  if (v.is_double())
    return postfix_decrement_double(v.access_double());

  if (v.is_string()) {
    decrement_string((Box&)v);
    return result;
  }

  if (v.is_object()) {
    assert(0 && "object decrement is not implemented yet");
  }
  TypeError("postfix decrement is not allowed for %0 type") << v.get_type_name();
  return result;
}


PHPRT_API double prefix_increment_double(double &v) {
  return v += 1;
}


PHPRT_API double postfix_increment_double(double &v) {
  double result = v;
  v += 1;
  return result;
}


PHPRT_API double prefix_decrement_double(double &v) {
  return v -= 1;
}


PHPRT_API double postfix_decrement_double(double &v) {
  double result = v;
  v -= 1;
  return result;
}

}