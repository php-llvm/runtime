//===--- layout.cpp ---------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definition of symbols that mark different sections in PHP application.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "layout.h"
#include "shared/string_ref.h"
#include "unit.h"
#include "globals.h"
//------------------------------------------------------------------------------

#if defined(_MSC_VER)     //------ MSVC ----------------------------------------

#pragma section("phpc", read)
#pragma section("phpd", read, write)


// Section of Unit descriptors
//
#pragma const_seg(push, UNIT_SECTION_NAME0)
__declspec(allocate(UNIT_SECTION_NAME0)) static void * const UnitSectionStart = nullptr;
#pragma const_seg(pop)

#pragma const_seg(push, UNIT_SECTION_NAME9)
__declspec(allocate(UNIT_SECTION_NAME9)) static void * const UnitSectionEnd = nullptr;
#pragma const_seg(pop)


// Section of UnitStates
//
#pragma data_seg(push, UNITSTATE_SECTION_NAME0)
__declspec(allocate(UNITSTATE_SECTION_NAME0)) static void *UnitStateSectionStart = nullptr;
#pragma data_seg(pop)

#pragma data_seg(push, UNITSTATE_SECTION_NAME9)
__declspec(allocate(UNITSTATE_SECTION_NAME9)) static void *UnitStateSectionEnd = nullptr;
#pragma data_seg(pop)

// Section of Global Vars Records
//
#pragma data_seg(push, GVR_SECTION_NAME0)
__declspec(allocate(GVR_SECTION_NAME0)) static void *GVRSectionStart = nullptr;
#pragma data_seg(pop)

#pragma data_seg(push, GVR_SECTION_NAME9)
__declspec(allocate(GVR_SECTION_NAME9)) static void *GVRSectionEnd = nullptr;
#pragma data_seg(pop)

// Section of compile time constants
//
#pragma const_seg(push, CTC_SECTION_NAME0)
__declspec(allocate(CTC_SECTION_NAME0)) static void * const CTConstantSectionStart = nullptr;
#pragma const_seg(pop)

#pragma const_seg(push, CTC_SECTION_NAME9)
__declspec(allocate(CTC_SECTION_NAME9)) static void * const CTConstantSectionEnd = nullptr;
#pragma const_seg(pop)

// Section of runtime constants
//
#pragma data_seg(push, RTC_SECTION_NAME0)
__declspec(allocate(RTC_SECTION_NAME0)) static void *RTConstantSectionStart = nullptr;
#pragma data_seg(pop)

#pragma data_seg(push, RTC_SECTION_NAME9)
__declspec(allocate(RTC_SECTION_NAME9)) static void *RTConstantSectionEnd = nullptr;
#pragma data_seg(pop)


// Merge php sections to respective C++ sections.
// #pragma comment(linker, "/merge:phpc=.rdata")
// #pragma comment(linker, "/merge:phpd=.data")
// #pragma comment(linker, "/merge:phpb=.bss")

#elif defined(__APPLE__)

extern "C" const phprt::Unit __start_phpcu __asm("section$start$__TEXT$__phpcu");
extern "C" const phprt::Unit __stop_phpcu __asm("section$end$__TEXT$__phpcu");
extern "C" phprt::UnitState  __start_phpdu __asm("section$start$__DATA$__phpdu");
extern "C" phprt::UnitState  __stop_phpdu __asm("section$end$__DATA$__phpdu");
extern "C" phprt::VarRecord __start_phpdg __asm("section$start$__DATA$__phpdg");
extern "C" phprt::VarRecord __stop_phpdg __asm("section$end$__DATA$__phpdg");
extern "C" const phprt::ValueInfo __start_phpcc __asm("section$start$__TEXT$__phpcc");
extern "C" const phprt::ValueInfo __stop_phpcc __asm("section$end$__TEXT$__phpcc");
extern "C" phprt::ValueInfo __start_phpdc __asm("section$start$__DATA$__phpdc");
extern "C" phprt::ValueInfo __stop_phpdc __asm("section$end$__DATA$__phpdc");

#elif defined(__GNUC__)   //------ GCC -----------------------------------------

extern "C" const phprt::Unit __start_phpcu[];
extern "C" const phprt::Unit __stop_phpcu[];
extern "C" phprt::UnitState  __start_phpdu[];
extern "C" phprt::UnitState  __stop_phpdu[];
extern "C" phprt::VarRecord __start_phpdg[];
extern "C" phprt::VarRecord __stop_phpdg[];
extern "C" const phprt::ValuInfo __start_phpcc[];
extern "C" const phprt::ValueInfo __stop_phpcc[];
extern "C" phprt::ValueInfo __start_phpdc[];
extern "C" phprt::ValueInfo __stop_phpdc[];

#else                     //----------------------------------------------------
#  error "Unsupported compiler"
#endif


namespace phprt {

const Unit *getUnitSectionStart() {
#ifdef __APPLE__
  return &__start_phpcu;
#elif defined(__GNUC__)
  return &__start_phpcu[0];
#else
  void * const *cursor = &UnitSectionStart + 1;
  while (!*cursor && cursor < &UnitSectionEnd)
    ++cursor;
  return (const Unit*)cursor;
#endif
}

const Unit *getUnitSectionEnd() {
#ifdef __APPLE__
  return &__stop_phpcu;
#elif defined(__GNUC__)
  return &__stop_phpcu[0];
#else
  return (const Unit*)(&UnitSectionEnd);
#endif
}

UnitState *getUnitStateSectionStart() {
#ifdef __APPLE__
  return &__start_phpdu;
#elif defined(__GNUC__)
  return &__start_phpdu[0];
#else
  void **cursor = &UnitStateSectionStart + 1;
  while (!*cursor && cursor < &UnitStateSectionEnd)
    ++cursor;
  return (UnitState*)cursor;
#endif
}

UnitState *getUnitStateSectionEnd() {
#ifdef __APPLE__
  return &__stop_phpdu;
#elif defined(__GNUC__)
  return &__stop_phpdu[0];
#else
  return (UnitState*)&UnitStateSectionEnd;
#endif
}

VarRecord * getGVRSectionStart() {
#ifdef __APPLE__
  return &__start_phpdg;
#elif defined(__GNUC__)
  return &__start_phpdg[0];
#else
  void **cursor = &GVRSectionStart + 1;
  while (!*cursor && cursor < &GVRSectionEnd)
    ++cursor;
  return (VarRecord*)cursor;
#endif
}

VarRecord * getGVRSectionEnd() {
#ifdef __APPLE__
  return &__stop_phpdg;
#elif defined(__GNUC__)
  return &__stop_phpdg[0];
#else
  return (VarRecord*)(&GVRSectionEnd);
#endif
}

const ValueInfo *getCTConstantSectionStart() {
#ifdef __APPLE__
  return &__start_phpcc;
#elif defined(__GNUC__)
  return &__start_phpcc[0];
#else
  void * const *cursor = &CTConstantSectionStart + 1;
  while (!*cursor && cursor < &CTConstantSectionEnd)
    ++cursor;
  return (const ValueInfo*)cursor;
#endif
}

const ValueInfo *getCTConstantSectionEnd() {
#ifdef __APPLE__
  return &__stop_phpcc;
#elif defined(__GNUC__)
  return &__stop_phpcc[0];
#else
  return (const ValueInfo*)(&CTConstantSectionEnd);
#endif
}

ValueInfo *getRTConstantSectionStart() {
#ifdef __APPLE__
  return &__start_phpdc;
#elif defined(__GNUC__)
  return &__start_phpdc[0];
#else
  void **cursor = &RTConstantSectionStart + 1;
  while (!*cursor && cursor < &RTConstantSectionEnd)
    ++cursor;
  return (ValueInfo*)cursor;
#endif
}

ValueInfo *getRTConstantSectionEnd() {
#ifdef __APPLE__
  return &__stop_phpdc;
#elif defined(__GNUC__)
  return &__stop_phpdc[0];
#else
  return (ValueInfo*)(&RTConstantSectionEnd);
#endif
}

}
