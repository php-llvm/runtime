//===--- log.cpp -- PHP Runtime ---------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of logging facility.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include <sstream>
#include <iostream>
#include "log.h"
//------------------------------------------------------------------------------

using namespace phprt;

//------ Global variables ------------------------------------------------------
THREAD_LOCAL unsigned suppress_counter = 0;
THREAD_LOCAL Logger logger;
Reaction default_reaction(cerr);
Facility CoreFacility("core");
Facility TypeFacility("type");
Facility SAPIFacility("sapi");
Facility ArithmeticFacility("arithmetic");
Facility ConversionFacility("conversion");
Facility DivisionByZeroFacility("division-by-zero");
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// facility implementation
//------------------------------------------------------------------------------

Facility::Facility(const char *name, Reaction *reaction) : name(name) {
  std::fill(reactions, reactions + ErrorLevelCount, reaction);
}

Reaction *Facility::get_reaction(ErrorLevel lvl) const {
  return reactions[lvl];
}


void Facility::set_reaction(ErrorLevel lvl, Reaction *reaction) {
  reactions[lvl] = reaction;
}

//------------------------------------------------------------------------------
// report_error_flags implementation
//------------------------------------------------------------------------------

void Logger::report_error_flags::set_error_level(ErrorLevel lvl, 
                                                 bool have_user_handler) {
  do_log_fatal   = (lvl <= Fatal)   || have_user_handler;
  do_log_error   = (lvl <= Error)   || have_user_handler;
  do_log_warning = (lvl <= Warning) || have_user_handler;
  do_log_notice  = (lvl <= Notice)  || have_user_handler;
  do_log_debug   = (lvl <= Debug)   || have_user_handler;
}


void Logger::report_error_flags::mute() {
  do_log_fatal   = false;
  do_log_error   = false;
  do_log_warning = false;
  do_log_notice  = false;
  do_log_debug   = false;
}

//------------------------------------------------------------------------------
// logger implementation
//------------------------------------------------------------------------------

void Logger::init() {
  logger.current_error_level = Notice;
  logger.error_report_flags.set_error_level(logger.current_error_level, false);
}

void Logger::suppress_start() {
  ++suppress_counter;
  logger.error_report_flags.mute();
}


void Logger::suppress_stop() {
  assert(suppress_counter > 0);
  if (--suppress_counter == 0)
    logger.error_report_flags.set_error_level(logger.current_error_level, 
                                             !logger.user_handler.is_null());
}


bool Logger::suppress_active() {
  return suppress_counter > 0;
}


DiagnosticBuilder Logger::log(const Facility &facility,
                              ErrorLevel severity, const char *msg) {
  return DiagnosticBuilder(facility, severity, string_ref::make(msg));
}


void Logger::emit_message(const DiagnosticBuilder &builder) {
  //TODO: call user handler
  
  auto facility = builder.get_facility();
  auto *reaction = facility.get_reaction(builder.get_level());
  if (!reaction)
    reaction = &default_reaction;

  if (auto *emitter = reaction->get_emitter())
    if (emitter(builder))
      return;

  default_emitter(builder);
}

#ifdef WIN32

#include <Windows.h>
#include <DbgHelp.h>

#if defined(_M_X64)
#define MACHINE_TYPE IMAGE_FILE_MACHINE_AMD64
#else
#define MACHINE_TYPE IMAGE_FILE_MACHINE_I386
#endif

std::vector<Logger::frame> Logger::get_backtrace(unsigned skip_frames_num) {
  std::vector<Logger::frame> backtrace;
  
  HANDLE process = GetCurrentProcess();
  HANDLE thread = GetCurrentThread();

  STACKFRAME64 frame = {};
  CONTEXT context = {};
  RtlCaptureContext(&context);
#if defined(_M_X64)
  frame.AddrPC.Offset = context.Rip;
  frame.AddrStack.Offset = context.Rsp;
  frame.AddrFrame.Offset = context.Rbp;
#else
  frame.AddrPC.Offset = context.Eip;
  frame.AddrStack.Offset = context.Esp;
  frame.AddrFrame.Offset = context.Ebp;
#endif
  frame.AddrPC.Mode = AddrModeFlat;
  frame.AddrStack.Mode = AddrModeFlat;
  frame.AddrFrame.Mode = AddrModeFlat;

  while (true) {
    if (!StackWalk64(MACHINE_TYPE, process, thread, &frame, &context, nullptr,
      SymFunctionTableAccess64, SymGetModuleBase64, nullptr)) {
      break;
    }

    if (skip_frames_num) {
      --skip_frames_num;
      continue;
    }

    DWORD64 process_counter = frame.AddrPC.Offset;
    DWORD64 frame_addr = frame.AddrFrame.Offset;
    DWORD64 frame_offset = 0; // offset relative to frame address

    if (frame_addr == 0)
      break;

    std::string fname;
    // Verify the PC belongs to a module in this process.
    if (!SymGetModuleBase64(process, process_counter))
      fname = "<unknown module>";
    else {
      // fetch the symbol name.
      char buffer[512];
      IMAGEHLP_SYMBOL64 *symbol = reinterpret_cast<IMAGEHLP_SYMBOL64 *>(buffer);
      memset(symbol, 0, sizeof(IMAGEHLP_SYMBOL64));
      symbol->SizeOfStruct = sizeof(IMAGEHLP_SYMBOL64);
      symbol->MaxNameLength = 512 - sizeof(IMAGEHLP_SYMBOL64);

      if (!SymGetSymFromAddr64(process, process_counter, &frame_offset, symbol))
        fname = "<unknown>";
      else
        fname = symbol->Name;
    }

    // fetch the source file and line number information.
    IMAGEHLP_LINE64 line = {};
    DWORD dwLineDisp;
    line.SizeOfStruct = sizeof(line);
    std::string filename;
    uint64_t line_number = 0;
    if (SymGetLineFromAddr64(process, process_counter, &dwLineDisp, &line)) {
      filename = line.FileName;
      line_number = line.LineNumber;
    }

    backtrace.push_back({fname, frame_addr, filename, line_number});
  }
  return backtrace;
}

#elif defined(HAVE_BACKTRACE_H)

#include <backtrace.h>
#include <cxxabi.h>
#include "llvm/Support/FileSystem.h"

WEAK_SYMBOL struct backtrace_state *get_backtrace_state() { return nullptr; }

static int add_frame_callback(void *data, uintptr_t pc, const char *fname, int lineno, const char *func_name) {
  auto result = (std::vector<Logger::frame>*)data;
  Logger::frame f;
  int status;
  if (!func_name)
    f.name = "<unknown function>";
  else if (char *demangled_name = 
            abi::__cxa_demangle(func_name, nullptr, nullptr, &status)) {
    f.name = demangled_name;
    free(demangled_name);
  } else
    f.name = func_name;
  f.filename = fname ? fname : "";
  f.line = lineno;
  f.ptr = pc;
  result->push_back(f);
  return 0;
}

static void error_callback(void *, const char *, int) {}

std::vector<Logger::frame> Logger::get_backtrace(unsigned skip_frames_num) {
  std::vector<Logger::frame> result;

  auto state = get_backtrace_state();
  if (!state)
    return result;
  backtrace_full(state, skip_frames_num + 1, add_frame_callback, 
      error_callback, &result);

  return result;
}

#else

std::vector<Logger::frame> Logger::get_backtrace(unsigned skip_frames_num) {
  std::vector<Logger::frame> result;
  return result;
}

#endif


//------------------------------------------------------------------------------
// message emitters implementation
//------------------------------------------------------------------------------
static const char *get_level_name(ErrorLevel lvl) {
  switch (lvl) {
  case Fatal:   return "Fatal";
  case Error:   return "Error";
  case Warning: return "Warning";
  case Notice:  return "Notice";
  case Debug:   return "Debug";
  }
  assert(0 && "unknown level type");
  return "<unknown>";
}


bool cerr(const DiagnosticBuilder &builder) {
  std::string msg = builder.build_message();
  std::cerr
    << get_level_name(builder.get_level()) << ": " << msg << std::endl;

  auto bt = Logger::get_backtrace();
  for (unsigned i = 0; i < bt.size(); ++i) {
    std::cerr << i << ": ";
    if (!bt[i].filename.empty())
      std::cerr << bt[i].filename << " line: " << bt[i].line << " ";
    std::cerr << bt[i].name << " 0x" << bt[i].ptr << std::endl;
  }

  return true;
}


bool throw_exception(const DiagnosticBuilder &builder) {
  //TODO: throw exception
  return cerr(builder);
}


bool throw_error(const DiagnosticBuilder &builder) {
  //TODO: throw error exception
  return cerr(builder);
}


bool throw_type_error(const DiagnosticBuilder &builder) {
  //TODO: throw type error exception
  return cerr(builder);
}


bool throw_arithmetic_error(const DiagnosticBuilder &builder) {
  //TODO: throw arithmetic error exception
  return cerr(builder);
}


bool throw_division_by_zero_error(const DiagnosticBuilder &builder) {
  //TODO: throw division by zero error exception
  return cerr(builder);
}


bool default_emitter(const DiagnosticBuilder &builder) {
  //TODO: implement default emitter
  return cerr(builder);
}


//------------------------------------------------------------------------------
// diagnostic builder implementation
//------------------------------------------------------------------------------
DiagnosticBuilder::~DiagnosticBuilder() {
  if (format.empty())
    return;
  
  logger.emit_message(*this);
}


std::string DiagnosticBuilder::build_message() const {
  assert(!format.empty());
  
  std::stringstream ss;
  unsigned str_start = 0;
  for (unsigned i = 0; i < format.length() - 1; ) {
    if (format[i] == '%') {
      bool add_substitution = true;
      if (format[i + 1] == '%') {
        ++i;
        add_substitution = false;
      }
      ss << std::string(format.data + str_start, i - str_start);
      if (add_substitution) {
        assert(isdigit(format[i + 1]));
        unsigned id = format[i + 1] - '0';
        assert(id < substitutions_count);
        string_ref str = substitutions[id].get_string();
        ss << std::string(str.data, str.len);
        ++i;
      }
      str_start = ++i;
      continue;
    }
    ++i;
  }
  ss << std::string(format.data + str_start, format.len - str_start);
  return ss.str();
}


DiagnosticBuilder &DiagnosticBuilder::operator << (bool v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box(v).to_string());
  return *this;
}


DiagnosticBuilder &DiagnosticBuilder::operator << (int v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box(v).to_string());
  return *this;
}


DiagnosticBuilder &DiagnosticBuilder::operator << (unsigned v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box(v).to_string());
  return *this;
}


DiagnosticBuilder &DiagnosticBuilder::operator << (long v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box(v).to_string());
  return *this;
}


DiagnosticBuilder &DiagnosticBuilder::operator << (unsigned long v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box((intvalue_t)v).to_string());
  return *this;
}


DiagnosticBuilder &DiagnosticBuilder::operator << (unsigned long long v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box((intvalue_t)v).to_string());
  return *this;
}


DiagnosticBuilder &DiagnosticBuilder::operator << (char v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box(v).to_string());
  return *this;
}


DiagnosticBuilder &DiagnosticBuilder::operator << (double v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box(v).to_string());
  return *this;
}


DiagnosticBuilder &DiagnosticBuilder::operator << (string_ref v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box(v).to_string());
  return *this;
}


DiagnosticBuilder &DiagnosticBuilder::operator << (const char *v) {
  assert(substitutions_count < max_format_substitutions);
  substitutions[substitutions_count++].copy(Box(v).to_string());
  return *this;
}

