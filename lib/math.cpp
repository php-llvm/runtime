//===--- math.cpp ---------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Part of PHP runtime responsible for math functions.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "argpack.h"
#include "shared/constants.h"
#include "functions.h"
#include "shared/functions.h"
#include "log.h"
#include "array.h"
#include <stdio.h>
#include <string.h>
#include <cmath>
#include <limits>
//------------------------------------------------------------------------------

namespace phprt {


PHPRT_API NumberBox abs(const Box &val) {
  Box number = val.to_number();

  if (number.is_double())
    return std::abs(number.get_double());

  if (number.is_integer()) {
    if (std::numeric_limits<intvalue_t>::min() == number.get_integer())
      return -(double)std::numeric_limits<intvalue_t>::min();
    return intvalue_t(std::abs((double)number.get_integer()));
  }

  return false;
}


PHPRT_API double ceil(double val) {
  return ::ceil(val);
}


PHPRT_API double floor(double val) {
  return ::floor(val);
}


PHPRT_API double round(double val, 
                       intvalue_t precision, intvalue_t imode) {
  RoundingMode mode = (RoundingMode)imode;
  if (mode < RoundingMode::HalfUp || mode > RoundingMode::HalfOdd) {
    CoreWarning("invalid mode value of %0 in round function, "
                "using ROUND_HALF_UP instead");
    mode = RoundingMode::HalfUp;
  }
  return phpfe::round(val, precision, mode);
}


PHPRT_API double fmod(double x, double y) {
  return ::fmod(x, y);
}


PHPRT_API intvalue_t intdiv(intvalue_t x, intvalue_t y) {
  if (y == 0) {
    CoreError("Division dy zero in intdiv function");
    return 0;
  }
  if (y == -1 && x == std::numeric_limits<intvalue_t>::min()) {
    CoreError("Integer overflow dividing integer minimum value by -1 in intdiv function");
    return 0;
  }
  return x / y;
}


PHPRT_API Box max_array(IArray *a) {
  if (!a->get_size())
    return Box();

  Box result = a->begin();
  for (auto it = a->begin(); it->is_valid(); ++(*it)) {
    if (compare(result, it->value()) < 0)
      result.copy(it->value());
  }
  return result;
}

PHPRT_API Box max(const Box &val1, const Box &val2) {
  if (compare(val1, val2) < 0)
    return val2;
  return val1;
}

PHPRT_API Box max_argpack(const Box &val1, const Box &val2, ArgPack<Box> rest) {
  Box result = max(val1, val2);
  for (Box &v : rest) {
    if (compare(result, v) < 0)
      result.copy(v);
  }
  return result;
}

PHPRT_API Box min_array(IArray *a) {
  if (!a->get_size())
    return Box();

  Box result = a->begin();
  for (auto it = a->begin(); it->is_valid(); ++(*it)) {
    if (compare(result, it->value()) > 0)
      result.copy(it->value());
  }
  return result;
}

PHPRT_API Box min(const Box &val1, const Box &val2) {
  if (compare(val1, val2) <= 0)
    return val1;
  return val2;
}

PHPRT_API Box min_argpack(const Box &val1, const Box &val2, ArgPack<Box> rest) {
  Box result = min(val1, val2);
  for (Box &v : rest) {
    if (compare(result, v) > 0)
      result.copy(v);
  }
  return result;
}

} // namespace phprt
