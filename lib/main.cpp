//===--- main.cpp ---------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Entry point of PHP application.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "server.h"
#include "unit.h"
#include "log.h"
#include "platform.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Support/CommandLine.h"
#include <iostream>
#ifdef WIN32
#include <io.h>
#include <fcntl.h>
#endif
//------------------------------------------------------------------------------


//------ Command line options --------------------------------------------------
//

static llvm::cl::opt<std::string> sapi(
  "sapi",
  llvm::cl::desc("specifies used server side"),
  llvm::cl::init(""));

//------ Debug options --------------------------

static llvm::cl::opt<bool> verbose(
  "verbose",
  llvm::cl::desc("verbose mode"),
  llvm::cl::init(false));

static llvm::cl::opt<bool> dump_stat(
  "stat",
  llvm::cl::desc("print statistic counters"),
  llvm::cl::init(false));

static llvm::cl::opt<bool> dump_units(
  "dump-units",
  llvm::cl::desc("prints compiled units"),
  llvm::cl::init(false));

static llvm::cl::opt<bool> dump_unit_states(
  "dump-loaded",
  llvm::cl::desc("prints loaded unit states"),
  llvm::cl::init(false));

//------ Positional arguments -------------------

// The first argument that is not an option is considered as a script name.
//
static llvm::cl::opt<std::string> script(
  llvm::cl::Positional,
  llvm::cl::desc("<script name>"),
  llvm::cl::init(""));

// All remained positional arguments are considered as script arguments.
//
static llvm::cl::list<std::string> script_args(
  llvm::cl::ConsumeAfter,
  llvm::cl::desc("<script arguments>..."));

//
//------------------------------------------------------------------------------

//------ Global variables ------------------------------------------------------

// Server used in this application.
static phprt::Server *server;

// Stream used to report messages by the code outside a request.
static std::ostream *msg_stream = &std::cerr;

//------------------------------------------------------------------------------

// Forward declarations
static void process_command_line(int argc, char *argv[]);
static bool setup_server();


/// \brief Application entry point.
///
int main(int argc, char* argv[]) {
  initialize_platform(argc, argv);
  Logger::init();

  // Process command line arguments.
  process_command_line(argc, argv);

  // Determine server to be used.
  if (!setup_server())
    return 1;

  //--- Earlier initialization of the used Server

  int res = server->install(&msg_stream);
  assert(msg_stream);
  if (res < 0)
    // Earlier initialization failed. Error must be already reported.
    return 1;
  if (res > 0)
    // All job was made during earlier initialization.
    return 0;

  //--- Initialization of PHP runtime

  //--- Late initialization of the selected server

  res = server->initialize(script, script_args);
  if (res < 0)
    return -1;
  if (res > 0)
    return 0;

  //--- Work loop
  while (phprt::Request *request = server->create_request()) {
    res = server->process_request(request);
    if (res)
      break;
  }

  //--- Finalization of PHP runtime

#ifdef _DEBUG
  if (dump_unit_states)
    phprt::dump_unit_states(nullptr, verbose);
#endif

  deinitialize_platform();
  return 0;
}


/// \brief Processes command line.
///
static void process_command_line(int argc, char *argv[]) {
  llvm::cl::ParseCommandLineOptions(argc, argv);

#ifdef _DEBUG
  if (dump_stat)
    llvm::EnableStatistics();

  // Immediately process information printing.
  if (dump_units) {
    phprt::dump_compiled_units(nullptr, verbose);
    exit(0);
  }
#endif
}


bool setup_server() {
  using namespace phprt;

  if (sapi == "cli") {
    server = CLIServer::get();
  } else if (sapi == "cgi") {
    server = CGIServer::get();
  } else if (sapi.empty()) {
    CoreDebug("Server API is not specified, use CLI by default");
    server = CLIServer::get();
  } else {
    CoreError("unsupported server API: %0") << sapi.getValue().c_str();
    return false;
  }
  return true;
}

