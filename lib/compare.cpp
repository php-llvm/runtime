//===--- compare.cpp ------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implements comparison for universal type.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "array.h"
#include "box.h"
#include "functions.h"
#include <memory>
//------------------------------------------------------------------------------


namespace phprt {

enum {
  less    = -1,
  equal   = 0,
  greater = 1
};


template <typename T>
static int three_way_compare(T a, T b) {
  if (a < b)
    return less;
  if (a > b)
    return greater;
  return equal;
}

static int compare_array(IArray *a, IArray *b) {
  if (a->get_size() != b->get_size())
    return three_way_compare(a->get_size(), b->get_size());
  std::unique_ptr<IIterator> It(a->begin());
  while (It->is_valid()) {
    if (!b->has_key(It->key()))
      return greater;
    auto status = compare(It->value(), b->get(It->key()));
    if (status != equal)
      return status;
    It->next();
  }
  return equal;
}

static int compare_object(IObject *a, IObject *b) {
  assert(0 && "compare for objects is not implemented yet");
  return 0;
}

// compare two universal types;
// return -1 if a < b
//         0 if a == b
//         1 if a > b
PHPRT_API int compare(const Box &val1, const Box &val2) {
  auto a = val1.dereference();
  auto b = val2.dereference();

  if (a.is_bool())
    return three_way_compare(a.get_bool(), b.to_bool());

  if (b.is_bool())
    return three_way_compare(a.to_bool(), b.get_bool());

  if (a.is_array()) {
    if (b.is_array())
      return compare_array(a.get_array(), b.get_array());
    if (b.is_bool())
      return three_way_compare(a.to_bool(), b.get_bool());
    return 1;
  }
  if (b.is_array())
    return -compare((Box&)b, (Box&)a);

  if (a.is_string() && b.is_string())
    return a.get_string().compare(b.get_string());

  if (a.is_object() && b.is_object())
    return compare_object(a.get_object(), b.get_object());

  if (a.is_object())
    return compare(1, (Box&)b);
  if (b.is_object())
    return -compare((Box&)a, 1);

  if (a.is_integer()) {
    if (b.is_integer())
      return three_way_compare(a.get_integer(), b.get_integer());

    if (b.is_double())
      return three_way_compare(double(a.get_integer()), b.get_double());

    return three_way_compare(a.get_integer(), b.to_number().get_integer());
  }
  
  if (a.is_double()) {
    if (b.is_integer())
      return three_way_compare(a.get_double(), double(b.get_integer()));

    if (b.is_double())
      return three_way_compare(a.get_double(), b.get_double());

    return three_way_compare(a.get_double(), double(b.to_number().get_integer()));
  } 

  return three_way_compare(a.to_number().get_integer(), b.to_number().get_integer());
}


/// \brief Makes strict comparison (=== and !==).
///
PHPRT_API bool identical(const Box &val1, const Box &val2) {
  return val1.get_type() == val2.get_type() &&
         compare(val1, val2) == 0;
}

} // namespace phprt

