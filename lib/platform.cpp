//===--- platform.cpp -----------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Platform specific initialization/deinitialization.
//
//===----------------------------------------------------------------------===//

#ifdef WIN32
//------ Windows ---------------------------------------------------------------
//

//------ Dependencies ----------------------------------------------------------
#include <fcntl.h>
#include <io.h>
#include <Windows.h>
#include <DbgHelp.h>
//------------------------------------------------------------------------------

void initialize_platform(int, char*[]) {
  // set stdout to binary mode to prevent replacement of \n with \r\n on windows
  _setmode(1 /* stdout */, _O_BINARY);

  // initialize symbol table for stack backtrace
  HANDLE process = GetCurrentProcess();
  SymSetOptions(SYMOPT_DEFERRED_LOADS | SYMOPT_LOAD_LINES | SYMOPT_UNDNAME);
  SymInitialize(process, nullptr, true);
}


void deinitialize_platform() {
  HANDLE process = GetCurrentProcess();
  SymCleanup(process);
}


//------------------------------------------------------------------------------

#elif defined(__clang__)
//------ clang -----------------------------------------------------------------

void initialize_platform(int, char *argv[]) {
}


void deinitialize_platform() {
}


#elif defined(__linux__) || defined(__APPLE__)

#ifdef HAVE_BACKTRACE_H
#include <backtrace.h>
#include "llvm/Support/FileSystem.h"

static struct backtrace_state *bt_state;

struct backtrace_state *get_backtrace_state() { 
  return bt_state; 
}

static void error_callback(void *, const char *, int) {}
#endif

void initialize_platform(int, char *argv[]) {
#ifdef HAVE_BACKTRACE_H
  std::string exe_path = llvm::sys::fs::getMainExecutable(argv[0], nullptr);
  bt_state = backtrace_create_state(exe_path.c_str(), 
      /*multithreaded:*/1, error_callback, nullptr);
#endif
}


void deinitialize_platform() {
}

#else
//------ other -----------------------------------------------------------------
//

void initialize_platform(int, char *argv[]) {
}


void deinitialize_platform() {
}

//------------------------------------------------------------------------------

#endif
