//===--- globals.cpp ---------------------------------------------------------===//
//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implements classes and methods related to global variables implementation
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "globals.h"
#include "globals_array.h"
#include "php_variables.h"
#include "layout.h"
#include "object.h"
#include <limits.h>
#include <cmath>
//------------------------------------------------------------------------------


namespace phprt {

//$GLOBALS array does not need reference counting
unsigned GlobalsArray::remove_ref() {
  return UINT_MAX;
}


unsigned GlobalsArray::add_ref() {
  return UINT_MAX;
}


// TODO: currently size is computed by iterating over all elements 
// in global variables array every time it is needed.
// There is no other way to compute size, however size does not change
// as long as elements are added/removed. So if can detect this event
// (i.e. by setting special flag in unset function) we could avoid
// unnecessary iterations over array
unsigned GlobalsArray::get_size() const {
  unsigned size = 0;
  for (VarRecord *cursor = first; cursor < last; ++cursor) {
    intptr_t Base = (intptr_t)(base);
    Box *Var = (Box *)(Base - cursor->offset);
    if (!Var->is_undef())
      ++size;
  }
  return size;
}


value_kind GlobalsArray::get_kind(const Box &key) const {
  //TODO: use tree or hash access.
  for (VarRecord *cursor = first; cursor < last; ++cursor) {
    if (cursor->name.equals_lower(key.get_string())) {
      intptr_t Base = (intptr_t)(base);
      return ((Box *)(Base - cursor->offset))->get_kind();
    }
  }
  return vkUndef;
}


Box GlobalsArray::get(const Box &key) const {
  //TODO: use tree or hash access.
  for (VarRecord *cursor = first; cursor < last; ++cursor) {
    if (cursor->name.equals_lower(key.get_string())) {
      intptr_t Base = (intptr_t)(base);
      return *((Box *)(Base - cursor->offset));
    }
  }
  return Box();
}


bool GlobalsArray::set(const Box &key, const Box &value) {
  //TODO: use tree or hash access.
  for (VarRecord *cursor = first; cursor < last; ++cursor) {
    if (cursor->name.equals_lower(key.get_string())) {
      intptr_t Base = (intptr_t)(base);
      Box *Var = (Box *)(Base - cursor->offset);
      Var->copy(value);
      return true;
    }
  }
  return false;
}


Box GlobalsArray::remove(const Box &key) {
  //TODO: use tree or hash access.
  for (VarRecord *cursor = first; cursor < last; ++cursor) {
    if (cursor->name.equals_lower(key.get_string())) {
      intptr_t Base = (intptr_t)(base);
      Box *Var = (Box *)(Base - cursor->offset);
      Box Ret = Var;
      Var->set_undef();
      return Ret;
    }
  }
  return Box();
}


IIterator *GlobalsArray::begin() const {
  return const_cast<GlobalsArray*>(this)->begin_edit();
}


IIterator *GlobalsArray::end() const  {
  return const_cast<GlobalsArray*>(this)->end_edit();
}


IEditIterator *GlobalsArray::begin_edit() {
  return new GlobalsArrayIterator(this);
}


IEditIterator *GlobalsArray::end_edit() {
  return new GlobalsArrayIterator(this, last);
}


IEditIterator *GlobalsArray::edit(const Box &key) {
  //TODO:
  return nullptr;
}


GlobalsArrayIterator::GlobalsArrayIterator(GlobalsArray *b)
  :  base(b), cursor(b->first) {
  reset(true);
}


GlobalsArrayIterator::GlobalsArrayIterator(GlobalsArray *b, VarRecord *c)
  :  base(b), cursor(c) {
}


void GlobalsArrayIterator::move_forward() {
  if (is_valid())
    do {
      ++cursor;
    } while (is_valid() && value().is_undef());
}


void GlobalsArrayIterator::move_backward() {
  if (is_valid())
    do {
      --cursor;
    } while (is_valid() && value().is_undef());
}


bool GlobalsArrayIterator::is_set() const {
  return is_valid() && !value().is_undef();
}


void GlobalsArrayIterator::reset(bool at_start) {
  if (at_start) {
    cursor = base->first;
    if (value().is_undef())
      move_forward();
  } else {
    cursor = base->last;
    move_backward();
  }
}


bool GlobalsArrayIterator::equals(const IIterator &x) const {
  assert(is_of_same_class(this, &x));
  const GlobalsArrayIterator& OtherIterator = (const GlobalsArrayIterator&)(x);
  return cursor == OtherIterator.cursor;
}


bool GlobalsArrayIterator::is_valid() const {
  return (cursor >= base->first && cursor < base->last);
}


KeyBox GlobalsArrayIterator::key() const {
  return cursor->name;
}


Box GlobalsArrayIterator::value() const {
  intptr_t baseptr = (intptr_t)(base->base);
  Box *var = (Box *)(baseptr - cursor->offset);
  return *var;
}


bool GlobalsArrayIterator::next() {
  move_forward();
  return is_valid();
}


bool GlobalsArrayIterator::prev() {
  move_backward();
  return is_valid();
}


bool GlobalsArrayIterator::set_value(const Box &new_value) {
  if (base->is_fixed() && is_valid()) {
    intptr_t baseptr = (intptr_t)(base->base);
    Box *var = (Box *)(baseptr - cursor->offset);
    var->copy(new_value);
    return true;
  }
  return false;
}

}
