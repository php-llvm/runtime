//===--- operators.cpp ----------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of binary and unary operators.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "argpack.h"
#include "functions.h"
#include "array.h"
#include "log.h"
#include <stdio.h>
#include <string.h>
#include <cmath>
#include <limits>
#include <algorithm>
//------------------------------------------------------------------------------

namespace phprt {


/// \brief Makes 'array addition' according to PHP rules.
///
PHPRT_API ArrayBox add_arrays(const IArray *a, const IArray *b) {
  assert(0 && "add_arrays is not implemented yet");
  return Box();
}


/// \brief Makes generic addition.
///
PHPRT_API Box add_box(const Box& val1, const Box& val2) {
  if (val1.is_array() && val2.is_array())
    return add_arrays(val1.get_array(), val2.get_array());

  if ((val1.is_integer() || val1.is_bool()) &&
      (val2.is_integer() || val2.is_bool())) {
    double result = val1.to_double() + val2.to_double();
    if (result > std::numeric_limits<intvalue_t>::max() ||
        result < std::numeric_limits<intvalue_t>::min())
      return result;
    return (long)result;
  }

  if (val1.is_double() || val2.is_double())
    return val1.to_double() + val2.to_double();

  assert(0 && "Unimplemented");
  return Box();
}


/// \brief Makes addition using C arithmetic, - ignoring potential integer
/// overflow.
///
PHPRT_API Box add_box_integer(const Box &val1, const Box &val2) {
  if (val1.is_array() && val2.is_array())
    return add_arrays(val1.get_array(), val2.get_array());
  if ((val1.is_integer() || val2.is_bool()) &&
      (val2.is_integer() || val2.is_bool()))
    return val1.to_integer() + val2.to_integer();
  if (val1.is_double() || val2.is_double())
    return val1.to_double() + val2.to_double();

  assert(0 && "Unimplemented");
  return Box();
}

  
PHPRT_API ArrayBox add_assign_arrays(ArrayBox a, const IArray *b) {
  a.copy(add_arrays(a.get_array(), b));
  return a;
}

PHPRT_API Box add_assign_box(Box &val1, const Box &val2) {
  val1.copy(add_box(val1, val2));
  return val1;
}

PHPRT_API Box add_assign_box_integer(Box &val1, const Box &val2) {
  val1.copy(add_box_integer(val1, val2));
  return val1;
}
  
  
/// \brief Makes generic subtraction.
///
PHPRT_API Box sub_box(const Box& val1, const Box& val2) {
  if ((val1.is_integer() || val2.is_bool()) &&
      (val2.is_integer() || val2.is_bool())) {
    double result = val1.to_double() - val2.to_double();
    if (result > std::numeric_limits<intvalue_t>::max() ||
        result < std::numeric_limits<intvalue_t>::min())
      return result;
    return (long)result;
  }

  if (val1.is_double() || val2.is_double())
    return val1.to_double() - val2.to_double();

  assert(0 && "Unimplemented");
  return Box();
}


/// \brief Makes subtraction using C arithmetic, - ignoring potential integer
/// overflow.
///
PHPRT_API Box sub_box_integer(const Box& val1, const Box& val2) {
  if ((val1.is_integer() || val2.is_bool()) &&
      (val2.is_integer() || val2.is_bool()))
    return val1.to_integer() - val2.to_integer();
  if (val1.is_double() || val2.is_double())
    return val1.to_double() - val2.to_double();

  assert(0 && "Unimplemented");
  return Box();
}

  
PHPRT_API Box sub_assign_box(Box & val1, const Box & val2) {
  val1.copy(sub_box(val1, val2));
  return val1;
}


PHPRT_API Box sub_assign_box_integer(Box & val1, const Box & val2) {
  val1.copy(sub_box_integer(val1, val2));
  return val1;
}

PHPRT_API Box mul_box(const Box &val1, const Box &val2) {
  if ((val1.is_integer() || val1.is_bool() || val1.is_null()) &&
      (val2.is_integer() || val2.is_bool() || val2.is_null())) {
    double result = val1.to_double() * val2.to_double();
    if (result > std::numeric_limits<intvalue_t>::max() ||
        result < std::numeric_limits<intvalue_t>::min())
      return result;
    
    return (long)result;
  }

  if (val1.is_double() || val2.is_double())
    return val1.to_double() * val2.to_double();

  if (val1.is_object()) {
    assert(0 && "multiplication for objects is not implemente yet");
  } 
  if (val2.is_object()) {
    assert(0 && "multiplication for objects is not implemente yet");
  }
  if (val1.is_string())
    return mul_box(val1.to_number(), val2);
  if (val2.is_string())
    return mul_box(val1, val2.to_number());
  
  CoreError("Unsupported operand types in multiplication: %0, %1") <<
    val1.get_type_name() << val2.get_type_name();
  return Box();
}


PHPRT_API Box mul_box_integer(const Box &val1, const Box &val2) {
  if ((val1.is_integer() || val1.is_bool() || val1.is_null()) &&
      (val2.is_integer() || val2.is_bool() || val2.is_null()))
    return val1.to_integer() * val2.to_integer();

  if (val1.is_double() || val2.is_double())
    return val1.to_double() * val2.to_double();

  if (val1.is_object()) {
    assert(0 && "multiplication for objects is not implemente yet");
  }
  if (val2.is_object()) {
    assert(0 && "multiplication for objects is not implemente yet");
  }
  if (val1.is_string())
    return mul_box(val1.to_number(), val2);
  if (val2.is_string())
    return mul_box(val1, val2.to_number());
  
  CoreError("Unsupported operand types in multiplication: %0, %1") <<
    val1.get_type_name() << val2.get_type_name();
  return Box();
}
  
  
PHPRT_API Box mul_assign_box(Box &val1, const Box & val2) {
  val1.copy(mul_box(val1, val2));
  return val1;
}


PHPRT_API Box mul_assign_box_integer(Box &val1, const Box & val2) {
  val1.copy(mul_box_integer(val1, val2));
  return val1;
}


PHPRT_API Box div_box(const Box &val1, const Box &val2) {
  if ((val1.is_integer() || val1.is_bool() || val1.is_null()) &&
      (val2.is_integer() || val2.is_bool() || val2.is_null())) {
    
    double val = val2.to_double();
    if (val == 0.0)
      DivByZeroError("Division by zero");
    
    double num = val1.to_double();
    double result = num / val;
    if (result > std::numeric_limits<intvalue_t>::max())
      return result;
    
    if (intvalue_t(num) % intvalue_t(val) == 0)
      return (long)result;
    return result;
  }

  if (val1.is_double() || val2.is_double()) {
    double val = val2.to_double();
    if (val == 0.0)
      DivByZeroError("Division by zero");

    return val1.to_double() / val2.to_double();
  }

  if (val1.is_object()) {
    assert(0 && "division for objects is not implemente yet");
  }
  if (val2.is_object()) {
    assert(0 && "division for objects is not implemente yet");
  }
  if (val1.is_string())
    return div_box(val1.to_number(), val2);
  if (val2.is_string())
    return div_box(val1, val2.to_number());
  
  CoreError("Unsupported operand types in division: %0, %1") <<
    val1.get_type_name() << val2.get_type_name();
  return Box();
}


PHPRT_API Box div_box_integer(const Box &val1, const Box &val2) {
  if ((val1.is_integer() || val1.is_bool() || val1.is_null()) &&
    (val2.is_integer() || val2.is_bool() || val2.is_null())) {

    auto val = val2.to_integer();
    if (val == 0)
      DivByZeroError("Division by zero");

    return val1.to_integer() / val;
  }

  if (val1.is_double() || val2.is_double()) {
    auto val = val2.to_double();
    if (val == 0.0)
      DivByZeroError("Division by zero");

    return val1.to_double() / val;
  }

  if (val1.is_object()) {
    assert(0 && "division for objects is not implemente yet");
  }
  if (val2.is_object()) {
    assert(0 && "division for objects is not implemente yet");
  }
  if (val1.is_string())
    return div_box(val1.to_number(), val2);
  if (val2.is_string())
    return div_box(val1, val2.to_number());
  
  CoreError("Unsupported operand types in division: %0, %1") <<
    val1.get_type_name() << val2.get_type_name();
  return Box();
}

  
PHPRT_API Box div_assign_box(Box &val1, const Box &val2) {
  val1.copy(div_box(val1, val2));
  return val1;
}


PHPRT_API Box div_assign_box_integer(Box &val1, const Box &val2) {
  val1.copy(div_box_integer(val1, val2));
  return val1;
}
  

PHPRT_API Box rem_box(const Box &val1, const Box &val2) {
  if (val1.is_object()) {
    assert(0 && "division for objects is not implemente yet");
  } else if (val2.is_object()) {
    assert(0 && "division for objects is not implemente yet");
  } 
  auto val = val2.to_integer();
  if (val == 0)
    DivByZeroError("Modulo by zero");
  if (val == -1)
    return 0;
  return val1.to_integer() % val;
}

  
PHPRT_API Box rem_assign_box(Box &val1, const Box &val2) {
  val1.copy(rem_box(val1, val2));
  return val1;
}
  

PHPRT_API StringBox bitwise_and_string(string_ref s1, string_ref s2) {
  StringBox result;
  char immediatebuf[MaxImmediateSize];
  size_t size = std::min(s1.len, s2.len);
  char *buf;
  bool use_immediate = false;
  if (size > MaxImmediateSize) {
    String *s = String::create(size);
    result.set_str(s);
    buf = s->get_content();
  } else {
    buf = immediatebuf;
    use_immediate = true;
  }
  for (size_t i = 0; i < size; ++i)
    buf[i] = s1[i] & s2[i];
  if (use_immediate)
    result.set_copy(buf, size);
  return result;
}


PHPRT_API Box bitwise_and_box(const Box &val1, const Box &val2) {
  if (val1.is_integer() && val2.is_integer())
    return val1.get_integer() & val2.get_integer();

  if (val1.is_string() && val2.is_string())
    return bitwise_and_string(val1.get_string(), val2.get_string());

  if (val1.is_object()) {
    assert(0 && "bitwise and for objects is not implemente yet");
  }
  if (val2.is_object()) {
    assert(0 && "bitwise and for objects is not implemente yet");
  }

  return val1.to_integer() & val2.to_integer();
}
  
  
PHPRT_API Box bitwise_and_assign_box(Box &val1, const Box &val2) {
  val1.copy(bitwise_and_box(val1, val2));
  return val1;
}


PHPRT_API StringBox bitwise_and_assign_string(StringBox val1, string_ref val2) {
  val1.copy(bitwise_and_string(val1.get_string(), val2));
  return val1;
}
  


PHPRT_API StringBox bitwise_or_string(string_ref s1, string_ref s2) {
  StringBox result;
  char immediatebuf[MaxImmediateSize];

  if (s1.len < s2.len)
    std::swap(s1, s2);

  size_t size = s1.len;
  char *buf;
  bool use_immediate = false;
  if (size > MaxImmediateSize) {
    String *s = String::create(size);
    result.set_str(s);
    buf = s->get_content();
  } else {
    buf = immediatebuf;
    use_immediate = true;
  }
  for (size_t i = 0; i < s2.len; ++i)
    buf[i] = s1[i] | s2[i];
  if (s1.len != s2.len)
    memcpy(&buf[s2.len], &s1.data[s2.len], s1.len - s2.len);
  if (use_immediate)
    result.set_copy(buf, size);
  return result;
}


PHPRT_API Box bitwise_or_box(const Box &val1, const Box &val2) {
  if (val1.is_integer() && val2.is_integer())
    return val1.get_integer() | val2.get_integer();

  if (val1.is_string() && val2.is_string())
    return bitwise_or_string(val1.get_string(), val2.get_string());

  if (val1.is_object()) {
    assert(0 && "bitwise or for objects is not implemente yet");
  }
  if (val2.is_object()) {
    assert(0 && "bitwise or for objects is not implemente yet");
  }

  return val1.to_integer() | val2.to_integer();
}
  
  
PHPRT_API Box bitwise_or_assign_box(Box &val1, const Box &val2) {
  val1.copy(bitwise_or_box(val1, val2));
  return val1;
}


PHPRT_API StringBox bitwise_or_assign_string(StringBox val1, string_ref val2) {
  val1.copy(bitwise_or_string(val1.get_string(), val2));
  return val1;
}


PHPRT_API StringBox bitwise_xor_string(string_ref s1, string_ref s2) {
  StringBox result;
  char immediatebuf[MaxImmediateSize];
  size_t size = std::min(s1.len, s2.len);
  char *buf;
  bool use_immediate = false;
  if (size > MaxImmediateSize) {
    String *s = String::create(size);
    result.set_str(s);
    buf = s->get_content();
  } else {
    buf = immediatebuf;
    use_immediate = true;
  }
  for (size_t i = 0; i < size; ++i)
    buf[i] = s1[i] ^ s2[i];
  if (use_immediate)
    result.set_copy(buf, size);
  return result;
}


PHPRT_API Box bitwise_xor_box(const Box &val1, const Box &val2) {
  if (val1.is_integer() && val2.is_integer())
    return val1.get_integer() ^ val2.get_integer();

  if (val1.is_string() && val2.is_string())
    return bitwise_xor_string(val1.get_string(), val2.get_string());

  if (val1.is_object()) {
    assert(0 && "bitwise xor for objects is not implemente yet");
  }
  if (val2.is_object()) {
    assert(0 && "bitwise xor for objects is not implemente yet");
  }

  return val1.to_integer() ^ val2.to_integer();
}

  
PHPRT_API Box bitwise_xor_assign_box(Box &val1, const Box &val2) {
  val1.copy(bitwise_xor_box(val1, val2));
  return val1;
}


PHPRT_API StringBox bitwise_xor_assign_string(StringBox val1, string_ref val2) {
  val1.copy(bitwise_xor_string(val1.get_string(), val2));
  return val1;
}
  

PHPRT_API intvalue_t shl_integer(intvalue_t val1, intvalue_t val2) {
  if ((size_t)val2 >= sizeof(intvalue_t) * 8) {
    if (val2 > 0)
      return 0;
    CoreError("Bit shift by negative number");
    return 0;
  }
  return val1 << val2;
}


PHPRT_API Box shl_box(const Box &val1, const Box &val2) {
  if (val1.is_object()) {
    assert(0 && "shift left is not implemented for objects");
  }
  if (val2.is_object()) {
    assert(0 && "shift left is not implemented for objects");
  }
  return shl_integer(val1.to_integer(), val2.to_integer());
}

  
PHPRT_API Box shl_assign_box(Box &val1, const Box &val2) {
  val1.copy(shl_box(val1, val2));
  return val1;
}
  
  
PHPRT_API intvalue_t shl_assign_integer(intvalue_t &val1, intvalue_t val2) {
  return val1 = shl_integer(val1, val2);
}
  

PHPRT_API Box shr_box(const Box &val1, const Box &val2) {
  if (val1.is_object()) {
    assert(0 && "shift right is not implemented for objects");
  }
  if (val2.is_object()) {
    assert(0 && "shift right is not implemented for objects");
  }
  return val1.to_integer() >> val2.to_integer();
}

  
PHPRT_API Box shr_assign_box(Box &val1, const Box &val2) {
  val1.copy(shr_box(val1, val2));
  return val1;
}
  
}
