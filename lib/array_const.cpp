//===--- array_const.cpp --------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implements class ConstArray.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "array.h"
#include "layout.h"
#include "object.h"
#include <algorithm>
#include <string>
#include <iostream>
#include <limits.h>
//------------------------------------------------------------------------------

namespace phprt {

//------ Global variables ------------------------------------------------------
const ConstArray empty_array;
//------------------------------------------------------------------------------


class ConstArrayIterator : public IEditIterator {
private:
  long position;
  const ConstArray *base;

public:
  
  ConstArrayIterator(const ConstArray* base, long position = 0) 
    : position(position), base(base) {
    assert(base && "expected valid array");
  }

  virtual ~ConstArrayIterator() override {};

  void reset(bool at_start = true) {
    if (at_start)
      position = 0;
    else {
      position = base->get_size();
      if (!position)
        --position;
    }
  }

  virtual bool equals(const IIterator &x) const override {
    assert(is_of_same_class(this, &x));
    return position == static_cast<const ConstArrayIterator&>(x).position;
  }

  virtual bool is_valid() const override {
    return 0 <= position && position < long(base->size());
  }


  virtual KeyBox key() const override {
    if (!is_valid())
      return false;
    return base->get_key(position);
  }

  virtual Box value() const override {
    if (!is_valid())
      return false;
    return base->get_value(position);
  }

  virtual bool next() override {
    if (!is_valid())
      return false;
    ++position;
    return is_valid();
  }

  virtual bool prev() override {
    if (!is_valid())
      return false;
    --position;
    return is_valid();
  }

  bool set_value(const Box &new_value) override {
    assert(is_valid());
    return base->set_value(position, new_value);
  }
};


//------------------------------------------------------------------------------
// Implementation of ConstArray
//------------------------------------------------------------------------------

const unsigned InvalidSlot = ~0U;


unsigned ConstArray::remove_ref() {
  return UINT_MAX;
}


unsigned ConstArray::add_ref() {
  return UINT_MAX;
}


unsigned ConstArray::get_size() const {
  return items;
}


value_kind ConstArray::get_kind(const Box &key) const {
  unsigned ndx = find(key);
  if (ndx < items) {
    if (is_vector())
      return content.get_vector(ndx).get_kind();
    return content.get_map(ndx).value.get_kind();
  }
  return vkUndef;
}


Box ConstArray::get(const Box &key) const {
  unsigned ndx = find(key);
  if (ndx < items) {
    if (is_vector())
      return Box(content.get_vector(ndx));
    return Box(content.get_map(ndx).value);
  }
  return Box(undefined());
}


bool ConstArray::set(const Box &key, const Box &value) {
  if (is_fixed()) {
    unsigned ndx = find(key);
    if (ndx < items) {
      const box *item_ptr;
      if (is_vector())
        item_ptr = &content.get_vector(ndx);
      else
        item_ptr = &content.get_map(ndx).value;
      const_cast<box *>(item_ptr)->copy(value);
      return true;
    }
  }
  // TODO: standard handling - exception?
  assert(0);
  return false;
}


Box ConstArray::remove(const Box &key) {
  // TODO: standard handling - exception?
  assert(0);
  return Box();
}


ILinearAccessor *ConstArray::linear() {
  return static_cast<ILinearAccessor*>(this);
}


IIterator *ConstArray::begin() const {
  return new ConstArrayIterator(this);
}


IIterator *ConstArray::end() const {
  return new ConstArrayIterator(this, get_size());
}


IEditIterator *ConstArray::begin_edit() {
  return new ConstArrayIterator(this);
}


IEditIterator *ConstArray::end_edit() {
  return new ConstArrayIterator(this, get_size());
}


IEditIterator *ConstArray::edit(const Box &key) {
  if (is_fixed()) {
    assert(0);//TODO:
  }
  return nullptr;
}


KeyBox ConstArray::get_key(slot_t s) const {
  if (!is_valid_slot(s)) {
    // TODO: standard handling - exception?
    return Box(undefined());
  }
  if (is_vector())
    return KeyBox(intvalue_t(first_index + s));
  return Box(content.get_map(s).key);
}


Box ConstArray::get_value(slot_t s) const {
  if (!is_valid_slot(s)) {
    // TODO: standard handling - exception?
    return Box(undefined());
  }
  if (is_vector())
    return Box(content.get_vector(s));
  return Box(content.get_map(s).value);
}


bool ConstArray::set_value(slot_t s, const Box &v) const {
  if (!is_valid_slot(s) || !is_fixed()) {
    // TODO: standard handling - exception?
    return false;
  }
  if (is_vector()) {
    content.access_vector(s).copy(v);
  } else {
    content.access_map(s).copy(v);
  }
  return true;
}


bool ConstArray::is_valid(slot_t s) const {
  return is_valid_slot(s);
}


bool ConstArray::begin(slot_t &s) const {
  if (items == 0)
    return false;
  s = 0;
  return true;
}


bool ConstArray::end(slot_t &s) const {
  if (items == 0)
    return false;
  s = items;
  return true;
}


bool ConstArray::next(slot_t &s) const {
  return ++s < items;
}


bool ConstArray::prev(slot_t &s) const {
  if (s == 0)
    return false;
  --s;
  return true;
}


bool ConstArray::shift(slot_t &s, int delta) const {
  if (delta < 0) {
    if (slot_t(-delta) > s) {
      s = 0;
      return false;
    }
    s -= slot_t(-delta);
    return true;
  }
  s += delta;
  if (s >= items) {
    s = items;
    return false;
  }
  return true;
}


unsigned ConstArray::find(const KeyBox &key) const {
  if (is_vector()) {
    if (!key.is_integer())
      return InvalidSlot;
    intvalue_t ndx = key.get_integer();
    if (ndx < first_index)
      return InvalidSlot;
    unsigned offset = ndx - first_index;
    if (offset < items)
      return offset;
    return InvalidSlot;
  }

  const association *assoc_ptr = content.as_map;
  auto lb = std::lower_bound(locators, locators + items, key,
    [assoc_ptr](unsigned ndx, const KeyBox &key) -> bool {
      return static_cast<KeyBox>(assoc_ptr[ndx].value).less(key);
    });
  if (static_cast<KeyBox>(assoc_ptr[*lb].value).equal(key))
    return *lb;
  return InvalidSlot;
}


#ifdef _DEBUG

void ConstArray::dump(unsigned indent) const {
  std::cerr << "ConstArray #" << get_size();
  std::cerr << " (" << (is_vector() ? "vector" : "map") << ") {" << std::endl;

  indent += 2;
  for (unsigned i = 0; i < get_size(); ++i) {
    std::cerr << std::string(indent, ' ');
    get_key(i).dump(indent);
    std::cerr << " => ";
    get_value(i).dump(indent);
    std::cerr << std::endl;
  }
  indent -= 2;

  std::cerr << std::string(indent, ' ') << "}" << std::endl;
}
#endif

}
