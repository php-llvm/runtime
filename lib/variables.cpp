//===--- variables.cpp ------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Variable support functions
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "box.h"
#include "functions.h"
#include "array.h"
#include "conversion/number_recognizer.h"
//------------------------------------------------------------------------------

namespace phprt {

PHPRT_API string_ref gettype(const Box& var) {
  switch (var.get_type()) {
  case tyInteger:
    return string_ref::make("integer");
  case tyDouble:
    return string_ref::make("double");
  case tyBool:
    return string_ref::make("boolean");
  case tyString:
    return string_ref::make("string");
  case tyArray:
    return string_ref::make("array");
  case tyObject:
    return string_ref::make("object");
  case tyNull:
    return string_ref::make("NULL");
  default:
    return string_ref::make("unknown type");
  }
}


// This function is used when we can easily obtain
// lvalue reference of pointer to variable.
// Variable value is changed directly
PHPRT_API bool settype(box& var, string_ref type) {

  if (type.equals_lower("boolean") || 
      type.equals_lower("bool")) {
    return convert_to_bool(var);
  }

  if (type.equals_lower("integer") ||
      type.equals_lower("int")) {
    return convert_to_integer(var);
  }

  if (type.equals_lower("float") ||
      type.equals_lower("double")) {
    return convert_to_double(var);
  }

  if (type.equals_lower("string")) {
    return convert_to_string(var);
  }

  if (type.equals_lower("array")) {
    return convert_to_array(var);
  }

  if (type.equals_lower("object")) {
    return convert_to_object(var);
  }

  if (type.equals_lower("null")) {
    var.set_null();
    return true;
  }

  // TOOD: display warning
  return false;
}


// This function is used when obtaining reference to value is
// difficult (i.e. access to arrays or objects). 
// In this case we divide set types into two operations:
// 1) obtainign new value of correct type
// 2) overwriting ols value. 
PHPRT_API Box get_settype_val(const Box & var, string_ref type) {
  if (type.equals_lower("boolean") ||
      type.equals_lower("bool")) {
    return var.to_bool();
  }

  if (type.equals_lower("integer") ||
      type.equals_lower("int")) {
    return var.to_integer();
  }
  if (type.equals_lower("float") ||
      type.equals_lower("double")) {
    return var.to_double();
  }

  if (type.equals_lower("string")) {
    return var.to_string();
  }

  if (type.equals_lower("array")) {
    assert(0 && "unimplemented");
    return Box();
  }

  if (type.equals_lower("object")) {
    assert(0 && "unimplemented");
    return Box();
  }

  if (type.equals_lower("null")) {
    return Box();
  }

  // TOOD: display warning
  return Box();
}


PHPRT_API intvalue_t intval(const Box& var, intvalue_t base) {
  // Floats, integers and booleans are directly convertible to integer.
  if (var.is_number() || var.is_bool())
    return var.to_integer();

  // String are converted taking the base into account.
  if (var.is_string()) {
    string_ref str = var.get_string();
    conversion::NumberRecognizer<> Recog(0U);
    Recog.recognize(str.data, str.len);
    if (Recog.failure()) {
      //TODO: issue message
      return 0;
    }
    if (Recog.partial()) {
      // TODO: issue message
    }

    intvalue_t result;
    auto st = conversion::convert_int(result, Recog.get_int(), base);
    if (st == conversion::NegativeLimit && Recog.is_negative())
      return std::numeric_limits<intvalue_t>::min();
    if (Recog.get_status() != conversion::OK) {
      //TODO: process
    }
    if (Recog.is_negative())
      result = -result;
    return result;
  }

  // Empty arrays are converted to 0, other arrays are converted to 1.
  if (var.is_array())
    return var.get_array()->get_size() != 0;

  // Objects convert using handlers.
  if (var.is_object()) {
    // TODO: implement
  }

  assert(0 && "unimlemented");
  return 0;
}

PHPRT_API double floatval(const Box& var) {
  return var.to_double();
}

PHPRT_API bool empty(const Box& var) {
  switch (var.get_type()) {
  case tyBool:
    return !var.get_bool(); // false
  case tyDouble:
    return !var.get_double(); // 0.0
  case tyInteger:
    return !var.get_integer(); // 0
  case tyString:
    return empty_str(var.get_string()); // "" , "0"
  case tyArray:
    return empty_arr(var.get_array()); // array()
  case tyNull:
    return true;  // null, undef
  case tyObject:
    return empty_obj(var.get_object());
  default:
    return false; // objects,resources, etc...
  }
}

PHPRT_API bool empty_arr(const IArray* Arr) {
  return Arr->get_size() == 0;
}

PHPRT_API bool empty_str(string_ref var) {
  return var.equals("") || var.equals("0");
}

PHPRT_API bool empty_obj(const IObject *obj) {
  return false; // TODO: call handler?
}

}