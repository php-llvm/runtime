//===--- constants.cpp ----------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2016, Serge Pavlov.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Support of PHP constants in runtime.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "box.h"
#include "functions.h"
#include "log.h"
#include "value.h"
#include "layout.h"
#include <utility>
#include <assert.h>
//------------------------------------------------------------------------------

namespace phprt {

PHPRT_API bool is_constant_defined(string_ref name) {
  assert(0);
  return false;
}


PHPRT_API Box get_constant_value(string_ref name) {
  assert(0);
  return Box();
}


static const ValueInfo *search_constant(string_ref name, string_ref nsname,
                                 const ValueInfo *start, const ValueInfo *end) {
  for (const ValueInfo *cursor = start; cursor < end; ++cursor) {
    string_ref current_name = cursor->name;
    if (!nsname.empty()) {
      if (cursor->is_case_insensitive()) {
        if (!current_name.starts_with_lower(nsname))
          continue;
      } else {
        if (!current_name.starts_with(nsname))
          continue;
      }
      current_name.skip(nsname.len);
    }

    if (cursor->is_case_insensitive()) {
      if (!current_name.equals_lower(name))
        continue;
    } else {
      if (current_name.equals(name))
        return cursor;
    }
  }
  return nullptr;
}


// TODO: make search faster, now it is linear.
PHPRT_API Box find_constant(string_ref name, string_ref nsname) {
  // First try dynamic constants, those created by 'define'.
  //TODO:

  // Next try runtime constants, defined in loaded modules.
  // TODO:

  // Try runtime constants defined in the application.
  if (const ValueInfo *result = search_constant(name, nsname,
                                                getRTConstantSectionStart(),
                                                getRTConstantSectionEnd())) {
    return Box(static_cast<const Box&>(result->value));
  }

  // Finally try compile time constants defined in the application.
  if (const ValueInfo *result = search_constant(name, nsname,
                                                getCTConstantSectionStart(),
                                                getCTConstantSectionEnd())) {
    return Box(static_cast<const Box&>(result->value));
  }

  return Box();
}


PHPRT_API void init_constant(ValueInfo &variable, Box value) {
  if (variable.is_initialized()) {
    CoreError("Reinitialization of constant %0") << variable.name;
    return;
  }
  variable.value.init_move(value);
  //TODO: add to map of active constants?
  variable.set_initialized();
}

}
