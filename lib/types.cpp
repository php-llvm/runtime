//===--- types.cpp --------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of runtime type support.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "argpack.h"
#include "functions.h"
#include "array.h"
#include <stdio.h>
#include <string.h>
#include <cmath>
#include <limits>
//------------------------------------------------------------------------------

namespace phprt {

//------ Explicit template instantiations --------------------------------------

template struct ArgPack<bool>;
template struct ArgPack<intvalue_t>;
template struct ArgPack<double>;
template struct ArgPack<bool*>;
template struct ArgPack<intvalue_t*>;
template struct ArgPack<double*>;
template struct ArgPack<Box>;
template struct ArgPack<StringBox>;
template struct ArgPack<Box*>;
template struct ArgPack<StringBox*>;

template class Boxed<bool>;
template class Boxed<intvalue_t>;
template class Boxed<double>;
template class Boxed<StringBox>;
template class Boxed<ArrayBox>;

template class RefBox<bool>;
template class RefBox<intvalue_t>;
template class RefBox<double>;
template class RefBox<StringBox>;
template class RefBox<ArrayBox>;

//------------------------------------------------------------------------------

}
