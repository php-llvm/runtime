//===--- string.cpp ---------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of string classes.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "string_data.h"
#include <atomic>
#include <iostream>
//------------------------------------------------------------------------------

//------ Global variables ----------------------------------------------------
// Keep statistics for debug builds.
#ifdef _DEBUG
static std::atomic<unsigned> total_number_of_strings(0);
static std::atomic<unsigned> total_size_of_strings(0);
#endif
//----------------------------------------------------------------------------

namespace phprt {

void *String::operator new(size_t sz) {
#ifdef _DEBUG
  ++total_number_of_strings;
#endif
  return allocate(sz);
}


void *String::operator new(size_t sz, size_t extra) {
#ifdef _DEBUG
  ++total_number_of_strings;
  total_size_of_strings += extra;
#endif
  return allocate(sz + extra);
}


void String::operator delete(void *p) {
  deallocate(p);
}


void *String::allocate(unsigned sz) {
  return ::operator new(sz);
}


void String::deallocate(void *ptr) {
  ::operator delete(ptr);
}


/// \brief Creates a string that has the given length.
///
/// \param sz - specifies maximal number of symbols the string may have.
///
/// \returns New string that has space for exactly \c sz symbols.
///
/// The function reserves one byte more for trailing null character. It makes
/// operation c_str faster.
///
String *String::create(unsigned sz) {
  // We must allocate additional memory after the object for string data. We
  // need one byte of extra space and some characters can be placed into the
  // header.
  unsigned extra_space = 0;
  if (sz + 1 > sizeof(char*))
    extra_space = sz + 1 - sizeof(char*);
  String *result = new (extra_space) String(sz);
  return result;
}


unsigned String::remove_ref() {
  if (counter == 0 || --counter == 0) {
    delete this;
    return 0;
  }
  return counter;
}


#ifdef _DEBUG
unsigned String::get_total() {
  return total_number_of_strings;
}

unsigned String::get_total_memory() {
  return total_size_of_strings;
}

void String::dump() const {
  std::cout << "[" << size() << "] \"";
  //TODO: use method that converts unprintable chars to esc codes.
  std::cout.write(get_content(), length);
  std::cout << "\"";
}
#endif

}
