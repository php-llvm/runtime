//===--- convert.cpp ------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Conversions between runtime types.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "box.h"
#include "log.h"
#include "conversion/number_recognizer.h"
#include "llvm/Support/ErrorHandling.h"
//------------------------------------------------------------------------------

namespace phprt {

using namespace conversion;

PHPRT_API string_ref convert_bool_to_string(bool x) {
  if (x)
    return string_ref{ "1", 1 };
  return string_ref{ "", 0 };
}


PHPRT_API bool convert_cstring_to_bool(const char* str) {
  return !(str[0] == 0 || (str[0] == '0' && str[1] == 0));
}


static void report_conversion_error(Status status) {
  switch (status) {
  case OK:
  case DoubleToInt:
  case IntToDouble:
  case DoubleUnderflow:
    break;
  case NegativeLimit:
  case IntOverflow:
  case IntOverflowNegative:
    LogWarning(ConversionFacility, "Too large integer");
    break;
  case DoubleOverflow:
    LogWarning(ConversionFacility, "Too large floating number");
    break;
  case NaN:
  default:
    llvm_unreachable("unexpected conversion error");
  }
}


/// \brief Converts the specified string to integer representation.
///
intvalue_t convert_string_to_integer(string_ref str) {
  NumberRecognizer<char> Recog;
  Recog.recognize(str.data, str.len);
  if (Recog.failure()) {
    // The string does not contain a number.
    LogWarning(ConversionFacility, "String does not contain a number");
    return 0;
  }
  if (Recog.is_float()) {
    // Number is in float format, it will be read as integer.
    LogWarning(ConversionFacility,
               "String contains a floating number but is read as integer");
  }
  if (Recog.partial()) {
    // String contains non-numerical suffix.
    LogNotice(ConversionFacility, "String contains non-numeric part");
  }

  intvalue_t result = Recog.to<intvalue_t>();
  report_conversion_error(Recog.get_status());

  return result;
}


intvalue_t convert_cstring_to_integer(const char *str) {
  return convert_string_to_integer(string_ref::make(str));
}


double convert_string_to_double(string_ref str) {
  NumberRecognizer<char> Recog;
  Recog.recognize(str.data, str.len);
  if (Recog.failure()) {
    // The string does not contain a number.
    LogWarning(ConversionFacility, "String does not contain a number");
    return 0;
  }
  if (Recog.partial()) {
    // String contains non-numerical suffix.
    LogNotice(ConversionFacility, "String contains non-numeric part");
  }

  double result = Recog.to<double>();
  report_conversion_error(Recog.get_status());
  return result;
}


double convert_cstring_to_double(const char *str) {
  return convert_string_to_double(string_ref::make(str));
}


bool starts_with_number(string_ref str) {
  NumberRecognizer<char> Recog;
  Recog.recognize(str.data, str.len);
  return !Recog.failure();
}

}
