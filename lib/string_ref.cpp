//===--- string_ref.cpp -- PHP Runtime --------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of class 'string_ref'.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "shared/string_ref.h"
#include <algorithm>
#include <locale>
#include <iostream>
#include <assert.h>
#include <string.h>
//------------------------------------------------------------------------------


void string_ref::init(const char *s) {
  data = s;
  len = s ? strlen(s) : 0;
}

bool string_ref::equals(const char *x) const{
  assert(x);
  unsigned slen = strlen(x);
  return equals(string_ref::make(x, slen));
}

bool string_ref::equals(const string_ref& s) const {
  if (len != s.len)
    return false;
  return std::equal(data, data + len, s.data);
}

bool string_ref::equals_lower(const char *x) const {
  assert(x);
  unsigned slen = strlen(x);
  return equals_lower(string_ref::make(x, slen));
}

bool string_ref::equals_lower(const string_ref& s) const {
  if (len != s.len)
    return false;
  std::locale loc;
  return std::equal(data, data + len, s.data,
                    [&loc](char x1, char x2) -> bool {
    return std::tolower(x1, loc) == std::tolower(x2, loc);
  });
}


int string_ref::compare(const string_ref & s) const {
  if (len > s.len)
    return +1;
  if (len < s.len)
    return -1;
  return memcmp(data, s.data, len);
}


string_ref string_ref::trim_front() const {
  unsigned leading_ws = 0;
  for (; leading_ws < len; ++leading_ws) {
    char ch = data[leading_ws];
    if (strchr(" \t\n\r\f\v", ch) == nullptr)
      break;
  }
  return skip(leading_ws);
}


string_ref string_ref::trim_back() const {
  unsigned new_len = len;
  for (; new_len; --new_len) {
    char ch = data[new_len - 1];
    if (strchr(" \t\n\r\f\v", ch) == nullptr)
      break;
  }
  return trunc(new_len);
}


string_ref string_ref::trim() const {
  return trim_back().trim_front();
}
