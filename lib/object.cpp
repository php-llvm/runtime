//===--- object.cpp -------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implements basic object functions.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "object.h"
#include "log.h"
#include "llvm/Support/ErrorHandling.h"
#include <iostream>
//------------------------------------------------------------------------------


static void unsupported_operation(const char *op_name) {
  //TODO: log error, mask, exception...
  CoreFatal("operation %0 is not supported") << op_name;
}


namespace phprt {

IObject::~IObject() {
}


Box IObject::get_value() {
  unsupported_operation("get_value");
  return Box();
}


void IObject::set_value(const Box &v) {
  unsupported_operation("set_value");
}


Box IObject::cast(value_type) {
  unsupported_operation("cast");
  return Box();
}

intvalue_t IObject::as_int() {
  unsupported_operation("as_int");
  return 0;
}


double IObject::as_float() {
  unsupported_operation("as_int");
  return 0;
}


IObject::cmp_res IObject::compare(const Box &x, bool rhs) {
  if (x.is_object()) {
    IObject *other = x.get_object();
    return (this == other) ? Equals : Unordered;
  }
  return Unordered;
}


bool IObject::strict_equals(const Box &x, bool rhs) {
  return compare(x, rhs) == Equals;
}


bool IObject::as_bool() {
  unsupported_operation("as_bool");
  return false;
}


Box IObject::log_not() {
  unsupported_operation("log_not");
  return Box();
}


Box IObject::xor(const Box &x, bool rhs) {
  unsupported_operation("xor");
  return Box();
}


Box IObject::add(const Box &x, bool rhs) {
  unsupported_operation("add");
  return Box();
}


Box IObject::sub(const Box &x, bool rhs) {
  unsupported_operation("sub");
  return Box();
}


Box IObject::mul(const Box &x, bool rhs) {
  unsupported_operation("mul");
  return Box();
}


Box IObject::div(const Box &x, bool rhs) {
  unsupported_operation("div");
  return Box();
}


Box IObject::mod(const Box &x, bool rhs) {
  unsupported_operation("mod");
  return Box();
}


Box IObject::pow(const Box &x, bool rhs) {
  unsupported_operation("pow");
  return Box();
}


Box IObject::plus() {
  unsupported_operation("plus");
  return Box();
}


Box IObject::minus() {
  unsupported_operation("minus");
  return Box();
}


IObject *IObject::add_assign(const Box &x) {
  unsupported_operation("add_assign");
  return this;
}


IObject *IObject::sub_assign(const Box &x) {
  unsupported_operation("sub_assign");
  return this;
}


IObject *IObject::mul_assign(const Box &x) {
  unsupported_operation("mul_assign");
  return this;
}


IObject *IObject::div_assign(const Box &x) {
  unsupported_operation("div_assign");
  return this;
}


IObject *IObject::mod_assign(const Box &x) {
  unsupported_operation("mod_assign");
  return this;
}


IObject *IObject::pow_assign(const Box &x) {
  unsupported_operation("pow_assign");
  return this;
}


Box IObject::concat(const Box &x, bool rhs) {
  unsupported_operation("concat");
  return Box();
}


IObject *IObject::concat_assign(const Box &x) {
  unsupported_operation("concat_assign");
  return this;
}


Box IObject::bin_and(const Box &x, bool rhs) {
  unsupported_operation("bin_and");
  return Box();
}


Box IObject::bin_or(const Box &x, bool rhs) {
  unsupported_operation("bin_or");
  return Box();
}


Box IObject::bin_xor(const Box &x, bool rhs) {
  unsupported_operation("bin_xor");
  return Box();
}


Box IObject::bin_shl(const Box &x, bool rhs) {
  unsupported_operation("bin_shl");
  return Box();
}


Box IObject::bin_shr(const Box &x, bool rhs) {
  unsupported_operation("bin_shr");
  return Box();
}


Box IObject::bin_not() {
  unsupported_operation("bin_not");
  return Box();
}


IObject *IObject::bin_and_assign(const Box &x) {
  unsupported_operation("bin_and_assign");
  return this;
}


IObject *IObject::bin_or_assign(const Box &x) {
  unsupported_operation("bin_or_assign");
  return this;
}


IObject *IObject::bin_xor_assign(const Box &x) {
  unsupported_operation("bin_xor_assign");
  return this;
}


IObject *IObject::bin_shl_assign(const Box &x) {
  unsupported_operation("bin_shl_assign");
  return this;
}


IObject *IObject::bin_shr_assign(const Box &x) {
  unsupported_operation("bin_shr_assign");
  return this;
}


//------ Implementation of Class -----------------------------------------------

IObject *Class::create(ArgPack<Box*>) {
  //TODO:
  llvm_unreachable("not implemented");
  return nullptr;
}


//------ Service functions -----------------------------------------------------

// This function ensures that two object belong to the same class
// by comparing their pointers to virtual table (vptr).
// It is assumed that vptr is stored at the start of the object.
// This function is intented to be used to check object type
// before performing base-to-derived cast.
bool is_of_same_class(const void *objectA, const void *objectB) {
  assert(objectA);
  assert(objectB);
  const void *VptrA = (const void *)(*(const uintptr_t *)(objectA));
  const void *VptrB = (const void *)(*(const uintptr_t *)(objectB));
  return VptrA == VptrB;
}

}
