//===--- var_dump.cpp -------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Definitions of classes and methods needed to implement var_dump() function
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "functions.h"
#include "array.h"
#include "request.h"
#include "llvm/Support/ErrorHandling.h"
#include <memory>
#include <stdarg.h>
#include <iomanip>
#include <set>
#include <string>
//------------------------------------------------------------------------------


namespace phprt {

class Dumper {
  // This set contains pointer to entities which have been already visited.
  // It is used to prevent infinite recursion when dumping arrays, references
  // and objects.
  //
  // For example:
  // $a = array(1,2,&$a);
  //
  // The method which dumps entities is responsible for adding and removing it
  // from this set.
  std::set<void *> visited;

  // Current level of var_dump call. Used to compute correct offset when
  // dumping arrays and objects.
  unsigned level;

  // Number of spaces used to indent one level of var_dump
  const unsigned IndentSize;

public:

  Dumper(unsigned ISize) : level(0), IndentSize(ISize) {
  }

  void indent() {
    out() << std::setw(level * IndentSize) << " ";
  }

  void new_line() {
    out() << std::endl;
    indent();
  }

  void dump(const Box &val) {
    switch (val.get_type()) {
    case tyNull:
      phprt::out() << "NULL";
      break;
    case tyBool:
      if (val.is_true())
        out() << "bool(true)";
      else
        out() << "bool(false)";
      break;
    case tyInteger:
      out() << "int(" << val.get_integer() << ")";
      break;
    case tyDouble:
      out() << std::setprecision(get_precision())
            << "float(" << val.get_double() << ")";
      break;
    case tyString: {
        string_ref str = val.get_string();
        out() << "string(" << str.len << ") \"";
        out().write(str.data, str.len);
        out() << "\"";
      }
      break;
    case tyArray:
      dump_array(val.get_array());
      break;
    default:
      llvm_unreachable("unexpected type");
    }
  }

  void dump_array(IArray *Arr) {
    if (visited.find(Arr) != visited.end()) {
      out() << "*RECURSION*";
      return;
    }
    visited.insert(Arr);
    out() << "array(" << Arr->get_size() << ") {";
    ++level;
    std::unique_ptr<IIterator> cursor(Arr->begin());
    while (cursor->is_valid()) {
      new_line();
      KeyBox key = cursor->key();
      if (key.is_string()) {
        string_ref str = key.get_string();
        out() << "[\"";
        out().write(str.data, str.len);
        out() << "\"]=>";
      } else {
        out() << "[" << key.get_integer() << "]=>";
      }
      new_line();
      dump(cursor->value());
      cursor->next();
    }
    --level;
    new_line();
    out() << "}";
  }
};


PHPRT_API void var_dump(const Box &var) {
  const unsigned IndentSize = 2;
  Dumper(IndentSize).dump(var);
  out() << "\n";
}

}