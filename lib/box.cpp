//===--- box.cpp ----------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implements universal type.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "box.h"
#include "array.h"
#include "functions.h"
#include "reference.h"
#include "request.h"
#include "string_data.h"
#include "conversion/number_recognizer.h"
#include "llvm/Support/ErrorHandling.h"
#include <utility>
#include <cstring>
#include <cstdlib>
#include <cerrno>
#include <limits.h>
#include <limits>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
//------------------------------------------------------------------------------


using namespace phprt;
using namespace conversion;

namespace phprt {

value_type get_type(value_kind kind) {
  if (kind == vkUndef)
    return tyNull;
  if (is_string(kind))
    return tyString;
  if (is_array(kind))
    return tyArray;
  if (is_object(kind))
    return tyObject;
  if (is_value(kind))
    return static_cast<value_type>(kind);
  return tyInternal;
}


PHPRT_API const char *get_type_name(value_type type) {
  switch (type) {
  case tyNull:    return "NULL";
  case tyBool:    return "bool";
  case tyInteger: return "integer";
  case tyDouble:  return "double";
  case tyString:  return "string";
  case tyArray:   return "array";
  case tyObject:  return "object";
  case tyInternal: return "INTERNAL";
  default:
    llvm_unreachable("invalid box type");
  }
}

}

//TODO: void init_array(unsigned sz = 0);

void box::init(IArray *a) {
  assert(a);
  a->add_ref();
  e.t.type = a->is_constant() ? vkConstArray : vkDynamicArray;
  v.v_array = a;
}

void box::init(const ConstArray *a) {
  assert(a);
  e.t.type = vkConstArray;
  v.v_array = const_cast<IArray*>(static_cast<const IArray*>(a));
}

//TODO: void init(IObject *obj);

void box::init_reference() {
  v.v_reference = Reference::create();
  e.t.type = vkReference;
}


void box::init_reference(Reference *r) {
  assert(r);
  e.t.type = vkReference;
  v.v_reference = r;
  r->add_ref();
}


void box::init_reference(box &val) {
  if (this == &val)
    make_reference();
  else if (val.is_reference()) {
    // Share the reference object.
    init_reference(val.get_reference());
  } else {
    // Original box must be converted to reference.
    Reference *ref = Reference::create();
    ref->get_value().move(std::move(val));
    val.init_reference(ref);
    init_reference(ref);
  }
}


/// \brief Convert value to reference.
///
void box::make_reference() {
  // If we already have reference do nothing.
  box &value = remove_indirection();
  if (value.is_reference())
    return;

  Reference *r = Reference::create();
  r->get_value().move(std::move(value));
  value.set_reference(r);
}


void box::init_block(const void *p, unsigned sz) {
  e.t.type = vkBlock;
  e.t.size = sz;
  if (p && sz) {
    v.v_pointer = new char[sz];
    memcpy(v.v_pointer, p, sz);
  } else {
    v.v_pointer = const_cast<void*>(p);
  }
}


void box::init_copy(const char *s, unsigned len) {
  if (len == 0) {
    init_str();
    return;
  }
  if (len <= MaxImmediateSize) {
    memcpy(v.v_imm_str, s, len);
    e.t.type = vkClassImmediate | (len - 1);
    return;
  }
  String *str = String::create(len);
  memcpy(str->get_content(), s, len);
  init_str(str);
}


void box::init_copy(const char *s) {
  init_copy(s, strlen(s));
}


void box::init_str(const char *s, unsigned len) {
  assert(s || len == 0); //string of zero length may be nullptr
  if (len > MaxConstLen) {
    String *str = String::create(len);
    auto tail = std::copy(s, s + len, str->begin());
    *tail = 0;
    init_str(s);
    return;
  }
  v.v_cstring = s;
  e.t.size = len;
  e.t.type = vkConstString;
}


void box::init_str(const char *s) {
  assert(s);
  init_str(s, strlen(s));
}


void box::init_str(String *s) {
  assert(s);
  s->add_ref();
  v.v_string = s;
  e.t.type = vkDynamicString;
}

//TODO: void init_move(char *s, unsigned len);

void box::init_str(const StringBox &s) {
  v.slot1 = s.v.slot1;
  e.slot2 = s.e.slot2;
  if (phprt::is_string_dynamic(s.get_kind()))
    v.v_string->add_ref();
}



void box::init_copy(const box &x) {
  const box& source = x.dereference();
  if (!is_value(source.get_kind()))
    switch (source.get_kind()) {
    case vkReference:
      init_reference(source.get_reference());
      return;
    case vkDynamicString:
      init_str(source.get_var_string());
      return;
    case vkDynamicArray:
      init(source.v.v_array);
      return;
    case vkObject:
      assert(0 && "Not implemented");
      break;
    default:
      break;
    }

  // We hit this code if the value may be copied by simple bit copy.
  v = x.v;
  e = x.e;
}


void box::init_move(box &x) {
  v.slot1 = x.v.slot1;
  e.slot2 = x.e.slot2;
  x.e.t.type = vkUndef;
}


void box::clear() {
  switch (get_kind()) {
    case vkReference:
      //TODO: replace delete with garbage collector call
      if(!v.v_reference->remove_ref())
        v.v_reference = nullptr; 
      break;
    default:
     // Types without reference counting do not need any special clean up.
     break;
  }
  e.t.type = vkUndef;
}


void box::copy(const box& s) {
  box& destination = remove_indirection();
  const box& source = s.dereference();

  // No need to perform copying if both boxes point to the same value
  if (&destination == &source)
    return;

  // Create temporary value to hold previous value until assignment is complete.
  Box Tmp(static_cast<Box&&>(destination));

  init_copy(s);
}


void box::move(box &&s) {
  v = s.v;
  e = s.e;
  s.e.t.type = vkUndef;
}


void box::swap(box &x) {
  std::swap(v.slot1, x.v.slot1);
  std::swap(e.slot2, x.e.slot2);
}


bool box::get_bool() const {
  assert(is_bool());
  const box &target = dereference();
  return target.v.v_bool;
}


intvalue_t box::get_integer() const {
  const box &target = dereference();
  switch (target.get_type()) {
  case tyBool:
    return target.v.v_bool;
  case tyInteger:
    return target.v.v_integer;
  default:
    break;
  }
  assert(0 && "invalid type");
  return 0;
}


double box::get_double() const {
  assert(is_double());
  const box &target = dereference();
  return target.v.v_double;
}


string_ref box::get_string() const {
  assert(is_string());
  const box &target = dereference();
  string_ref result;

  if (is_string_dynamic()) {
    assert(target.v.v_string);
    result.data = target.v.v_string->get_content();
    result.len = target.v.v_string->get_length();
  } else if (is_string_constant()) {
    result.data = target.v.v_cstring;
    result.len = target.e.t.size;
  } else {
    assert(is_string_immediate());
    result.data = target.v.v_imm_str;
    result.len = get_immediate_len(static_cast<value_kind>(target.e.t.type));
  }
  return result;
}


unsigned box::get_string_len() const {
  assert(is_string());
  const box &target = dereference();

  if (is_string_dynamic()) {
    assert(target.v.v_string);
    return target.v.v_string->get_length();
  }
  
  if (is_string_constant())
    return target.e.t.size;
  
  assert(is_string_immediate());
  return get_immediate_len(static_cast<value_kind>(target.e.t.type));
}


char *box::get_string_buffer(unsigned extra) {
  assert(is_string());
  box &target = dereference();

  if (is_string_dynamic()) {
    assert(target.v.v_string);
    if (target.v.v_string->get_counter() == 1 && extra == 0)
      return target.v.v_string->get_content();
  }
  else if (is_string_immediate()) {
    if (extra + get_string_len() <= MaxImmediateSize)
      return target.v.v_imm_str;
  } else {
    assert(is_string_constant());
  }

  string_ref str = target.get_string();
  String *s = String::create(str.len + extra);
  memcpy(s->get_content(), str.data, str.len);
  set_str(s);
  return s->get_content();
}


IArray *box::get_array() const {
  assert(is_array());
  const box &target = dereference();
  assert(target.v.v_array);
  return const_cast<IArray*>(target.v.v_array);
}


IObject *box::get_object() const {
  assert(is_object());
  const box &target = dereference();
  assert(target.v.v_object);
  return const_cast<IObject*>(target.v.v_object);
}


void *box::get_pointer() const {
  assert(is_pointer());
  const box &target = dereference();
  return target.v.v_pointer;
}


void *box::get_block() const {
  assert(is_block());
  const box &target = dereference();
  assert(target.v.v_pointer);
  return target.v.v_pointer;
}


unsigned box::get_block_size() const {
  assert(is_block());
  const box &target = dereference();
  assert(target.v.v_pointer);
  return target.e.t.size;
}


bool &box::access_bool() {
  assert(is_bool());
  box &target = dereference();
  return target.v.v_bool;
}


intvalue_t &box::access_integer() {
  assert(is_integer());
  box &target = dereference();
  return target.v.v_integer;
}


double &box::access_double() {
  assert(is_double());
  box &target = dereference();
  return target.v.v_double;
}


bool box::to_bool()const {
  const box &val = dereference();

  switch (val.get_type()) {
  case tyNull:
    return false;

  case tyBool:
    return val.get_bool();

  case tyInteger:
    return val.get_integer() != 0;

  case tyDouble:
    return val.get_double() != 0;

  case tyArray:
    return val.get_array()->get_size() != 0;

  case tyString: {
    string_ref str = val.get_string();
    return !str.empty() && !str.equals("0");
  }

  case tyObject:
    // Must call conversion method.
    assert(0 && "unimplemented");
    return true;

  default:
    //TODO:
    assert(0 && "unimplemented");
    return false;
  }
}


StringBox box::to_string() const {
  const box &val = dereference();

  switch (val.get_type()) {
  case tyNull:
    return "";

  case tyBool:
    return val.get_bool() ? "1" : "";

  case tyInteger: {
    std::stringstream s;
    s << get_integer();
    auto str = s.str();
    StringBox result;
    result.set_copy(str.data(), str.size());
    return result;
  }

  case tyDouble: {
    std::stringstream s;
    s << std::setprecision(get_precision()) << get_double();
    auto str = s.str();
    StringBox result;
    result.set_copy(str.data(), str.size());
    return result;
  }

  case tyArray:
    //TODO: warning?
    return "Array";

  case tyString:
    return StringBox(*static_cast<const Box *>(this));

  case tyObject:
    // Must call magic method.
    assert(0 && "unimplemented");
    return "";

  default:
    //TODO:
    assert(0 && "unimplemented");
    return "";
  }
}


//TODO: IArray *to_array() const {}


intvalue_t box::to_integer() const {
  const box &val = dereference();

  // Other conversions.
  switch (val.get_type()) {
  case tyNull:
    return 0;

  case tyBool:
    return val.get_bool();

  case tyInteger:
    return val.v.v_integer;

  case tyDouble:
    if (val.v.v_double > std::numeric_limits<intvalue_t>::max()) {
      //TODO: process overflow
      return std::numeric_limits<intvalue_t>::max();
    }
    else if (val.v.v_double < std::numeric_limits<intvalue_t>::min()) {
      //TODO: process overflow
      return std::numeric_limits<intvalue_t>::min();
    }
    return static_cast<intvalue_t>(val.v.v_double);

  case tyArray:
    //TODO: warning?
    return val.get_array()->get_size() != 0;

  case tyString: {
    string_ref str = val.get_string();
    NumberRecognizer<char> Recog(str.begin(), str.length(), 0);
    intvalue_t result = Recog.to<intvalue_t>();
    // TODO: warning on invalid strings?
    return result;
  }
  case tyObject:
    // Must call conversion method.
    assert(0 && "unimplemented");
    return 0;

  default:
    //TODO:
    assert(0 && "unimplemented");
    return false;
  }
}


double box::to_double() const {
  const box &val = dereference();

  if (val.is_double())
    return val.v.v_double;

  if (val.is_string()) {
    string_ref str = val.get_string();
    NumberRecognizer<char> Recog(str.begin(), str.length());
    double result;
    Recog.read(result);
    // TODO: warning on invalid strings?
    return result;
  }

  if (val.is_object()) {
    // Must call conversion method.
    assert(0 && "unimplemented");
    return 0;
  }

  // TODO: catch loss of precision?
  return to_integer();
}


NumberBox box::to_number() const {
  const box &val = dereference();

  // Other conversions.
  switch(val.get_type()) {
  case tyNull:
    return 0;

  case tyBool:
    return val.get_bool();

  case tyInteger:
    return val.get_integer();

  case tyDouble:
    return val.get_double();

  case tyArray:
    return val.to_integer();

  case tyString: {
    string_ref str = val.get_string();
    NumberRecognizer<char> Recog(str.begin(), str.length());
    if (Recog.recognize().is_float())
      return NumberBox(Recog.to<double>());
    intvalue_t ival;
    switch (Recog.read(ival)) {
    case OK:
      return NumberBox(ival);
    case NaN:
      return NumberBox(0);
    case NegativeLimit:
    case IntOverflow:
    case IntOverflowNegative:
      return NumberBox(Recog.to<double>());
    default:
      assert(0);
    }
  }

  case tyObject:
    // Must call conversion method.
    assert(0 && "unimplemented");
    return 0;

  default:
    //TODO:
    assert(0 && "unimplemented");
    return false;
  }
}


template<>
void box::attach(IArray *x) {
  assert(x);
  box &target = dereference();
  Box old_value(std::move(target));
  target.v.v_array = x;
  target.e.t.type = x->is_constant() ? vkConstArray : vkDynamicArray;
}


template<>
void box::attach(IObject *x) {
  assert(x);
  box &target = dereference();
  Box old_value(std::move(target));
  target.v.v_object = x;
  target.e.t.type = vkObject;
}


template<>
IArray *box::detach<IArray>() {
  assert(is_array());
  box &target = dereference();
  IArray *res = target.v.v_array;
  target.e.t.type = vkUndef;
  return res;
}


template<>
IObject *box::detach<IObject>() {
  assert(is_object());
  box &target = dereference();
  IObject *res = target.v.v_object;
  target.e.t.type = vkUndef;
  return res;
}


box &box::dereference() {
 if(is_reference())
   return v.v_reference->get_value();
 return *this;
}

box &box::remove_indirection() {
  if (is_indirect())
    return *v.v_indirect;
  return *this;
}

#ifdef _DEBUG
void box::dump(unsigned indent) const {
  switch (get_kind()) {
  case vkUndef:
    std::cerr << "<undef>";
    break;

  case vkNull:
    std::cerr << "null";
    break;

  case vkBool:
    std::cerr << (get_bool() ? "true" : "false");
    break;

  case vkInteger:
    std::cerr << get_integer();
    break;

  case vkDouble:
    std::cerr << get_double();
    break;

  case vkConstString: {
    string_ref str = get_string();
    std::cerr << "[CS]'" << std::string(str.data, str.len) << "'";
    break;
  }

  case vkDynamicString: {
    string_ref str = get_string();
    std::cerr << "[DS]'" << std::string(str.data, str.len) << "'";
    break;
  }

  case vkConstArray:
    static_cast<ConstArray*>(get_array())->dump(indent);
    break;

  case vkDynamicArray:
    assert(0 && "dynamic array dump is not implemented yet");
    break;

  case vkPointer:
    std::cerr << get_pointer();
    break;

  case vkIndirect:
    std::cerr << "[Ind]";
    get_indirect()->dump();
    break;

  case vkReference:
    std::cerr << "&";
    get_reference()->get_value().dump();
    break;

  case vkObject:
    assert(0 && "object dump is not implemented yet");
    break;

  case vkBlock:
    std::cerr << "block[" << get_block_size() << "] " << get_block();
    break;

  default:
    assert(0 && "unknown box type");
    return;
  }
}
#endif


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

bool KeyBox::equal(const KeyBox &key) {
  if (is_integer()) {
    if (key.is_integer())
      return get_integer() == key.get_integer();
    //TODO: compare integer with numeric string
    return false;
  }
  assert(is_string());
  if (key.is_integer()) {
    //TODO: compare integer with numeric string
    return false;
  }
  assert(key.is_string());
  return get_string().equals(key.get_string());
}


bool KeyBox::less(const KeyBox &key) {
  if (is_integer()) {
    if (key.is_integer())
      return get_integer() < key.get_integer();
    //TODO: compare integer with numeric string
    // int < key
    return true;
  }
  assert(is_string());
  if (key.is_integer())
    //TODO: compare integer with numeric string
    // string > int
    return false;
  assert(key.is_string());
  return get_string().less(key.get_string());
}


#ifdef _DEBUG
void KeyBox::dump(unsigned indent) const {
  std::cerr << "[KeyBox]";
  box::dump(indent);
}
#endif


StringBox::StringBox(const box &s) : Box(undefined()) {
  if (s.is_string())
    copy(s);
  else
    to_string().swap(*this);
}


StringBox::StringBox(box &&s) : Box(undefined()) {
  if (s.is_string())
    move(std::move(s));
  else
    move(s.to_string());
}


StringBox::StringBox(const Box &s) : Box(undefined()) {
  if (s.is_string())
    copy(s);
  else
    to_string().swap(*this);
}


StringBox::StringBox(Box &&s) : Box(undefined()) {
  if (s.is_string())
    move(std::move(s));
  else
    move(s.to_string());
}
