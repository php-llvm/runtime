//===--- output.cpp -------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Part of PHP runtime responsible for output operations.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "functions.h"
#include "request.h"
#include "llvm/Support/ErrorHandling.h"
#include <stdio.h>
#include <string.h>
#include <iomanip>
#include <string>
//------------------------------------------------------------------------------


// TODO: take from request
namespace phprt {


PHPRT_API void echo_txt(const char *str, unsigned len) {
  out().write(str, len);
}


PHPRT_API void echo_string(string_ref str) {
  out().write(str.data, str.len);
}


PHPRT_API void echo_double(double d) {
  out() << std::setprecision(get_precision()) << d;
}


PHPRT_API void echo_bool(bool b) {
  if (b)
    out().write("1", 1);
}


PHPRT_API void echo_integer(intvalue_t l) {
  out() << l;
}


PHPRT_API void echo_box(const box& b) {
  switch (b.get_type()) {
  case tyNull:
    // These types do not echo anything.
    return;
  case tyBool:
    echo_bool(b.get_bool());
    break;
  case tyInteger:
    echo_integer(b.get_integer());
    break;
  case tyDouble:
    echo_double(b.get_double());
    break;
  case tyString:
    echo_string(b.get_string());
    break;
  default:
    llvm_unreachable("unsupported type");
  }
}

}
