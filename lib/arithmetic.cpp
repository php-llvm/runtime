//===--- arithmetic.cpp ---------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  PHP runtime, implementation of binary and unary operations.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "functions.h"
#include "string_data.h"
#include "log.h"
#include <algorithm>
#include <cmath>
#include <limits>
#include <stdio.h>
#include <string.h>
//------------------------------------------------------------------------------

namespace phprt {


PHPRT_API intvalue_t pow_int(intvalue_t x, intvalue_t y) {
  auto res = std::pow(x, y);
  if (!std::isfinite(res)) {
    // TODO: handle error
    return std::numeric_limits<intvalue_t>::max();
  }
  if (res > std::numeric_limits<intvalue_t>::max()) {
    // TODO: handle overflow
    return std::numeric_limits<intvalue_t>::max();
  }
  if (res < std::numeric_limits<intvalue_t>::min()) {
    // TODO: handle overflow
    return std::numeric_limits<intvalue_t>::min();
  }
  return static_cast<intvalue_t>(res);
}


PHPRT_API double pow_float(double x, double y) {
  return std::pow(x, y);
}


PHPRT_API Box pow_generic(const Box &x, const Box &y) {
  if ((x.is_integer() || x.is_bool() || x.is_null()) &&
      (y.is_integer() || y.is_bool() || y.is_null())) {
    double result = std::pow(x.to_integer(), y.to_integer());
    if (!std::isfinite(result) ||
        result > std::numeric_limits<intvalue_t>::max() ||
        result < std::numeric_limits<intvalue_t>::min())
      return result;
    return intvalue_t(result);
  }
  
  if (x.is_number() && y.is_number())
    return std::pow(x.to_double(), y.to_double());
  
  if (x.is_object()) {
    assert(0 && "power with objects is not implemente yet");
  }
  
  if (y.is_object()) {
    assert(0 && "power with objects is not implemente yet");
  }
  
  if (x.is_string())
    return pow_generic(x.to_number(), y);
  if (y.is_string())
    return pow_generic(x, y.to_number());
  
  CoreError("Unsupported operand types in multiplication: %0, %1") <<
    x.get_type_name() << y.get_type_name();
  return Box();
}

  
PHPRT_API intvalue_t pow_assign_int(intvalue_t &x, intvalue_t y) {
  return x = pow_int(x, y);
}

  
PHPRT_API intvalue_t pow_assign_int_box(intvalue_t &x, const Box &y) {
  return x = intvalue_t(pow_float(x, y.to_double()));
}


PHPRT_API double pow_assign_float(double &x, double y) {
  return x = pow_float(x, y);
}
 
  
PHPRT_API double pow_assign_float_box(double &x, const Box &y) {
  return x = pow_float(x, y.to_double());
}


PHPRT_API Box pow_assign_generic(Box &x, const Box &y) {
  x.copy(pow_generic(x, y));
  return x;
}
  
  

PHPRT_API StringBox concat_const(string_ref s1, string_ref s2) {
  unsigned res_len = s1.len + s2.len;
  if (res_len <= MaxImmediateSize) {
    StringBox result;
    if (res_len) {
      result.e.t.type = get_immediate_kind(res_len);
      auto tail = std::copy(s1.begin(), s1.end(), (char *)result.v.v_imm_str);
      std::copy(s2.begin(), s2.end(), tail);
      return result;
    }
  }
  String *result = String::create(res_len);
  auto tail = std::copy(s1.begin(), s1.end(), result->begin());
  std::copy(s2.begin(), s2.end(), tail);
  return StringBox(result);
}


PHPRT_API StringBox concat(const StringBox &x1, const StringBox &x2) {
  string_ref s1 = x1.get_string();
  if (s1.len == 0)
    return x2;
  string_ref s2 = x2.get_string();
  if (s2.len == 0)
    return x1;
  return concat_const(s1, s2);
}


PHPRT_API StringBox concat_generic(const Box &x1, const Box &x2) {
  return concat(x1.to_string(), x2.to_string());
}


PHPRT_API StringBox &concat_assign(box &var, const Box &rhs) {
  StringBox result = concat_generic(static_cast<Box>(var), rhs);
  box &target = var.dereference();
  target.move(static_cast<StringBox&&>(result));
  return static_cast<StringBox&>(var);
}


PHPRT_API StringBox &concat_assign_str(StringBox &var, const Box &rhs) {
  StringBox result = concat(var, rhs.to_string());
  box &target = var.dereference();
  target.move(static_cast<StringBox&&>(result));
  return var;
}

}
