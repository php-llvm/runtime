//===--- unit.cpp ---------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implements class Unit.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "unit.h"
#include "layout.h"
#include <string>
#include <iostream>
//------------------------------------------------------------------------------

//------ Global variables ------------------------------------------------------

//------------------------------------------------------------------------------

//------ UnitRecord ------------------------------------------------------------

namespace phprt {


UnitState *UnitState::find(int kind, const char *name) {
  UnitState *cursor = getUnitStateSectionStart();
  UnitState *last = getUnitStateSectionEnd();
  
  while (cursor != last) {
    // If module name is specified, find module with the given name.
    if (name && *name)
      while (cursor && !cursor->get_unit()->name.equals(name))
        ++cursor;
    if (cursor != last) {
      if (cursor->get_unit()->flags & kind)
        break;
      ++cursor;
    }
  }
  
  return cursor == last ? nullptr : cursor;
}


#ifdef _DEBUG

void Unit::dump() const {
  std::cout << "Unit[ " << std::string(name.data, name.len);
  if (!version.empty())
    std::cout << " | " << std::string(version.data, version.len);
  std::cout << " ] {" << std::endl;

  std::cout << "    flags: " << flags << " (";
  switch (flags & UnitFlag::KindMask) {
  case Script:    std::cout << "Script"; break;
  case Page:      std::cout << "Page"; break;
  case Module:    std::cout << "Module"; break;
  case Static:    std::cout << "Static"; break;
  case Config:    std::cout << "Config"; break;
  case Extension: std::cout << "Extension"; break;
  default:        std::cout << "Unknown"; break;
  }
  std::cout << ")" << std::endl;

  std::ios  state(NULL);
  state.copyfmt(std::cout);
  std::cout << "    on_unit_init    : " << std::hex
      << (intptr_t)on_unit_init << std::endl;
  std::cout << "    on_unit_shutdown: " << std::hex
      << (intptr_t)on_unit_shutdown << std::endl;
  std::cout << "    on_unit_load    : " << std::hex
      << (intptr_t)on_unit_load << std::endl;
  std::cout << "    on_unit_unload  : " << std::hex
      << (intptr_t)on_unit_unload << std::endl;
  std::cout << "    on_unit_execute : " << std::hex
      << (intptr_t)on_unit_execute << std::endl;
  std::cout << "}" << std::endl;
  std::cout.copyfmt(state);
}


void UnitState::dump() const {
  //TODO: implement
}


PHPRT_API void dump_compiled_units(const char *filter, bool verbose) {
  const Unit *cur = getUnitSectionStart();
  const Unit *last = getUnitSectionEnd();

  assert(last >= cur);
  while (cur != last && cur->name.data) {
    if (verbose)
      cur->dump();
    else
      std::cerr << cur->name.data << std::endl;
    ++cur;
  }
  
  std::cerr << "  Total: " << int(cur - getUnitSectionStart()) << std::endl;
}


PHPRT_API void dump_unit_states(const char *filter, bool verbose) {
  UnitState *cur = getUnitStateSectionStart();
  UnitState *last = getUnitStateSectionEnd();

  std::cerr << std::endl;
  assert(last >= cur);
  while (cur != last && cur->unit) {
    if (verbose)
      cur->dump();
    else if (cur->is_loaded())
      std::cerr << cur->unit->name.data << std::endl;
    ++cur;
  }

  std::cerr << "  Total: " << int(cur - getUnitStateSectionStart()) << std::endl;
}

#endif
}
