//===--- server-cgi.cpp ---------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of class CGIServer.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "server.h"
#include "request.h"
#include <iostream>
//------------------------------------------------------------------------------

//------ Global variables ------------------------------------------------------
static phprt::CGIServer cgi;
//------------------------------------------------------------------------------

namespace phprt {

CGIServer::CGIServer()
  : Server("cgi", "CGI Interface"),
  StreamServerSite(std::cout, std::cerr),
  unit(nullptr) {
}


CGIServer::~CGIServer() {
}


CGIServer *CGIServer::get() {
  return &cgi;
}


int CGIServer::initialize(std::string page,
                          const std::vector<std::string> &args) {
  // For test purposes accept script units too.
  unit = UnitState::find(UnitFlag::Page | UnitFlag::Script, nullptr);
  //TODO: 404
  if (!unit) {
    std::cerr << "Cannot find page code" << std::endl;
    return -1;
  }
  return 0;
}


Request *CGIServer::create_request() {
  Request *result = new Request(this, unit);
  return result;
}


int CGIServer::process_request(Request *req) {
  process_single_request(req, unit);
  // Execution is successful, but we do not need processing another request.
  return 1;
}


void CGIServer::send_headers(const HttpHeaders &headers) {
  format_headers(get_output(), headers);
}

}
