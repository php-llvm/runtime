//===--- server.cpp -------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of classes Server and StreamServerSite.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include <iostream>
#include "server.h"
#include "request.h"
#include "shared/string_tools.h"
//------------------------------------------------------------------------------

namespace phprt {

//------------------------------------------------------------------------------
// Implementation of Server.
//------------------------------------------------------------------------------

Server::Server(const char *n, const char *d)
  : flags(0) {
  name.init(n);
  description.init(d);
}


Server::~Server() {
}


Box Server::process_single_request(Request *req, UnitState *code) {
  assert(req);
  assert(request::current == nullptr);

  request::current = req;
  Box result = req->get_main_unit()->get_unit()->on_unit_execute(code);
  finish_request(req);
  request::current = nullptr;

  return result;
}


int Server::install(std::ostream **msg_stream) {
  // Need to continue application.
  return 0;
}


int Server::initialize(std::string page, const std::vector<std::string> &args) {
  return 0;
}


void Server::finish_request(Request *request) {
  request->finish();
}


//------------------------------------------------------------------------------
// Implementation of ServerSite.
//------------------------------------------------------------------------------

bool ServerSite::on_header(const Box &hdr, HeaderOp op, HttpHeaders &headers) {
  return true;
}


void ServerSite::format_headers(std::ostream &out, const HttpHeaders &headers) {
  // If response code differs from default, status header must present.
  bool status_done = false;
  if (headers.get_response_code() != HttpHeaders::DefaultHttpResponseCode) {
    if (!headers.get_status().is_null()) {
      string_ref status_str = headers.get_status().get_string();
      if (!status_str.empty()) {
        out << "Status: " << status_str << "\n";
        status_done = true;
      }
    }
    if (!status_done) {
      if (const HttpHeaders::Header *hdr = headers.get(STRING_REF("Status"))) {
        out << hdr->text.get_string() << "\n";
        status_done = true;
      }
    }
    if (!status_done) {
      if (headers.get_response_code()) {
        int code = headers.get_response_code();
        out << "Status: " << code;
        const char *as_text = get_http_response_as_text(code);
        if (as_text)
          out << " " << as_text << "\n";
        status_done = true;
      }
    }
  }

  if (!status_done) {
    if (const HttpHeaders::Header *hdr = headers.get(STRING_REF("Status"))) {
      out << hdr->text.get_string() << "\n";
      status_done = true;
    }
  }

  // Header 'Content-type' is mandatory.
  if (!headers.has(STRING_REF("Content-type"))) {
    out << "Content-type: " << get_default_mime_type()
        << "; charset=" << get_default_charset()
        << "\n";
  }

  // Output all remaining headers.
  for (const auto &hdr : headers.get_headers_list()) {
    if (hdr.name.equals_lower(STRING_REF("Status")) && status_done)
      continue;
    out << hdr.text.get_string() << "\n";
  }

  // Blank line separates headers from the body.
  out << "\n";
}


string_ref ServerSite::get_default_mime_type() {
  //TODO: take from object
  return STRING_REF("text/html");
}


string_ref ServerSite::get_default_charset() {
  //TODO: take from object
  return STRING_REF("UTF-8");
}


//------------------------------------------------------------------------------
// Implementation of StreamServerSite.
//------------------------------------------------------------------------------

StreamServerSite::StreamServerSite(std::ostream &out, std::ostream &err)
  : output(out), messages(err) {
}


StreamServerSite::~StreamServerSite() {
}


unsigned StreamServerSite::send_data(const char *d, unsigned len) {
  output.write(d, len);
  return len;
}


void StreamServerSite::flush() {
  output.flush();
}


void StreamServerSite::send_message(int level, string_ref message) {
  messages.write(message.data, message.len);
}


void StreamServerSite::exit(int retcode, string_ref message) {
  assert(0 && "not implemented");
}


bool StreamServerSite::get_current_user(Box &userid) {
  assert(0 && "not implemented");
  return false;
}

}
