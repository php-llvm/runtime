//===--- http_headers.cpp ---------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Managing HTTP headers.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "functions.h"
#include "server_support.h"
#include "log.h"
#include "shared/string_ref.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/Support/ErrorHandling.h"
#include <algorithm>
//------------------------------------------------------------------------------

namespace phprt {


HttpHeaders::HttpHeaders() : response_code(DefaultHttpResponseCode) {
}


HttpHeaders::~HttpHeaders() {
}


static int extract_response_code(string_ref &str) {
  // Response code goes after space, it must be an integer number.
  for (unsigned ndx = 0; ndx < str.len - 1; ++ndx) {
    if (str.data[ndx] == ' ' && str.data[ndx + 1] != ' ') {
      ++ndx;
      // Remove all symbols before the space, they must comprise protocol version.
      str = str.skip(ndx);
      if (!isdigit(str.data[0])) {
        // No response code specified in the header.
        return 0;
      }
      ndx = 1;
      while (ndx < str.len && isdigit(str.data[ndx]))
        ++ndx;
      if (ndx != str.len && (str.data[ndx] != ' ')) {
        //TODO: report broken format: digits are not separated from status text.
      }
      if (ndx > 3) {
        //TODO: report broken format: no more than 3 digits may be seen.
      }
      int code = atoi(str.data);
      return code;
    }
  }

  //TODO: report broken format;
  str = string_ref{ nullptr, 0 };
  return HttpHeaders::DefaultHttpResponseCode;
}


static string_ref extract_header_name(string_ref header) {
  const char *colon_ptr = std::find(header.begin(), header.end(), ':');
  unsigned len = colon_ptr - header.data;
  return string_ref{ header.data, len }.trim();
}


void HttpHeaders::add(const Box &aheader, bool replace) {
  if (!aheader.is_string())
    return;
  string_ref header = aheader.get_string();

  // Headers started with 'HTTP/' defines status line including response code.
  const unsigned HttpPrefixLen = sizeof("HTTP/") - 1;
  if ((header.len > HttpPrefixLen) &&
      header.starts_with_lower(STRING_REF("HTTP/"))) {
    string_ref rest = header.skip(HttpPrefixLen);
    response_code = extract_response_code(rest);
    status_string.set_copy(rest);
    return;
  }

  // Format the new item.
  Header new_item;
  new_item.text.copy(aheader);
  header = new_item.text.get_string();
  const char *colon_ptr = std::find(header.begin(), header.end(), ':');
  if (colon_ptr != header.end()) {
    new_item.name = extract_header_name(header);
    new_item.value = header.skip(colon_ptr + 1 - header.data).trim();
  }

  if (replace)
    remove(new_item.name);
  headers.push_back(std::move(new_item));
}


PHPRT_API bool HttpHeaders::remove(string_ref header) {
  Header *ptr = std::find_if(headers.begin(), headers.end(),
                             [header](const Header &hdr) -> bool {
    return hdr.name.equals_lower(header);
  });
  if (ptr != headers.end()) {
    headers.erase(ptr);
    return true;
  }
  return false;
}


void HttpHeaders::remove_all() {
  headers.clear();
}


const HttpHeaders::Header *HttpHeaders::get(string_ref header) const {
  const Header *ptr = std::find_if(headers.begin(), headers.end(),
                                   [header](const Header &hdr) -> bool {
    return hdr.name.equals_lower(header);
  });
  if (ptr == headers.end())
    return nullptr;
  return ptr;
}


void HttpHeaders::set_response_code(int code) {
  if (code != response_code) {
    response_code = code;
  }
}


void HttpHeaders::set_status(const Box &status) {
  status_string.copy(status);
}


void HttpHeaders::clear_status() {
  status_string.clear();
}


static bool check_header(const Box &hdr) {
  if (!hdr.is_string()) {
    LogError(SAPIFacility, 0)//TODO:code
      << "Header must be a string";
    return false;
  }
  if (hdr.get_string().empty())
    return false;
  // Header must not contain null or new line chars.
  for (const char ch : hdr.get_string()) {
    if (ch == '\0') {
      LogWarning(SAPIFacility, 0)//TODO:code
        << "Header may not contain NUL bytes";
      return false;
    }
    if (ch == '\n' || ch == '\r') {
      LogWarning(SAPIFacility, 0)//TODO:code
        << "Header may not contain "
           "more than a single header, new line detected";
      return false;
    }
  }
  return true;
}


static bool check_header_modification() {
  if (request::headers_sent) {
    if (request::headers_sent_in_file) {
      LogWarning(SAPIFacility, 0) //TODO:code
        << "Cannot modify header information - headers already sent by "
        << request::headers_sent_in_file << ":"
        << request::headers_sent_at_line;
    } else {
      LogWarning(SAPIFacility, 0 )//TODO:code
        << "Cannot modify header information - headers already sent";
    }
    return false;
  }
  return true;
}


PHPRT_API void add_header(const Box &hdr, bool replace) {
  if (!check_header(hdr) || !check_header_modification())
    return;
  string_ref hdr_name = extract_header_name(hdr.get_string());
  if (hdr_name.equals_lower(STRING_REF("Content-Type"))) {
    //TODO:
  } else if (hdr_name.equals_lower(STRING_REF("Content-Length"))) {
    //TODO:
  } else if (hdr_name.equals_lower(STRING_REF("Location"))) {
    //TODO:
  } else if (hdr_name.equals_lower(STRING_REF("WWW-Authenticate"))) {
    set_http_response_code(401); /* authentication-required */
  }
  if (Request *rq = request::current) {
    HttpHeaders &headers = rq->get_headers();
    if (rq->get_server()->on_header(hdr, HeaderOp::Add, headers)) {
      headers.add(hdr, replace);
    }
  }
}


PHPRT_API void add_header_with_response_code(const Box &hdr, bool replace,
                                             int http_response_code) {
  if (request::current) {
    request::current->get_headers().set_response_code(http_response_code);
    add_header(hdr, replace);
  }
}


PHPRT_API void remove_header(const Box &hdr) {
  if (!check_header(hdr) || !check_header_modification())
    return;
  string_ref hdr_text = hdr.get_string();
  if (std::find(hdr_text.begin(), hdr_text.end(), ':') != hdr_text.end()) {
    LogWarning(SAPIFacility, 0) //TODO:code
      << "Header to delete may not contain colon";
    return;
  }
  if (Request *rq = request::current) {
    HttpHeaders &headers = rq->get_headers();
    if (rq->get_server()->on_header(hdr, HeaderOp::Delete, headers)) {
      headers.remove(hdr_text);
    }
  }
}


PHPRT_API void remove_all_headers() {
  if (!check_header_modification())
    return;
  if (Request *rq = request::current) {
    HttpHeaders &headers = rq->get_headers();
    if (rq->get_server()->on_header(Box(), HeaderOp::DeleteAll, headers)) {
      headers.remove_all();
    }
  }
}


PHPRT_API bool headers_sent() {
  return request::current && request::headers_sent;
}


PHPRT_API bool headers_sent_file(StringBox &file) {
  if (request::current) {
    if (request::headers_sent_in_file)
      file.set_str(request::headers_sent_in_file);
    else
      file.set_null();
    return request::headers_sent;
  }
  return false;
}


PHPRT_API bool headers_sent_file_line(StringBox &file, Boxed<intvalue_t> &line) {
  if (request::current) {
    if (request::headers_sent_in_file)
      file.set_str(request::headers_sent_in_file);
    else
      file.set_null();
    line.set_integer(request::headers_sent_at_line);
    return request::headers_sent;
  }
  return false;
}


PHPRT_API ArrayBox headers_list() {
  // TODO:
  llvm_unreachable("not implemented");
  return ArrayBox();
}


PHPRT_API bool set_header_register_callback(const Box &handler) {
  // TODO:
  llvm_unreachable("not implemented");
  return false;
}


PHPRT_API Box get_http_response_code() {
  if (request::current)
    if (int code = request::current->get_headers().get_response_code())
      return Box(code);
  return Box(false);
}


PHPRT_API Box set_http_response_code(int ret_code) {
  if (request::current) {
    int old_rcode = request::current->get_headers().get_response_code();
    request::current->get_headers().set_response_code(ret_code);
    if (old_rcode)
      return Box(old_rcode);
  }
  return Box(true);
}

}
