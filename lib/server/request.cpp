//===--- request.cpp ------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of class Request and other entities related to request state.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "layout.h"
#include "request.h"
#include "globals_array.h"
#include "php_variables.h"
//------------------------------------------------------------------------------

//------ Global data -----------------------------------------------------------
namespace phprt {

// Mapping of variable name "GLOBALS" to $GLOBALS.
static GVR_SECTION VarRecord GVR_GLOBALS = { { "GLOBALS", 7 }, 0 };

// Array that implements $GLOBALS.
static GlobalsArray Globals(getGVRSectionStart(), getGVRSectionEnd(),
                            &php::var::GLOBALS);
}
//------------------------------------------------------------------------------


namespace phprt { namespace request {

THREAD_LOCAL Request *current;
THREAD_LOCAL unsigned precision;

THREAD_LOCAL bool headers_sent;
THREAD_LOCAL const char *headers_sent_in_file;
THREAD_LOCAL int headers_sent_at_line;
THREAD_LOCAL box header_callback;

}}


namespace php { namespace var {

THREAD_LOCAL box GLOBALS;

}}


namespace phprt {

Request::Request(ServerSite *s, UnitState *main)
  : server(s), main_unit(main),
  //TODO: this may be used only for cli.
  out_stream(std::cout.rdbuf()), err_stream(std::cerr.rdbuf()) {
  assert(s);

  // Set default float precision.
  request::precision = 14;

  // Initialize $GLOBALS.
  php::var::GLOBALS.set(&Globals);
  // Suppress warning on unused static.
  (void)GVR_GLOBALS;

  // Initialize headers.
  request::headers_sent = false;
  request::headers_sent_in_file = nullptr;
  request::headers_sent_at_line = 0;
}


std::ostream &Request::out() {
  if (!request::headers_sent) {
    start_output();
  }
  return out_stream;
}


std::ostream &Request::err() {
  return err_stream;
}


void Request::start_output() {
  server->send_headers(headers);
  request::headers_sent = true;
}


void Request::finish() {
  // If no output occurred, headers must be send anyway.
  if (!request::headers_sent) {
    server->send_headers(headers);
  }
}

}
