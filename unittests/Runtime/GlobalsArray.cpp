//===--- GlobalsArray.cpp ---------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Unit test for class GlobalsArray and GlobalsArrayIterator
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "globals_array.h"
#include "layout.h"
#include "unit.h"
#include "gtest/gtest.h"
//------------------------------------------------------------------------------

// These globals are required to generate correct section start and
// end symbols.
UNIT_SECTION phprt::Unit TEST_UNIT = {};
UNITSTATE_SECTION phprt::UnitState TEST_UNIT_STATE = {};
GVR_SECTION phprt::VarRecord TEST_GVR_GLOBALS = {};

namespace {
using namespace phprt;

// The test fixture.
class GlobalsArrayTest : public ::testing::Test {
};

// This test checks general GlobalsArray get functionality
TEST(GlobalsArrayTest, GlobalArrayGet) {
  phprt::Box GLOBALS, A, B, C;
  VarRecord GVR_GLOBALS[3] = {
    { string_ref::make("A"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&A) },
    { string_ref::make("B"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&B) },
    { string_ref::make("C"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&C) }
  };

  GlobalsArray GA(&GVR_GLOBALS[0], &GVR_GLOBALS[3], &GLOBALS);

  A.set_integer(333);
  B.set_str("adefeferf");
  C.set_double(3343.333434);
  
  EXPECT_TRUE(GA.get("A").is_integer());
  EXPECT_TRUE(GA.get("A").get_integer() == 333);
  EXPECT_TRUE(GA.get("B").is_string());
  EXPECT_TRUE(GA.get("B").get_string().equals("adefeferf"));
  EXPECT_TRUE(GA.get("C").is_double());
  EXPECT_TRUE(GA.get("C").get_double() == 3343.333434);

}

// This test checks general GlobalsArray set functionality
TEST(GlobalsArrayTest, GlobalArrayset) {
  phprt::Box GLOBALS, A, B, C;
  VarRecord GVR_GLOBALS[3] = {
    { string_ref::make("A"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&A) },
    { string_ref::make("B"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&B) },
    { string_ref::make("C"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&C) }
  };

  GlobalsArray GA(&GVR_GLOBALS[0], &GVR_GLOBALS[3], &GLOBALS);

  A.set_integer(333);
  B.set_str("adefeferf");
  C.set_double(3343.333434);

  GA.set("A", 777);
  EXPECT_TRUE(GA.get("A").is_integer());
  EXPECT_TRUE(GA.get("A").get_integer() == 777);
  GA.set("A", true);
  EXPECT_TRUE(GA.get("A").is_bool());
  EXPECT_TRUE(GA.get("A").get_bool());
  GA.set("A", 777.7);
  EXPECT_TRUE(GA.get("A").is_double());
  EXPECT_TRUE(GA.get("A").get_double() == 777.7);
  GA.set("A", "AAAAA");
  EXPECT_TRUE(GA.get("A").is_string());
  EXPECT_TRUE(GA.get("A").get_string().equals("AAAAA"));

}

// This test cheks general GlobalsArray get_size functionality
TEST(GlobalsArrayTest, GlobalArrayGetSize) {
  phprt::Box GLOBALS, A, B, C;
  VarRecord GVR_GLOBALS[3] = {
    { string_ref::make("A"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&A) },
    { string_ref::make("B"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&B) },
    { string_ref::make("C"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&C) }
  };

  GlobalsArray GA(&GVR_GLOBALS[0], &GVR_GLOBALS[3], &GLOBALS);

  A.set_undef();
  B.set_undef();
  C.set_undef();

  EXPECT_TRUE(GA.get_size() == 0);

  A.set_integer(333);
  B.set_str("adefeferf");
  C.set_double(3343.333434);

  EXPECT_TRUE(GA.get_size() == 3);

  GA.remove("A");

  EXPECT_TRUE(GA.get_size() == 2);
}

// This test cheks general GlobalsArray get_size functionality
TEST(GlobalsArrayTest, GlobalArrayRemove) {
  phprt::Box GLOBALS, A, B, C;
  VarRecord GVR_GLOBALS[3] = {
    { string_ref::make("A"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&A) },
    { string_ref::make("B"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&B) },
    { string_ref::make("C"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&C) }
  };

  GlobalsArray GA(&GVR_GLOBALS[0], &GVR_GLOBALS[3], &GLOBALS);

  A.set_integer(333);
  B.set_str("adefeferf");
  C.set_double(3343.333434);

  EXPECT_TRUE(GA.has_key("A"));
  GA.remove("A");
  EXPECT_FALSE(GA.has_key("A"));

  EXPECT_TRUE(GA.has_key("B"));
  GA.remove("B");
  EXPECT_FALSE(GA.has_key("B"));

  EXPECT_TRUE(GA.has_key("C"));
  GA.remove("C");
  EXPECT_FALSE(GA.has_key("C"));
}

// this test checks iteration over globals array
TEST(GlobalsArrayTest, GlobalArrayIterator1) {
  phprt::Box GLOBALS, A, B, C;
  VarRecord GVR_GLOBALS[3] = {
    {string_ref::make("A"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&A)}, 
    {string_ref::make("B"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&B)},
    {string_ref::make("C"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&C)} 
  };

  GlobalsArray GA(&GVR_GLOBALS[0], &GVR_GLOBALS[3], &GLOBALS);

  A.set_integer(333);
  B.set_str("adefeferf");
  C.set_double(3343.333434);

  auto it = GA.begin();
  EXPECT_TRUE(it->is_valid());
  EXPECT_TRUE(it->key().is_string());
  EXPECT_TRUE(it->key().get_string().equals_lower("A"));
  EXPECT_TRUE(it->value().is_integer());
  EXPECT_TRUE(it->value().get_integer() == 333);
  ++(*it);
  EXPECT_TRUE(it->is_valid());
  EXPECT_TRUE(it->key().is_string());
  EXPECT_TRUE(it->key().get_string().equals_lower("B"));
  EXPECT_TRUE(it->value().is_string());
  EXPECT_TRUE(it->value().get_string().equals("adefeferf"));
  ++(*it);
  EXPECT_TRUE(it->is_valid());
  EXPECT_TRUE(it->key().is_string());
  EXPECT_TRUE(it->key().get_string().equals_lower("C"));
  EXPECT_TRUE(it->value().is_double());
  EXPECT_TRUE(it->value().get_double() == 3343.333434);
  ++(*it);
  EXPECT_FALSE(it->is_valid());
}

// this test checks that unset global variables are skipped
TEST(GlobalsArrayTest, GlobalArrayIterator2) {
  phprt::Box GLOBALS, A, B, C;
  VarRecord GVR_GLOBALS[3] = {
    { string_ref::make("A"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&A) },
    { string_ref::make("B"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&B) },
    { string_ref::make("C"), (intptr_t)((intptr_t)&GLOBALS - (intptr_t)&C) }
  };

  GlobalsArray GA(&GVR_GLOBALS[0], &GVR_GLOBALS[3], &GLOBALS);

  A.set_integer(333);
  B.set_undef();
  C.set_double(3343.333434);

  auto it = GA.begin();
  EXPECT_TRUE(it->is_valid());
  EXPECT_TRUE(it->key().is_string());
  EXPECT_TRUE(it->key().get_string().equals_lower("A"));
  EXPECT_TRUE(it->value().is_integer());
  EXPECT_TRUE(it->value().get_integer() == 333);
  ++(*it);
  EXPECT_TRUE(it->is_valid());
  EXPECT_TRUE(it->key().is_string());
  EXPECT_TRUE(it->key().get_string().equals_lower("C"));
  EXPECT_TRUE(it->value().is_double());
  EXPECT_TRUE(it->value().get_double() == 3343.333434);
  ++(*it);
  EXPECT_FALSE(it->is_valid());
}
}
