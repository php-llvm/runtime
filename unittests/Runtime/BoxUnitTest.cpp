//===--- BoxUnitTest.cpp --------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Unit tests for type 'Box'.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "box.h"
#include "gtest/gtest.h"
//------------------------------------------------------------------------------

using namespace llvm;
using namespace phprt;

namespace {

// The test fixture.
class RuntimeTest : public ::testing::Test {
};


TEST_F(RuntimeTest, BoxDefaultConstructor) {
  Box Tmp;
  EXPECT_TRUE(Tmp.is_null());
}

TEST_F(RuntimeTest, BoxLong) {
  Box Tmp = 4445;
  EXPECT_TRUE(Tmp.get_integer() == 4445);

  EXPECT_TRUE(Tmp.is_integer());
  EXPECT_FALSE(Tmp.is_double());
  EXPECT_FALSE(Tmp.is_bool());
  EXPECT_FALSE(Tmp.is_true());
  EXPECT_FALSE(Tmp.is_false());
  EXPECT_FALSE(Tmp.is_indirect());
  EXPECT_FALSE(Tmp.is_null());
  EXPECT_FALSE(Tmp.is_refcounted());
  EXPECT_FALSE(Tmp.is_reference());
  EXPECT_FALSE(Tmp.is_string_constant());
}

TEST_F(RuntimeTest, BoxCopyMethod) {
  box Tmp;
  Tmp.init_integer(333);

  EXPECT_TRUE(Tmp.is_integer());
  EXPECT_TRUE(Tmp.get_integer() == 333);

  box Tmp2;
  Tmp2.copy(Tmp);

  EXPECT_TRUE(Tmp2.is_integer());
  EXPECT_TRUE(Tmp2.get_integer() == 333);
}

TEST_F(RuntimeTest, BoxMoveMethod) {
  box Tmp;
  Tmp.init_integer(333);

  EXPECT_TRUE(Tmp.is_integer());
  EXPECT_TRUE(Tmp.get_integer() == 333);

  box Tmp2;
  Tmp2.move(std::move(Tmp));

  EXPECT_TRUE(Tmp2.is_integer());
  EXPECT_TRUE(Tmp2.get_integer() == 333);

  EXPECT_TRUE(Tmp.is_undef());
}

TEST_F(RuntimeTest, BoxCopyConstructor) {
  Box Tmp = 333;

  EXPECT_TRUE(Tmp.is_integer());
  EXPECT_TRUE(Tmp.get_integer() == 333);

  Box Tmp2 = Tmp;

  EXPECT_TRUE(Tmp2.is_integer());
  EXPECT_TRUE(Tmp2.get_integer() == 333);
}

TEST_F(RuntimeTest, BoxMoveConstructor) {
  Box Tmp = 333;

  EXPECT_TRUE(Tmp.is_integer());
  EXPECT_TRUE(Tmp.get_integer() == 333);

  Box Tmp2 = std::move(Tmp);

  EXPECT_TRUE(Tmp2.is_integer());
  EXPECT_TRUE(Tmp2.get_integer() == 333);

  EXPECT_TRUE(Tmp.is_undef());
}


TEST_F(RuntimeTest, BoxDouble) {
  Box Tmp = 4556.77886;
  EXPECT_TRUE(Tmp.get_double() == 4556.77886);
  
  EXPECT_FALSE(Tmp.is_integer());
  EXPECT_TRUE(Tmp.is_double()); //<== TRUE
  EXPECT_FALSE(Tmp.is_bool());
  EXPECT_FALSE(Tmp.is_true());
  EXPECT_FALSE(Tmp.is_false());
  EXPECT_FALSE(Tmp.is_indirect());
  EXPECT_FALSE(Tmp.is_null());
  EXPECT_FALSE(Tmp.is_refcounted());
  EXPECT_FALSE(Tmp.is_reference());
  EXPECT_FALSE(Tmp.is_string_constant());
}

TEST_F(RuntimeTest, BoxTrue) {
  Box Tmp = true;
  EXPECT_TRUE(Tmp.get_type() == tyBool);
  EXPECT_TRUE(Tmp.get_kind() == vkBool);
  EXPECT_TRUE(Tmp.get_bool());

  EXPECT_FALSE(Tmp.is_integer());
  EXPECT_FALSE(Tmp.is_double());
  EXPECT_TRUE(Tmp.is_bool()); //<== TRUE
  EXPECT_TRUE(Tmp.is_true()); //<== TRUE
  EXPECT_FALSE(Tmp.is_indirect());
  EXPECT_FALSE(Tmp.is_null());
  EXPECT_FALSE(Tmp.is_refcounted());
  EXPECT_FALSE(Tmp.is_reference());
  EXPECT_FALSE(Tmp.is_string_constant());
}

TEST_F(RuntimeTest, BoxFalse) {
  Box Tmp = false;
  EXPECT_TRUE(Tmp.get_type() == tyBool);
  EXPECT_TRUE(Tmp.get_kind() == vkBool);
  EXPECT_FALSE(Tmp.get_bool());

  EXPECT_FALSE(Tmp.is_integer());
  EXPECT_FALSE(Tmp.is_double());
  EXPECT_TRUE(Tmp.is_bool()); //<== TRUE
  EXPECT_FALSE(Tmp.is_true());
  EXPECT_TRUE(Tmp.is_false()); //<== TRUE
  EXPECT_FALSE(Tmp.is_indirect());
  EXPECT_FALSE(Tmp.is_null());
  EXPECT_FALSE(Tmp.is_refcounted());
  EXPECT_FALSE(Tmp.is_reference());
  EXPECT_FALSE(Tmp.is_string_constant());
}

TEST_F(RuntimeTest, BoxReference) {
  Box Tmp = 333;
  EXPECT_TRUE(Tmp.is_integer());
  EXPECT_TRUE(Tmp.get_integer() == 333);
  Tmp.make_reference();
  EXPECT_TRUE(Tmp.is_reference());
  EXPECT_TRUE(Tmp.dereference().is_integer());
  EXPECT_TRUE(Tmp.dereference().get_integer() == 333);
}

TEST_F(RuntimeTest, BoxConstantString) {
  Box Tmp = "AAA";
  EXPECT_FALSE(Tmp.is_integer());
  EXPECT_FALSE(Tmp.is_double()); 
  EXPECT_FALSE(Tmp.is_bool());
  EXPECT_FALSE(Tmp.is_true());
  EXPECT_FALSE(Tmp.is_false());
  EXPECT_FALSE(Tmp.is_indirect());
  EXPECT_FALSE(Tmp.is_null());
  EXPECT_FALSE(Tmp.is_refcounted());
  EXPECT_FALSE(Tmp.is_reference());
  EXPECT_TRUE(Tmp.is_string_constant()); //<== TRUE


  EXPECT_TRUE(Tmp.get_string().equals("AAA"));
  EXPECT_TRUE(Tmp.get_string().len == 3);
}

} // anonymous namespace
