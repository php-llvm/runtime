//===--- ArrayConstVector.cpp ---------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Unit test for class ConstArray.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "array.h"
#include "gtest/gtest.h"
//------------------------------------------------------------------------------

namespace {
using namespace phprt;



TEST(ArrayVectorTest, EmptyArrayTest) {
  ConstArray x;

  ASSERT_TRUE (x.is_constant());
  ASSERT_FALSE(x.is_fixed());
  ASSERT_TRUE (x.is_vector());

  ASSERT_EQ   (x.size(), 0);
  ASSERT_TRUE (x.empty());

  ASSERT_TRUE (x.linear() != nullptr);
  ASSERT_TRUE (x.linear()->begin_index().is_undef());
  ASSERT_TRUE (x.linear()->end_index().is_undef());

  Box key(0);
  string_ref str_key;
  str_key.init("ABC");

  ASSERT_FALSE(x.has_key(key));
  ASSERT_FALSE(x.has_key(11));
  ASSERT_FALSE(x.has_key(str_key));
  ASSERT_FALSE(x.has_key("PQRT"));

  ASSERT_EQ   (x.get_kind(key), vkUndef);
  ASSERT_EQ   (x.get_kind(11), vkUndef);
  ASSERT_EQ   (x.get_kind(str_key), vkUndef);
  ASSERT_EQ   (x.get_kind("PQRT"), vkUndef);

  ASSERT_TRUE (x.get(key).is_undef());
  ASSERT_TRUE (x.get(11).is_undef());
  ASSERT_TRUE (x.get(str_key).is_undef());
  ASSERT_TRUE (x.get("PQRT").is_undef());
}

}
