//===--- box.h --------------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definition of universal type used in PHP runtime.
//
//  This file must compile in C mode, as it may be included by C implementations
//  (Zend engine, PHP extensions etc).
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_BOX_H
#define PHPRT_BOX_H

//------ Dependencies ----------------------------------------------------------
#include "shared/shared_defs.h"
#include "shared/value_kinds.h"
#include "shared/string_ref.h"
//------------------------------------------------------------------------------


#ifdef __cplusplus
namespace phprt {
class String;
class IArray;
class ConstArray;
class IObject;
class Reference;
class Box;
class StringBox;
class NumberBox;
class ArrayBox;
}
#endif


/// \brief Type used to represent integer value in universal type. Usually it is
/// 'long' but other mappings also possible. For instance, on Windows it makes
/// sense to use 'long long' because 'long' is 32-bit even on 64 platform.
typedef long intvalue_t;


PHPRT_API struct box {

#ifdef __cplusplus
  typedef phprt::String String;
  typedef phprt::IArray IArray;
  typedef phprt::IObject IObject;
  typedef phprt::Reference Reference;
  typedef phprt::Box Box;
  typedef phprt::ConstArray ConstArray;
  typedef phprt::StringBox StringBox;
  typedef phprt::NumberBox NumberBox;
#endif

  // Value slot.
  //
  union {
    long long slot1;
    bool v_bool;
    intvalue_t v_integer;
    double v_double;
    const char *v_cstring;
    char v_imm_str[ImmediatePart1Length];
    void *v_pointer;
#ifdef __cplusplus
    String *v_string;
    IArray *v_array;
    IObject *v_object;
    Reference *v_reference;
    box *v_indirect;
#endif
  } v;

  // Extension slot. Must immediately follow main value union, as it may contain
  // part of the stored value.
  // It also contains type information about the stored value.
  union {
    struct {
      unsigned size  : 24;
      unsigned type  : 6;
      unsigned extra : 2;
    } t;
    unsigned slot2;
  } e;

  // Extra slot. Contains data not associated with the values itself. Used in
  // containers to keep information such as link to the next element in a chain.
  unsigned link;

#ifdef __cplusplus

  // These assignments break reference counting mechanism.
  void operator = (Box &&x) = delete;
  void operator = (const Box&) = delete;

  //--- Initialization ---
  //
  // These methods work as constructors. They format uninitialized memory so
  // that it contains valid box object of the given value. As this class must
  // be a plain C type, it cannot have real constructors.

  void init_undef()                 { e.t.type = vkUndef; }
  void init()                       { e.t.type = vkNull; }
  void init_bool(bool b)            { e.t.type = vkBool; v.v_bool = b; }
  void init_true()                  { e.t.type = vkBool; v.v_bool = true; }
  void init_false()                 { e.t.type = vkBool; v.v_bool = false; }
  void init_integer(intvalue_t l)   { e.t.type = vkInteger; v.v_integer = l; }
  void init_double(double d)        { e.t.type = vkDouble; v.v_double = d; }

  void init_array(unsigned sz = 0);
  void init(IArray *a);
  void init(const ConstArray *a);
  void init(IObject *obj);

  void init_reference();
  void init_reference(Reference *r);
  void init_reference(box &val);

  void init_pointer(void *p) { e.t.type = vkPointer; v.v_pointer = p; }
  void init_block(const void *p, unsigned sz);
  void init_indirect(box *p) { assert(p); e.t.type = vkIndirect; v.v_indirect = p; }

  // There are several ways to initialize a string depending on ownership over
  // the content:
  //
  // - by copy. In this case new box object gets its own copy of string data.
  // - by sharing. New box object shares string data with other box.
  // - by move. Ownership over string data is moved to the new box object.

  // Special string initialization.
  void init_str() { v.v_cstring = ""; e.t.size = 0; e.t.type = vkConstString; }
  void init_char(char x) { v.v_imm_str[0] = x; e.t.type = vkImmediateStr_1; }

  // Initialization by copying.
  void init_copy(const char *s, unsigned len);
  void init_copy(string_ref s) { init_copy(s.data, s.len); }
  void init_copy(const char *s);

  // Initialization by sharing.
  void init_str(const char *s, unsigned len);
  void init_str(string_ref s) { init_str(s.data, s.len); }
  void init_str(const char *s);
  void init_str(String *s);
  void init_str(const StringBox &s);

  // Initialization by move.
  void init_move(char *s, unsigned len);

  // Copy and move 'constructors'.
  void init_copy(const box &x);
  void init_move(box &x);

  //--- Assignment ---
  //
  // These methods work as assign operator. They replace previous value stored
  // in the box object with the given one.

  // Acts as destructor.
  void clear();

  void set_undef()                  { clear(); init_undef(); }
  void set_null()                   { clear(); init(); }
  void set_bool(bool v)             { clear(); init_bool(v); }
  void set_true()                   { clear(); init_true(); }
  void set_false()                  { clear(); init_false(); }
  void set_integer(intvalue_t l)    { clear(); init_integer(l); }
  void set_double(double d)         { clear(); init_double(d); }

  void set_array(unsigned sz = 0)   { clear(); init_array(sz); }
  void set(const ConstArray *a)     { clear(); init(a); }
  void set(IArray *a)               { clear(); init(a); }
  void set(IObject *obj)            { clear(); init(obj); }

  void set_reference()              { clear(); init_reference(); }
  void set_reference(Reference *r)  { clear(); init_reference(r); }
  void set_reference(box &val)      { clear(); init_reference(val); }
  void make_reference();

  void set_str()                    { clear(); init_str(); }
  void set_char(char ch)            { clear(); init_char(ch); }

  void set_copy(const char *s, unsigned len)  { clear(); init_copy(s, len); }
  void set_copy(string_ref s)       { clear();  init_copy(s.data, s.len); }
  void set_copy(const char *s)      { clear(); init_copy(s); }

  void set_str(const char *s, unsigned len) { clear(); init_str(s, len); }
  void set_str(string_ref s)        { clear(); init_str(s); }
  void set_str(const char *s)       { clear(); init_str(s); }
  void set_str(String *s)           { clear(); init_str(s); }
  void set_str(const StringBox &s)  { clear(); init_str(s); }

  void set_move(char *s, unsigned len)  { clear(); init_move(s, len); }

  void copy(const box& s);
  void copy_value(const box& s) {
    dereference().copy(s.dereference());
  }

  void move(box &&x);
  void swap(box &x);

  //--- Type queries ---
  //

  value_kind get_kind() const { return static_cast<value_kind>(e.t.type); }
  value_kind get_direct_kind() const { return remove_indirection().get_kind(); }
  value_kind get_target_kind() const { return dereference().get_kind(); }

  value_type get_type() const {
    return phprt::get_type(static_cast<value_kind>(e.t.type));
  }
  value_type get_direct_type() const { return remove_indirection().get_type(); }
  value_type get_target_type() const { return dereference().get_type(); }

  const char *get_type_name() const { return phprt::get_type_name(get_type()); }

  // Checks if the object holds value of particular type.

  bool is_null() const { return get_target_type() == tyNull; }
  bool is_bool() const { return get_target_type() == tyBool; }
  bool is_true() const { return is_bool() && get_bool(); }
  bool is_false() const { return is_bool() && !get_bool(); }
  bool is_integer() const { return get_target_type() == tyInteger; }
  bool is_double() const { return get_target_type() == tyDouble; }
  bool is_number() const {
    value_type type = get_target_type();
    return type == tyInteger || type == tyDouble;
  }
  bool is_scalar() const { return is_string() || is_bool() || is_number(); }
  bool is_string() const { return get_target_type() == tyString; }
  bool is_array() const { return get_target_type() == tyArray; }
  bool is_object() const { return get_target_type() == tyObject; }

  // Check if the object holds value of particular type.

  bool is_undef() const { return phprt::is_undef(get_target_kind()); }
  bool is_reference() const { return phprt::is_reference(get_direct_kind()); }
  bool is_indirect() const { return phprt::is_indirect(get_kind()); }
  bool is_string_constant() const {
    return phprt::is_string_constant(get_target_kind());
  }
  bool is_string_immediate() const {
    return phprt::is_string_immediate(get_target_kind());
  }
  bool is_string_dynamic() const {
    return phprt::is_string_dynamic(get_target_kind());
  }

  bool is_pointer() const { return phprt::is_pointer(get_target_kind()); }
  bool is_block() const { return phprt::is_block(get_target_kind()); }

  bool is_refcounted() const {return phprt::is_refcounted(get_target_kind()); }

  //--- Value queries ---
  //
  // Returns value stored in the object.

  bool get_bool() const;
  intvalue_t get_integer() const;
  double get_double() const;

  string_ref get_string() const;
  String *get_var_string() const {
    assert(e.t.type == vkDynamicString);
    return v.v_string;
  }
  unsigned get_string_len() const;
  char *get_string_buffer(unsigned extra = 0);

  IArray *get_array() const;
  IObject *get_object() const;
  Reference *get_reference() const {
    assert(is_reference());
    return v.v_reference;
  }

  void *get_pointer() const;
  box *get_indirect() const { assert(is_indirect()); return v.v_indirect; }
  void *get_block() const;
  unsigned get_block_size() const;

  //--- Accessors ---
  //
  // Returns reference to value stored in the object.

  bool &access_bool();
  intvalue_t &access_integer();
  double &access_double();

  //--- Conversion ---
  //
  // Convert box value to specified type and return it.

  bool to_bool()const;
  intvalue_t to_integer() const;
  double to_double() const;
  StringBox to_string() const;
  NumberBox to_number() const;
  IArray *to_array() const;


  //--- Smart pointer functionality ---
  //
  // When a box object stores ref-counted value, it acts as a smart pointer. So
  // 'box' needs methods that transfer ownership to and from it.
  // - 'attach_XXX' transfer ownership to the 'box', they are similar to the
  //   method 'reset' of std smart pointers.
  // - 'detach_XXX' transfer ownership from the 'box' object, they are
  //   counterparts of the method 'release' of std smart pointer.

  template<typename T>
  void attach(T *x);

  template<typename T>
  T *detach();

  // Internal methods.
  box &remove_indirection();
  box &dereference();
  const box &remove_indirection() const {
    return const_cast<box*>(this)->remove_indirection();
  }
  const box &dereference() const {
    return const_cast<box*>(this)->dereference();
  }

#ifdef _DEBUG
  void dump(unsigned indent) const;
  void dump() const { dump(0); }
#endif

#endif
};


#ifdef __cplusplus
namespace phprt {

// Marker type. Used to select constructor of Box that builds undefined value.
struct undefined {};


// Wrapper over universal value.
//
// This class adds constructors, destructors etc, which allows correct lifetime
// management of shared object. Also is these properties simplify building of
// IR due to use of C++ semantics.
//
PHPRT_API class Box : public box {
public:

  // Construction

  Box()                 { box::init(); }
  Box(bool b)           { box::init_bool(b); }
  Box(int l)            { box::init_integer(l); }
  Box(unsigned i)       { box::init_integer(i); }
  Box(intvalue_t l)     { box::init_integer(l); }
  Box(double d)         { box::init_double(d); }
  Box(char ch)          { box::init_char(ch); }
  Box(const char *s)    { box::init_str(s); }
  Box(string_ref s)     { box::init_str(s); }
  Box(String *s)        { box::init_str(s); }
  Box(IArray *a)        { box::init(a); }
  Box(const ConstArray *a) { box::init(a); }

  ~Box()                { box::clear(); }
  Box(const Box &s)     { box::init(); box::copy(s); }
  Box(Box &&s)          { box::init_move(static_cast<box&>(s)); }

  Box(undefined)        { box::init_undef(); }

  // This constructor must not be used to silently convert box& to Box& by
  // making temporary object.
  explicit Box(const box &x) { box::init_copy(x); }

  // Remove all 'init_xxx' methods, - they are senseless for a type that has
  // real constructors.
  void init_undef() = delete;
  void init() = delete;
  void init_bool(bool v) = delete;
  void init_true() = delete;
  void init_false() = delete;
  void init_integer(intvalue_t l) = delete;
  void init_double(double d) = delete;
  void init_array(unsigned sz) = delete;
  void init(const ConstArray *a) = delete;
  void init(IArray *a) = delete;
  void init(IObject *obj) = delete;
  void init_reference() = delete;
  void init_reference(Reference *r) = delete;
  void init_reference(box &val) = delete;
  void init_pointer(void *p) = delete;
  void init_block(const void *p, unsigned sz) = delete;
  void init_indirect(box *p) = delete;
  void init_str() = delete;
  void init_char(char x) = delete;
  void init_copy(const char *s, unsigned len) = delete;
  void init_copy(string_ref s) = delete;
  void init_copy(const char *s) = delete;
  void init_str(const char *s, unsigned len) = delete;
  void init_str(string_ref s) = delete;
  void init_str(const char *s) = delete;
  void init_str(String *s) = delete;
  void init_str(const StringBox &s) = delete;
  void init_move(char *s, unsigned len) = delete;
  void init_copy(const box &x) = delete;
  void init_move(box &&x) = delete;
  void init_move(Box &&x) = delete;
};


// Boxed variants of PHP types.
//
// These are classes that encapsulate value of basic type into a box. Such types
// provide advantages of box - proper clean up during exceptions, multiple
// representations for the same type (like string), ability to contain subset
// of possible types. At the same time, they allow static type checking.


// Box that contains string data.
//
// Strings can exist in many forms, box representation is necessary if it is
// unknown which form is used.
//
PHPRT_API class StringBox : public Box {
public:

  StringBox()                 : Box()  { box::init_str(); }
  StringBox(bool b)           : Box(b) { to_string().swap(*this); }
  StringBox(int l)            : Box(l) { to_string().swap(*this); }
  StringBox(unsigned i)       : Box(i) { to_string().swap(*this); }
  StringBox(intvalue_t l)     : Box(l) { to_string().swap(*this); }
  StringBox(double d)         : Box(d) { to_string().swap(*this); }
  StringBox(char s)           : Box(s) {}
  StringBox(const char *s)    : Box(s) {}
  StringBox(string_ref s)     : Box(s) {}
  StringBox(String *s)        : Box(s) {}
  StringBox(const box &s);
  StringBox(box &&s);
  StringBox(const Box &s);
  StringBox(Box &&s);
};


// Box that contains a number.
//
// A number may be an integer or a float.
//
PHPRT_API class NumberBox : public Box {
public:

  NumberBox()             : Box() {}
  NumberBox(bool x)       : Box(x) {}
  NumberBox(intvalue_t x) : Box(x) {}
  NumberBox(int x)        : Box(x) {}
  NumberBox(unsigned x)   : Box(x) {}
  NumberBox(double x)     : Box(x) {}
  NumberBox(Box &&s)      : Box(s) { assert(s.is_number()); }
};


// Box that contains an array.
//
// Arrays may have different representations.
//
PHPRT_API class ArrayBox : public Box {
public:

  ArrayBox()            : Box() {}
  ArrayBox(IArray *x)   : Box(x) {}
  ArrayBox(const ConstArray *x) : Box(x) {}
  ArrayBox(Box &&s)     : Box(s) { assert(s.is_array()); }
};


// A value that may be used as a key into an array.
//
// This box keeps either integer or a string. It also performs transformations,
// required by PHP semantics, like "123" -> 123.
//
PHPRT_API class KeyBox : public Box {
public:

  KeyBox()              : Box(undefined()) {}
  KeyBox(bool x)        : Box(x) {}
  KeyBox(intvalue_t x)  : Box(x) {}
  KeyBox(int x)         : Box(x) {}
  KeyBox(unsigned x)    : Box(x) {}
  KeyBox(char s)        : Box(s) {}
  KeyBox(const char *s) : Box(s) {}
  KeyBox(string_ref s)  : Box(s) {}
  KeyBox(const box &x)  : Box(x) { assert(x.is_integer() || x.is_string()); }
  KeyBox(Box &&s)       : Box(s) { assert(s.is_integer() || s.is_string()); }

  bool equal(const KeyBox &key);
  bool less(const KeyBox &key);

#ifdef _DEBUG
  void dump(unsigned indent) const;
  void dump() const { dump(0); }
#endif
};


class CallableBox : public Box {
public:

  CallableBox()              : Box(undefined()) {}
  CallableBox(const char *s) : Box(s) {}
  CallableBox(string_ref s)  : Box(s) {}
  CallableBox(String *s)     : Box(s) {}
  CallableBox(IArray *x)     : Box(x) {}
  CallableBox(const ConstArray *x) : Box(x) {}
  //TODO: object
};


// Helper class used to represent a value that may correspond to PHP reference.
// We use these types when a PHP functions returns a reference, or for function
// parameters that are passed by reference.
//
template<typename T>
class RefBox : public Box {
public:
  RefBox() {}
  RefBox(const Box &x) : Box(x) {}
//  RefBox(const T &x) : Box(x.getAs<T>(x)) {}
};

extern template class RefBox<bool>;
extern template class RefBox<intvalue_t>;
extern template class RefBox<double>;
extern template class RefBox<StringBox>;
extern template class RefBox<ArrayBox>;
extern template class RefBox<Box>;


// Generic boxed type for pointers.
//
template<typename T>
class Boxed : public Box {
public:
  Boxed() {}
  Boxed(T *x) : Box(x) {}
  Boxed(const Box &x) : Box(x) {}
};

extern template class Boxed<bool>;
extern template class Boxed<intvalue_t>;
extern template class Boxed<double>;
extern template class Boxed<StringBox>;
extern template class Boxed<ArrayBox>;


// Helper functions
bool is_callable(const box &x);

}
#endif
#endif
