//===--- log.h --------------------------------------------------*- C++ -*-===//
//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definition of logging facility.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_LOG_H
#define PHPRT_LOG_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "shared/string_ref.h"
#include "string_data.h"
#include "box.h"
#include "request.h"
//------------------------------------------------------------------------------

enum ErrorLevel {
  Debug,
  Notice,
  Warning,
  Error,
  Fatal,
  ErrorLevelCount = Fatal
};

#define LogFatal(facility, msg)   if (Logger::should_emit_fatal())   Logger::log(facility, ErrorLevel::Fatal,   msg)
#define LogError(facility, msg)   if (Logger::should_emit_error())   Logger::log(facility, ErrorLevel::Error,   msg)
#define LogWarning(facility, msg) if (Logger::should_emit_warning()) Logger::log(facility, ErrorLevel::Warning, msg)
#define LogNotice(facility, msg)  if (Logger::should_emit_notice())  Logger::log(facility, ErrorLevel::Notice,  msg)
#define LogDebug(facility, msg)   if (Logger::should_emit_debug())   Logger::log(facility, ErrorLevel::Debug,   msg)


#define CoreFatal(msg)   LogFatal(CoreFacility, msg)
#define CoreError(msg)   LogError(CoreFacility, msg)
#define CoreWarning(msg) LogWarning(CoreFacility, msg)
#define CoreNotice(msg)  LogNotice(CoreFacility, msg)
#define CoreDebug(msg)   LogDebug(CoreFacility, msg)

#define TypeError(msg)      LogError(TypeFacility, msg)
#define TypeWarning(msg)    LogWarning(TypeFacility, msg)
#define ArithmError(msg)    LogError(ArithmeticFacility, msg)
#define DivByZeroError(msg) LogError(DivisionByZeroFacility, msg)


// returns true if message handled
typedef bool(message_emitter)(const class DiagnosticBuilder &builder);

//------ Global forward declarations -------------------------------------------
extern class Reaction default_reaction;
extern class Facility CoreFacility;
extern class Facility TypeFacility;
extern class Facility ArithmeticFacility;
extern class Facility ConversionFacility;
extern class Facility DivisionByZeroFacility;
extern class Facility SAPIFacility;
extern THREAD_LOCAL class Logger logger;
extern THREAD_LOCAL unsigned suppress_counter;

// forward declarations for emit functions
message_emitter cerr;
message_emitter throw_exception;
message_emitter throw_error;
message_emitter throw_type_error;
message_emitter throw_arithmetic_error;
message_emitter throw_division_by_zero_error;

message_emitter default_emitter;
//------------------------------------------------------------------------------

class Reaction {
private:
  message_emitter *emitter;

public:
  Reaction() : emitter(nullptr) {}
  Reaction(message_emitter *emitter) : emitter(emitter) {}

  void set_emitter(message_emitter *e) { emitter = e;    }
  message_emitter *get_emitter() const { return emitter; }
};


class Facility {
private:
  Reaction *reactions[ErrorLevelCount];
  const char *name;

public:
  Facility(const char *name, Reaction *reaction = &default_reaction);

  Reaction *get_reaction(ErrorLevel lvl) const;
  void set_reaction(ErrorLevel lvl, Reaction *reaction);

  const char *get_name() const { return name; }
};


class DiagnosticBuilder {
public:
  enum {
    max_format_substitutions = 10 // %0, ..., %9
  };


private:
  ErrorLevel severity;
  string_ref format;
  const Facility facility;

  phprt::StringBox substitutions[max_format_substitutions];
  unsigned substitutions_count;

public:

  DiagnosticBuilder(const Facility &facility, ErrorLevel lvl, string_ref format)
    : severity(lvl), format(format), facility(facility),
      substitutions_count(0) {
  }

  ~DiagnosticBuilder();

  ErrorLevel       get_level()    const { return severity; }
  const Facility  &get_facility() const { return facility; }

  std::string build_message() const;

  DiagnosticBuilder &operator << (bool               v);
  DiagnosticBuilder &operator << (int                v);
  DiagnosticBuilder &operator << (unsigned           v);
  DiagnosticBuilder &operator << (long               v);
  DiagnosticBuilder &operator << (unsigned long      v);
  DiagnosticBuilder &operator << (unsigned long long v);
  DiagnosticBuilder &operator << (char               v);
  DiagnosticBuilder &operator << (double             v);
  DiagnosticBuilder &operator << (string_ref         v);
  DiagnosticBuilder &operator << (const char        *v);

};

class Logger {
private:
  struct report_error_flags {
    bool do_log_fatal;
    bool do_log_error;
    bool do_log_warning;
    bool do_log_notice;
    bool do_log_debug;

    void set_error_level(ErrorLevel lvl, bool have_user_handler);
    void mute();
  };

public:
  static void init();

  static bool should_emit_fatal()   { return logger.error_report_flags.do_log_fatal;   }
  static bool should_emit_error()   { return logger.error_report_flags.do_log_error;   }
  static bool should_emit_warning() { return logger.error_report_flags.do_log_warning; }
  static bool should_emit_notice()  { return logger.error_report_flags.do_log_notice;  }
  static bool should_emit_debug()   { return logger.error_report_flags.do_log_debug;   }

  static ErrorLevel get_level()          { return logger.current_error_level; }
  static void  set_level(ErrorLevel lvl) { logger.current_error_level = lvl;  }

  static DiagnosticBuilder log(const Facility &facility, ErrorLevel severity, const char *msg);
  
  static void suppress_start();
  static void suppress_stop();
  static bool suppress_active();

  struct frame {
    std::string name;     // function name
    uint64_t    ptr;      // stack frame pointer
    std::string filename; // source file name
    uint64_t    line;     // line number
  };
  static std::vector<frame> get_backtrace(unsigned skip_frames_num = 3);

private:
  friend class DiagnosticBuilder;
  void emit_message(const DiagnosticBuilder &builder);

  box                user_handler;
  ErrorLevel         current_error_level;
  report_error_flags error_report_flags;
};

#endif //PHPRT_LOG_H
