//===--- request.h --------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Declaration of class Request.
//
//===----------------------------------------------------------------------===//

#ifndef PHPRT_REQUEST_H
#define PHPRT_REQUEST_H

//------ Dependencies ----------------------------------------------------------
#include "server.h"
#include "server_support.h"
#include <iostream>
//------------------------------------------------------------------------------


namespace phprt {


PHPRT_API class Request {
public:
  Request(ServerSite *s, UnitState *main);

  ServerSite *get_server() const { return server; }
  UnitState *get_main_unit() const { return main_unit; }
  std::ostream &out();
  std::ostream &err();
  HttpHeaders &get_headers() { return headers; }

  void finish();

private:
  void start_output();

  ServerSite *server;
  UnitState *main_unit;
  std::ostream out_stream;
  std::ostream err_stream;
  HttpHeaders headers;
};


namespace request {

extern THREAD_LOCAL Request *current;
extern THREAD_LOCAL unsigned precision;

// HTTP headers support.
extern THREAD_LOCAL bool headers_sent;
extern THREAD_LOCAL const char *headers_sent_in_file;
extern THREAD_LOCAL int headers_sent_at_line;
extern THREAD_LOCAL box header_callback;

}


inline unsigned get_precision() { return request::precision; }

inline unsigned set_precision(unsigned x) {
  unsigned result = request::precision;
  request::precision = x;
  return result;
}

inline std::ostream &out() {
  if (request::current)
    return request::current->out();
  return std::cout;
}

inline std::ostream &err() {
  if (request::current)
    return request::current->err();
  return std::cerr;
}

}
#endif
