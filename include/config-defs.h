//===--- config-defs.h ------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definitions which are independent of particular configuration or are
//  calculated.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_CONFIG_DEFS_H
#define PHPRT_CONFIG_DEFS_H

//------ Dependencies ----------------------------------------------------------
//------------------------------------------------------------------------------


// PHPRT_API - specifies calling convention for API functions.
//
#if defined (PHPRT_SHARED_RUNTIME) && defined (WIN32)
# if defined PHPRT_EXPORT_API
#  define  PHPRT_API __declspec(dllexport)
# else
#  define  PHPRT_API __declspec(dllimport)
# endif
#else
#define PHPRT_API
#endif


// WEAK_SYMBOL - portable way to declare weak symbols.
//
#ifdef _WIN32
#  define WEAK_SYMBOL __declspec(selectany)
#else
#  define WEAK_SYMBOL __attribute__((weak))
#endif


#ifdef _MSC_VER
// Have to have this macro because VS does not understand 'noexcept' and
// GCC does not support 'throw(...)'.
#  define THROWS  throw(...)
#else
#  define THROWS noexcept(false)
#endif


#ifdef _MSC_VER
#define __attribute__(x)
#endif


// Thread local specifier.
//
// NOTE: MSVS2015 also supports thread_local. We need to investigate is it the same
// as __desclspec(thread) 
#ifdef _MSC_VER
#  define THREAD_LOCAL  __declspec(thread)
#elif __APPLE__
#  define THREAD_LOCAL __thread
#else
#  define THREAD_LOCAL  thread_local
#endif


// Visual Studio 2013 does not know about constexpr.
#ifdef _MSC_VER
# if _MSC_VER < 1900
#   define _ALLOW_KEYWORD_MACROS
#   define constexpr
# endif
#endif


// Convenience macros, taken from PHP sources.
//
#ifdef __cplusplus
# define BEGIN_EXTERN_C() extern "C" {
# define END_EXTERN_C() }
#else
# define BEGIN_EXTERN_C()
# define END_EXTERN_C()
#endif


// We need to define types, which are usually found in system headers, to avoid
// dependency on target in compiler.
//
# if defined _LP64
  typedef long ptrslot_t;
# elif defined _ILP32
  typedef int ptrslot_t;
# elif defined __SIZEOF_POINTER__
#   if defined(__SIZEOF_INT__) && __SIZEOF_POINTER__ == __SIZEOF_INT__
  typedef int ptrslot_t;
#   elif defined(__SIZEOF_LONG__) && __SIZEOF_POINTER__ == __SIZEOF_LONG__
  typedef long ptrslot_t;
#   elif defined(__SIZEOF_LONG_LONG__) && __SIZEOF_POINTER__ == __SIZEOF_LONG_LONG__
  typedef long long ptrslot_t;
#   else
#     error "Cannot determine intptr_t"
#   endif
# elif defined _WIN64
  typedef long long ptrslot_t;
# elif defined _WIN32
  typedef int ptrslot_t;
# else
#   error "Cannot determine intptr_t"
# endif

#endif
