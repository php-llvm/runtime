//===--- unit.h -------------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definition of descriptor that represents PHP file or extension module
//  in runtime.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_UNIT_H
#define PHPRT_UNIT_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "shared/constants.h"
#include "shared/value_kinds.h"
#include "shared/string_ref.h"
#include "box.h"
//------------------------------------------------------------------------------


namespace phprt {

struct UnitState;
class Request;


// Initialization handler.
//
// The handler is called when a unit becomes known to runtime but prior when
// that unit becomes loaded. The handler may, for instance, register classes,
// functions etc if the unit is compiled in 'emodule' mode.
//
// Returns 0 if completed with success and code error otherwise.
//
typedef int (unit_init_handler)(UnitState *state);


// Shutdown handler.
//
// The handler is called when the unit is unloaded. It occurs only when entire
// application is shutdown.
//
// Returns 0 in case of success and code error otherwise.
//
typedef int (unit_shutdown_handler)(UnitState *state);


// Load handler.
//
// The handler is called when unit is activated in the running request or script
// invocation. This occurs when page code executes 'include' statement. The
// handler may register classes, functions and constants defined in the unit.
//
// Returns 0 if success and code error otherwise.
//
typedef int (unit_load_handler)(UnitState*, Request*);


// Unload handler.
//
// The handler is called when the request, in which the unit was activated, has
// finished.
//
// Returns 0 in case of success and code error otherwise.
//
typedef int (unit_unload_handler)(UnitState*, Request*);


// Execute handler.
//
// The handler is called when the unit just loaded contains executable code.
// The code is executed by this handler.
//
// Returns result in corresponding 'include' expression in PHP.
//
typedef Box (unit_execute_handler)(UnitState*);


/// \brief Unit descriptor, constant part.
///
/// Object of this type represents information about the unit known at compile
/// time. So this object may live in read-only section.
///
PHPRT_API struct Unit {
  string_ref name;      // Null terminated full path of the unit
  string_ref version;   // Null terminated version string
  unsigned flags;

  // Event handlers
  unit_init_handler       *on_unit_init;
  unit_shutdown_handler   *on_unit_shutdown;
  unit_load_handler       *on_unit_load;
  unit_unload_handler     *on_unit_unload;
  unit_execute_handler    *on_unit_execute;

  //TODO: exports (functions, classes, constants)
  //TODO: dependencies

  bool is_script() const {
    return (flags & UnitFlag::KindMask) == UnitFlag::Script;
  }
  bool is_page() const {
    return (flags & UnitFlag::KindMask) == UnitFlag::Page;
  }
  bool is_module() const {
    return (flags & UnitFlag::KindMask) == UnitFlag::Module;
  }
  bool is_static() const {
    return (flags & UnitFlag::KindMask) == UnitFlag::Static;
  }
  bool is_config() const {
    return (flags & UnitFlag::KindMask) == UnitFlag::Config;
  }
  bool is_extension() const {
    return (flags & UnitFlag::KindMask) == UnitFlag::Extension;
  }

#ifdef _DEBUG
  void dump() const;
#endif
};


/// \brief Unit descriptor, variable part.
///
/// Object of this type keeps information about the unit state, which can change
/// during the unit life time.
///
PHPRT_API struct UnitState {
  // Flags
  enum {
    Loaded = 0x0001
  };

  // Fields
  const Unit *unit;
  unsigned flags;
  UnitState *next_loaded; // Makes list of all loaded units

  const Unit *get_unit() const { return unit; }

  bool is_loaded() const { return flags & Loaded; }
  UnitState *get_next_loaded() const { return next_loaded; }

  void mark_loaded() {
    assert(!(flags & Loaded));
    flags |= Loaded;
  }
  void set_next_loaded(UnitState *reg) { next_loaded = reg; }
  void set_unit(Unit *m) { unit = m; }

  static UnitState *find(int kind, const char *name);

#ifdef _DEBUG
  void dump() const;
#endif
};


#ifdef _DEBUG
PHPRT_API void dump_compiled_units(const char *filter, bool verbose);
PHPRT_API void dump_unit_states(const char *filter, bool verbose);
#endif
}


#endif
