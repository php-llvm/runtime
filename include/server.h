//===--- box.h --------------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definition of the interface to server.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_SERVER_H
#define PHPRT_SERVER_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "shared/string_ref.h"
#include "box.h"
#include "unit.h"
#include <iostream>
#include <string>
#include <vector>
//------------------------------------------------------------------------------


namespace phprt {

class Request;
class HttpHeaders;


/// \brief Enumerates possible operations on http headers.
///
enum class HeaderOp {
  Replace,
  Add,
  Delete,
  DeleteAll,
  SetStatus
};


PHPRT_API class Server {
protected:
  unsigned flags;
  string_ref name;
  string_ref description;

  Server(const char *n, const char *d);
  virtual ~Server();

public:

  string_ref get_name() const { return name; }
  string_ref get_description() const { return description; }

  void set_flag(unsigned flag) { flags |= flag; }
  unsigned get_flags() const { return flags; }

  /// \brief Implements early setup of server.
  ///
  /// This method is called as early as possible, when PHP execution environment
  /// is not set up (extensions are not loaded, builtin constants is not defined
  /// etc). A server may do all its job entirely in this function if it does not
  /// need to execute PHP code. For instance, a FCGI server listens to the
  /// selected socket, creates requests and pass them to workers. Then this work
  /// finishes, entire application may be shutdown.
  ///
  /// \param msg_stream pointer to the pointer that will be assigned pointer to
  /// a stream that will be used for message output (including error messages).
  ///
  /// \returns <0 if error occured, 0 if the execution must continue, >0 if
  ///          the execution must finish but it is a normal condition.
  ///
  virtual int install(std::ostream **msg_stream);

  /// \brief Implements late setup of server.
  ///
  /// This method is called late, when PHP execution environment has set up. If
  /// the server need to set some constants, classes or similar, this is the
  /// right place to do it.
  ///
  /// \param page String or page name.
  /// \param args Script arguments in the case of CLI server.
  /// \returns <0 if error occured, 0 if the execution must continue, >0 if
  ///          the execution must finish but it is a normal condition.
  ///
  virtual int initialize(std::string page, const std::vector<std::string> &args);

  /// \brief Creates new request.
  ///
  /// \returns New request or NULL if no more job remained.
  ///
  virtual Request *create_request() = 0;

  /// \brief Processes the given request.
  ///
  /// \returns 0 if request processed successfully, <0 if error occured, >0 if
  /// some event occurred which does not prevent from processing other requests.
  ///
  /// \note the fuction must set request::current while it is executed and clear
  /// it when the execution finishes.
  ///
  virtual int process_request(Request *request) = 0;

  /// \brief Finish processing request.
  ///
  virtual void finish_request(Request *request);

  /// \brief Implementation of process_request for single thread.
  ///
  Box process_single_request(Request *request, UnitState *code);
};


/// \brief Service provided by server for code that processes request.
///
PHPRT_API class ServerSite {
protected:
  void format_headers(std::ostream &out, const HttpHeaders &headers);
  virtual std::ostream &get_output() = 0;

public:

  string_ref get_default_mime_type();
  void       set_default_mime_type(string_ref);
  string_ref get_default_charset();
  void       set_default_charset(string_ref);

  /// \brief Write data into the current reply.
  ///
  virtual unsigned send_data(const char *d, unsigned len) = 0;

  /// \brief Write headers into the current reply.
  ///
  virtual void send_headers(const HttpHeaders &headers) = 0;

  /// \brief Initiate actual sending data.
  ///
  virtual void flush() = 0;

  /// \brief write a message associated with the current reply.
  ///
  ///TODO: use enumeration as level.
  virtual void send_message(int level, string_ref message) = 0;

  /// \brief Called by script body if it executes action 'exit'.
  ///
  virtual void exit(int retcode, string_ref message) = 0;

  /// \brief Reports current user identification.
  ///
  virtual bool get_current_user(Box &userid) = 0;

  virtual bool on_header(const Box &hdr, HeaderOp op, HttpHeaders &headers);
};


/// \brief Implementation of ServerSite for a server that uses stream as output.
///
class StreamServerSite : public ServerSite {
  std::ostream &output;
  std::ostream &messages;

public:

  StreamServerSite(std::ostream &out, std::ostream &err);
  ~StreamServerSite();

  std::ostream &get_output() override { return output; }
  unsigned send_data(const char *d, unsigned len) override;
  void flush() override;
  void send_message(int level, string_ref message) override;
  void exit(int retcode, string_ref message) override;
  bool get_current_user(Box &userid) override;
};


/// \brief Command Line Server.
///
class CLIServer : public Server, StreamServerSite {
  UnitState *unit;

public:
  CLIServer();
  ~CLIServer();

  static CLIServer *get();

  // Server
  int initialize(std::string page, const std::vector<std::string> &args) override;
  Request *create_request() override;
  int process_request(Request *request) override;

  // ServerSite
  void send_headers(const HttpHeaders &headers) override;
};


/// \brief Common Gateway Interface Server.
///
class CGIServer : public Server, StreamServerSite {
  UnitState *unit;

public:
  CGIServer();
  ~CGIServer();

  static CGIServer *get();

  // Server
  int initialize(std::string page, const std::vector<std::string> &args) override;
  Request *create_request() override;
  int process_request(Request *request) override;

  // ServerSite
  void send_headers(const HttpHeaders &headers) override;
};


/// \brief Embedded HTTP server.
///
class HttpServer : public Server, ServerSite {
public:
  HttpServer();
  ~HttpServer();

  static HttpServer *get();

  // Server
  int install(std::ostream **msg_stream) override;
  int initialize(std::string page, const std::vector<std::string> &args) override;
  Request *create_request() override;
  int process_request(Request *request) override;

  // ServerSite
  unsigned send_data(const char *d, unsigned len) override;
  void send_headers(const HttpHeaders &headers) override;
  void flush() override;
  void send_message(int level, string_ref message) override;
  void exit(int retcode, string_ref message) override;
  bool get_current_user(Box &userid) override;
};


/// \brief FGCI server.
///
class FcgiServer : public Server {
public:
  FcgiServer();
  ~FcgiServer();

  static FcgiServer *get();

  // Server
  int install(std::ostream **msg_stream) override;
  int initialize(std::string page, const std::vector<std::string> &args) override;
  Request *create_request() override;
  int process_request(Request *request) override;
};


/// \brief FCGI worker server.
///
class FcgiWorkerServer : public Server, ServerSite {
public:
  FcgiWorkerServer();
  ~FcgiWorkerServer();

  static FcgiWorkerServer *get();

  // Server
  int install(std::ostream **msg_stream) override;
  int initialize(std::string page, const std::vector<std::string> &args) override;
  Request *create_request() override;
  int process_request(Request *request) override;

  // ServerSite
  unsigned send_data(const char *d, unsigned len) override;
  void send_headers(const HttpHeaders &headers) override;
  void flush() override;
  void send_message(int level, string_ref message) override;
  void exit(int retcode, string_ref message) override;
  bool get_current_user(Box &userid) override;
};

}
#endif
