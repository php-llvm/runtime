//===--- string_data.h ------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definition of string object.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_STRING_DATA_H
#define PHPRT_STRING_DATA_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "shared/value_kinds.h"
#include "shared/string_ref.h"
#include <cstddef>
//------------------------------------------------------------------------------

namespace phprt {

class String {
public:
  // Enumerates possible string implementations.
  enum {
    WithHeader  = 0,
    Buffer      = 1
  };

  // Creation.
  static String *create(unsigned sz);

  // Reference counting.
  unsigned add_ref() { return ++counter; }
  unsigned remove_ref();
  unsigned get_counter() const { return counter; }

  // Querying properties.
  unsigned get_length() const { return length; }
  unsigned get_hash() const { return hash; }

  // Getting content.
  const char *get_content() const {
    return (kind == WithHeader) ? v.val :v.content;
  }
  string_ref get_string() const {
    return string_ref::make(get_content(), length);
  }
  char *get_content() {
    return (kind == WithHeader) ? v.val :v.content;
  }

  // Container interface
  typedef char *iterator;
  typedef const char *const_iterator;

  unsigned size() const { return length; }
  bool empty() const { return length == 0; }
  const char *begin() const { return get_content(); }
  const char *end() const { return get_content() + length; }
  char *begin() { return get_content(); }
  char *end() { return get_content() + length; }

#ifdef _DEBUG
  void dump() const;
  static unsigned get_total();
  static unsigned get_total_memory();
#endif

protected:

  unsigned counter;       // Reference counter
  unsigned length;        // String length
  unsigned hash    : 16;  // Hash value of this string
  unsigned kind    : 2;   // How string is implemented
  bool hash_valid  : 1;   // True, if field 'hash' contains actual string hash
  union {
    char *content;              // Points to string content
    char val[sizeof(char*)];    // Start of content
  } v;

  String() : counter(0), length(0), hash(0), kind(Buffer), hash_valid(false) {
    v.content = nullptr;
  }

  String(unsigned sz)
    : counter(0), length(sz), hash(0), kind(WithHeader), hash_valid(false) {}

  void *operator new(size_t);
  void *operator new(size_t, size_t);
  void operator delete(void *p);

  static void *allocate(unsigned sz);
  static void deallocate(void *);
};

}
#endif
