//===--- functions.h --------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definition of functions called from PHP code.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_FUNCTIONS_H
#define PHPRT_FUNCTIONS_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "shared/value_kinds.h"
#include "shared/string_ref.h"
#include "argpack.h"
#include "box.h"
#include "value.h"
//------------------------------------------------------------------------------

namespace phprt {

// Constant support
PHPRT_API bool is_constant_defined(string_ref name);
PHPRT_API Box  get_constant_value(string_ref name);
PHPRT_API Box  find_constant(string_ref name, string_ref nsname);
PHPRT_API void init_constant(ValueInfo &variable, Box value);


// box accessors functions for type hints
PHPRT_API bool        checked_get_bool_strict   (const Box& v);
PHPRT_API bool        checked_get_bool_weak     (const Box& v);
PHPRT_API intvalue_t  checked_get_integer_strict(const Box& v);
PHPRT_API intvalue_t  checked_get_integer_weak  (const Box& v);
PHPRT_API double      checked_get_double_strict (const Box& v);
PHPRT_API double      checked_get_double_weak   (const Box& v);
PHPRT_API StringBox   checked_get_string_strict (const Box& v);
PHPRT_API StringBox   checked_get_string_weak   (const Box& v);

PHPRT_API ArrayBox    checked_get_array         (const Box& v);

PHPRT_API RefBox<bool>       checked_get_bool_ref_strict   (Box& v);
PHPRT_API RefBox<bool>       checked_get_bool_ref_weak     (Box& v);
PHPRT_API RefBox<intvalue_t> checked_get_integer_ref_strict(Box& v);
PHPRT_API RefBox<intvalue_t> checked_get_integer_ref_weak  (Box& v);
PHPRT_API RefBox<double>     checked_get_double_ref_strict (Box& v);
PHPRT_API RefBox<double>     checked_get_double_ref_weak   (Box& v);
PHPRT_API RefBox<StringBox>  checked_get_string_ref_strict (Box& v);
PHPRT_API RefBox<StringBox>  checked_get_string_ref_weak   (Box& v);
PHPRT_API RefBox<ArrayBox>   checked_get_array_ref         (Box& v);

PHPRT_API bool        &checked_access_bool_strict   (Box& v);
PHPRT_API bool        &checked_access_bool_weak     (Box& v);
PHPRT_API intvalue_t  &checked_access_integer_strict(Box& v);
PHPRT_API intvalue_t  &checked_access_integer_weak  (Box& v);
PHPRT_API double      &checked_access_double_strict (Box& v);
PHPRT_API double      &checked_access_double_weak   (Box& v);
PHPRT_API StringBox   &checked_access_string_strict (Box& v);
PHPRT_API StringBox   &checked_access_string_weak   (Box& v);

PHPRT_API IArray      *checked_access_array(Box& v);

// Dynamic function call.
PHPRT_API Box call_function(string_ref name/*, ArgPack<Box> args*/);

// Output.
PHPRT_API void echo_txt(const char *str, unsigned len);
PHPRT_API void echo_string(string_ref str);
PHPRT_API void echo_integer(intvalue_t l);
PHPRT_API void echo_bool(bool b);
PHPRT_API void echo_double(double d);
PHPRT_API void echo_box(const box& b);

// Value conversions.
PHPRT_API bool convert_bool_to_string(bool x);
PHPRT_API bool convert_cstring_to_bool(const char* str);
PHPRT_API intvalue_t convert_string_to_integer(string_ref str);
PHPRT_API intvalue_t convert_cstring_to_integer(const char *str);
PHPRT_API double convert_string_to_double(string_ref str);
PHPRT_API double convert_cstring_to_double(const char *str);
PHPRT_API bool starts_with_number(string_ref str);

// Operators.
PHPRT_API intvalue_t pow_int(intvalue_t x, intvalue_t y);
PHPRT_API double pow_float(double x, double y);
PHPRT_API Box pow_generic(const Box &x, const Box &y);

PHPRT_API intvalue_t pow_assign_int(intvalue_t &x, intvalue_t y);
PHPRT_API intvalue_t pow_assign_int_box(intvalue_t &x, const Box &y);
PHPRT_API double pow_assign_float(double &x, double y);
PHPRT_API double pow_assign_float_box(double &x, const Box &y);
PHPRT_API Box pow_assign_generic(Box &x, const Box &y);

  
PHPRT_API ArrayBox add_arrays(const IArray *a, const IArray *b);
PHPRT_API Box add_box(const Box &val1, const Box &val2);
PHPRT_API Box add_box_integer(const Box &val1, const Box &val2);

PHPRT_API ArrayBox add_assign_arrays(ArrayBox a, const IArray *b);
PHPRT_API Box add_assign_box(Box &val1, const Box &val2);
PHPRT_API Box add_assign_box_integer(Box &val1, const Box &val2);
  
PHPRT_API Box sub_box(const Box & val1, const Box & val2);
PHPRT_API Box sub_box_integer(const Box & val1, const Box & val2);
  
PHPRT_API Box sub_assign_box(Box & val1, const Box & val2);
PHPRT_API Box sub_assign_box_integer(Box & val1, const Box & val2);

PHPRT_API Box mul_box(const Box & val1, const Box & val2);
PHPRT_API Box mul_box_integer(const Box & val1, const Box & val2);
  
PHPRT_API Box mul_assign_box(Box & val1, const Box & val2);
PHPRT_API Box mul_assign_box_integer(Box & val1, const Box & val2);

PHPRT_API Box div_box(const Box &val1, const Box &val2);
PHPRT_API Box div_box_integer(const Box &val1, const Box &val2);
  
PHPRT_API Box div_assign_box(Box &val1, const Box &val2);
PHPRT_API Box div_assign_box_integer(Box &val1, const Box &val2);

PHPRT_API Box rem_box(const Box &val1, const Box &val2);
PHPRT_API Box rem_assign_box(Box &val1, const Box &val2);

PHPRT_API Box bitwise_and_box(const Box &val1, const Box &val2);
PHPRT_API StringBox bitwise_and_string(string_ref val1, string_ref val2);
  
PHPRT_API Box bitwise_and_assign_box(Box &val1, const Box &val2);
PHPRT_API StringBox bitwise_and_assign_string(StringBox val1, string_ref val2);

PHPRT_API Box bitwise_or_box(const Box &val1, const Box &val2);
PHPRT_API StringBox bitwise_or_string(string_ref val1, string_ref val2);
  
PHPRT_API Box bitwise_or_assign_box(Box &val1, const Box &val2);
PHPRT_API StringBox bitwise_or_assign_string(StringBox val1, string_ref val2);

PHPRT_API Box bitwise_xor_box(const Box &val1, const Box &val2);
PHPRT_API StringBox bitwise_xor_string(string_ref val1, string_ref val2);
  
PHPRT_API Box bitwise_xor_assign_box(Box &val1, const Box &val2);
PHPRT_API StringBox bitwise_xor_assign_string(StringBox val1, string_ref val2);

PHPRT_API Box shl_box(const Box &val1, const Box &val2);
PHPRT_API intvalue_t shl_integer(intvalue_t val1, intvalue_t val2);
  
PHPRT_API Box shl_assign_box(Box &val1, const Box &val2);
PHPRT_API intvalue_t shl_assign_integer(intvalue_t &val1, intvalue_t val2);

PHPRT_API Box shr_box(const Box &val1, const Box &val2);
PHPRT_API Box shr_assign_box(Box &val1, const Box &val2);

PHPRT_API StringBox concat(const StringBox &s1, const StringBox &s2);
PHPRT_API StringBox concat_const(string_ref s1, string_ref s2);
PHPRT_API StringBox concat_generic(const Box &x1, const Box &x2);
PHPRT_API StringBox &concat_assign(box &var, const Box &rhs);
PHPRT_API StringBox &concat_assign_str(StringBox &var, const Box &rhs);

PHPRT_API double prefix_increment_double(double &v);
PHPRT_API double postfix_increment_double(double &v);
PHPRT_API double prefix_decrement_double(double &v);
PHPRT_API double postfix_decrement_double(double &v);

PHPRT_API Box prefix_increment(box &v);
PHPRT_API Box postfix_increment(box &v);
PHPRT_API Box prefix_decrement(box &v);
PHPRT_API Box postfix_decrement(box &v);

PHPRT_API void increment_string(Box &v);
PHPRT_API void decrement_string(Box &v);

// Mathematic functions.
PHPRT_API NumberBox abs(const Box &number);
PHPRT_API double ceil(double val);
PHPRT_API double floor(double val);
PHPRT_API double round(double val, intvalue_t precision, intvalue_t mode);
PHPRT_API double fmod(double x, double y);
PHPRT_API intvalue_t intdiv(intvalue_t x, intvalue_t y);

PHPRT_API int compare(const Box &val1, const Box &val2);
PHPRT_API bool identical(const Box &val1, const Box &val2);

PHPRT_API Box max_array(IArray *a);
PHPRT_API Box max(const Box &val1, const Box &val2);
PHPRT_API Box max_argpack(const Box &val1, const Box &val2, ArgPack<Box> rest);

PHPRT_API Box min_array(IArray *a);
PHPRT_API Box min(const Box &val1, const Box &val2);
PHPRT_API Box min_argpack(const Box &val1, const Box &val2, ArgPack<Box> rest);

// Collection functions
PHPRT_API Box read_subscript(const Box &arr, const Box &key);

// Debug functions.
PHPRT_API void var_dump(const Box &var);
PHPRT_API void rt_assert(bool value, const char *filename, unsigned line_no);

// Variable support functions
PHPRT_API string_ref gettype(const Box& var);

// Helper functions for settype implementation.
PHPRT_API inline bool convert_to_bool(box &var) {
  if (!var.is_bool())
    var.set_bool(var.to_bool());
  return true;
}

PHPRT_API inline bool convert_to_integer(box& var) {
  if (!var.is_integer())
    var.set_integer(var.to_integer());
  return true;
}

PHPRT_API inline bool convert_to_double(box& var) {
  if (!var.is_double())
    var.set_double(var.to_double());
  return true;
}

PHPRT_API inline bool convert_to_string(box& var) {
  if (!var.is_string())
    var.set_str(var.to_string());
  return true;
}

PHPRT_API inline bool convert_to_array(box& var) {
  assert(0 && "unimplemented");
  return false;
}

PHPRT_API inline bool convert_to_object(box& var) {
  assert(0 && "unimplemented");
  return false;
}

PHPRT_API bool settype(box& var, string_ref type);
PHPRT_API Box get_settype_val(const Box& var, string_ref type);

PHPRT_API intvalue_t intval(const Box& var, intvalue_t base);

PHPRT_API bool empty(const Box& var);
PHPRT_API bool empty_arr(const IArray* Arr);
PHPRT_API bool empty_str(string_ref var);
PHPRT_API bool empty_obj(const IObject *obj);

// Server support.
PHPRT_API void add_header(const Box &hdr, bool replace = true);
PHPRT_API void add_header_with_response_code(const Box &hdr, bool replace,
                                             int http_response_code);
PHPRT_API void remove_header(const Box &hdr);
PHPRT_API void remove_all_headers();
PHPRT_API bool headers_sent();
PHPRT_API bool headers_sent_file(StringBox &file);
PHPRT_API bool headers_sent_file_line(StringBox &file, Boxed<intvalue_t> &line);
PHPRT_API ArrayBox headers_list();
PHPRT_API bool set_header_register_callback(const Box &handler);//TODO: Callable
PHPRT_API Box get_http_response_code();
PHPRT_API Box set_http_response_code(int ret_code);
}


namespace php { namespace test { namespace __Func {

void fna_string_ref(string_ref x);
string_ref fnr_string_ref();

}}}
#endif
