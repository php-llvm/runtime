//===--- server_support.h ---------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Helper classes and functions to implement server functionality.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_SERVER_SUPPORT_H
#define PHPRT_SERVER_SUPPORT_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "shared/string_ref.h"
#include "box.h"
#include "unit.h"
#include "llvm/ADT/SmallVector.h"
#include <string>
#include <vector>
//------------------------------------------------------------------------------


namespace phprt {

class HttpHeaders {
public:
  static const int DefaultHttpResponseCode = 200;

  struct Header {
    Box text;         // Text of the header, f.e. "Content-Length: 123"
    string_ref name;  // Header type, as "Content-Length"
    string_ref value; // Header value, as "123"

    Header() : text(), name{ nullptr, 0 }, value{ nullptr, 0 } {}
    Header(Header &&x) : text(std::move(x.text)), name(x.name), value(x.value) {}

    void operator = (Header &&x) {
      text.move(std::move(x.text));
      name = x.name;
      value = x.value;
    }
  };

  HttpHeaders();
  ~HttpHeaders();

  void add(const Box &header, bool replace = true);
  bool remove(string_ref header);
  void remove_all();
  bool has(string_ref header) const { return get(header) != nullptr; }
  const Header *get(string_ref header) const;

  const llvm::SmallVectorImpl<Header> &get_headers_list() const {
    return headers;
  }

  int get_response_code() const { return response_code; }
  void set_response_code(int code);

  void set_status(const Box &status);
  void clear_status();
  Box get_status() const { return status_string; }

  void prepare();

private:
  llvm::SmallVector<Header, 10> headers;
  int response_code;
  Box status_string;
};


const char *get_http_response_as_text(int code);

}
#endif
