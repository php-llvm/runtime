//===--- globals_array.h -------------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definition of special array to hold global variables.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "array.h"
#include "globals.h"
//------------------------------------------------------------------------------


namespace phprt {

// TODO: expand this definition
// This class is intended to represent all global variables
class GlobalsArray : public IArray {
private:
  void      *base;  // base pointer for all offsets. Usually it is &php::var::GLOBALS
  VarRecord *first; // first element of globals array
  VarRecord *last;  // element behind the last

  friend class GlobalsArrayIterator;

public:

  GlobalsArray(VarRecord *s, VarRecord *e, void *b)
    : base(b), first(s), last(e) {}

  unsigned get_flags() const override { return IArray::Fixed; }
  unsigned remove_ref() override;
  unsigned add_ref() override;
  unsigned get_size() const override;
  value_kind get_kind(const Box &key) const override;
  Box get(const Box &key) const override;
  bool set(const Box &key, const Box &value) override;
  Box remove(const Box &key) override;
  IIterator *begin() const override;
  IIterator *end() const override;
  IEditIterator *begin_edit() override;
  IEditIterator *end_edit() override;
  IEditIterator *edit(const Box &key) override;
};


// TODO: implement support for dynamically created global variables
class GlobalsArrayIterator : public IEditIterator {
private:
  GlobalsArray *base;
  VarRecord *cursor;

  // GLOBALS may contain variables that had been unset or 
  // not yet initialized. However logically those variable should 
  // not be visible in $GLOBALS array.

  // The following two methods are needed to deal with this situation.

  // Move iterator one step backwards or forward, skipping undefined variables.
  void move_forward();
  void move_backward();

  // Checks if current Cursor points at defined variable
  bool is_set() const;

public:
  GlobalsArrayIterator(GlobalsArray* b);
  GlobalsArrayIterator(GlobalsArray* Base, VarRecord *C);
  ~GlobalsArrayIterator() override {};

  void reset(bool at_start = true);
  bool equals(const IIterator & x) const override;
//  virtual IArray *get_base()  override { return base; }
  bool is_valid() const override;
  KeyBox key() const override;
  Box value() const override;
  bool next() override;
  bool prev() override;
//  virtual bool shift(int offset) override;
  bool set_value(const Box &new_value) override;
};

}
