//===--- array.h ----------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Declaration of classes that represent PHP arrays.
//
//===----------------------------------------------------------------------===//

#ifndef PHPRT_ARRAY_H
#define PHPRT_ARRAY_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "box.h"
#include "fundamental.h"
//------------------------------------------------------------------------------


namespace phprt {
class ILinearAccessor;
class IQueueAccessor;


/// \brief Interface to array object.
///
/// This type is in global namespace because it is used in 'box', which exposes
/// variant values to C code. However definition of this class cannot be seen by
/// C code.
///
class IArray : public ICollection {
public:

  // Flags in the field 'flags'.
  enum {
    Constant = 0x01,   // Instance of ConstantArray.
    Fixed    = 0x02,   // Structure cannot be changed, but item values can.
    Vector   = 0x04,   // Only integers are used as keys in the array and they
                       // all form continuous sequence.
    FirstAvailableFlag = 0x08
  };

  virtual unsigned get_flags() const = 0;
  bool is_constant() const { return get_flags() & Constant; }
  bool is_fixed() const { return get_flags() & Fixed; }
  bool is_vector() const { return get_flags() & Vector; }

  // Cast to particular interface.
  virtual ILinearAccessor *linear() { return nullptr; }
  virtual IQueueAccessor *queue() { return nullptr; }
};


/// \brief Exposes array as linearly ordered set.
///
/// Linear order in PHP array means that there are next and previous items for
/// the given array element and there are first and last elements in array that
/// do not previous and next elements respectively.
///
class ILinearAccessor {
public:
  typedef unsigned slot_t;

  virtual KeyBox get_key(slot_t s) const = 0;
  virtual Box    get_value(slot_t s) const = 0;
  virtual bool   set_value(slot_t s, const Box &v) const = 0;

  virtual bool is_valid(slot_t s) const = 0;
  virtual bool begin(slot_t &s) const = 0;
  virtual bool end(slot_t &s) const = 0;
  virtual bool next(slot_t &s) const = 0;
  virtual bool prev(slot_t &s) const = 0;
  virtual bool shift(slot_t &s, int d) const = 0;

  KeyBox begin_index() const;
  KeyBox end_index() const;
};


/// \brief Provides queue interface for an array.
///
class IQueueAccessor {
public:
  virtual Box &back() const = 0;
  virtual Box &front() const = 0;
  virtual void push_back(const Box &c) = 0;
  virtual void push_front(const Box &c) = 0;
  virtual Box pop_back() = 0;
  virtual Box pop_front() = 0;
};


class ArrayBase {
public:

  // Type of integer value used as a key into array.
  typedef intvalue_t index_t;

  // Pair of key and value.
  struct association {
    box value;
    box key;
  };

  constexpr ArrayBase(unsigned f) : items(0), flags(f) {}
  constexpr ArrayBase(unsigned f, unsigned items) : items(items), flags(f) {}

  // Querying properties.
  unsigned size() const { return items; }
  bool empty() const { return items == 0; }

protected:
  unsigned items;   // Total number of items in the array.
  unsigned flags;   // Flags representing array properties.
};


class ConstArray : public ArrayBase, public IArray, public ILinearAccessor {
public:

  union Content {
    constexpr Content() : as_vector(nullptr) {}
    constexpr Content(const box *v) : as_vector(v) {}
    constexpr Content(const association *v) : as_map(v) {}

    const box &get_vector(unsigned ndx) const { return as_vector[ndx]; }
    const association &get_map(unsigned ndx) const { return as_map[ndx]; }
    box &access_vector(unsigned ndx) const {
      return const_cast<box &>(as_vector[ndx]);
    }
    box &access_map(unsigned ndx) const {
      return const_cast<box &>(as_map[ndx].value);
    }

    const box *as_vector;
    const association *as_map;
  };

  constexpr ConstArray()
    : ArrayBase(Constant | Vector), first_index(0), locators(nullptr),
      content() {
  }
  constexpr ConstArray(const box *values, unsigned num, long first_index = 0)
    : ArrayBase(Constant | Vector, num), first_index(first_index),
      locators(nullptr), content(values) {
  }
  constexpr ConstArray(const association *values, unsigned num,
                       const unsigned *locs)
    : ArrayBase(Constant, num), first_index(0), locators(locs), 
      content(values) {
  }

  // Interface of IArray.

  unsigned get_flags() const override { return flags; }
  unsigned remove_ref() override;
  unsigned add_ref() override;
  unsigned get_size() const override;

  value_kind get_kind(const Box &key) const override;
  Box get(const Box &key) const override;
  bool set(const Box &key, const Box &value) override;
  Box remove(const Box &key) override;

  ILinearAccessor *linear() override;

  IIterator *begin() const override;
  IIterator *end() const override;
  IEditIterator *begin_edit() override;
  IEditIterator *end_edit() override;
  IEditIterator *edit(const Box &key) override;

  // Interface of ILinearAccessor.
  KeyBox get_key(slot_t s) const override;
  Box    get_value(slot_t s) const override;
  bool   set_value(slot_t s, const Box &v) const override;

  bool is_valid(slot_t s) const override;
  bool begin(slot_t &s) const override;
  bool end(slot_t &s) const override;
  bool next(slot_t &s) const override;
  bool prev(slot_t &s) const override;
  bool shift(slot_t &s, int d) const override;

#ifdef _DEBUG
  void dump(unsigned indent) const;
  void dump() const { dump(0); }
#endif

  // Implementation.
  bool is_valid_slot(slot_t x) const { return x < items; }
  unsigned find(const KeyBox &key) const;

  // Fields.
  intvalue_t first_index;     // If array is a vector, this is the index of the
                              // first array element.
  const unsigned *locators;   // If array is not a vector, this is a pointer to
                              // array of element indices ordered by keys.
  // Pointer to the array content.
  Content content;
};


// Constant empty array.
extern const ConstArray empty_array;

}

#endif
