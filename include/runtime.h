//===--- runtime.h ----------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Declarations of runtime functions exposed to compiler.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_RUNTIME_H
#define PHPRT_RUNTIME_H


#ifndef RUNTIME_SYM_FILE
# error "File runtime.h may be used only in preparation of runtime symbol file"
#endif


//------ Dependencies ----------------------------------------------------------

// We don't want to import all the stuff defined in system headers for symbol
// file, because it can introduce dependency on build machine but target must
// be selected in compiler invocation.  So remove some symbols defined in system
// headers and used in runtime symbol file.
#define assert(x)

#include "config-defs.h"
#include "shared/value_kinds.h"
#include "shared/string_ref.h"
#include "box.h"
#include "php_constants.h"
#include "php_variables.h"
#include "functions.h"
#include "globals.h"
#include "unit.h"
#include "argpack.h"
#include "array.h"
#include "value.h"

//------------------------------------------------------------------------------

#endif
