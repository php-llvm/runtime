//===--- value.h ------------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Defines class used to represent PHP values accessible by names.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_VALUE_H
#define PHPRT_VALUE_H

//------ Dependencies ----------------------------------------------------------
#include "shared/constants.h"
#include "shared/string_ref.h"
#include "box.h"
//------------------------------------------------------------------------------

namespace phprt {

struct ValueInfo;

/// \brief Type of function that returns value described by the given value
/// object.
///
/// The main purpose of this function is to initialize ValueInfo. For
/// initialized values call to this function must return address of current
/// value.
///
/// Such function may be thought as a virtual method of class ValueInfo, but we
/// choose explicit function pointers to avoid problems with construction of
/// values in constant memory.
typedef const box *ValueAccessor(ValueInfo *v);


/// \brief PHP value accessible in runtime.
///
/// These values include:
/// - constants, both known in compile time and defined in runtime,
/// - class constants,
/// - object properties,
/// - class properties.
/// All of them have attributes:
/// - they can be accessed by name at runtime. This is a key feature for PHP
///   dynamic properties.
/// - they are initialized before use. Initialization is different for different
///   value kinds.
///
struct ValueInfo {
  string_ref name;        ///< Value name.
  unsigned   flags : 16;  ///< Initialized, constant etc.
  unsigned   index : 16;  ///< Offset of the property or module number.
  box        value;       ///< Current value of reference to value.

  /// \brief Returns value described by the given value object.
  ///
  ValueAccessor *access;

  bool is_initialized() const { return (flags & vInitialized) != 0; }
  bool is_constant() const { return (flags & vConstant) != 0; }
  bool is_case_insensitive() const { return (flags & vNoCase) != 0; }
  bool is_public() const { return (flags & member_access) == member_public; }
  bool is_protected() const { return (flags & member_access) == member_protected; }
  bool is_private() const { return (flags & member_access) == member_private; }

  const box &read() const {
    if (access)
      return *(access)(const_cast<ValueInfo *>(this));
    return value;
  }

  const Box &get() const { return static_cast<const Box&>(value); }

  void set_initialized() { flags |= vInitialized; }
};

}
#endif
