//===--- sections.h ---------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Specifies section names for various kind of objects created by compiler and
//  used by runtime.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_SHARED_SECTIONS_H
#define PHPRT_SHARED_SECTIONS_H

//------ Dependencies ----------------------------------------------------------
#include "shared/section_names.h"
//------------------------------------------------------------------------------

// Unit descriptors
//
// This section contains records of type Unit, which contain module information
// available at compile time. This is read-only section.
#ifdef _WIN32
#  define UNIT_SECTION_NAME0    WIN_UNIT_SECTION_PROLOG
#  define UNIT_SECTION_NAME     WIN_UNIT_SECTION_NAME
#  define UNIT_SECTION_NAME9    WIN_UNIT_SECTION_EPILOG
#  define BEGIN_UNIT_SECTION __pragma (const_seg(push, UNIT_SECTION_NAME))
#  define UNIT_SECTION __declspec(allocate(UNIT_SECTION_NAME))
#  define END_UNIT_SECTION __pragma (const_seg(pop))
#elif __APPLE__
#  define UNIT_SECTION_NAME     OSX_UNIT_SECTION_NAME
#  define BEGIN_UNIT_SECTION
#  define UNIT_SECTION __attribute__((section(UNIT_SECTION_NAME)))
#  define END_UNIT_SECTION
#else
#  define UNIT_SECTION_NAME     LINUX_UNIT_SECTION_NAME
#  define BEGIN_UNIT_SECTION
#  define UNIT_SECTION __attribute__((section(UNIT_SECTION_NAME)))
#  define END_UNIT_SECTION
#endif


// Unit states
//
// This section contains records of type UnitState, which contain information
// about current module state. This is read-write section.
#ifdef _WIN32
#  define UNITSTATE_SECTION_NAME0     WIN_UNITSTATE_SECTION_PROLOG
#  define UNITSTATE_SECTION_NAME      WIN_UNITSTATE_SECTION_NAME
#  define UNITSTATE_SECTION_NAME9     WIN_UNITSTATE_SECTION_EPILOG
#  define BEGIN_UNITSTATE_SECTION __pragma (data_seg(push, UNITSTATE_SECTION_NAME))
#  define UNITSTATE_SECTION __declspec(allocate(UNITSTATE_SECTION_NAME))
#  define END_UNITSTATE_SECTION __pragma (data_seg(pop))
#elif __APPLE__
#  define UNITSTATE_SECTION_NAME      OSX_UNITSTATE_SECTION_NAME
#  define BEGIN_UNITSTATE_SECTION
#  define UNITSTATE_SECTION __attribute__((section(UNITSTATE_SECTION_NAME)))
#  define END_UNITSTATE_SECTION
#else
#  define UNITSTATE_SECTION_NAME      LINUX_UNITSTATE_SECTION_NAME
#  define BEGIN_UNITSTATE_SECTION
#  define UNITSTATE_SECTION __attribute__((section(UNITSTATE_SECTION_NAME)))
#  define END_UNITSTATE_SECTION
#endif


// Global variable records
//
// This section contains records of type VarRecord, which form content of PHP
// array $GLOBALS. This is read-write section.
#ifdef _WIN32
#  define GVR_SECTION_NAME0     WIN_GVR_SECTION_PROLOG
#  define GVR_SECTION_NAME      WIN_GVR_SECTION_NAME
#  define GVR_SECTION_NAME9     WIN_GVR_SECTION_EPILOG
#  define BEGIN_GVR_SECTION __pragma (data_seg(push, GVR_SECTION_NAME))
#  define GVR_SECTION __declspec(allocate(GVR_SECTION_NAME))
#  define END_GVR_SECTION __pragma (data_seg(pop))
#elif __APPLE__
#  define GVR_SECTION_NAME      OSX_GVR_SECTION_NAME
#  define BEGIN_GVR_SECTION
#  define GVR_SECTION __attribute__((section(GVR_SECTION_NAME)))
#  define END_GVR_SECTION
#else
#  define GVR_SECTION_NAME      LINUX_GVR_SECTION_NAME
#  define BEGIN_GVR_SECTION
#  define GVR_SECTION __attribute__((section(GVR_SECTION_NAME)))
#  define END_GVR_SECTION
#endif


// Compile time constant descriptors
//
// This section contains records of type ValueInfo, which contain state and
// value of constants known at compile time. This is read-only section.
#ifdef _WIN32
#  define CTC_SECTION_NAME0     WIN_CTC_SECTION_PROLOG
#  define CTC_SECTION_NAME      WIN_CTC_SECTION_NAME
#  define CTC_SECTION_NAME9     WIN_CTC_SECTION_EPILOG
#  define BEGIN_CTC_SECTION __pragma (const_seg(push, CTC_SECTION_NAME))
#  define CTC_SECTION __declspec(allocate(CTC_SECTION_NAME))
#  define END_CTC_SECTION __pragma (const_seg(pop))
#elif __APPLE__
#  define CTC_SECTION_NAME      OSX_CTC_SECTION_NAME
#  define BEGIN_CTC_SECTION
#  define CTC_SECTION __attribute__((section(CTC_SECTION_NAME)))
#  define END_CTC_SECTION
#else
#  define CTC_SECTION_NAME      LINUX_CTC_SECTION_NAME
#  define BEGIN_CTC_SECTION
#  define CTC_SECTION __attribute__((section(CTC_SECTION_NAME)))
#  define END_CTC_SECTION
#endif


// Runtime time constant descriptors
//
// This section contains records of type ValueInfo, which contain state and
// value of constants that must be initialized at runtime. This is read-write
// section.
#ifdef _WIN32
#  define RTC_SECTION_NAME0     WIN_RTC_SECTION_PROLOG
#  define RTC_SECTION_NAME      WIN_RTC_SECTION_NAME
#  define RTC_SECTION_NAME9     WIN_RTC_SECTION_EPILOG
#  define BEGIN_RTC_SECTION __pragma (data_seg(push, RTC_SECTION_NAME))
#  define RTC_SECTION __declspec(allocate(RTC_SECTION_NAME))
#  define END_RTC_SECTION __pragma (data_seg(pop))
#elif __APPLE__
#  define RTC_SECTION_NAME      OSX_RTC_SECTION_NAME
#  define BEGIN_RTC_SECTION
#  define RTC_SECTION __attribute__((section(RTC_SECTION_NAME)))
#  define END_RTC_SECTION
#else
#  define RTC_SECTION_NAME      LINUX_RTC_SECTION_NAME
#  define BEGIN_RTC_SECTION
#  define RTC_SECTION __attribute__((section(RTC_SECTION_NAME)))
#  define END_RTC_SECTION
#endif

#endif
