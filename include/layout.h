//===--- layout.h -----------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Declaration of symbols that mark different sections in PHP application.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_LAYOUT_H
#define PHPRT_LAYOUT_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "sections.h"
//------------------------------------------------------------------------------


//------ Tools for declaring entities in particular sections -------------------

BEGIN_UNIT_SECTION
END_UNIT_SECTION
BEGIN_UNITSTATE_SECTION
END_UNITSTATE_SECTION
BEGIN_GVR_SECTION
END_GVR_SECTION
BEGIN_CTC_SECTION
END_CTC_SECTION
BEGIN_RTC_SECTION
END_RTC_SECTION


PHPRT_API struct string_ref;
PHPRT_API class Box;
PHPRT_API struct box;

namespace phprt {

PHPRT_API struct Unit;
PHPRT_API struct UnitState;
PHPRT_API struct VarRecord;
PHPRT_API struct ValueInfo;

const Unit *getUnitSectionStart();
const Unit *getUnitSectionEnd();

UnitState *getUnitStateSectionStart();
UnitState *getUnitStateSectionEnd();

VarRecord *getGVRSectionStart();
VarRecord *getGVRSectionEnd();

const ValueInfo *getCTConstantSectionStart();
const ValueInfo *getCTConstantSectionEnd();

ValueInfo *getRTConstantSectionStart();
ValueInfo *getRTConstantSectionEnd();

}

#endif
