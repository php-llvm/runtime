//===--- object.h -----------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Classes implementing PHP object model.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_OBJECT_H
#define PHPRT_OBJECT_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "fundamental.h"
//------------------------------------------------------------------------------

namespace phprt {

class Class;

class IObject : public ICollection {
public:
  virtual ~IObject();

  virtual const Class &get_class() const = 0;

  virtual Box get_property(string_ref prop_name) = 0;
  virtual bool set_property(string_ref prop_name, const Box &value) = 0;
  virtual value_kind get_property_kind(string_ref prop_name) = 0;
  virtual Box remove_property(string_ref prop_name) = 0;

  virtual int call(box &result, string_ref meth_name, ArgPack<Box> args) = 0;

  virtual Box get_value();
  virtual void set_value(const Box &v);
  virtual Box cast(value_type);
  virtual intvalue_t as_int();
  virtual double as_float();

  // Support for operator override.

  enum cmp_res {
    Less      = -1,
    Equals    = 0,
    Greater   = 1,
    Unordered = 2
  };

  virtual cmp_res compare(const Box &x, bool rhs = false);
  virtual bool strict_equals(const Box &x, bool rhs = false);

  // Do not allow overloading logical binary operations 'or' and 'and'. They
  // have special semantics related to sequence points, which is difficult to
  // emulate in the presence of operation overloading. Instead these operations
  // may be implemented with usual operations || and && using conversion to
  // bool.

  virtual bool as_bool();
  virtual Box log_not();
  virtual Box xor(const Box &x, bool rhs = false);

  virtual Box add(const Box &x, bool rhs = false);
  virtual Box sub(const Box &x, bool rhs = false);
  virtual Box mul(const Box &x, bool rhs = false);
  virtual Box div(const Box &x, bool rhs = false);
  virtual Box mod(const Box &x, bool rhs = false);
  virtual Box pow(const Box &x, bool rhs = false);
  virtual Box plus();
  virtual Box minus();
  virtual IObject *add_assign(const Box &x);
  virtual IObject *sub_assign(const Box &x);
  virtual IObject *mul_assign(const Box &x);
  virtual IObject *div_assign(const Box &x);
  virtual IObject *mod_assign(const Box &x);
  virtual IObject *pow_assign(const Box &x);

  virtual Box concat(const Box &x, bool rhs = false);
  virtual IObject *concat_assign(const Box &x);

  virtual Box bin_and(const Box &x, bool rhs = false);
  virtual Box bin_or(const Box &x, bool rhs = false);
  virtual Box bin_xor(const Box &x, bool rhs = false);
  virtual Box bin_shl(const Box &x, bool rhs = false);
  virtual Box bin_shr(const Box &x, bool rhs = false);
  virtual Box bin_not();
  virtual IObject *bin_and_assign(const Box &x);
  virtual IObject *bin_or_assign(const Box &x);
  virtual IObject *bin_xor_assign(const Box &x);
  virtual IObject *bin_shl_assign(const Box &x);
  virtual IObject *bin_shr_assign(const Box &x);
};


class Class {
public:
  enum {
    ClassKindMask = 0x03,
    Concrete      = 0x00,
    Interface     = 0x01,
    Trait         = 0x02,
    Abstract      = 0x03,

    Final         = 0x04
  };

  // Generic class properties.
  string_ref name;
  unsigned flags;
  string_ref file;
  unsigned start_line;
  unsigned end_line;

  // Inheritance.
  //TODO:
  // Class properties (writable and constants).
  //TODO:
  // Object properties layout.
  //TODO:
  // Class method table.
  //TODO:
  // Object method table.
  // TODO:

  bool is_concrete() const { return (flags & ClassKindMask) == Concrete; }
  bool is_interface() const { return (flags & ClassKindMask) == Interface; }
  bool is_trait() const { return (flags & ClassKindMask) == Trait; }
  bool is_abstract() const { return (flags & ClassKindMask) == Abstract; }
  bool is_final() const { return flags & Final; }

  virtual IObject *create(ArgPack<Box*>);
};


bool is_of_same_class(const void *objectA, const void *objectB);

}
#endif
