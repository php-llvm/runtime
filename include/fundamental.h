//===--- fundamental.h ------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Classes used in the base of runtime object model.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_FUNDAMENTAL_H
#define PHPRT_FUNDAMENTAL_H

//------ Dependencies ----------------------------------------------------------
#include "config-defs.h"
#include "shared/value_kinds.h"
#include "shared/string_ref.h"
#include "box.h"
#include "argpack.h"
//------------------------------------------------------------------------------

namespace phprt {

// Forward declarations.
class uuid;


/// \brief Interface of objects that support reference counting and querying
/// interfaces in runtime.
///
/// It is a counterpart of basic COM interface 'IUnknown' but has method
/// declarations more natural for C++ and names more close to naming conventions
/// of the runtime layer. It also uses 'unsigned int' for reference counter
/// rather than 'unsigned long'.
///
class IUnknown {
public:
  static const int Success = 0;
  static const int Failure = -1;

  virtual int query_interface(const uuid &id, IUnknown **iface);
  virtual unsigned add_ref() = 0;
  virtual unsigned remove_ref() = 0;
};


/// \brief Interface of objects that point to an element of a collection.
///
/// This is bidirectional iterator, that logically points to a pair key + value.
///
class IIterator {
public:
  virtual ~IIterator();

  virtual bool is_valid() const = 0;
  virtual KeyBox key() const = 0;
  virtual Box value() const = 0;

  virtual bool prev() = 0;
  virtual bool next() = 0;
  virtual bool equals(const IIterator &x) const = 0;

  Box operator * () const { return value(); }
  IIterator  &operator ++() { next(); return *this; }
  IIterator  &operator --() { prev(); return *this; }
  bool operator == (const IIterator& it) const { return equals(it); }
  bool operator != (const IIterator& it) const { return !operator == (it); }
};


/// \brief Iterator that allows changing pointed value.
///
class IEditIterator : public IIterator {
public:
  virtual bool set_value(const Box &new_value) = 0;
};


/// \brief Interface of objects that store values and allow accessing them by a
/// key.
///
class ICollection : public IUnknown {
public:

  typedef IIterator iterator;
  typedef Box value_type;
  typedef Box &reference;
  typedef const Box &const_reference;
  typedef Box *pointer;
  typedef const Box *const_pointer;

  /// \brief Returns number of elements in the collection.
  virtual unsigned get_size() const = 0;

  /// \brief Given a key returns kind of the value stored in the collection
  /// under this key. If no such element exists, returns vkUndef.
  ///
  virtual value_kind get_kind(const Box &key) const = 0;

  /// \brief Given a key returns the value stored in the collection under this
  /// key. If no such element exists, returns undefined value.
  ///
  virtual Box get(const Box &key) const = 0;

  /// \brief Adds or updates the value stored in the collection under the given
  /// key.
  /// \returns true, if operation is finished successfully.
  ///
  /// Some collections do not allow modifications, \c set always return false
  /// in them. Depending on options, they may also throw an exception.
  ///
  virtual bool set(const Box &key, const Box &value) = 0;

  /// \brief Removes the value stored in the collection under the given key.
  /// \returns The removed value, if removal did not occur (no such key, removal
  /// is not supported), returns undefined value.
  ///
  /// Some collections do not allow modifications, \c remove always return false
  /// in them. Depending on options, they may also throw an exception.
  ///
  virtual Box remove(const Box &key) = 0;

  /// \brief Creates iterator that points to the first element of the
  /// collection.
  ///
  /// The function may return null pointer, it means that the collection is not
  /// iteratable.
  ///
  virtual IIterator *begin() const = 0;

  /// \brief Returns iterator that may be used as end-of-collection marker.
  ///
  /// It is an error to call this method if call to \c begin returned null
  /// pointer.
  ///
  virtual IIterator *end() const = 0;

  // Same as \c begin but returns editing iterator.
  virtual const IEditIterator *begin_edit() = 0;
  virtual const IEditIterator *end_edit() = 0;

  /// \brief Returns editing iterator that points to the element designated by
  /// the specified key.
  ///
  /// The resulting iterator may lack support of move operations. The method
  /// also may return nullptr, if editing is not allowed.
  ///
  virtual IEditIterator *edit(const Box &key) = 0;

  bool not_empty() const { return get_size() != 0; }
  bool has_key(const Box &key) const { return get_kind(key) != vkUndef; }
};

}
#endif
