//===--- Path.cpp ---------------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Service for working with files and directories.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "llvm/ADT/SmallString.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Path.h"
#include "phpfe/Support/Path.h"
//------------------------------------------------------------------------------


namespace phpfe {

using namespace llvm;

/// \brief Normalizes path removing '.' and '..' if possible.
///
std::string normalizePath(StringRef FilePath) {
  SmallString<256> NormalizedString;
  llvm::SmallVector<llvm::sys::path::const_iterator, 32> Components;
  for (auto PathComponent = llvm::sys::path::begin(FilePath),
            E = llvm::sys::path::end(FilePath);
       PathComponent != E; ++PathComponent) {
    // . in files path has no effect
    if (*PathComponent == ".")
      continue;
    if (*PathComponent == "..") {
      // There are two possible scenarios here. 
      // A/B/.. <- in this case we simply remove previous component i.e. tun path into A/
      // ../.. or ../ <- in this case we can't really resolve .., so we pass it as is.
      if (!Components.empty() && *Components.back() != "..") {
        Components.pop_back();
        continue;
      }
    }
    Components.push_back(PathComponent);
  }

  for (auto& Component : Components)
    llvm::sys::path::append(NormalizedString, *Component);

#ifdef WIN32
  // Replace backslashes with slashes
  // TODO: use target options instead of define
  std::replace(NormalizedString.begin(), NormalizedString.end(), '\\', '/');
#endif

  return std::string(NormalizedString.begin(), NormalizedString.end());
}


std::string makeRelativePath(StringRef Path, StringRef Base) {
  SmallString<256> AbsPath(Path);
  sys::fs::make_absolute(AbsPath);
  std::string NormPath = normalizePath(AbsPath);
  std::string NormBase;
  if (!Base.empty()) {
    SmallString<256> AbsBase(Base);
    sys::fs::make_absolute(AbsBase);
    NormBase = normalizePath(AbsBase);
  } else {
    return NormPath;
  }

  if (!StringRef(NormPath).startswith(NormBase))
    return std::string();
  NormPath.erase(0, NormBase.size());
  if (NormPath.empty())
    // It is possible if Base == Path, but this is not normal case as Base is a
    // directory and Path must be a file path.
    return std::string();
  if (NormPath[0] == '/')
    NormPath.erase(0, 1);

  return NormPath;
}


std::string makeNameFromPath(StringRef Path) {
  std::string Name;
  for (char Ch : Path) {
    if (Ch == '/')
      Name.push_back('_');
    else if (Ch == '_')
      Name.append("__");
    else if (isalnum(Ch))
      Name.push_back(Ch);
    else {
      const char *hexdigits = "0123456789ABCDEF";
      Name.append("_x");
      Name.push_back(hexdigits[(Ch >> 4) & 0x0F]);
      Name.push_back(hexdigits[Ch & 0x0F]);
    }
  }

  return Name;
}

}
