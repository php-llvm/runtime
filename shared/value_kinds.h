//===--- value_types.h ------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Constants that are used to specify universal value kinds.
//
//  This file must compile in C mode, as it may be included by C implementations
//  (Zend engine, PHP extensions etc).
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_VALUE_KINDS_H
#define PHPRT_VALUE_KINDS_H

//------ Dependencies ----------------------------------------------------------
#include "shared_defs.h"
#ifndef RUNTIME_SYM_FILE
#include <assert.h>
#endif
//------------------------------------------------------------------------------


/// \brief Constants used to encode value kind into a byte.
///
/// Kind of universal value is packed into a 6-bit entity. The bits organized
/// into the pattern:
///
/// \verbatim
///
///       5   4   3   2   1   0
///     +---+---+---+---+---+---+
///     | Class | Value type    |
///     +---+---+---+---+---+---+
///
/// \endverbatim
///
/// where \c Class denotes a group of value types:
///
///     Class bits | Meaning
///     :----------|:-------------------------
///     00         | Values copied trivially
///     01         | Reference counting values
///     10         | Special values
///     11         | Immediate values
///
/// Meaning of bits of <tt> Value Type </tt> depends on used \c Class.
///
enum value_kind {

  // Masks that selects particular bit groups.
  vkClassMask       = 0x30,
  vkTypeMask        = 0x0F,

  // Class of value type.
  //
  vkClassValue      = 0,
  vkRefCounting     = 0x10,
  vkClassSpecial    = 0x20,
  vkClassImmediate  = 0x30,

  // Values
  //
  // Value types can be copied trivially, bit by bit.
  //
  vkUndef           = 0,
  vkNull            = 1,
  vkBool            = 2,
  vkInteger         = 3,
  vkDouble          = 4,
  vkConstString     = 5,
  vkConstArray      = 6,
  vkPointer         = 7,
  vkIndirect        = 8,

  // Types that require reference counting.
  //
  vkDynamicString   = vkRefCounting,
  vkDynamicArray    = vkRefCounting | 1,
  vkObject          = vkRefCounting | 2,
  vkReference       = vkRefCounting | 3,

  // Immediate strings.
  //
  // String kinds pack type and length into the value type bits:
  //    0-10  - immediate strings of length 1-11,
  //    11-15 - reserved.
  //
  vkImmediateStr_1  = vkClassImmediate | 0,
  vkImmediateStr_2  = vkClassImmediate | 1,
  vkImmediateStr_3  = vkClassImmediate | 2,
  vkImmediateStr_4  = vkClassImmediate | 3,
  vkImmediateStr_5  = vkClassImmediate | 4,
  vkImmediateStr_6  = vkClassImmediate | 5,
  vkImmediateStr_7  = vkClassImmediate | 6,
  vkImmediateStr_8  = vkClassImmediate | 7,
  vkImmediateStr_9  = vkClassImmediate | 8,
  vkImmediateStr_10 = vkClassImmediate | 9,
  vkImmediateStr_11 = vkClassImmediate | 10,
                    // Reserved code: 11
                    // Reserved code: 12
                    // Reserved code: 13
                    // Reserved code: 14
                    // Reserved code: 15
  vkStrLenMask      = 0x0F,

  // Internal types
  //
  vkBlock           = vkClassSpecial,
};


/// \brief Enumerates available types that can be stored in universal value.
///
/// In contrast to value kinds, these constants represent user viewpoint, not
/// implementation one. For instance, all string kinds are mapped to one
/// string type.
///
enum value_type {
  tyNull      = vkNull,
  tyBool      = vkBool,
  tyInteger   = vkInteger,
  tyDouble    = vkDouble,
  tyString    = vkConstString,
  tyArray     = vkConstArray,
  tyObject    = vkObject,

  tyInternal  = vkClassSpecial | vkTypeMask
};


// Some parameters of value packing in universal type.
//
// Need to be known here because it affect packing of immediate values into
// value_kind.

// If a value is short enough, its content may be stored immediately in the
// value object. This constant defines maximal length of a value that can be
// stored in such a way.
const unsigned MaxImmediateSize = 11;

// Values stored immediately in the value object is spread by two fields.
// These constants define size of each pieces.
const unsigned ImmediatePart1Length = 8;
const unsigned ImmediatePart3Length = 3;

// A value may be moved by simple memory copy. However a value object may
// contain data that does not relate to the value, for instance a container
// may keep some information there. This constant defines size of value object
// part that represents stored value.
const unsigned ValuePartSize = 12;

// Value object reserves some bits for representing constant string length but
// it must fit 3 bytes. This constant specifies maximal allowed length of
// constant strings.
const unsigned MaxConstLen = (1 << 24) - 1;


#ifdef __cplusplus

// Constants and functions for working with value kinds.
//
namespace phprt {

//------ General value type queries

value_type get_type(value_kind kind);

// Returns zero terminated string that represents the value type.
PHPRT_API const char *get_type_name(value_type type);

// Checks if the specified value kind allows trivial copying.
inline bool is_value(value_kind x) { return (x & vkClassMask) == 0; }

// Checks if the specified value kind supports reference counting.
inline bool is_refcounted(value_kind x) {
  return (x & vkClassMask) == vkRefCounting;
}


//------ Queries for particular type.

inline bool is_null(value_kind x) { return x == vkNull || x == vkUndef; }
inline bool is_bool(value_kind x) { return x == vkBool; }
inline bool is_integer(value_kind x) { return x == vkInteger; }
inline bool is_double(value_kind x) { return x == vkDouble; }
inline bool is_number(value_kind x) {
  return x == vkInteger || x == vkDouble || x == vkBool;
}
inline bool is_string(value_kind x);
inline bool is_array(value_kind x) {
  return x == vkDynamicArray || x == vkConstArray;
}
inline bool is_object(value_kind x) { return x == vkObject; }


//------ Queries for particular kind.

inline bool is_undef(value_kind x) { return x == vkUndef; }
inline bool is_pointer(value_kind x) { return x == vkPointer; }
inline bool is_indirect(value_kind x) { return x == vkIndirect; }
inline bool is_string_constant(value_kind x) { return x == vkConstString; }
inline bool is_string_dynamic(value_kind x) { return x == vkDynamicString; }
inline bool is_string_immediate(value_kind x) {
  return ((x & vkClassMask) == vkClassImmediate) &&
         ((x & vkStrLenMask) <= vkImmediateStr_11);
}
inline bool is_reference(value_kind x) { return x == vkReference; }
inline bool is_block(value_kind x) { return x == vkBlock; }


//------ Queries for particular kind properties

inline unsigned get_immediate_len(value_kind x) {
  assert(is_string_immediate(x));
  return 1 + (x & vkStrLenMask);
}
inline value_kind get_immediate_kind(unsigned len) {
  assert(static_cast<unsigned>(len) > 0 &&
         static_cast<unsigned>(len) <= MaxImmediateSize);
  return static_cast<value_kind>(vkClassImmediate | (len - 1));
}


//------ Service functions

inline bool is_string(value_kind x) {
  return is_string_constant(x) || is_string_dynamic(x) || is_string_immediate(x);
}

}
#endif

#endif
