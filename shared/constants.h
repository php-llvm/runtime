//===--- constants.h --------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Definition of constants that are used in runtime and must be known to the
//  compiler.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_CONSTANTS_H
#define PHPRT_CONSTANTS_H

//------ Dependencies ----------------------------------------------------------
//------------------------------------------------------------------------------


// Unit attribute flags
//
enum UnitFlag {
  Script    = 0x01,   // Command line script
  Page      = 0x02,   // Page handler called by web server
  Module    = 0x03,   // Dynamically loadable unit
  Static    = 0x04,   // Preloaded unit (emodule)
  Config    = 0x05,   // Config file
  Extension = 0x06,   // Extension module
  KindMask  = 0x07
};


// Rounding policy
//
enum class RoundingMode {
  HalfUp   = 0x01, // away from zero
  HalfDown = 0x02, // toward zero
  HalfEven = 0x03, // toward even
  HalfOdd  = 0x04  // toward odd
};


/// \brief Flags used to describe values.
///
enum ValueFlags {
  vInitialized     = 0x0001,   //> Value is initialized.
  vConstant        = 0x0002,   //> Unmodifiable value.
  vNoCase          = 0x0004,   //> Value name is case insensitive.
  vInitializing    = 0x0008,   //> Initialization started but not finished.

  // Flags pertinent to class members.
  member_public    = 0,
  member_protected = 0x0010,
  member_private   = 0x0020,
  member_access    = 0x0030,
};

#endif
