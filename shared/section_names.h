//===--- section_names.h ----------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2016, Serge Pavlov.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Specifies section names for various kind of objects created by compiler and
//  used by runtime.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_SHARED_SECTION_NAMES_H
#define PHPRT_SHARED_SECTION_NAMES_H

// Unit descriptor

#define LINUX_UNIT_SECTION_NAME     "__phpcu"
#define WIN_UNIT_SECTION_PROLOG     "phpc$A"
#define WIN_UNIT_SECTION_NAME       "phpc$B"
#define WIN_UNIT_SECTION_EPILOG     "phpc$C"
#define OSX_UNIT_SECTION_NAME       "__TEXT,__phpcu"

#define UNIT_SECTION_FLAGS          ASTContext::PSF_Read
#define UNIT_SECTION_RT_FLAGS       "ar"


// Unit state

#define LINUX_UNITSTATE_SECTION_NAME    "__phpdu"
#define WIN_UNITSTATE_SECTION_PROLOG    "phpd$A"
#define WIN_UNITSTATE_SECTION_NAME      "phpd$B"
#define WIN_UNITSTATE_SECTION_EPILOG    "phpd$C"
#define OSX_UNITSTATE_SECTION_NAME      "__DATA,__phpdu"

#define UNITSTATE_SECTION_FLAGS   (ASTContext::PSF_Read | ASTContext::PSF_Write)
#define UNITSTATE_SECTION_RT_FLAGS     "aw"


// Global variable record

#define LINUX_GVR_SECTION_NAME    "__phpdg"
#define WIN_GVR_SECTION_PROLOG    "phpd$D"
#define WIN_GVR_SECTION_NAME      "phpd$E"
#define WIN_GVR_SECTION_EPILOG    "phpd$F"
#define OSX_GVR_SECTION_NAME      "__DATA,__phpdg"

#define GVR_SECTION_FLAGS         (ASTContext::PSF_Read | ASTContext::PSF_Write)
#define GVR_SECTION_RT_FLAGS      "aw"


// Compile time constant

#define LINUX_CTC_SECTION_NAME    "__phpcc"
#define WIN_CTC_SECTION_PROLOG    "phpc$G"
#define WIN_CTC_SECTION_NAME      "phpc$H"
#define WIN_CTC_SECTION_EPILOG    "phpc$I"
#define OSX_CTC_SECTION_NAME      "__TEXT,__phpcc"

#define CTC_SECTION_FLAGS         ASTContext::PSF_Read
#define CTC_SECTION_RT_FLAGS      "ar"


// Runtime constant

#define LINUX_RTC_SECTION_NAME    "__phpdc"
#define WIN_RTC_SECTION_PROLOG    "phpd$G"
#define WIN_RTC_SECTION_NAME      "phpd$H"
#define WIN_RTC_SECTION_EPILOG    "phpd$I"
#define OSX_RTC_SECTION_NAME      "__DATA,__phpdc"

#define RTC_SECTION_FLAGS         (ASTContext::PSF_Read | ASTContext::PSF_Write)
#define RTC_SECTION_RT_FLAGS      "aw"

#endif
