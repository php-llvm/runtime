//===--- round.cpp --------------------------------------------------------===//
/*
   +----------------------------------------------------------------------+
   | PHP Version 7                                                        |
   +----------------------------------------------------------------------+
   | Copyright (c) 1997-2015 The PHP Group                                |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors: Jim Winstead <jimw@php.net>                                 |
   |          Stig Sæther Bakken <ssb@php.net>                            |
   |          Zeev Suraski <zeev@zend.com>                                |
   | PHP 4.0 patches by Thies C. Arntzen <thies@thieso.net>               |
   |                                                                      |
   | round function was ported from php sources by Denis Polunin.         |
   |                                                                      |
   +----------------------------------------------------------------------+
*/
//===----------------------------------------------------------------------===//
//
// round function implementation.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "shared/constants.h"
#include "shared/functions.h"
#include <cmath>
#include <cstdio>
#include <cstdlib>
//------------------------------------------------------------------------------


namespace phpfe {

/* php_round_helper function port */
static double round_helper(double value, RoundingMode mode) {
  double tmp_value;

  if (value >= 0.0) {
    tmp_value = ::floor(value + 0.5);
    if ((mode == RoundingMode::HalfDown && value == (-0.5 + tmp_value)) ||
      (mode == RoundingMode::HalfEven && value == (0.5 + 2 * ::floor(tmp_value / 2.0))) ||
      (mode == RoundingMode::HalfOdd  && value == (0.5 + 2 * ::floor(tmp_value / 2.0) - 1.0))) {
      tmp_value = tmp_value - 1.0;
    }
  } else {
    tmp_value = ::ceil(value - 0.5);
    if ((mode == RoundingMode::HalfDown && value == (0.5 + tmp_value)) ||
      (mode == RoundingMode::HalfEven && value == (-0.5 + 2 * ::ceil(tmp_value / 2.0))) ||
      (mode == RoundingMode::HalfOdd  && value == (-0.5 + 2 * ::ceil(tmp_value / 2.0) + 1.0))) {
      tmp_value = tmp_value + 1.0;
    }
  }

  return tmp_value;
}


static int get_double_precision(double value) {
  return 14 - ::floor(::log10(::fabs(value)));
}


// _php_math_round function port
double round(double value, int places, RoundingMode mode) {
  if (!(RoundingMode::HalfUp <= mode && mode <= RoundingMode::HalfOdd))
    mode = RoundingMode::HalfUp;

  if (!std::isfinite(value))
    return value;

  double f1, f2;
  double tmp_value;
  int precision_places = get_double_precision(value);

  f1 = ::pow(10, ::fabs(places));

  /* If the decimal precision guaranteed by FP arithmetic is higher than
  the requested places BUT is small enough to make sure a non-zero value
  is returned, pre-round the result to the precision */
  if (precision_places > places && precision_places - places < 15) {
    f2 = ::pow(10, ::fabs(precision_places));
    if (precision_places >= 0) {
      tmp_value = value * f2;
    } else {
      tmp_value = value / f2;
    }
    /* preround the result (tmp_value will always be something * 1e14,
    thus never larger than 1e15 here) */
    tmp_value = round_helper(tmp_value, mode);
    /* now correctly move the decimal point */
    f2 = ::pow(10, ::fabs(places - precision_places));
    /* because places < precision_places */
    tmp_value = tmp_value / f2;
  } else {
    /* adjust the value */
    if (places >= 0) {
      tmp_value = value * f1;
    } else {
      tmp_value = value / f1;
    }
    /* This value is beyond our precision, so rounding it is pointless */
    if (::fabs(tmp_value) >= 1e15) {
      return value;
    }
  }

  /* round the temp value */
  tmp_value = round_helper(tmp_value, mode);

  /* see if it makes sense to use simple division to round the value */
  if (::fabs(places) < 23) {
    if (places > 0) {
      tmp_value = tmp_value / f1;
    } else {
      tmp_value = tmp_value * f1;
    }
  } else {
    /* Simple division can't be used since that will cause wrong results.
    Instead, the number is converted to a string and back again using
    strtod(). strtod() will return the nearest possible FP value for
    that string. */

    /* 40 Bytes should be more than enough for this format string. The
    float won't be larger than 1e15 anyway. But just in case, use
    snprintf() and make sure the buffer is zero-terminated */
    char buf[40];
    snprintf(buf, 39, "%15fe%d", tmp_value, -places);
    buf[39] = '\0';
    tmp_value = strtod(buf, nullptr);
    /* couldn't convert to string and back */
    if (!std::isfinite(tmp_value) || std::isnan(tmp_value)) {
      tmp_value = value;
    }
  }

  return tmp_value;
}

}
