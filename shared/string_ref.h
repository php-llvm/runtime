//===--- string_ref.h -------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Simple type that represent string data of known length. It is able to
//  represent string data that are not C-strings, because they do not end with
//  null byte or contain null symbols in the body, as PHP strings used to.
//
//  This file must compile in C mode, as it may be included by C implementations
//  (Zend engine, PHP extensions etc).
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_STRING_REF_H
#define PHPRT_STRING_REF_H

//------ Dependencies ----------------------------------------------------------
#include "shared_defs.h"
#ifndef RUNTIME_SYM_FILE
#include <assert.h>
#endif
//------------------------------------------------------------------------------


/// \brief Reference to string data of known length.
///
/// The class is used to perform non-modifying access to the string data. The
/// data must have life time greater than that of string_ref object.
///
/// \note This class is analog of llvm::StringRef. We do not want to use the
/// latter because:
/// - it creates unneeded dependencies on llvm declarations,
/// - we need C type as its declaration may be used in plain C code.
///
PHPRT_API struct string_ref {
  const char *data;
  unsigned len;

#ifdef __cplusplus
  typedef const char *iterator;
  typedef const char *const_iterator;
  typedef unsigned size_type;
  static const size_type npos = ~size_type(0);

  // Work as constructor
  void init() { data = ""; len = 0; }
  void init(const char *s);
  void init(const char *s, unsigned l) {
    data = s; len = l;
  }

  static string_ref make() { string_ref r; r.init(); return r; }
  static string_ref make(const char *s, unsigned l) {
    return string_ref{ s, l };
  }
  static string_ref make(const char *s) {
    string_ref r; r.init(s); return r;
  }

  // Queries.
  size_type length() const { return len; }
  bool empty() const { return len == 0; }

  const char *begin() const { return data; }
  const char *end() const { return data + len; }

  char front() const { assert(len); return *data; }
  char back() const  { assert(len); return data[len - 1]; }
  char get(unsigned ndx) const {
    assert((ndx < len));
    return data[ndx];
  }

  // Comparison.
  int compare(const string_ref &s) const;
  int compare_lower(const string_ref &s) const;
  bool equals(const string_ref &x) const;
  bool equals_lower(const string_ref &x) const;
  bool equals(const char *x) const;
  bool equals_lower(const char *x) const;
  bool less(const string_ref &s) const { return compare(s) < 0; }
  bool less_lower(const string_ref &s) const { return compare_lower(s) < 0; }

  bool starts_with(const string_ref &x) {
    if (len < x.len)
      return false;
    return x.prefix(x.len).equals(x);
  }
  bool starts_with_lower(const string_ref &x) {
    if (len < x.len)
      return false;
    return prefix(x.len).equals_lower(x);
  }

  // Operators.
  friend bool operator < (const string_ref &s1, const string_ref &s2) {
    return s1.less(s2);
  }
  friend bool operator == (const string_ref &s1, const string_ref &s2) {
    return s1.equals(s2);
  }
  friend bool operator != (const string_ref &s1, const string_ref &s2) {
    return !s1.equals(s2);
  }
  char operator [] (unsigned i) const { return get(i); }

  // String reference modification.
  char pop_front();
  char pop_back();

  // Building new string reference.
  string_ref trim_front() const;
  string_ref trim_back() const;
  string_ref trim() const;
  string_ref remove_enclosing(char quote) const;
  string_ref prefix(unsigned l) const {
    if (len < l)
      return *this;
    return string_ref{ data, l };
  }
  string_ref substr(unsigned start, unsigned l) const {
    if (start >= len)
      return string_ref{ nullptr, 0 };
    if (start + l > len)
      l = len - start;
    return string_ref{ data + start, l };
  }
  string_ref skip(unsigned l) const {
    if (l >= len)
      return string_ref{ nullptr, 0 };
    return string_ref{ data + l, len - l };
  }
  string_ref trunc(unsigned l) const {
    if (l >= len)
      return *this;
    return string_ref{ data, l };
  }

  // Conversions
  bool to_bool() const {
    return len > 0 && (len > 1 || data[0] != '0');
  }

  // Helper class for using in ordered sets.

  struct Less {
    bool operator()(const string_ref &s1, const string_ref &s2) const {
      return s1.less(s2);
    }
  };

  struct LessLower {
    bool operator()(const string_ref &s1, const string_ref &s2) const {
      return s1.less_lower(s2);
    }
  };

  void dump() const;

#endif
};


#ifndef __cplusplus
typedef struct string_ref string_ref;
#endif


// Macros for initializing string_ref value with string literal.
#define INIT_STRING_REF(x)   { x, sizeof(x) - 1 }

// Creates string_ref from string literal.
#define STRING_REF(x)  string_ref { x, sizeof(x) - 1 }

#endif
