cmake_minimum_required(VERSION 2.8)

include(CheckIncludeFiles)
include(CheckFunctionExists)
include(CheckLibraryExists)
include(CheckTypeSize)
include(TestBigEndian)


# --- Setting up PHP constants ------------------------------------------------

set(PHP_MAJOR_VERSION 8)
set(PHP_MINOR_VERSION 0)
set(PHP_RELEASE_VERSION 0)
set(PHP_EXTRA_VERSION "-phlang")
set(PHP_VERSION_ID 80000)
set(PHP_VERSION "8.0.0-phlang")

# --- Setting up configuration constants ---------------------------------------

# Check headers.

CHECK_INCLUDE_FILE(alloca.h      HAVE_ALLOCA_H)
CHECK_INCLUDE_FILE(assert.h      HAVE_ASSERT_H)
CHECK_INCLUDE_FILE(backtrace.h   HAVE_BACKTRACE_H)
CHECK_INCLUDE_FILE(dirent.h      HAVE_DIRENT_H)
CHECK_INCLUDE_FILE(dlfcn.h       HAVE_DLFCN_H)
CHECK_INCLUDE_FILE(fcntl.h       HAVE_FCNTL_H)
CHECK_INCLUDE_FILE(iconv.h       HAVE_ICONV)
CHECK_INCLUDE_FILE(imap.h        HAVE_IMAP)
CHECK_INCLUDE_FILE(inttypes.h    HAVE_INTTYPES_H)
CHECK_INCLUDE_FILE(limits.h      HAVE_LIMITS_H)
CHECK_INCLUDE_FILE(locale.h      HAVE_LOCALE_H)
CHECK_INCLUDE_FILE(malloc.h      HAVE_MALLOC_H)
CHECK_INCLUDE_FILE(math.h        HAVE_MATH_H)
CHECK_INCLUDE_FILE(mcrypt.h      HAVE_LIBMCRYPT)
CHECK_INCLUDE_FILE(ndir.h        HAVE_NDIR_H)
CHECK_INCLUDE_FILE(sched.h       HAVE_SCHED_H)
CHECK_INCLUDE_FILE(signal.h      HAVE_SIGNAL_H)
CHECK_INCLUDE_FILE(stdarg.h      HAVE_STDARG_H)
CHECK_INCLUDE_FILE(stdint.h      HAVE_STDINT_H)
CHECK_INCLUDE_FILE(stdlib.h      HAVE_STDLIB_H)
CHECK_INCLUDE_FILE(string.h      HAVE_STRING_H)
CHECK_INCLUDE_FILE(sys/dir.h     HAVE_SYS_DIR_H)
CHECK_INCLUDE_FILE(sys/select.h  HAVE_SYS_SELECT_H)
CHECK_INCLUDE_FILE(sys/socket.h  HAVE_SYS_SOCKET_H)
CHECK_INCLUDE_FILE(sys/time.h    HAVE_SYS_TIME_H)
CHECK_INCLUDE_FILE(sys/times.h   HAVE_TIMES)
CHECK_INCLUDE_FILE(sys/types.h   HAVE_SYS_TYPES_H)
CHECK_INCLUDE_FILE(sys/un.h      HAVE_SYS_UN_H)
CHECK_INCLUDE_FILE(sys/utsname.h HAVE_UTSNAME_H)
CHECK_INCLUDE_FILE(sys/wait.h    HAVE_SYS_WAIT_H)
CHECK_INCLUDE_FILE(unistd.h      HAVE_UNISTD_H)
CHECK_INCLUDE_FILE(unix.h        HAVE_UNIX_H)
CHECK_INCLUDE_FILE(utime.h       HAVE_UTIME_H)


# Check functions.

CHECK_FUNCTION_EXISTS(clearenv   HAVE_CLEARENV)
CHECK_FUNCTION_EXISTS(ctermid    HAVE_CTERMID)
CHECK_FUNCTION_EXISTS(finite     HAVE_FINITE)
CHECK_FUNCTION_EXISTS(fpclass    HAVE_FPCLASS)
CHECK_FUNCTION_EXISTS(fpclass    HAVE_FPCLASS)
CHECK_FUNCTION_EXISTS(getgroups  HAVE_GETGROUPS)
CHECK_FUNCTION_EXISTS(getlogin   HAVE_GETLOGIN)
CHECK_FUNCTION_EXISTS(getpgid    HAVE_GETPGID)
CHECK_FUNCTION_EXISTS(getpid     HAVE_GETPID)
CHECK_FUNCTION_EXISTS(getrlimit  HAVE_GETRLIMIT)
CHECK_FUNCTION_EXISTS(getsid     HAVE_GETSID)
CHECK_FUNCTION_EXISTS(gettimeofday HAVE_GETTIMEOFDAY)
CHECK_FUNCTION_EXISTS(initgroups HAVE_INITGROUPS)
CHECK_FUNCTION_EXISTS(isfinite   HAVE_ISFINITE)
CHECK_FUNCTION_EXISTS(isinf      HAVE_ISINF)
CHECK_FUNCTION_EXISTS(isnan      HAVE_ISNAN)
CHECK_FUNCTION_EXISTS(kill       HAVE_KILL)
CHECK_FUNCTION_EXISTS(makedev    HAVE_MAKEDEV)
CHECK_FUNCTION_EXISTS(memcpy     HAVE_MEMCPY)
CHECK_FUNCTION_EXISTS(memmove    HAVE_MEMMOVE)
CHECK_FUNCTION_EXISTS(mkfifo     HAVE_MKFIFO)
CHECK_FUNCTION_EXISTS(mknod      HAVE_MKNOD)
CHECK_FUNCTION_EXISTS(readdir_r  HAVE_POSIX_READDIR_R)
CHECK_FUNCTION_EXISTS(scandir    HAVE_SCANDIR)
CHECK_FUNCTION_EXISTS(setegid    HAVE_SETEGID)
CHECK_FUNCTION_EXISTS(setenv     HAVE_SETENV)
CHECK_FUNCTION_EXISTS(seteuid    HAVE_SETEUID)
CHECK_FUNCTION_EXISTS(setlocale  HAVE_SETLOCALE)
CHECK_FUNCTION_EXISTS(setsid     HAVE_SETSID)
CHECK_FUNCTION_EXISTS(sigsetjmp  HAVE_SIGSETJMP)
CHECK_FUNCTION_EXISTS(slprintf   HAVE_SLPRINTF)
CHECK_FUNCTION_EXISTS(snprintf   HAVE_SNPRINTF)
CHECK_FUNCTION_EXISTS(sprintf    HAVE_SPRINTF)
CHECK_FUNCTION_EXISTS(strcasecmp HAVE_STRCASECMP)
CHECK_FUNCTION_EXISTS(strdup     HAVE_STRDUP)
CHECK_FUNCTION_EXISTS(strerror   HAVE_STRERROR)
CHECK_FUNCTION_EXISTS(strftime   HAVE_STRFTIME)
CHECK_FUNCTION_EXISTS(strncasecmp HAVE_STRNCASECMP)
CHECK_FUNCTION_EXISTS(strnlen    HAVE_STRNLEN)
CHECK_FUNCTION_EXISTS(strtod     HAVE_STRTOD)
CHECK_FUNCTION_EXISTS(strtok_r   HAVE_STRTOK_R)
CHECK_FUNCTION_EXISTS(strtol     HAVE_STRTOL)
CHECK_FUNCTION_EXISTS(uname      HAVE_UNAME)
CHECK_FUNCTION_EXISTS(unsetenv   HAVE_UNSETENV)
CHECK_FUNCTION_EXISTS(usleep     HAVE_USLEEP)
CHECK_FUNCTION_EXISTS(vprintf    HAVE_VPRINTF)
CHECK_FUNCTION_EXISTS(vslprintf  HAVE_VSLPRINTF)
CHECK_FUNCTION_EXISTS(vsnprintf  HAVE_VSNPRINTF)


# Check libraries.

CHECK_INCLUDE_FILES(dlfcn.h HAVE_LIBDL)

# Generate config file

configure_file(
  ${CMAKE_CURRENT_SOURCE_DIR}/cmake/phprt-config.h.cmake
  ${CONFIG_INCLUDE_DIR}/phprt-config.h
)

