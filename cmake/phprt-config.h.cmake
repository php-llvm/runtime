//===--- phprt-config.cmake -----------------------------------------------===//
//
//  LLVM based PHP compiler
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//  Configuration file for PHP runtime.
//
//  This file must be usable in C and C++ files.
//
//  This file is generated by configuration script from the templare
//  phprt-config.h.cmake.
//
//===----------------------------------------------------------------------===//
#ifndef PHPRT_CONFIG_H
#define PHPRT_CONFIG_H

#define PHPFE_RESOURCE_DIR "${PHP_FE_RESOURCE_DIR}"
#define PHPFE_VERSION_MAJOR ${PHPFE_VERSION_MAJOR}
#define PHPFE_VERSION_MINOR ${PHPFE_VERSION_MINOR}
#define PHPFE_VERSION_STRING "${PHPFE_VERSION}"

#define PHPRT_MAJOR_VERSION ${PHP_MAJOR_VERSION}
#define PHPRT_MINOR_VERSION ${PHP_MINOR_VERSION}
#define PHPRT_RELEASE_VERSION ${PHP_RELEASE_VERSION}
#define PHPRT_VERSION_ID ${PHP_VERSION_ID}
#define PHPRT_EXTRA_VERSION "${PHP_EXTRA_VERSION}"
#define PHPRT_VERSION "${PHP_VERSION}"

/* check headers */

#cmakedefine HAVE_ALLOCA_H ${HAVE_ALLOCA_H}
#cmakedefine HAVE_ASSERT_H ${HAVE_ASSERT_H}
#cmakedefine HAVE_BACKTRACE_H ${HAVE_BACKTRACE_H}
#cmakedefine HAVE_DIRENT_H ${HAVE_DIRENT_H}
#cmakedefine HAVE_DLFCN_H ${HAVE_DLFCN_H}
#cmakedefine HAVE_FCNTL_H ${HAVE_FCNTL_H}
#cmakedefine HAVE_ICONV ${HAVE_ICONV}
#cmakedefine HAVE_INTTYPES_H ${HAVE_INTTYPES_H}
#cmakedefine HAVE_LIBMCRYPT ${HAVE_LIBMCRYPT}
#cmakedefine HAVE_LIBXML ${HAVE_LIBXML}
#cmakedefine HAVE_LIMITS_H ${HAVE_LIMITS_H}
#cmakedefine HAVE_LOCALE_H ${HAVE_LOCALE_H}
#cmakedefine HAVE_MALLOC_H ${HAVE_MALLOC_H}
#cmakedefine HAVE_MATH_H ${HAVE_MATH_H}
#cmakedefine HAVE_NDIR_H ${HAVE_NDIR_H}
#cmakedefine HAVE_SCHED_H ${HAVE_SCHED_H}
#cmakedefine HAVE_SIGNAL_H ${HAVE_SIGNAL_H}
#cmakedefine HAVE_STDARG_H ${HAVE_STDARG_H}
#cmakedefine HAVE_STDINT_H ${HAVE_STDINT_H}
#cmakedefine HAVE_STDINT_H ${HAVE_STDINT_H}
#cmakedefine HAVE_STDLIB_H ${HAVE_STDLIB_H}
#cmakedefine HAVE_STRING_H ${HAVE_STRING_H}
#cmakedefine HAVE_SYS_DIR_H ${HAVE_SYS_DIR_H}
#cmakedefine HAVE_SYS_SELECT_H ${HAVE_SYS_SELECT_H}
#cmakedefine HAVE_SYS_SOCKET_H ${HAVE_SYS_SOCKET_H}
#cmakedefine HAVE_SYS_STAT_H ${HAVE_SYS_STAT_H}
#cmakedefine HAVE_SYS_TIME_H ${HAVE_SYS_TIME_H}
#cmakedefine HAVE_SYS_TYPES_H ${HAVE_SYS_TYPES_H}
#cmakedefine HAVE_SYS_UN_H ${HAVE_SYS_UN_H}
#cmakedefine HAVE_SYS_WAIT_H ${HAVE_SYS_WAIT_H}
#cmakedefine HAVE_TIMES ${HAVE_TIMES}
#cmakedefine HAVE_UNISTD_H ${HAVE_UNISTD_H}
#cmakedefine HAVE_UNISTD_H ${HAVE_UNISTD_H}
#cmakedefine HAVE_UNIX_H ${HAVE_UNIX_H}
#cmakedefine HAVE_UTIME_H ${HAVE_UTIME_H}

/* check functions */

#cmakedefine HAVE_CLEARENV ${HAVE_CLEARENV}
#cmakedefine HAVE_CTERMID ${HAVE_CTERMID}
#cmakedefine HAVE_FINITE ${HAVE_FINITE}
#cmakedefine HAVE_FPCLASS ${HAVE_FPCLASS}
#cmakedefine HAVE_GETGROUPS ${HAVE_GETGROUPS}
#cmakedefine HAVE_GETLOGIN ${HAVE_GETLOGIN}
#cmakedefine HAVE_GETPGID ${HAVE_GETPGID}
#cmakedefine HAVE_GETPID ${HAVE_GETPID}
#cmakedefine HAVE_GETRLIMIT ${HAVE_GETRLIMIT}
#cmakedefine HAVE_GETSID ${HAVE_GETSID}
#cmakedefine HAVE_INITGROUPS ${HAVE_INITGROUPS}
#cmakedefine HAVE_ISFINITE ${HAVE_ISFINITE}
#cmakedefine HAVE_ISINF ${HAVE_ISINF}
#cmakedefine HAVE_ISNAN ${HAVE_ISNAN}
#cmakedefine HAVE_KILL ${HAVE_KILL}
#cmakedefine HAVE_MAKEDEV ${HAVE_MAKEDEV}
#cmakedefine HAVE_MEMCPY ${HAVE_MEMCPY}
#cmakedefine HAVE_MEMMOVE ${HAVE_MEMMOVE}
#cmakedefine HAVE_MKFIFO ${HAVE_MKFIFO}
#cmakedefine HAVE_MKNOD ${HAVE_MKNOD}
#cmakedefine HAVE_POSIX_READDIR_R ${HAVE_POSIX_READDIR_R}
#cmakedefine HAVE_SCANDIR ${HAVE_SCANDIR}
#cmakedefine HAVE_SETEGID ${HAVE_SETEGID}
#cmakedefine HAVE_SETENV ${HAVE_SETENV}
#cmakedefine HAVE_SETEUID ${HAVE_SETEUID}
#cmakedefine HAVE_SETLOCALE ${HAVE_SETLOCALE}
#cmakedefine HAVE_SETSID ${HAVE_SETSID}
#cmakedefine HAVE_SIGSETJMP ${HAVE_SIGSETJMP}
#cmakedefine HAVE_SLPRINTF ${HAVE_SLPRINTF}
#cmakedefine HAVE_SNPRINTF ${HAVE_SNPRINTF}
#cmakedefine HAVE_SPRINTF ${HAVE_SPRINTF}
#cmakedefine HAVE_STRCASECMP ${HAVE_STRCASECMP}
#cmakedefine HAVE_STRDUP ${HAVE_STRDUP}
#cmakedefine HAVE_STRERROR ${HAVE_STRERROR}
#cmakedefine HAVE_STRFTIME ${HAVE_STRFTIME}
#cmakedefine HAVE_STRNCASECMP ${HAVE_STRNCASECMP}
#cmakedefine HAVE_STRNLEN ${HAVE_STRNLEN}
#cmakedefine HAVE_STRTOD ${HAVE_STRTOD}
#cmakedefine HAVE_STRTOK_R ${HAVE_STRTOK_R}
#cmakedefine HAVE_STRTOL ${HAVE_STRTOL}
#cmakedefine HAVE_UNSETENV ${HAVE_UNSETENV}
#cmakedefine HAVE_USLEEP ${HAVE_USLEEP}
#cmakedefine HAVE_VPRINTF ${HAVE_VPRINTF}
#cmakedefine HAVE_VSLPRINTF ${HAVE_VSLPRINTF}
#cmakedefine HAVE_VSNPRINTF ${HAVE_VSNPRINTF}


/* check libraries */

#cmakedefine HAVE_LIBDL ${HAVE_LIBDL}


/* check integers size */

/* Define to 1 if the system has the type `int16'. */
#cmakedefine HAVE_INT16 ${HAVE_INT16}

/* Define to 1 if the system has the type `int16_t'. */
#cmakedefine HAVE_INT16_T ${HAVE_INT16_T}

/* Define to 1 if the system has the type `int32'. */
#cmakedefine HAVE_INT32 ${HAVE_INT32}

/* Define if int32_t type is present. */
#cmakedefine HAVE_INT32_T ${HAVE_INT32_T}

/* Define to 1 if the system has the type `int64'. */
#cmakedefine HAVE_INT64 ${HAVE_INT64}

/* Define to 1 if the system has the type `int64_t'. */
#cmakedefine HAVE_INT64_T ${HAVE_INT64_T}

/* Define to 1 if the system has the type `int8'. */
#cmakedefine HAVE_INT8 ${HAVE_INT8}

/* Define to 1 if the system has the type `int8_t'. */
#cmakedefine HAVE_INT8_T ${HAVE_INT8_T}

/* Define to 1 if the system has the type `uint16'. */
#cmakedefine HAVE_UINT16 ${HAVE_UINT16}

/* Define to 1 if the system has the type `uint16_t'. */
#cmakedefine HAVE_UINT16_T ${HAVE_UINT16_T}

/* Define to 1 if the system has the type `uint32'. */
#cmakedefine HAVE_UINT32 ${HAVE_UINT32}

/* Define if uint32_t type is present. */
#cmakedefine HAVE_UINT32_T ${HAVE_UINT32_T}

/* Define to 1 if the system has the type `uint64'. */
#cmakedefine HAVE_UINT64 ${HAVE_UINT64}

/* Define to 1 if the system has the type `uint64_t'. */
#cmakedefine HAVE_UINT64_T ${HAVE_UINT64_T}

/* Define to 1 if the system has the type `uint8'. */
#cmakedefine HAVE_UINT8 ${HAVE_UINT8}

/* Define to 1 if the system has the type `uint8_t'. */
#cmakedefine HAVE_UINT8_T ${HAVE_UINT8_T}

/* Whether intmax_t is available */
#cmakedefine HAVE_INTMAX_T ${HAVE_INTMAX_T}

#endif // PHPRT_CONFIG_H
