<?

// integer 0

$x = 0;
assert(++$x === 1);
assert($x++ === 1);
assert($x === 2);

$x = 0;
assert(--$x === -1);
assert($x-- === -1);
assert($x === -2);

// floating 0

$x = 0.0;
assert(++$x === 1.0);
assert($x++ === 1.0);
assert($x === 2.0);

$x = 0.0;
assert(--$x === -1.0);
assert($x-- === -1.0);
assert($x === -2.0);

// floating number

$x = 0.2;
assert(++$x === 1.2);
assert($x++ === 1.2);
assert($x === 2.2);

$x = 0.2;
assert(--$x === -0.8);
assert($x-- === -0.8);
assert($x === -1.8);

// empty string
$x = "";
assert(++$x === "1");

$x = "";
assert($x++ === "");
assert($x === "1");

$x = "";
assert(--$x === -1);

$x = "";
assert($x-- === "");
assert($x === -1);

// numeric strings

$x = "0";
assert(++$x === 1);

$x = "0";
assert($x++ === "0");
assert($x === 1);

$x = "0";
assert(--$x === -1);

$x = "0";
assert($x-- === "0");
assert($x === -1);

$x = "1";
assert(++$x === 2);

$x = "1";
assert($x++ === "1");
assert($x === 2);

$x = "1";
assert(--$x === 0);

$x = "1";
assert($x-- === "1");
assert($x === 0);

$x = "9";
assert(++$x === 10);

$x = "9";
assert($x++ === "9");
assert($x === 10);

$x = "9";
assert(--$x === 8);

$x = "9";
assert($x-- === "9");
assert($x === 8);

$x = "10";
assert(++$x === 11);

$x = "10";
assert($x++ === "10");
assert($x === 11);

$x = "10";
assert(--$x === 9);

$x = "10";
assert($x-- === "10");
assert($x === 9);

$x = "-1";
assert(++$x === 0);

$x = "-1";
assert($x++ === "-1");
assert($x === 0);

$x = "-1";
assert(--$x === -2);

$x = "-1";
assert($x-- === "-1");
assert($x === -2);

$x = "1.1";
assert(++$x === 2.1);

$x = "1.1";
assert($x++ === "1.1");
assert($x === 2.1);

$x = "1.1";
$y = --$x;
assert(is_float($y));
assert(abs($y - 0.1) < .0001);

$x = "1.1";
$y = $x--;
assert($y === "1.1");
assert(is_float($x));
assert(abs($x - 0.1) < .0001);

// semi-numeric strings

$x = "1a";
assert(++$x === "1b");

$x = "1a";
assert($x++ === "1a");
assert($x === "1b");

$x = "1a";
assert(--$x === "1a");

$x = "2b";
assert(--$x === "2b");

// generic strings

$x = "a";
assert(++$x === "b");

$x = "a";
assert($x++ === "a");
assert($x === "b");

$x = "b";
assert(--$x === "b");

$x = "b";
assert($x-- === "b");
assert($x === "b");

$x = "z";
assert(++$x === "aa");


var_dump($x);


/* TODO:
//--------------------------------------------------------------------
// postfix increment
//--------------------------------------------------------------------
function post_inc_int(int $a, $expected) {
  $result = $a++;
  assert($a === $expected);
  return $result;
}

function post_inc_float(float $a, $expected) {
  $result = $a++;
  assert($a === $expected);
  return $result;
}

function post_inc_string(string $a, $expected) {
  $result = $a++;
  assert($a === $expected);
  return $result;
}

function post_inc($a, $expected) {
  $result = $a++;
  assert($a === $expected);
  return $result;
}

assert(post_inc_int(10, 11) === 10);
//TODO: make 32-64bit overflow check
//assert(post_inc_int(2147483647, -2147483648) === 2147483647); // PHP_INT_MAX
assert(post_inc_float(10.11, 11.11) === 10.11);
assert(post_inc_string('', '1') === '');
assert(post_inc_string("qwe9", 'qwf0') === "qwe9");
assert(post_inc_string("qwe", 'qwf') === "qwe");
assert(post_inc_string("az", 'ba') === "az");
assert(post_inc_string("z", 'aa') === "z");
//TODO: make 32-64bit overflow check
//assert(post_inc_string("2147483647", 2147483648.0) === "2147483647");
assert(post_inc_string("123", 124) === "123");
assert(post_inc_string("129", 130) === "129");
assert(post_inc_string("9", 10) === "9");
assert(post_inc_string("9qwe", '9qwf') === "9qwe");
assert(post_inc_string("9.9", 10.9) === "9.9");
assert(post_inc_string("9z9", '10a0') === "9z9");

assert(post_inc(10, 11) === 10);
//TODO: make 32-64bit overflow check
//assert(post_inc(2147483647, 2147483648.0) === 2147483647); // PHP_INT_MAX
assert(post_inc(10.11, 11.11) === 10.11);
assert(post_inc('', '1') === '');
assert(post_inc("qwe9", 'qwf0') === "qwe9");


//--------------------------------------------------------------------
// prefix increment
//--------------------------------------------------------------------
function pre_inc_int(int $a, $expected) {
  $result = ++$a;
  assert($a === $expected);
  return $result;
}

function pre_inc_float(float $a, $expected) {
  $result = ++$a;
  assert($a === $expected);
  return $result;
}

function pre_inc_string(string $a, $expected) {
  $result = ++$a;
  assert($a === $expected);
  return $result;
}

function pre_inc($a, $expected) {
  $result = ++$a;
  assert($a === $expected);
  return $result;
}

assert(pre_inc_int(10, 11) === 11);
//TODO: make 32-64bit overflow check
//assert(pre_inc_int(2147483647, -2147483648) === -2147483648); // PHP_INT_MAX
assert(pre_inc_float(10.11, 11.11) === 11.11);
assert(pre_inc_string('', '1') === '1');
assert(pre_inc_string("qwe9", 'qwf0') === "qwf0");
assert(pre_inc_string("qwe", 'qwf') === "qwf");
assert(pre_inc_string("az", 'ba') === "ba");
assert(pre_inc_string("z", 'aa') === "aa");
//TODO: make 32-64bit overflow check
//assert(pre_inc_string("2147483647", 2147483648.0) === 2147483648.0);
assert(pre_inc_string("123", 124) === 124);
assert(pre_inc_string("129", 130) === 130);
assert(pre_inc_string("9", 10) === 10);
assert(pre_inc_string("9qwe", '9qwf') === "9qwf");
assert(pre_inc_string("9.9", 10.9) === 10.9);
assert(pre_inc_string("9z9", '10a0') === "10a0");

assert(pre_inc(10, 11) === 11);
//TODO: make 32-64bit overflow check
//assert(pre_inc(2147483647, 2147483648.0) === 2147483648.0); // PHP_INT_MAX
assert(pre_inc(10.11, 11.11) === 11.11);
assert(pre_inc('', '1') === '1');
assert(pre_inc("qwe9", 'qwf0') === "qwf0");


//--------------------------------------------------------------------
// postfix decrement
//--------------------------------------------------------------------
function post_dec_int(int $a, $expected) {
  $result = $a--;
  assert($a === $expected);
  return $result;
}

function post_dec_float(float $a, $expected) {
  $result = $a--;
  assert($a === $expected);
  return $result;
}

function post_dec_string(string $a, $expected) {
  $result = $a--;
  assert($a === $expected);
  return $result;
}

function post_dec($a, $expected) {
  $result = $a--;
  assert($a === $expected);
  return $result;
}

assert(post_dec_int(10, 9) === 10);
//TODO: make 32-64bit overflow check
//assert(post_dec_int(-2147483648, 2147483647) === -2147483648); // PHP_INT_MIN
assert(post_dec_float(10.11, 9.11) === 10.11);
//TODO: make 32-64bit overflow check
//assert(post_dec_string("-2147483648", -2147483649.0) === "-2147483648");
assert(post_dec_string("123", 122) === "123");

assert(post_dec(10, 9) === 10);
//TODO: make 32-64bit overflow check
//assert(post_dec(-2147483648, -2147483649.0) === -2147483648); // PHP_INT_MIN
assert(post_dec(10.11, 9.11) === 10.11);
assert(post_dec('', -1) === '');


//--------------------------------------------------------------------
// prefix decrement
//--------------------------------------------------------------------
function pre_dec_int(int $a, $expected) {
  $result = --$a;
  assert($a === $expected);
  return $result;
}

function pre_dec_float(float $a, $expected) {
  $result = --$a;
  assert($a === $expected);
  return $result;
}

function pre_dec_string(string $a, $expected) {
  $result = --$a;
  assert($a === $expected);
  return $result;
}

function pre_dec($a, $expected) {
  $result = --$a;
  assert($a === $expected);
  return $result;
}

assert(pre_dec_int(10, 9) === 9);
//TODO: make 32-64bit overflow check
//assert(pre_dec_int(-2147483648, 2147483647) === 2147483647); // PHP_INT_MIN
assert(pre_dec_float(10.11, 9.11) === 9.11);
//TODO: make 32-64bit overflow check
//assert(pre_dec_string("-2147483648", -2147483649.0) === -2147483649.0);
assert(pre_dec_string("123", 122) === 122);

assert(pre_dec(10, 9) === 9);
//TODO: make 32-64bit overflow check
//assert(pre_dec(-2147483648, -2147483649.0) === -2147483649.0); // PHP_INT_MIN
assert(pre_dec(10.11, 9.11) === 9.11);
assert(pre_dec('', -1) === -1);
*/
?>
