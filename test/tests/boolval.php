<?php
assert(boolval(0) === false);
assert(boolval(1) === true);
assert(boolval(1.0) === true);
assert(boolval("1") === true);
assert(boolval("1.556") === true);
assert(boolval("0") === false);
assert(boolval("abcde") === true);
assert(boolval(array()) === false);
assert(boolval(array(1,2,3,4)) === true);
assert(boolval("") === false);

$v1 = 0;
$v2 = 1;
$v3 = 1.0;
$v4 = "1";
$v5 = "1.556";
$v6 = "0";
$v7 = "abcde";
$v8 = array();
$v9 = array(1,2,3,4);
$v10 = "";

assert(boolval($v1) === false);
assert(boolval($v2) === true);
assert(boolval($v3) === true);
assert(boolval($v4) === true);
assert(boolval($v5) === true);
assert(boolval($v6) === false);
assert(boolval($v7) === true);
assert(boolval($v8) === false);
assert(boolval($v9) === true);
assert(boolval($v10) === false);
assert(boolval(intval($v2)) === true);
assert(boolval(floatval($v3)) === true);

function f1() {
	return 0;
}

function f2() {
	return 1;
}

function f3() {
	return 1.0;
}

function f4() {
	return "1";
}

function f5() {
	return "1.556";
}

function f6() {
	return "0";
}

function f7() {
	return "abcde";
}

function f8() {
	return array();
}

function f9() {
	return array(1,2,3,4);
}

function f10() {
	return "";
}

assert(boolval(f1()) === false);
assert(boolval(f2()) === true);
assert(boolval(f3()) === true);
assert(boolval(f4()) === true);
assert(boolval(f5()) === true);
assert(boolval(f6()) === false);
assert(boolval(f7()) === true);
assert(boolval(f8()) === false);
assert(boolval(f9()) === true);
assert(boolval(f10()) === false);


?>