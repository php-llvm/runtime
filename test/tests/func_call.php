<?

//--------------------------------------------------------------------
// call simple function
//--------------------------------------------------------------------
function fn() {
  echo "fn!\n";
}
fn();

//--------------------------------------------------------------------
// call simple function returns void
//--------------------------------------------------------------------
function fv(): void {
  echo "fv!\n";
}
echo fv();

//--------------------------------------------------------------------
// call conditional function
//--------------------------------------------------------------------
function fn2() {
  function cond() {
    echo "cond!\n";
  }
  cond();
}
fn2();

//--------------------------------------------------------------------
// call function with arguments
//--------------------------------------------------------------------
function arg1($a) {
  echo "arg1!\n";
}
arg1(1);

//--------------------------------------------------------------------
// call function with arguments with type hints
//--------------------------------------------------------------------
function arg2($a, int $b, bool $c, string $d, float $e) {
  echo "arg2!\n";
}
arg2(1, 2, true, "qwe", 123.456);

//--------------------------------------------------------------------
// call function with forward declaration
//--------------------------------------------------------------------
forward();
function forward() {
  echo "forward!\n";
}

//--------------------------------------------------------------------
// call function with forward declaration with type hints
//--------------------------------------------------------------------
forward_args(1, 2, true, "qwe", 123.456);
function forward_args($a, int $b, bool $c, string $d, float $e2) {
  echo "forward_args!\n";
}

//--------------------------------------------------------------------
// accessing function arguments
//--------------------------------------------------------------------
function fn_100(         $a) { return $a; }
function fn_101(bool     $a) { return $a; }
function fn_102(boolean  $a) { return $a; }
function fn_103(int      $a) { return $a; }
function fn_104(long     $a) { return $a; }
function fn_105(integer  $a) { return $a; }
function fn_106(float    $a) { return $a; }
function fn_107(real     $a) { return $a; }
function fn_108(double   $a) { return $a; }
function fn_109(string   $a) { return $a; }
function fn_110(binary   $a) { return $a; }
//function fn_111(array    $a) { return $a; }

function fn_200(         $a)          { return $a; }
function fn_201(bool     $a): bool    { return $a; }
function fn_202(boolean  $a): boolean { return $a; }
function fn_203(int      $a): int     { return $a; }
function fn_204(long     $a): long    { return $a; }
function fn_205(integer  $a): integer { return $a; }
function fn_206(float    $a): float   { return $a; }
function fn_207(real     $a): real    { return $a; }
function fn_208(double   $a): double  { return $a; }
function fn_209(string   $a): string  { return $a; }
function fn_210(binary   $a): binary  { return $a; }
//function fn_211(array    $a): array   { return $a; }

echo fn_100(123);   echo "\n";
echo fn_101(true);  echo "\n";
echo fn_102(false); echo "\n";
echo fn_103(123);   echo "\n";
echo fn_104(456);   echo "\n";
echo fn_105(789);   echo "\n";
echo fn_106(1.1);   echo "\n";
echo fn_107(2.2);   echo "\n";
echo fn_108(3.3);   echo "\n";
echo fn_109("qwe"); echo "\n";
echo fn_110("rty"); echo "\n";

echo fn_200(123);   echo "\n";
echo fn_201(true);  echo "\n";
echo fn_202(false); echo "\n";
echo fn_203(123);   echo "\n";
echo fn_204(456);   echo "\n";
echo fn_205(789);   echo "\n";
echo fn_206(1.1);   echo "\n";
echo fn_207(2.2);   echo "\n";
echo fn_208(3.3);   echo "\n";
echo fn_209("qwe"); echo "\n";
echo fn_210("rty"); echo "\n";


//--------------------------------------------------------------------
// use local variable
//--------------------------------------------------------------------
$a = 123;
function fn_local() {
  echo "local\n";
//TODO:  $a = "local";
//TODO:  echo $a, "\n";
}
fn_local();
echo $a; echo "\n";

?>