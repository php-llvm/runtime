<?php
assert(empty(false));
assert(empty(0));
assert(empty(0.0));
assert(empty("0"));
assert(empty(""));
assert(empty(null));
assert(empty(array()));
assert(!empty(true));
assert(!empty(1));
assert(!empty(1.1));
assert(!empty("A"));
assert(!empty("0.0"));
assert(!empty(array(1,2,3)));

$v1 = false;
$v2 = 0;
$v3 = 0.0;
$v4 = "0";
$v5 = "";
$v6 = null;
$v7 = array();
$v8 = true;
$v9 = 1;
$v10 = 1.1;
$v11 = "A";
$v12 = "0.0";
$v13 = array(1,2,3);

assert(empty($v1));
assert(empty($v2));
assert(empty($v3));
assert(empty($v4));
assert(empty($v5));
assert(empty($v6));
assert(empty($v7));
assert(!empty($v8));
assert(!empty($v9));
assert(!empty($v10));
assert(!empty($v11));
assert(!empty($v12));
assert(!empty($v13));
assert(!empty($v13[1]));
assert(empty($v13[33]));
assert(empty($v13["AAAA"]));

function f1() {
	return false;
}

function f2() {
	return 0;
}

function f3() {
	return 0.0;
}

function f4() {
	return "0";
}

function f5() {
	return "";
}

function f6() {
	return null;
}

function f7() {
	return array();
}

function f8() {
	return true;
}

function f9() {
	return 1;
}

function f10() {
	return 1.1;
}

function f11() {
	return "A";
}

function f12() {
	return "0.0";
}

function f13() {
	return array(1,2,3);
}

assert(empty(f1()));
assert(empty(f2()));
assert(empty(f3()));
assert(empty(f4()));
assert(empty(f5()));
assert(empty(f6()));
assert(empty(f7()));
assert(!empty(f8()));
assert(!empty(f9()));
assert(!empty(f10()));
assert(!empty(f11()));
assert(!empty(f12()));
assert(!empty(f13()));
?>