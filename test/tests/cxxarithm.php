<?
$eps = 0.0000000001;

$a = 1;
$b = 2;
assert($a + $b === 3);
$a = 1.4;
$b = 4.3;
assert(abs(($a + $b) - 5.7) < $eps);
$a = 1.0;
$b = 4.0;
assert(abs(($a + $b) - 5.0) < $eps);
$a = 1;
$b = 3.3;
assert(abs(($a + $b) - 4.3) < $eps);
$a = 3343545454545345345344634636.354545;
$b = 1;
assert(abs(($a + $b) - 3343545454545345345344634637.354545) < $eps);


$a = 1;
$b = 2;
assert($a - $b === -1);
$a = 1.4;
$b = 4.3;
assert(abs(($a - $b) - -2.9) < $eps);
$a = 1.0;
$b = 4.0;
assert(abs(($a - $b) - -3.0) < $eps);
$a = 1;
$b = 3.3;
assert(abs(($a - $b) - -2.3) < $eps);
$a = 3343545454545345345344634636.354545;
$b = 1;
assert(abs(($a - $b) - 3343545454545345345344634635.354545) < $eps);


//-------------------------------------------------------------------
// binary '*'
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
$a = false; $b = null   ; assert(($a * $b) === 0       );
$a = true ; $b = null   ; assert(($a * $b) === 0       );
$a = false; $b = false  ; assert(($a * $b) === 0       );
$a = false; $b = true   ; assert(($a * $b) === 0       );
$a = true ; $b = false  ; assert(($a * $b) === 0       );
$a = true ; $b = true   ; assert(($a * $b) === 1       );
$a = true ; $b = 123    ; assert(($a * $b) === 123     );
$a = false; $b = 123    ; assert(($a * $b) === 0       );
$a = true ; $b = 123.456; assert(($a * $b) === 123.456 );

$a = false; $b = 123.456; assert(($a * $b) === 0.0     );
$a = true ; $b = ''     ; assert(($a * $b) === 0       );
$a = false; $b = ''     ; assert(($a * $b) === 0       );
$a = true ; $b = "0"    ; assert(($a * $b) === 0       );
$a = false; $b = "0"    ; assert(($a * $b) === 0       );
$a = true ; $b = "qwe"  ; assert(($a * $b) === 0       );
$a = false; $b = "qwe"  ; assert(($a * $b) === 0       );

$a = null   ; $b = false; assert(($a * $b) === 0       );
$a = null   ; $b = true ; assert(($a * $b) === 0       );
$a = 123    ; $b = true ; assert(($a * $b) === 123     );
$a = 123    ; $b = false; assert(($a * $b) === 0       );
$a = 123.456; $b = true ; assert(($a * $b) === 123.456 );
$a = 123.456; $b = false; assert(($a * $b) === 0.0     );
$a = ''     ; $b = true ; assert(($a * $b) === 0       );
$a = ''     ; $b = false; assert(($a * $b) === 0       );
$a = "0"    ; $b = true ; assert(($a * $b) === 0       );
$a = "0"    ; $b = false; assert(($a * $b) === 0       );
$a = "qwe"  ; $b = true ; assert(($a * $b) === 0       );
$a = "qwe"  ; $b = false; assert(($a * $b) === 0       );


//----- null with types ---------------------------------------------
$a = null; $b = null   ; assert(($a * $b) === 0      );
$a = null; $b = ''     ; assert(($a * $b) === 0      );
$a = null; $b = "0"    ; assert(($a * $b) === 0      );
$a = null; $b = "qwe"  ; assert(($a * $b) === 0      );
$a = null; $b = "123"  ; assert(($a * $b) === 0      );
$a = null; $b = 123    ; assert(($a * $b) === 0      );
$a = null; $b = -123   ; assert(($a * $b) === 0      );
$a = null; $b = 123.123; assert(($a * $b) === 0.0    );

$a = ''     ; $b = null; assert(($a * $b) === 0      );
$a = "0"    ; $b = null; assert(($a * $b) === 0      );
$a = "qwe"  ; $b = null; assert(($a * $b) === 0      );
$a = "123"  ; $b = null; assert(($a * $b) === 0      );
$a = 123    ; $b = null; assert(($a * $b) === 0      );
$a = -123   ; $b = null; assert(($a * $b) === 0      );
$a = 123.123; $b = null; assert(($a * $b) === 0.0    );


//----- integer with types ------------------------------------------
$a = 123; $b = 123.4; assert(($a * $b) === 15178.2 );
$a = 123; $b = 456  ; assert(($a * $b) === 56088   );
$a = 456; $b = 123  ; assert(($a * $b) === 56088   );
$a = 123; $b = 123  ; assert(($a * $b) === 15129   );
$a = 123; $b = ''   ; assert(($a * $b) === 0       );
$a = 123; $b = "qwe"; assert(($a * $b) === 0       );
$a = 123; $b = "456"; assert(($a * $b) === 56088   );

$a = 123.4; $b = 123; assert(($a * $b) === 15178.2  );
$a = 456  ; $b = 123; assert(($a * $b) === 56088    );
$a = 123  ; $b = 456; assert(($a * $b) === 56088    );
$a = 123  ; $b = 123; assert(($a * $b) === 15129    );
$a = ''   ; $b = 123; assert(($a * $b) === 0        );
$a = "qwe"; $b = 123; assert(($a * $b) === 0        );
$a = "456"; $b = 123; assert(($a * $b) === 56088    );


//----- double with types -------------------------------------------
$a = 1.2; $b = 1.2  ; assert(($a * $b) === 1.44          );
$a = 1.3; $b = 1.2  ; assert(($a * $b) === 1.56          );
$a = 1.2; $b = 1.3  ; assert(($a * $b) === 1.56          );
$a = -.2; $b = ''   ; assert(($a * $b) === 0.0           );
$a = 1.2; $b = "qwe"; assert(($a * $b) === 0.0           );
$a = 1.2; $b = "1e5"; assert(($a * $b) === 120000.0      );

$a = ''   ; $b = -.2; assert(($a * $b) === 0.0           );
$a = "qwe"; $b = 1.2; assert(($a * $b) === 0.0           );
$a = "1e5"; $b = 1.2; assert(($a * $b) === 120000.0      );


//----- string with types -------------------------------------------
$a = "123"    ; $b = "0456"   ; assert(($a * $b) ===  56088  );
$a = "qwe"    ; $b = "0456"   ; assert(($a * $b) ===  0      );
$a = "0456"   ; $b = "qwe"    ; assert(($a * $b) ===  0      );
$a = "qwe"    ; $b = "qwe"    ; assert(($a * $b) ===  0      );
$a = "-123"   ; $b = ".123"   ; assert(($a * $b) === -15.129 );
$a = "+123"   ; $b = ".123"   ; assert(($a * $b) ===  15.129 );
$a = "+123"   ; $b = ' .123'  ; assert(($a * $b) ===  15.129 );
$a = ' +123'  ; $b = ".123"   ; assert(($a * $b) ===  15.129 );
$a = "+123asd"; $b = ".123qwe"; assert(($a * $b) ===  15.129 );

$a = "0456"   ; $b = "123"    ; assert(($a * $b) ===  56088   );
$a = "0456"   ; $b = "qwe"    ; assert(($a * $b) ===  0       );
$a = "qwe"    ; $b = "0456"   ; assert(($a * $b) ===  0       );
$a = "qwe"    ; $b = "qwe"    ; assert(($a * $b) ===  0       );
$a = ".123"   ; $b = "-123"   ; assert(($a * $b) === -15.129  );
$a = ".123"   ; $b = "+123"   ; assert(($a * $b) ===  15.129  );
$a = ' .123'  ; $b = "+123"   ; assert(($a * $b) ===  15.129  );
$a = ".123"   ; $b = ' +123'  ; assert(($a * $b) ===  15.129  );
$a = ".123qwe"; $b = "+123asd"; assert(($a * $b) ===  15.129  );










//-------------------------------------------------------------------
// binary '/'
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
$a = false; $b = true; assert(($a / $b) === 0);
$a = true ; $b = true; assert(($a / $b) === 1);
$a = true ; $b = 2   ; assert(($a / $b) === 0);
$a = false; $b = 123 ; assert(($a / $b) === 0);
$a = true ; $b = 4   ; assert(($a / $b) === 0);

$a = false; $b = 123.456; assert(($a / $b) === 0.0);

$a = null   ; $b = true; assert(($a / $b) === 0      );
$a = 123    ; $b = true; assert(($a / $b) === 123    );
$a = 123.456; $b = true; assert(($a / $b) === 123.456);
$a = ''     ; $b = true; assert(($a / $b) === 0      );
$a = "0"    ; $b = true; assert(($a / $b) === 0      );
$a = "qwe"  ; $b = true; assert(($a / $b) === 0      );


//----- null with types ---------------------------------------------
$a = null; $b = "123"  ; assert(($a / $b) === 0  );
$a = null; $b = 123    ; assert(($a / $b) === 0  );
$a = null; $b = -123   ; assert(($a / $b) === 0  );
$a = null; $b = 123.123; assert(($a / $b) === 0.0);


//----- integer with types ------------------------------------------
$a = 123; $b = 123.4; assert(abs(($a / $b) - 0.9967585089) < 1e-10 );
$a = 512; $b = 128;   assert(($a / $b) === 4);
$a = 128; $b = 512;   assert(($a / $b) === 0);
$a = 123; $b = 123;   assert(($a / $b) === 1);
$a = 123; $b = "456"; assert(abs(($a / $b) - 0.2697368421) < 1e-10 );

$a = 123.4; $b = 123; assert(abs(($a / $b) - 1.0032520325) < 1e-10 );
$a = ''    ; $b = 123; assert(($a / $b) === 0 );
$a = "qwe" ; $b = 123; assert(($a / $b) === 0 );
$a = "1024"; $b =  32; assert(($a / $b) === 32);


//----- double with types -------------------------------------------
$a = 1.2; $b = 1.2  ; assert(($a / $b) === 1.0     );
$a = 2.4; $b = 1.2  ; assert(($a / $b) === 2.0     );
$a = 1.2; $b = 2.4  ; assert(($a / $b) === 0.5     );
$a = 1.2; $b = "1e5"; assert(($a / $b) === 0.000012);

$a = ''   ; $b = -.2; assert(($a  / $b) === 0.0     );
$a = "qwe"; $b = 1.2; assert(($a  / $b) === 0.0     );
$a = "1e5"; $b = 0.2; assert(($a  / $b) === 500000.0);


//----- string with types -------------------------------------------
$a = "123"    ; $b = "246"    ; assert(($a / $b) ===  0.5   );
$a = "qwe"    ; $b = "0456"   ; assert(($a / $b) ===  0     );
$a = "-123"   ; $b = ".123"   ; assert(($a / $b) === -1000.0);
$a = "+123"   ; $b = ".123"   ; assert(($a / $b) ===  1000.0);
$a = "+123"   ; $b = ' .123'  ; assert(($a / $b) ===  1000.0);
$a = ' +123'  ; $b = ".123"   ; assert(($a / $b) ===  1000.0);
$a = "+123asd"; $b = ".123qwe"; assert(($a / $b) ===  1000.0);

$a = "246"    ; $b = "123"     ; assert(($a / $b) ===  2    );
$a = "qwe"    ; $b =  "0456"   ; assert(($a / $b) ===  0    );
$a = ".123"   ; $b =  "-123"   ; assert(($a / $b) === -0.001);
$a = ".123"   ; $b =  "+123"   ; assert(($a / $b) ===  0.001);
$a = ' .123'  ; $b =  "+123"   ; assert(($a / $b) ===  0.001);
$a = ".123"   ; $b =  ' +123'  ; assert(($a / $b) ===  0.001);
$a = ".123qwe"; $b =  "+123asd"; assert(($a / $b) ===  0.001);


?>