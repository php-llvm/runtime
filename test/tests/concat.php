<?php

// Short strings are immmediate.
$a = 'abc';
$b = '123';
$c = $a . $b;
assert($c === 'abc123');

$a = 'abc';
$b = '';
$c = $a . $b;
assert($c === 'abc');

$a = '';
$b = 'abc';
$c = $a . $b;
assert($c === 'abc');

$a = '';
$b = '';
$c = $a . $b;
assert($c === '');

// Result has boundary length.
$a = '12345678';
$b = '12';
$c = $a . $b;
assert($c === '1234567812');

$a = '12345678';
$b = '123';
$c = $a . $b;
assert($c === '12345678123');

$a = '12345678';
$b = '1234';
$c = $a . $b;
assert($c === '123456781234');

// Long strings are dynamic.
$a = 'abcdefghijklmnopqrstuvwxyz';
$b = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$c = $a . $b;
assert($c === 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

$a = 'abcdefghijklmnopqrstuvwxyz';
$b = '123456789';
$c = $a . $b;
assert($c === 'abcdefghijklmnopqrstuvwxyz123456789');

$a = 'abcdefghijklmnopqrstuvwxyz';
$b = '123456789';
$c = $b . $a;
assert($c === '123456789abcdefghijklmnopqrstuvwxyz');

$a = 'abcdefghijklmnopqrstuvwxyz';
$b = '';
$c = $a . $b;
assert($c === 'abcdefghijklmnopqrstuvwxyz');

$a = 'abcdefghijklmnopqrstuvwxyz';
$b = '';
$c = $b . $a;
assert($c === 'abcdefghijklmnopqrstuvwxyz');

// .=

$a = "123";
$a .= "abc";
assert($a === "123abc");

function conc_1(string &$x) {
  $x .= 'abc';
}
$a = '123';
conc_1($a);
assert($a === '123abc');

?>