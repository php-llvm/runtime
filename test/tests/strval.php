<?php
assert(strval(0) === "0");
assert(strval(1) === "1");
assert(strval(1.0) === "1");
assert(strval("1") === "1");
assert(strval("1.556") === "1.556");
assert(strval("0") === "0");
assert(strval("abcde") === "abcde");
assert(strval(array()) === "Array");
assert(strval(array(1,2,3,4)) === "Array");
assert(strval("") === "");


$v1 = 0;
$v2 = 1;
$v3 = 1.0;
$v4 = "1";
$v5 = "1.556";
$v6 = "0";
$v7 = "abcde";
$v8 = array();
$v9 = array(1,2,3,4);
$v10 = "";

assert(strval($v1) === "0");
assert(strval($v2) === "1");
assert(strval($v3) === "1");
assert(strval($v4) === "1");
assert(strval($v5) === "1.556");
assert(strval($v6) === "0");
assert(strval($v7) === "abcde");
assert(strval($v8) === "Array");
assert(strval($v9) === "Array");
assert(strval($v10) === "");

function f1() {
	return 0;
}

function f2() {
	return 1;
}

function f3() {
	return 1.0;
}

function f4() {
	return "1";
}

function f5() {
	return "1.556";
}

function f6() {
	return "0";
}

function f7() {
	return "abcde";
}

function f8() {
	return array();
}

function f9() {
	return array(1,2,3,4);
}

function f10() {
	return "";
}

assert(strval(f1()) === "0");
assert(strval(f2()) === "1");
assert(strval(f3()) === "1");
assert(strval(f4()) === "1");
assert(strval(f5()) === "1.556");
assert(strval(f6()) === "0");
assert(strval(f7()) === "abcde");
assert(strval(f8()) === "Array");
assert(strval(f9()) === "Array");
assert(strval(f10()) === "");

?>