<?php
$a = null;
settype($a, "integer");
assert(is_int($a));
assert($a === 0);
$a = null;
settype($a, "double");
assert(is_float($a));
assert($a === 0.0);
$a = null;
settype($a, "float");
assert(is_float($a));
assert($a === 0.0);
$a = null;
settype($a, "boolean");
assert(is_bool($a));
assert($a === false);
$a = null;
settype($a, "bool");
assert(is_bool($a));
assert($a === false);
$a = null;
settype($a, "null");
assert(is_null($a));
assert($a === null);


$integer = "integer";
$double = "double";
$float = "float";
$boolean = "boolean";
$bool = "bool";
//NOTE: can't name variable null, see issue https://bitbucket.org/php-llvm/phpfe/issues/13/null-as-keyword
$Null = "null"; 

$a = null;
settype($a, $integer);
assert(is_int($a));
assert($a === 0);
$a = null;
settype($a, $double);
assert(is_float($a));
assert($a === 0.0);
$a = null;
settype($a, $float);
assert(is_float($a));
assert($a === 0.0);
$a = null;
settype($a, $boolean);
assert(is_bool($a));
assert($a === false);
$a = null;
settype($a, $bool);
assert(is_bool($a));
assert($a === false);
$a = null;
settype($a, $Null);
assert(is_null($a));
assert($a === null);
?>