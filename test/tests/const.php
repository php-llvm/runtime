<?

namespace {
    assert(PHP_MAJOR_VERSION === 8);
    assert(PHP_MINOR_VERSION === 0);
    assert(PHP_RELEASE_VERSION === 0);
    assert(PHP_VERSION_ID === 80000);
    assert(PHP_EXTRA_VERSION === "-phlang");
    assert(PHP_VERSION === "8.0.0-phlang");

    assert(is_integer(PHP_MAJOR_VERSION));
    assert(is_integer(PHP_MINOR_VERSION));
    assert(is_integer(PHP_RELEASE_VERSION));
    assert(is_integer(PHP_VERSION_ID));
    assert(is_string(PHP_EXTRA_VERSION));
    assert(is_string(PHP_VERSION));

    $a = PHP_MAJOR_VERSION;     assert($a === 8);
    $a = PHP_MINOR_VERSION;     assert($a === 0);
    $a = PHP_RELEASE_VERSION;   assert($a === 0);
    $a = PHP_VERSION_ID;        assert($a === 80000);
    $a = PHP_EXTRA_VERSION;     assert($a === "-phlang");
    $a = PHP_VERSION;           assert($a === "8.0.0-phlang");
}


namespace using_constants_from_global_namespace {
    assert(PHP_MAJOR_VERSION === 8);
    assert(PHP_MINOR_VERSION === 0);
    assert(PHP_RELEASE_VERSION === 0);
    assert(PHP_VERSION_ID === 80000);
    assert(PHP_EXTRA_VERSION === "-phlang");
    assert(PHP_VERSION === "8.0.0-phlang");

    assert(is_integer(PHP_MAJOR_VERSION));
    assert(is_integer(PHP_MINOR_VERSION));
    assert(is_integer(PHP_RELEASE_VERSION));
    assert(is_integer(PHP_VERSION_ID));
    assert(is_string(PHP_EXTRA_VERSION));
    assert(is_string(PHP_VERSION));

    $a = PHP_MAJOR_VERSION;     assert($a === 8);
    $a = PHP_MINOR_VERSION;     assert($a === 0);
    $a = PHP_RELEASE_VERSION;   assert($a === 0);
    $a = PHP_VERSION_ID;        assert($a === 80000);
    $a = PHP_EXTRA_VERSION;     assert($a === "-phlang");
    $a = PHP_VERSION;           assert($a === "8.0.0-phlang");
}


namespace shadow_global_constants {
    const PHP_MAJOR_VERSION = 123;
    const PHP_MINOR_VERSION = 321;
    const PHP_RELEASE_VERSION = 'qwe';
    const PHP_VERSION_ID = null;
    const PHP_EXTRA_VERSION = 3.14;

    assert(PHP_MAJOR_VERSION === 123);
    assert(PHP_MINOR_VERSION === 321);
    assert(PHP_RELEASE_VERSION === 'qwe');
    assert(PHP_VERSION_ID === null);
    assert(PHP_EXTRA_VERSION === 3.14);
    assert(PHP_VERSION === "8.0.0-phlang");

    assert(is_integer(PHP_MAJOR_VERSION));
    assert(is_integer(PHP_MINOR_VERSION));
    assert(is_string(PHP_RELEASE_VERSION));
    assert(is_null(PHP_VERSION_ID));
    assert(is_float(PHP_EXTRA_VERSION));
    assert(is_string(PHP_VERSION));

    $a = PHP_MAJOR_VERSION;     assert($a === 123);
    $a = PHP_MINOR_VERSION;     assert($a === 321);
    $a = PHP_RELEASE_VERSION;   assert($a === 'qwe');
    $a = PHP_VERSION_ID;        assert($a === null);
    $a = PHP_EXTRA_VERSION;     assert($a === 3.14);
    $a = PHP_VERSION;           assert($a === "8.0.0-phlang");
}


namespace explicitly_use_global_constant {
    const PHP_MAJOR_VERSION = 123;
    const PHP_MINOR_VERSION = 321;
    const PHP_RELEASE_VERSION = 'qwe';
    const PHP_VERSION_ID = null;
    const PHP_EXTRA_VERSION = 3.14;

    assert(\PHP_MAJOR_VERSION === 8);
    assert(\PHP_MINOR_VERSION === 0);
    assert(\PHP_RELEASE_VERSION === 0);
    assert(\PHP_VERSION_ID === 80000);
    assert(\PHP_EXTRA_VERSION === '-phlang');
    assert(\PHP_VERSION === "8.0.0-phlang");

    assert(is_integer(\PHP_MAJOR_VERSION));
    assert(is_integer(\PHP_MINOR_VERSION));
    assert(is_integer(\PHP_RELEASE_VERSION));
    assert(is_integer(\PHP_VERSION_ID));
    assert(is_string(\PHP_EXTRA_VERSION));
    assert(is_string(\PHP_VERSION));

    $a = \PHP_MAJOR_VERSION;    assert($a === 8);
    $a = \PHP_MINOR_VERSION;    assert($a === 0);
    $a = \PHP_RELEASE_VERSION;  assert($a === 0);
    $a = \PHP_VERSION_ID;       assert($a === 80000);
    $a = \PHP_EXTRA_VERSION;    assert($a === '-phlang');
    $a = \PHP_VERSION;          assert($a === "8.0.0-phlang");
}


namespace shadow_global_constants_new_cs {
    const PHP_MAJOR_VERSION = 123;
    const PHP_MINOR_VERSION = 321;
    const PHP_RELEASE_VERSION = 'qwe';
    const PHP_VERSION_ID = null;
    const PHP_EXTRA_VERSION = 3.14;
}

namespace shadow_global_constants_new_cs {
    assert(PHP_MAJOR_VERSION === 123);
    assert(PHP_MINOR_VERSION === 321);
    assert(PHP_RELEASE_VERSION === 'qwe');
    assert(PHP_VERSION_ID === null);
    assert(PHP_EXTRA_VERSION === 3.14);
    assert(PHP_VERSION === "8.0.0-phlang");

    assert(is_integer(PHP_MAJOR_VERSION));
    assert(is_integer(PHP_MINOR_VERSION));
    assert(is_string(PHP_RELEASE_VERSION));
    assert(is_null(PHP_VERSION_ID));
    assert(is_float(PHP_EXTRA_VERSION));
    assert(is_string(PHP_VERSION));

    $a = PHP_MAJOR_VERSION;     assert($a === 123);
    $a = PHP_MINOR_VERSION;     assert($a === 321);
    $a = PHP_RELEASE_VERSION;   assert($a === 'qwe');
    $a = PHP_VERSION_ID;        assert($a === null);
    $a = PHP_EXTRA_VERSION;     assert($a === 3.14);
    $a = PHP_VERSION;           assert($a === "8.0.0-phlang");
}
?>