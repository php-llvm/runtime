<?

function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }

//--------------------------------------------------------------------
// builtin max, min
//--------------------------------------------------------------------
echo max(1, 2);
echo "\n";

echo max(1, 0);
echo "\n";

echo min(1, 2);
echo "\n";

echo min(1, 0);
echo "\n";

echo max(1, 2, 3, 4, 5);
echo "\n";

echo min(1, 2, 3, 4, 5);
echo "\n";

echo max(true, 123);
echo max(false, -123);
echo max(10, "123");
echo "\n";

echo min(true, 123);
echo min(false, 123);
echo min(10, "123");
echo "\n";

//--------------------------------------------------------------------
// builtin ceil
//--------------------------------------------------------------------

$a = 123;
assert(ceil($a) === 123.0);
$a = 1.1;
assert(ceil($a) === 2.0);
$a = 1.5;
assert(ceil($a) === 2.0);
$a = 1.8;
assert(ceil($a) === 2.0);

$a = -1.1;
assert(ceil($a) === -1.0);
$a = -1.5;
assert(ceil($a) === -1.0);
$a = -1.8;
assert(ceil($a) === -1.0);

$a = null;
assert(ceil($a) === 0.0);
$a = "qwe";
assert(ceil($a) === 0.0);
$a = "123";
assert(ceil($a) === 123.0);
$a = "1.12345e2";
assert(ceil($a) === 113.0);
$a = true;
assert(ceil($a) === 1.0);
$a = false;
assert(ceil($a) === 0.0);


assert(ceil(get_bool()) === 1.0);
assert(ceil(get_int()) === 123.0);
assert(ceil(get_double()) === 13.0);
assert(ceil(get_string()) === 0.0);


//--------------------------------------------------------------------
// builtin floor
//--------------------------------------------------------------------

$a = 123;
assert(floor($a) === 123.0);
$a = 1.1;
assert(floor($a) === 1.0);
$a = 1.5;
assert(floor($a) === 1.0);
$a = 1.8;
assert(floor($a) === 1.0);

$a = -1.1;
assert(floor($a) === -2.0);
$a = -1.5;
assert(floor($a) === -2.0);
$a = -1.8;
assert(floor($a) === -2.0);

$a = null;
assert(floor($a) === 0.0);
$a = "qwe";
assert(floor($a) === 0.0);
$a = "123";
assert(floor($a) === 123.0);
$a = "1.12345e2";
assert(floor($a) === 112.0);
$a = true;
assert(floor($a) === 1.0);
$a = false;
assert(floor($a) === 0.0);


assert(floor(get_bool()) === 1.0);
assert(floor(get_int()) === 123.0);
assert(floor(get_double()) === 12.0);
assert(floor(get_string()) === 0.0);

//-------------------------------------------------------------------
// round with defaults
//-------------------------------------------------------------------
$a =  2.375; assert(round($a) ===  2.0);
$a = -2.375; assert(round($a) === -2.0);

$a =  2.5; assert(round($a) ===  3.0);
$a = -2.5; assert(round($a) === -3.0);

$a =  2.825; assert(round($a) ===  3.0);
$a = -2.825; assert(round($a) === -3.0);

assert(round(get_int()) === 123.0); // expected-warning{{argument is already integer}}
//-------------------------------------------------------------------
// round with precision
//-------------------------------------------------------------------
assert(round(get_int(),  123) === 123.0);
assert(round(get_int(), -123) === 0.0);

$a =  2.375; assert(round($a, 1) ===  2.4);
$a = -2.375; assert(round($a, 1) === -2.4);

$a =  2.5; assert(round($a, 1) ===  2.5);
$a = -2.5; assert(round($a, 1) === -2.5);

$a =  2.825; assert(round($a, 1) ===  2.8);
$a = -2.825; assert(round($a, 1) === -2.8);


$a =  2.825; assert(round($a, -2) ===  0.0);
$a = -2.825; assert(round($a, -2) === -0.0);

$a =  282.5; assert(round($a, -2) ===  300.0);
$a = -282.5; assert(round($a, -2) === -300.0);


$a =  2.5555555555555555555555555555555555555555; assert(round($a, 30) ===  2.555555555555555555555555555556);
$a = -2.5555555555555555555555555555555555555555; assert(round($a, 30) === -2.555555555555555555555555555556);

$a = 2.8250123654789632147851598753214567896541; assert(round($a, 30) === 2.825012365478963214785159875321);
$a = -2.8250123654789632147851598753214567896541; assert(round($a, 30) === -2.825012365478963214785159875321);

//-------------------------------------------------------------------
// round with modes
//-------------------------------------------------------------------

$a =  2.372; assert(round($a, 2, 123) ===  2.37); // expected-warning{{invalid rounding mode, using PHP_ROUND_HALF_UP insted}}

assert(PHP_ROUND_HALF_UP   === 1);
assert(PHP_ROUND_HALF_DOWN === 2);
assert(PHP_ROUND_HALF_EVEN === 3);
assert(PHP_ROUND_HALF_ODD  === 4);

// PHP_ROUND_HALF_UP    = 1
$a =  2.375; assert(round($a, 0, 1) ===  2.0);
$a = -2.375; assert(round($a, 0, 1) === -2.0);

$a =  2.5; assert(round($a, 0, 1) ===  3.0);
$a = -2.5; assert(round($a, 0, 1) === -3.0);

$a =  2.825; assert(round($a, 0, 1) ===  3.0);
$a = -2.825; assert(round($a, 0, 1) === -3.0);

$a =  5.375; assert(round($a, 0, 1) ===  5.0);
$a = -5.375; assert(round($a, 0, 1) === -5.0);

$a =  5.5; assert(round($a, 0, 1) ===  6.0);
$a = -5.5; assert(round($a, 0, 1) === -6.0);

$a =  5.825; assert(round($a, 0, 1) ===  6.0);
$a = -5.825; assert(round($a, 0, 1) === -6.0);

// PHP_ROUND_HALF_DOWN  = 2
$a =  2.375; assert(round($a, 0, 2) ===  2.0);
$a = -2.375; assert(round($a, 0, 2) === -2.0);

$a =  2.5; assert(round($a, 0, 2) ===  2.0);
$a = -2.5; assert(round($a, 0, 2) === -2.0);

$a =  2.825; assert(round($a, 0, 2) ===  3.0);
$a = -2.825; assert(round($a, 0, 2) === -3.0);

$a =  5.375; assert(round($a, 0, 2) ===  5.0);
$a = -5.375; assert(round($a, 0, 2) === -5.0);

$a =  5.5; assert(round($a, 0, 2) ===  5.0);
$a = -5.5; assert(round($a, 0, 2) === -5.0);

$a =  5.825; assert(round($a, 0, 2) ===  6.0);
$a = -5.825; assert(round($a, 0, 2) === -6.0);

// PHP_ROUND_HALF_EVEN  = 3
$a =  2.375; assert(round($a, 0, 3) ===  2.0);
$a = -2.375; assert(round($a, 0, 3) === -2.0);

$a =  2.5; assert(round($a, 0, 3) ===  2.0);
$a = -2.5; assert(round($a, 0, 3) === -2.0);

$a =  2.825; assert(round($a, 0, 3) ===  3.0);
$a = -2.825; assert(round($a, 0, 3) === -3.0);

$a =  5.375; assert(round($a, 0, 3) ===  5.0);
$a = -5.375; assert(round($a, 0, 3) === -5.0);

$a =  5.5; assert(round($a, 0, 3) ===  6.0);
$a = -5.5; assert(round($a, 0, 3) === -6.0);

$a =  5.825; assert(round($a, 0, 3) ===  6.0);
$a = -5.825; assert(round($a, 0, 3) === -6.0);

// PHP_ROUND_HALF_ODD   = 4
$a =  2.375; assert(round($a, 0, 4) ===  2.0);
$a = -2.375; assert(round($a, 0, 4) === -2.0);

$a =  2.5; assert(round($a, 0, 4) ===  3.0);
$a = -2.5; assert(round($a, 0, 4) === -3.0);

$a =  2.825; assert(round($a, 0, 4) ===  3.0);
$a = -2.825; assert(round($a, 0, 4) === -3.0);

$a =  5.375; assert(round($a, 0, 4) ===  5.0);
$a = -5.375; assert(round($a, 0, 4) === -5.0);

$a =  5.5; assert(round($a, 0, 4) ===  5.0);
$a = -5.5; assert(round($a, 0, 4) === -5.0);

$a =  5.825; assert(round($a, 0, 4) ===  6.0);
$a = -5.825; assert(round($a, 0, 4) === -6.0);

//-------------------------------------------------------------------
// round with precision and modes
//-------------------------------------------------------------------

// PHP_ROUND_HALF_UP    = 1
$a =  2.372; assert(round($a, 2, PHP_ROUND_HALF_UP) ===  2.37);
$a = -2.372; assert(round($a, 2, PHP_ROUND_HALF_UP) === -2.37);

$a =  2.575; assert(round($a, 2, PHP_ROUND_HALF_UP) ===  2.58);
$a = -2.575; assert(round($a, 2, PHP_ROUND_HALF_UP) === -2.58);

$a =  2.877; assert(round($a, 2, PHP_ROUND_HALF_UP) ===  2.88);
$a = -2.877; assert(round($a, 2, PHP_ROUND_HALF_UP) === -2.88);


$a =  2.322; assert(round($a, 2, PHP_ROUND_HALF_UP) ===  2.32);
$a = -2.322; assert(round($a, 2, PHP_ROUND_HALF_UP) === -2.32);

$a =  2.525; assert(round($a, 2, PHP_ROUND_HALF_UP) ===  2.53);
$a = -2.525; assert(round($a, 2, PHP_ROUND_HALF_UP) === -2.53);

$a =  2.827; assert(round($a, 2, PHP_ROUND_HALF_UP) ===  2.83);
$a = -2.827; assert(round($a, 2, PHP_ROUND_HALF_UP) === -2.83);


// PHP_ROUND_HALF_DOWN  = 2
$a =  2.372; assert(round($a, 2, PHP_ROUND_HALF_DOWN) ===  2.37);
$a = -2.372; assert(round($a, 2, PHP_ROUND_HALF_DOWN) === -2.37);

$a =  2.575; assert(round($a, 2, PHP_ROUND_HALF_DOWN) ===  2.57);
$a = -2.575; assert(round($a, 2, PHP_ROUND_HALF_DOWN) === -2.57);

$a =  2.877; assert(round($a, 2, PHP_ROUND_HALF_DOWN) ===  2.88);
$a = -2.877; assert(round($a, 2, PHP_ROUND_HALF_DOWN) === -2.88);


$a =  2.322; assert(round($a, 2, PHP_ROUND_HALF_DOWN) ===  2.32);
$a = -2.322; assert(round($a, 2, PHP_ROUND_HALF_DOWN) === -2.32);

$a =  2.525; assert(round($a, 2, PHP_ROUND_HALF_DOWN) ===  2.52);
$a = -2.525; assert(round($a, 2, PHP_ROUND_HALF_DOWN) === -2.52);

$a =  2.827; assert(round($a, 2, PHP_ROUND_HALF_DOWN) ===  2.83);
$a = -2.827; assert(round($a, 2, PHP_ROUND_HALF_DOWN) === -2.83);

// PHP_ROUND_HALF_EVEN  = 3
$a =  2.372; assert(round($a, 2, PHP_ROUND_HALF_EVEN) ===  2.37);
$a = -2.372; assert(round($a, 2, PHP_ROUND_HALF_EVEN) === -2.37);

$a =  2.575; assert(round($a, 2, PHP_ROUND_HALF_EVEN) ===  2.58);
$a = -2.575; assert(round($a, 2, PHP_ROUND_HALF_EVEN) === -2.58);

$a =  2.877; assert(round($a, 2, PHP_ROUND_HALF_EVEN) ===  2.88);
$a = -2.877; assert(round($a, 2, PHP_ROUND_HALF_EVEN) === -2.88);


$a =  2.322; assert(round($a, 2, PHP_ROUND_HALF_EVEN) ===  2.32);
$a = -2.322; assert(round($a, 2, PHP_ROUND_HALF_EVEN) === -2.32);

$a =  2.525; assert(round($a, 2, PHP_ROUND_HALF_EVEN) ===  2.52);
$a = -2.525; assert(round($a, 2, PHP_ROUND_HALF_EVEN) === -2.52);

$a =  2.827; assert(round($a, 2, PHP_ROUND_HALF_EVEN) ===  2.83);
$a = -2.827; assert(round($a, 2, PHP_ROUND_HALF_EVEN) === -2.83);

// PHP_ROUND_HALF_ODD   = 4
$a =  2.372; assert(round($a, 2, PHP_ROUND_HALF_ODD) ===  2.37);
$a = -2.372; assert(round($a, 2, PHP_ROUND_HALF_ODD) === -2.37);

$a =  2.575; assert(round($a, 2, PHP_ROUND_HALF_ODD) ===  2.57);
$a = -2.575; assert(round($a, 2, PHP_ROUND_HALF_ODD) === -2.57);

$a =  2.877; assert(round($a, 2, PHP_ROUND_HALF_ODD) ===  2.88);
$a = -2.877; assert(round($a, 2, PHP_ROUND_HALF_ODD) === -2.88);


$a =  2.322; assert(round($a, 2, PHP_ROUND_HALF_ODD) ===  2.32);
$a = -2.322; assert(round($a, 2, PHP_ROUND_HALF_ODD) === -2.32);

$a =  2.525; assert(round($a, 2, PHP_ROUND_HALF_ODD) ===  2.53);
$a = -2.525; assert(round($a, 2, PHP_ROUND_HALF_ODD) === -2.53);

$a =  2.827; assert(round($a, 2, PHP_ROUND_HALF_ODD) ===  2.83);
$a = -2.827; assert(round($a, 2, PHP_ROUND_HALF_ODD) === -2.83);



//-------------------------------------------------------------------
// builtin fmod
//-------------------------------------------------------------------
$a =  1; assert(fmod($a, 2) ===  1.0);
$a =  3; assert(fmod($a, 5) ===  3.0);
$a =  6; assert(fmod($a, 5) ===  1.0);
$a = -7; assert(fmod($a, 5) === -2.0);
$a = 15; assert(fmod($a, 1) ===  0.0);

$a = 15.456; assert(abs(fmod($a, 2) - 1.456) < 1e-10);
$a = 15.456; assert(abs(fmod($a, 2.5) - 0.456) < 1e-10);
$a = 15.456; assert(abs(fmod($a, 2.12) - 0.616) < 1e-10);
$a = 15.456; assert(abs(fmod($a, 2.91) - 0.906) < 1e-10);

$a = true; assert(fmod($a, 2) ===  1.0);
$a = false; assert(fmod($a, 5) ===  0.0);
$a = "123"; assert(fmod($a, 5) ===  3.0);

//-------------------------------------------------------------------
// builtin intdiv
//-------------------------------------------------------------------
$a =  1; assert(intdiv($a, 2) ===  0);
$a = 15; assert(intdiv($a, 5) ===  3);
$a =  6; assert(intdiv($a, 5) ===  1);
$a = -7; assert(intdiv($a, 5) === -1);
$a = 15; assert(intdiv($a, 1) === 15);

$a = 15.456; assert(intdiv($a, 2) === 7);
$a = 15.456; assert(intdiv($a, 2.5) === 7);

$a = true; assert(intdiv($a, 2) ===  0);
$a = false; assert(intdiv($a, 5) ===  0);
$a = "123"; assert(intdiv($a, 5) ===  24);

?>