<?php
assert(floatval(0) === 0.0);
assert(floatval(1) === 1.0);
assert(floatval(1.0) === 1.0);
assert(floatval("1") === 1.0);
assert(floatval("1.556") === 1.556);
assert(floatval("0") === 0.0);
assert(floatval("abcde") === 0.0);
assert(floatval(array()) === 0.0);
assert(floatval(array(1,2,3,4)) === 1.0);

$v1 = 0;
$v2 = 1;
$v3 = 1.0;
$v4 = "1";
$v5 = "1.556";
$v6 = "0";
$v7 = "abcde";
$v8 = array();
$v9 = array(1,2,3,4);

assert(floatval($v1) === 0.0);
assert(floatval($v2) === 1.0);
assert(floatval($v3) === 1.0);
assert(floatval($v4) === 1.0);
assert(floatval($v5) === 1.556);
assert(floatval($v6) === 0.0);
assert(floatval($v7) === 0.0);
assert(floatval($v8) === 0.0);
assert(floatval($v9) === 1.0);
assert(floatval(intval($v2)) === 1.0);

function f1() {
	return 0;
}

function f2() {
	return 1;
}

function f3() {
	return 1.0;
}

function f4() {
	return "1";
}

function f5() {
	return "1.556";
}

function f6() {
	return "0";
}

function f7() {
	return "abcde";
}

function f8() {
	return array();
}

function f9() {
	return array(1,2,3,4);
}

assert(floatval(f1()) === 0.0);
assert(floatval(f2()) === 1.0);
assert(floatval(f3()) === 1.0);
assert(floatval(f4()) === 1.0);
assert(floatval(f5()) === 1.556);
assert(floatval(f6()) === 0.0);
assert(floatval(f7()) === 0.0);
assert(floatval(f8()) === 0.0);
assert(floatval(f9()) === 1.0);

?>