<?

//--------------------------------------------------------------------
// return values
//--------------------------------------------------------------------
function fn00() {
  return;
}
echo fn00();
echo "\n";

function fn01() {
  return null;
}
echo fn01();
echo "\n";

function fn02() {
  return true;
}
echo fn02();
echo "\n";

function fn03() {
  return 123;
}
echo fn03();
echo "\n";

function fn04() {
  return 123.456;
}
echo fn04();
echo "\n";

function fn05() {
  return "qwe";
}
echo fn05();
echo "\n";

//--------------------------------------------------------------------
// return type hints
//--------------------------------------------------------------------

function fr01(): bool {
  return true;
}
echo fr01();
echo "\n";

function fr02(): boolean {
  return false;
}
echo fr02();
echo "\n";

function fr03(): int {
  return 123;
}
echo fr03();
echo "\n";

function fr04(): integer {
  return 456;
}
echo fr04();
echo "\n";

function fr05(): real {
  return 1.2;
}
echo fr05();
echo "\n";

function fr06(): float {
  return 3.4;
}
echo fr06();
echo "\n";

function fr07(): double {
  return 5.6;
}
echo fr07();
echo "\n";

function fr08(): string {
  return "qwe";
}
echo fr08();
echo "\n";

function fr09(): binary {
  return "rty";
}
echo fr09();
echo "\n";

//--------------------------------------------------------------------

function box($a) {
	echo "boxed!\n";
}

//--------------------------------------------------------------------
// box constructor call
//--------------------------------------------------------------------

box(fr01());
box(fr02());
box(fr03());
box(fr04());
box(fr05());
box(fr06());
box(fr07());

//--------------------------------------------------------------------
// cast to box type
//--------------------------------------------------------------------

box(fr08());
box(fr09());


?>