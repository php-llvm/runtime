<?php

// No conversion

$a = true;
$b = true;
assert(($a && $b) === true);
assert(($a || $b) === true);

$a = true;
$b = false;
assert(($a && $b) === false);
assert(($a || $b) === true);

$a = false;
$b = true;
assert(($a && $b) === false);
assert(($a || $b) === true);

$a = false;
$b = false;
assert(($a && $b) === false);
assert(($a || $b) === false);


// Conversion of one argument

$a = 66;
$b = true;
assert($a == true);
assert(($a && $b) === true);
assert(($a || $b) === true);

$a = 66;
$b = false;
assert(($a && $b) === false);
assert(($a || $b) === true);


$a = 0.1;
$b = true;
assert($a == true);
assert(($a && $b) === true);
assert(($a || $b) === true);

$a = 0.1;
$b = false;
assert(($a && $b) === false);
assert(($a || $b) === true);


$a = "abc";
$b = true;
assert($a == true);
assert(($a && $b) === true);
assert(($a || $b) === true);

$a = "abc";
$b = false;
assert(($a && $b) === false);
assert(($a || $b) === true);


$a = [1];
$b = true;
assert($a == true);
assert(($a && $b) === true);
assert(($a || $b) === true);

$a = [1];
$b = false;
assert(($a && $b) === false);
assert(($a || $b) === true);


$a = [0];
$b = true;
assert($a == true);
assert(($a && $b) === true);
assert(($a || $b) === true);

$a = [0];
$b = false;
assert(($a && $b) === false);
assert(($a || $b) === true);


$a = "0.0";
$b = true;
assert($a == true);
assert(($a && $b) === true);
assert(($a || $b) === true);

$a = "0.0";
$b = false;
assert(($a && $b) === false);
assert(($a || $b) === true);


$a = "";
$b = true;
assert($a == false);
assert(($a && $b) === false);
assert(($a || $b) === true);

$a = "";
$b = false;
assert(($a && $b) === false);
assert(($a || $b) === false);


$a = "0";
$b = true;
assert($a == false);
assert(($a && $b) === false);
assert(($a || $b) === true);

$a = "0";
$b = false;
assert($a == false);
assert(($a && $b) === false);
assert(($a || $b) === false);


$a = array();
$b = true;
assert($a == false);
assert(($a && $b) === false);
assert(($a || $b) === true);

$a = array();
$b = false;
assert($a == false);
assert(($a && $b) === false);
assert(($a || $b) === false);


$a = null;
$b = true;
assert($a == false);
assert(($a && $b) === false);
assert(($a || $b) === true);

$a = null;
$b = false;
assert($a == false);
assert(($a && $b) === false);
assert(($a || $b) === false);


// Conversion of both arguments

$a = 11;
$b = 66;
assert(($a && $b) === true);
assert(($a || $b) === true);

$a = [1];
$b = 22;
assert(($a && $b) === true);
assert(($a || $b) === true);

$a = [1];
$b = [];
assert(($a && $b) === false);
assert(($a || $b) === true);

$a = "abc";
$b = "";
assert(($a && $b) === false);
assert(($a || $b) === true);
