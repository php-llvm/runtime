<?php

//-------------------------------------------------------------------
// cast with function type hints
//-------------------------------------------------------------------
function fn_01(): bool {
  echo "function fn_01 called!\n";
  return true;    
}

assert(((unset)fn_01()) === null);
assert(((bool)fn_01()) === true);
assert(((boolean)fn_01()) === true);
assert(((int)fn_01()) === 1);
assert(((integer)fn_01()) === 1);
assert(((real)fn_01()) === 1.0);
assert(((float)fn_01()) === 1.0);
assert(((double)fn_01()) === 1.0);
assert(((string)fn_01()) === '1');
assert(((binary)fn_01()) === '1');
//assert(((array)fn_01()) === [true]);
//var_dump((object)fn_01());


function fn_02(): int { 
  echo "function fn_02 called!\n";
  return 123;
}

assert(((unset)fn_02()) === null);
assert(((bool)fn_02()) === true);
assert(((boolean)fn_02()) === true);
assert(((int)fn_02()) === 123);
assert(((integer)fn_02()) === 123);
assert(((real)fn_02()) === 123.0);
assert(((float)fn_02()) === 123.0);
assert(((double)fn_02()) === 123.0);
assert(((string)fn_02()) === '123');
assert(((binary)fn_02()) === '123');
//assert(((array)fn_02()) === [123]);
//var_dump((object)fn_02());


function fn_03(): float { 
  echo "function fn_03 called!\n";
  return 123.456; 
}

assert(((unset)fn_03()) === null);
assert(((bool)fn_03()) === true);
assert(((boolean)fn_03()) === true);
assert(((int)fn_03()) === 123);
assert(((integer)fn_03()) === 123);
assert(((real)fn_03()) === 123.456);
assert(((float)fn_03()) === 123.456);
assert(((double)fn_03()) === 123.456);
assert(((string)fn_03()) === '123.456');
assert(((binary)fn_03()) === '123.456');
//assert(((array)fn_03()) === [123.456]);
//var_dump((object)fn_03());


function fn_04(): string { 
  echo "function fn_04 called!\n";
  return "qwe";
}

assert(((unset)fn_04()) === null);
assert(((bool)fn_04()) === true);
assert(((boolean)fn_04()) === true);
assert(((int)fn_04()) === 0);
assert(((integer)fn_04()) === 0);
assert(((real)fn_04()) === 0.0);
assert(((float)fn_04()) === 0.0);
assert(((double)fn_04()) === 0.0);
assert(((string)fn_04()) === 'qwe');
assert(((binary)fn_04()) === 'qwe');
//assert(((array)fn_04()) === ['qwe']);
//var_dump((object)fn_04());


function fn_05(): array  { 
  echo "function fn_05 called!\n";
  return [1,2,3];
}

assert(((unset)fn_05()) === null);
assert(((bool)fn_05()) === true);
assert(((boolean)fn_05()) === true);
assert(((int)fn_05()) === 1);
assert(((integer)fn_05()) === 1);
assert(((real)fn_05()) === 1.0);
assert(((float)fn_05()) === 1.0);
assert(((double)fn_05()) === 1.0);
assert(((string)fn_05()) === 'Array');
assert(((binary)fn_05()) === 'Array');
//assert(((array)fn_05()) === [1,2,3]);
//var_dump((object)fn_05());


/* TODO:
function &fn_11(): bool {
  echo "function fn_11 called!\n";
  $a =  true;
  return $a;
}

assert(((unset)fn_11()) === null);
assert(((bool)fn_11()) === true);
assert(((boolean)fn_11()) === true);
assert(((int)fn_11()) === 1);
assert(((integer)fn_11()) === 1);
assert(((real)fn_11()) === 1.0);
assert(((float)fn_11()) === 1.0);
assert(((double)fn_11()) === 1.0);
assert(((string)fn_11()) === '1');
assert(((binary)fn_11()) === '1');
//assert(((array)fn_11()) === [true]);
//var_dump((object)fn_11());


function &fn_12(): int { 
  echo "function fn_12 called!\n";
  $a =  123;
  return $a;
}

assert(((unset)fn_12()) === null);
assert(((bool)fn_12()) === true);
assert(((boolean)fn_12()) === true);
assert(((int)fn_12()) === 123);
assert(((integer)fn_12()) === 123);
assert(((real)fn_12()) === 123.0);
assert(((float)fn_12()) === 123.0);
assert(((double)fn_12()) === 123.0);
assert(((string)fn_12()) === '123');
assert(((binary)fn_12()) === '123');
//assert(((array)fn_12()) === [123]);
//var_dump((object)fn_12());


function &fn_13(): float { 
  echo "function fn_13 called!\n";
  $a =  123.456; 
  return $a;
}

assert(((unset)fn_13()) === null);
assert(((bool)fn_13()) === true);
assert(((boolean)fn_13()) === true);
assert(((int)fn_13()) === 123);
assert(((integer)fn_13()) === 123);
assert(((real)fn_13()) === 123.456);
assert(((float)fn_13()) === 123.456);
assert(((double)fn_13()) === 123.456);
assert(((string)fn_13()) === '123.456');
assert(((binary)fn_13()) === '123.456');
//assert(((array)fn_13()) === [123.456]);
//var_dump((object)fn_13());


function &fn_14(): string { 
  echo "function fn_14 called!\n";
  $a =  "qwe";
  return $a;
}

assert(((unset)fn_14()) === null);
assert(((bool)fn_14()) === true);
assert(((boolean)fn_14()) === true);
assert(((int)fn_14()) === 0);
assert(((integer)fn_14()) === 0);
assert(((real)fn_14()) === 0.0);
assert(((float)fn_14()) === 0.0);
assert(((double)fn_14()) === 0.0);
assert(((string)fn_14()) === 'qwe');
assert(((binary)fn_14()) === 'qwe');
//assert(((array)fn_14()) === ['qwe']);
//var_dump((object)fn_14());


function &fn_15(): array {
  echo "function fn_15 called!\n";
  $a =  [1,2,3];
  return $a;
}

assert(((unset)fn_15()) === null);
assert(((bool)fn_15()) === true);
assert(((boolean)fn_15()) === true);
assert(((int)fn_15()) === 1);
assert(((integer)fn_15()) === 1);
assert(((real)fn_15()) === 1.0);
assert(((float)fn_15()) === 1.0);
assert(((double)fn_15()) === 1.0);
assert(((string)fn_15()) === 'Array');
assert(((binary)fn_15()) === 'Array');
//assert(((array)fn_15()) === [1,2,3]);
//var_dump((object)fn_15());

*/







//-------------------------------------------------------------------
// null to types
//-------------------------------------------------------------------
$a = null;
assert(((unset)$a) === null);
assert(((bool)$a) === false);
assert(((boolean)$a) === false);
assert(((int)$a) === 0);
assert(((integer)$a) === 0);
assert(((real)$a) === 0.0);
assert(((float)$a) === 0.0);
assert(((double)$a) === 0.0);
assert(((string)$a) === '');
assert(((binary)$a) === '');
//assert(((array)$a) === []);
//var_dump((object)$a);

$a = null;
assert(((UNSET)$a) === null);
assert(((BOOL)$a) === false);
assert(((BOOLEAN)$a) === false);
assert(((INT)$a) === 0);
assert(((INTEGER)$a) === 0);
assert(((REAL)$a) === 0.0);
assert(((FLOAT)$a) === 0.0);
assert(((DOUBLE)$a) === 0.0);
assert(((STRING)$a) === '');
assert(((BINARY)$a) === '');
//assert(((ARRAY)$a) === []);
//var_dump((OBJECT)$a);

//-------------------------------------------------------------------
// boolean to types
//-------------------------------------------------------------------
$a = false;
assert(((unset)$a) === null);
assert(((bool)$a) === false);
assert(((boolean)$a) === false);
assert(((int)$a) === 0);
assert(((integer)$a) === 0);
assert(((real)$a) === 0.0);
assert(((float)$a) === 0.0);
assert(((double)$a) === 0.0);
assert(((string)$a) === '');
assert(((binary)$a) === '');
//assert(((array)$a) === [false]);
//var_dump((object)$a);

$a = true;
assert(((unset)$a) === null);
assert(((bool)$a) === true);
assert(((boolean)$a) === true);
assert(((int)$a) === 1);
assert(((integer)$a) === 1);
assert(((real)$a) === 1.0);
assert(((float)$a) === 1.0);
assert(((double)$a) === 1.0);
assert(((string)$a) === "1");
assert(((binary)$a) === "1");
//assert(((array)$a) === [true]);
//var_dump((object)$a);

//-------------------------------------------------------------------
// integer to types
//-------------------------------------------------------------------
$a = 123;
assert(((unset)$a) === null);
assert(((bool)$a) === true);
assert(((boolean)$a) === true);
assert(((int)$a) === 123);
assert(((integer)$a) === 123);
assert(((real)$a) === 123.0);
assert(((float)$a) === 123.0);
assert(((double)$a) === 123.0);
assert(((string)$a) === "123");
assert(((binary)$a) === "123");
//assert(((array)$a) === [123]);
//var_dump((object)$a);

//-------------------------------------------------------------------
// float to types
//-------------------------------------------------------------------
$a = 123.456;
assert(((unset)$a) === null);
assert(((bool)$a) === true);
assert(((boolean)$a) === true);
assert(((int)$a) === 123);
assert(((integer)$a) === 123);
assert(((real)$a) === 123.456);
assert(((float)$a) === 123.456);
assert(((double)$a) === 123.456);
assert(((string)$a) === "123.456");
assert(((binary)$a) === "123.456");
//assert(((array)$a) === [123.456]);
//var_dump((object)$a);

//-------------------------------------------------------------------
// string to types
//-------------------------------------------------------------------
$a = 'qwe';
assert(((unset)$a) === null);
assert(((bool)$a) === true);
assert(((boolean)$a) === true);
assert(((int)$a) === 0);
assert(((integer)$a) === 0);
assert(((real)$a) === 0.0);
assert(((float)$a) === 0.0);
assert(((double)$a) === 0.0);
assert(((string)$a) === "qwe");
assert(((binary)$a) === "qwe");
//assert(((array)$a) === ["qwe"]);
//var_dump((object)"qwe");

$a = '123';
assert(((unset)$a) === null);
assert(((bool)$a) === true);
assert(((boolean)$a) === true);
assert(((int)$a) === 123);
assert(((integer)$a) === 123);
assert(((real)$a) === 123.0);
assert(((float)$a) === 123.0);
assert(((double)$a) === 123.0);
assert(((string)$a) === "123");
assert(((binary)$a) === "123");
//assert(((array)$a) === ["123"]);
//var_dump((object)$a);

$a = '';
assert(((unset)$a) === null);
assert(((bool)$a) === false);
assert(((boolean)$a) === false);
assert(((int)$a) === 0);
assert(((integer)$a) === 0);
assert(((real)$a) === 0.0);
assert(((float)$a) === 0.0);
assert(((double)$a) === 0.0);
assert(((string)$a) === '');
assert(((binary)$a) === '');
//assert(((array)$a) === ['']);
//var_dump((object)$a);

$a = '0';
assert(((unset)$a) === null);
assert(((bool)$a) === false);
assert(((boolean)$a) === false);
assert(((int)$a) === 0);
assert(((integer)$a) === 0);
assert(((real)$a) === 0.0);
assert(((float)$a) === 0.0);
assert(((double)$a) === 0.0);
assert(((string)$a) === '0');
assert(((binary)$a) === '0');
//assert(((array)$a) === ['0']);
//var_dump((object)$a);

//-------------------------------------------------------------------
// array to types
//-------------------------------------------------------------------
$a = [];
assert(((unset)$a) === null);
assert(((bool)$a) === false);
assert(((boolean)$a) === false);
assert(((int)$a) === 0);
assert(((integer)$a) === 0);
assert(((real)$a) === 0.0);
assert(((float)$a) === 0.0);
assert(((double)$a) === 0.0);
assert(((string)$a) === 'Array');
assert(((binary)$a) === 'Array');
//assert(((array)$a) === []);
//var_dump((object)$a);

$a = [0];
assert(((unset)$a) === null);
assert(((bool)$a) === true);
assert(((boolean)$a) === true);
assert(((int)$a) === 1);
assert(((integer)$a) === 1);
assert(((real)$a) === 1.0);
assert(((float)$a) === 1.0);
assert(((double)$a) === 1.0);
assert(((string)$a) === 'Array');
assert(((binary)$a) === 'Array');
//assert(((array)$a) === [0]);
//var_dump((object)$a);

$a = [3,2,1];
assert(((unset)$a) === null);
assert(((bool)$a) === true);
assert(((boolean)$a) === true);
assert(((int)$a) === 1);
assert(((integer)$a) === 1);
assert(((real)$a) === 1.0);
assert(((float)$a) === 1.0);
assert(((double)$a) === 1.0);
assert(((string)$a) === 'Array');
assert(((binary)$a) === 'Array');
//assert(((array)$a) === [3,2,1]);
//var_dump((object)$a);

?>