<?
$a = 1;
$b = 456;
$c = true;
$d = false;
$e = 33.3;

assert($a < $b); // 1 < 456
assert($e < $b); // 33.3 < 456
assert($d < $a); // false < 1
assert($undef < $a); // null < 1

assert($b > $a); // 456 > 1
assert($b > $e); // 456 > 33.3
assert($a > $d); // 1 > false
assert($a > $undef); // 1 > null

assert($a <= $b); // 1 <= 456
assert($e <= $b); // 33.3 <= 456
assert($d <= $a); // false <= 1
assert($undef <= $a); // null <= 1
assert($c <= $a); // true <= 1
assert($a <= $a); // 1 <= 1

assert($b >= $a); // 456 >= 1
assert($b >= $e); // 456 >= 33.3
assert($a >= $d); // 1 >= false
assert($a >= $undef); // 1 >= null
assert($a >= $c); // 1 >= true
assert($a >= $a); // 1 >= 1

// spaceship operator
assert(($a <=> $b) < 0); 
assert(($a <=> $e) < 0);
assert(($a <=> $a) == 0);
assert(($b <=> $a) > 0);
assert(($e <=> $a) > 0);

$f = 1; // int
$g = 1.0; // float
assert($f == $g); 
assert(($f === $g) == false);
assert($f !== $g);

?>