<?

function fn_01(bool     $a) { echo $a; echo "\n"; }
function fn_02(boolean  $a) { echo $a; echo "\n"; }
function fn_03(int      $a) { echo $a; echo "\n"; }
function fn_04(long     $a) { echo $a; echo "\n"; }
function fn_05(integer  $a) { echo $a; echo "\n"; }
function fn_06(float    $a) { echo $a; echo "\n"; }
function fn_07(real     $a) { echo $a; echo "\n"; }
function fn_08(double   $a) { echo $a; echo "\n"; }
function fn_09(string   $a) { echo $a; echo "\n"; }
function fn_10(binary   $a) { echo $a; echo "\n"; }

//TODO: add error diagnostic check

// null
fn_01($v_01);
fn_02($v_02);
fn_03($v_03);
fn_04($v_04);
fn_05($v_05);
fn_06($v_06);
fn_07($v_07);
fn_08($v_08);
fn_09($v_09);
fn_10($v_10);

// string
fn_03('qwe');
fn_04('qwe');
fn_05('qwe');
fn_06('qwe');
fn_07('qwe');
fn_08('qwe');

?>