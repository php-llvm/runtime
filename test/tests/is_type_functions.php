<?php 
$a = 1;
$b = 2.3;
$c = "AAA";
$d = "1";
$e = array(1,2,3);
$f = true;
$g = null;

// is_int
assert(is_int(1));
assert(!is_int(2.3));
assert(!is_int("AAA"));
assert(!is_int("1"));
assert(!is_int(array(1,2,3)));
assert(!is_int(true));
assert(!is_int(null));
assert(is_int($a));
assert(!is_int($b));
assert(!is_int($c));
assert(!is_int($d));
assert(!is_int($e));
assert(!is_int($f));
assert(!is_int($g));

// aliases
assert(is_integer(1));
assert(!is_integer(2.3));
assert(!is_integer("AAA"));
assert(!is_integer("1"));
assert(!is_integer(array(1,2,3)));
assert(!is_integer(true));
assert(!is_integer(null));
assert(is_integer($a));
assert(!is_integer($b));
assert(!is_integer($c));
assert(!is_integer($d));
assert(!is_integer($e));
assert(!is_integer($f));
assert(!is_integer($g));

assert(is_long(1));
assert(!is_long(2.3));
assert(!is_long("AAA"));
assert(!is_long("1"));
assert(!is_long(array(1,2,3)));
assert(!is_long(true));
assert(!is_long(null));
assert(is_long($a));
assert(!is_long($b));
assert(!is_long($c));
assert(!is_long($d));
assert(!is_long($e));
assert(!is_long($f));
assert(!is_long($g));

// is_float
assert(!is_float(1));
assert(is_float(2.3));
assert(!is_float("AAA"));
assert(!is_float("1"));
assert(!is_float(array(1,2,3)));
assert(!is_float(true));
assert(!is_float(null));
assert(!is_float($a));
assert(is_float($b));
assert(!is_float($c));
assert(!is_float($d));
assert(!is_float($e));
assert(!is_float($f));
assert(!is_float($g));

// aliases
assert(!is_real(1));
assert(is_real(2.3));
assert(!is_real("AAA"));
assert(!is_real("1"));
assert(!is_real(array(1,2,3)));
assert(!is_real(true));
assert(!is_real(null));
assert(!is_real($a));
assert(is_real($b));
assert(!is_real($c));
assert(!is_real($d));
assert(!is_real($e));
assert(!is_real($f));
assert(!is_real($g));

assert(!is_double(1));
assert(is_double(2.3));
assert(!is_double("AAA"));
assert(!is_double("1"));
assert(!is_double(array(1,2,3)));
assert(!is_double(true));
assert(!is_double(null));
assert(!is_double($a));
assert(is_double($b));
assert(!is_double($c));
assert(!is_double($d));
assert(!is_double($e));
assert(!is_double($f));
assert(!is_double($g));

// is_string
assert(!is_string(1));
assert(!is_string(2.3));
assert(is_string("AAA"));
assert(is_string("1"));
assert(!is_string(array(1,2,3)));
assert(!is_string(true));
assert(!is_string(null));
assert(!is_string($a));
assert(!is_string($b));
assert(is_string($c));
assert(is_string($d));
assert(!is_string($e));
assert(!is_string($f));
assert(!is_string($g));

// is_bool
assert(!is_bool(1));
assert(!is_bool(2.3));
assert(!is_bool("AAA"));
assert(!is_bool("1"));
assert(!is_bool(array(1,2,3)));
assert(is_bool(true));
assert(!is_bool(null));
assert(!is_bool($a));
assert(!is_bool($b));
assert(!is_bool($c));
assert(!is_bool($d));
assert(!is_bool($e));
assert(is_bool($f));
assert(!is_bool($g));

// is_numeric
assert(is_numeric(1));
assert(is_numeric(2.3));
assert(!is_numeric("AAA"));
assert(is_numeric("1"));
assert(!is_numeric(array(1,2,3)));
assert(!is_numeric(true));
assert(!is_numeric(null));
assert(is_numeric($a));
assert(is_numeric($b));
assert(!is_numeric($c));
//assert(is_numeric($d)); <--- does not work at the moment
assert(!is_numeric($e));
assert(!is_numeric($f));
assert(!is_numeric($g));

// is_array
assert(!is_array(1));
assert(!is_array(2.3));
assert(!is_array("AAA"));
assert(!is_array("1"));
assert(is_array(array(1,2,3)));
assert(!is_array(true));
assert(!is_array(null));
assert(!is_array($a));
assert(!is_array($b));
assert(!is_array($c));
assert(!is_array($d));
assert(is_array($e));
assert(!is_array($f));
assert(!is_array($g));

// is_scalar
assert(is_scalar(1));
assert(is_scalar(2.3));
assert(is_scalar("AAA"));
assert(is_scalar("1"));
assert(!is_scalar(array(1,2,3)));
assert(is_scalar(true));
assert(!is_scalar(null));
assert(is_scalar($a));
assert(is_scalar($b));
assert(is_scalar($c));
assert(is_scalar($d));
assert(!is_scalar($e));
assert(is_scalar($f));
assert(!is_scalar($g));

// is_null
assert(!is_null(1));
assert(!is_null(2.3));
assert(!is_null("AAA"));
assert(!is_null("1"));
assert(!is_null(array(1,2,3)));
assert(!is_null(true));
assert(is_null(null));
assert(!is_null($a));
assert(!is_null($b));
assert(!is_null($c));
assert(!is_null($d));
assert(!is_null($e));
assert(!is_null($f));
assert(is_null($g));

// is_resource
assert(!is_resource(1));
assert(!is_resource(2.3));
assert(!is_resource("AAA"));
assert(!is_resource("1"));
assert(!is_resource(array(1,2,3)));
assert(!is_resource(true));
assert(!is_resource(null));
assert(!is_resource($a));
assert(!is_resource($b));
assert(!is_resource($c));
assert(!is_resource($d));
assert(!is_resource($e));
assert(!is_resource($f));
assert(!is_resource($g));

// is_object
assert(!is_object(1));
assert(!is_object(2.3));
assert(!is_object("AAA"));
assert(!is_object("1"));
assert(!is_object(array(1,2,3)));
assert(!is_object(true));
assert(!is_object(null));
assert(!is_object($a));
assert(!is_object($b));
assert(!is_object($c));
assert(!is_object($d));
assert(!is_object($e));
assert(!is_object($f));
assert(!is_object($g));
?>
