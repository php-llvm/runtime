<?php 
$a = 1;
$b = 2.3;
$c = "AAA";
$d = "1";
$e = array(1,2,3);
$f = true;
$g = null;

assert(gettype(1) === "integer");
assert(gettype(2.3) === "double");
assert(gettype("AAA") === "string");
assert(gettype("1") === "string");
assert(gettype(array(1,2,3)) === "array");
assert(gettype(true) === "boolean");
assert(gettype(null) === "NULL");

assert(gettype($a) === "integer");
assert(gettype($b) === "double");
assert(gettype($c) === "string");
assert(gettype($d) === "string");
assert(gettype($e) === "array");
assert(gettype($f) === "boolean");
assert(gettype($g) === "NULL");
?>
