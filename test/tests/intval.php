<?php
// TODO: this part should eventually be moved to statis_assert as 
// is computed in compile-time

assert(intval(42) === 42);                      // 42
assert(intval(4.2) === 4);                     // 4
assert(intval('42') === 42);                    // 42
assert(intval('+42') === 42);                   // 42
assert(intval('-42') === -42);                   // -42
assert(intval(042) === 34);                     // 34
assert(intval('042') === 42);                   // 42
//TODO: handle 64-bit case
//assert(intval(1e10) === 2147483647);            // INT_MAX
assert(intval('1e10') === 1);                  // 1
assert(intval(0x1A) === 26);                    // 26
assert(intval(42000000) === 42000000);                // 42000000
//TODO: handle 64-bit case
//assert(intval(420000000000000000000) === 2147483647);           // INT_MAX
//assert(intval('420000000000000000000') === 2147483647); // INT_MAX
assert(intval(42, 8) === 42);                   // 42
assert(intval('42', 8) === 34);                 // 34
assert(intval(array()) === 0);                 // 0
assert(intval(array('foo', 'bar')) === 1);     // 1

$int = 42;
$double = 4.2;
$simple_str_int = '42';
$plus_str_int = '+42';
$minus_str_int = '-42';
$oct = 042;
$oct_str_int = '042';
$sint = 1e10;
$sc_str_int = '1e10';
$hex = 0x1A;
$int2 = 42000000;
$large_num = 420000000000000000000;
$large_str_num = '420000000000000000000';
$empty_arr = array();
$arr = array('foo','bar');

assert(intval($int) === 42);                      // 42
assert(intval($double) === 4);                     // 4
assert(intval($simple_str_int) === 42);                    // 42
assert(intval($plus_str_int) === 42);                   // 42
assert(intval($minus_str_int) === -42);                   // -42
assert(intval($oct) === 34);                     // 34
assert(intval($oct_str_int) === 42);                   // 42
//TODO: handle 64-bit case
//assert(intval($sint) === 2147483647);                    // INT_MAX
assert(intval($sc_str_int) === 1);                  // 1
assert(intval($hex) === 26);                    // 26
assert(intval($int2) === 42000000);                // 42000000
//TODO: handle 64-bit case
//assert(intval($large_num) === 2147483647);           // INT_MAX
//assert(intval($large_str_num) === 2147483647); // INT_MAX
assert(intval($int, 8) === 42);                   // 42
assert(intval($simple_str_int, 8) === 34);                 // 34
assert(intval($empty_arr) === 0);                 // 0
assert(intval($arr) === 1);     // 1

?>
