<?
echo "AAAA";
echo "\n";

echo 1;
echo "\n";

echo 0.5;
echo "\n";

//echo true;
//echo false;

echo abs(123);
echo abs(-456);
echo "\n";

echo abs(123.456);
echo abs(-123.456);
echo "\n";

echo abs("qwe");  // 0 - not a number
echo abs("321");
echo "\n";

echo abs("-0123"); // 123 - should no be recognized as oct
echo "\n";

echo abs("-0x123"); // 0 - should no be recognized as hex
echo "\n";

echo abs("-21474836470123"); // double value 
echo "\n";

?>