<?php
assert(http_response_code() === 200);

header("HTTP/1.1 202");
assert(http_response_code() === 202);

header("HTTP/1.1 test status");
assert(http_response_code() === false);

header("HTTP/1.1 200");
assert(http_response_code() === 200);
?>
