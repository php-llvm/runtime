// RUN: %clang_php %s -verify
<?php 

$a = null;
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 22);

$a = null;
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 22);

$a = false;
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 22);

$a = 0;
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 22);

$a = 11;
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 11);

$a = 0.1;
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 11);

$a = 0.0;
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 22);

$a = '';
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 22);

$a = '0';
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 22);

$a = 'x';
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 11);

$a = '0';
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 22);


$a = [];
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 22);

$a = [1];
$b = 11;
$c = 22;
assert(($a    ? $b  : $c ) === 11);



$a = null;
$b = 11;
$c = '22';
assert(($a    ? $b  : $c ) === '22');

$a = true;
$b = 11;
$c = '22';
assert(($a    ? $b  : $c ) === 11);

$a = false;
$b = 11;
$c = '22';
assert(($a    ? $b  : $c ) === '22');


$a = null;
$b = 22;
assert(($a    ? : $b ) === 22   );

$a = true;
$b = 22;
assert(($a    ? : $b ) === true );

$a = false;
$b = 22;
assert(($a    ? : $b ) === 22   );

$a = 0;
$b = 22;
assert(($a    ? : $b ) === 22   );

$a = 11;
$b = 22;
assert(($a    ? : $b ) === 11   );

$a = 0.1;
$b = 22;
assert(($a    ? : $b ) === 0.1  );

$a = 0.0;
$b = 22;
assert(($a    ? : $b ) === 22   );

$a = '';
$b = 22;
assert(($a    ? : $b ) === 22   );

$a = '0';
$b = 22;
assert(($a    ? : $b ) === 22   );

$a = 'x';
$b = 22;
assert(($a    ? : $b ) === 'x'  );

$a = '0.0';
$b = 22;
assert(($a    ? : $b ) === '0.0');

$a = [];
$b = 22;
assert(($a    ? : $b ) === 22   );

$a = [1];
$b = 22;
assert(($a    ? : $b ) === [1]  );


// Associativity

$a = 0; $b = 1; $c = 2; $d = 3; $e = 4;
assert( ($a ? $b : $c ? $d : $e) === 3);
$a = 1; $b = 0; $c = 2; $d = 3; $e = 4;
assert( ($a ? $b : $c ? $d : $e) === 4);
$a = 1; $b = 0; $c = 0; $d = 3; $e = 4;
assert( ($a ? $b : $c ? $d : $e) === 4);


$a = 0; $b = 1; $c = 2; $d = 3;
assert( ($a ?: $b ?: $c ?: $d)  === 1 );
$a = 1; $b = 0; $c = 3; $d = 2;
assert( ($a ?: $b ?: $c ?: $d)  === 1 );
$a = 2; $b = 1; $c = 0; $d = 3;
assert( ($a ?: $b ?: $c ?: $d)  === 2 );
$a = 3; $b = 2; $c = 1; $d = 0;
assert( ($a ?: $b ?: $c ?: $d)  === 3 );
$a = 0; $b = 0; $c = 2; $d = 3;
assert( ($a ?: $b ?: $c ?: $d)  === 2 );
$a = 0; $b = 0; $c = 0; $d = 3;
assert( ($a ?: $b ?: $c ?: $d)  === 3 );

