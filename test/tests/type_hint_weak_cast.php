<?

function fn_01(bool     $a) { return $a; }
function fn_02(boolean  $a) { return $a; }
function fn_03(int      $a) { return $a; }
function fn_04(long     $a) { return $a; }
function fn_05(integer  $a) { return $a; }
function fn_06(float    $a) { return $a; }
function fn_07(real     $a) { return $a; }
function fn_08(double   $a) { return $a; }
function fn_09(string   $a) { return $a; }
function fn_10(binary   $a) { return $a; }


//--------------------------------------------------------------------
// implicit cast to bool
//--------------------------------------------------------------------
// bool
assert(fn_01(true) === true);
assert(fn_01(false) === false);
assert(fn_01(123) == true);
assert(fn_01(123.456) === true);
assert(fn_01('qwe') === true);
assert(fn_01('123qwe') === true);
assert(fn_01('123') === true);
assert(fn_01('-123') === true);
assert(fn_01('123.456') === true);

// boolean
assert(fn_02(true) === true);
assert(fn_02(false) === false);
assert(fn_02(123) === true);
assert(fn_02(123.456) === true);
assert(fn_02('qwe') === true);
assert(fn_02('123qwe') === true);
assert(fn_02('123') === true);
assert(fn_02('-123') === true);
assert(fn_02('123.456') === true);

//--------------------------------------------------------------------
// implicit cast to int
//--------------------------------------------------------------------
// int
assert(fn_03(true) === 1);
assert(fn_03(false) === 0);
assert(fn_03(123) === 123);
assert(fn_03(123.456) === 123);
assert(fn_03('123qwe') === 123); 
assert(fn_03('123') === 123);
assert(fn_03('-123') === -123);
assert(fn_03('123.456') === 123);

// long
assert(fn_04(true) === 1);
assert(fn_04(false) === 0);
assert(fn_04(123) === 123);
assert(fn_04(123.456) === 123);
assert(fn_04('123qwe') === 123); 
assert(fn_04('123') === 123);
assert(fn_04('-123') === -123);
assert(fn_04('123.456') === 123);

// integer
assert(fn_05(true) === 1);
assert(fn_05(false) === 0);
assert(fn_05(123) === 123);
assert(fn_05(123.456) === 123);
assert(fn_05('123qwe') === 123); 
assert(fn_05('123') === 123);
assert(fn_05('-123') === -123);
assert(fn_05('123.456') === 123);

//--------------------------------------------------------------------
// implicit cast to double
//--------------------------------------------------------------------
// float
assert(fn_06(true) === 1.0);
assert(fn_06(false) === 0.0);
assert(fn_06(123) === 123.0);
assert(fn_06(123.456) === 123.456);
assert(fn_06('123qwe') == 123.0);
assert(fn_06('123') === 123.0);
assert(fn_06('-123') === -123.0);
assert(fn_06('123.456') === 123.456);

// real
assert(fn_07(true) === 1.0);
assert(fn_07(false) === 0.0);
assert(fn_07(123) === 123.0);
assert(fn_07(123.456) === 123.456);
assert(fn_07('123qwe') == 123.0);
assert(fn_07('123') === 123.0);
assert(fn_07('-123') === -123.0);
assert(fn_07('123.456') === 123.456);

// double
assert(fn_08(true) === 1.0);
assert(fn_08(false) === 0.0);
assert(fn_08(123) === 123.0);
assert(fn_08(123.456) === 123.456);
assert(fn_08('123qwe') == 123.0);
assert(fn_08('123') === 123.0);
assert(fn_08('-123') === -123.0);
assert(fn_08('123.456') === 123.456);


//--------------------------------------------------------------------
// implicit cast to string
//--------------------------------------------------------------------
// string
assert(fn_09(true) === '1');
assert(fn_09(false) === '');
assert(fn_09(123) === '123');
assert(fn_09(123.456) === '123.456');
assert(fn_09('qwe') === 'qwe');
assert(fn_09('123qwe') === '123qwe');
assert(fn_09('123') === '123');
assert(fn_09('-123') === '-123');
assert(fn_09('123.456') === '123.456');

// binary
assert(fn_10(true) === '1');
assert(fn_10(false) === '');
assert(fn_10(123) === '123');
assert(fn_10(123.456) === '123.456');
assert(fn_10('qwe') === 'qwe');
assert(fn_10('123qwe') === '123qwe');
assert(fn_10('123') === '123');
assert(fn_10('-123') === '-123');
assert(fn_10('123.456') === '123.456');

?>