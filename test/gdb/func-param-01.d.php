// RUN: %phlang -g %s -o a.out
// RUN: %gdb a.out -x %s.gdb | FileCheck %s
<?php


function fn($param) {
// CHECK: Breakpoint 1, php::func::fn (param=...)
// CHECK: return $param;
// CHECK-NEXT: $1 = {<box>
// CHECK-SAME: v_long = 123456
	return $param;
}
echo fn(123456);

// CHECK: exited normally

?>
