<?

function static_assert($a) { assert($a); } // expected-warning{{static_assert has special treatment that cannot be redefined}}

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }

function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_bool_01(bool $a) {
  assert($a === true);

  $a = true;  $a |= false; assert($a === true);
  $a = false; $a |= true;  assert($a === true);
  $a = false; $a |= false; assert($a === false);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a |= null;    assert($a === true); // expected-warning{{implicit convertion from NULL to integer}}
  $a = true; $a |= 123;     assert($a === true);
  $a = true; $a |= 12.34;   assert($a === true); // expected-warning{{implicit convertion from double to integer}}
  $a = true; $a |= "qwe";   assert($a === true); // expected-warning{{implicit convertion from string to integer}} 

  $a = false; $a |= null;   assert($a === false);// expected-warning{{implicit convertion from NULL to integer}}
  $a = false; $a |= 123;    assert($a === true);
  $a = false; $a |= 12.34;  assert($a === true); // expected-warning{{implicit convertion from double to integer}}
  $a = false; $a |= "qwe";  assert($a === false);// expected-warning{{implicit convertion from string to integer}} 

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a |= get_void();   assert($a === true);  // expected-warning{{implicit convertion from NULL to integer}}
  $a = true; $a |= get_bool();   assert($a === true);
  $a = true; $a |= get_int();    assert($a === true);
  $a = true; $a |= get_double(); assert($a === true); // expected-warning{{implicit convertion from double to integer}}
  //$a = true; $a |= get_string(); assert($a === false); // expected-warning{{implicit convertion from string to integer}}

  $a = false; $a |= get_void();   assert($a === false); // expected-warning{{implicit convertion from NULL to integer}}
  $a = false; $a |= get_bool();   assert($a === true);
  $a = false; $a |= get_int();    assert($a === true);
  $a = false; $a |= get_double(); assert($a === true);  // expected-warning{{implicit convertion from double to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a |= get_bool_ref();   assert($a === true);
  $a = true; $a |= get_int_ref();    assert($a === true);
  $a = true; $a |= get_double_ref(); assert($a === true); // expected-warning{{implicit convertion from double to integer}}
  //$a = true; $a |= get_string_ref(); assert($a === false); // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  //$b = null;    $a = true; $a |= $b; assert($a === true);
  $b = true;    $a = true; $a |= $b; assert($a === true);
  $b = 123;     $a = true; $a |= $b; assert($a === true);
  $b = 12.45;   $a = true; $a |= $b; assert($a === true);
  //$b = 'qwe';   $a = true; $a |= $b; assert($a === true);
  //$b = [1,2,3]; $a = true; $a |= $b; assert($a === false);

  $b = true;    $a = false; $a |= $b; assert($a === true);
  $b = 123;     $a = false; $a |= $b; assert($a === true);
  $b = 12.45;   $a = false; $a |= $b; assert($a === true);
}
check_assign_to_bool_01(true);


function check_assign_to_bool_02(bool &$a) {
  assert($a === true);

  $a = true;  $a |= false; assert($a === true);
  $a = false; $a |= true;  assert($a === true);
  $a = false; $a |= false; assert($a === false);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a |= null;    assert($a === true); // expected-warning{{implicit convertion from NULL to integer}}
  $a = true; $a |= 123;     assert($a === true);
  $a = true; $a |= 12.34;   assert($a === true); // expected-warning{{implicit convertion from double to integer}}
  $a = true; $a |= "qwe";   assert($a === true); // expected-warning{{implicit convertion from string to integer}} 

  $a = false; $a |= null;   assert($a === false);// expected-warning{{implicit convertion from NULL to integer}}
  $a = false; $a |= 123;    assert($a === true);
  $a = false; $a |= 12.34;  assert($a === true); // expected-warning{{implicit convertion from double to integer}}
  $a = false; $a |= "qwe";  assert($a === false);// expected-warning{{implicit convertion from string to integer}} 

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a |= get_void();   assert($a === true);  // expected-warning{{implicit convertion from NULL to integer}}
  $a = true; $a |= get_bool();   assert($a === true);
  $a = true; $a |= get_int();    assert($a === true);
  $a = true; $a |= get_double(); assert($a === true); // expected-warning{{implicit convertion from double to integer}}
  //$a = true; $a |= get_string(); assert($a === false); // expected-warning{{implicit convertion from string to integer}}

  $a = false; $a |= get_void();   assert($a === false); // expected-warning{{implicit convertion from NULL to integer}}
  $a = false; $a |= get_bool();   assert($a === true);
  $a = false; $a |= get_int();    assert($a === true);
  $a = false; $a |= get_double(); assert($a === true);  // expected-warning{{implicit convertion from double to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a |= get_bool_ref();   assert($a === true);
  $a = true; $a |= get_int_ref();    assert($a === true);
  $a = true; $a |= get_double_ref(); assert($a === true); // expected-warning{{implicit convertion from double to integer}}
  //$a = true; $a |= get_string_ref(); assert($a === false); // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  //$b = null;    $a = true; $a |= $b; assert($a === true);
  $b = true;    $a = true; $a |= $b; assert($a === true);
  $b = 123;     $a = true; $a |= $b; assert($a === true);
  $b = 12.45;   $a = true; $a |= $b; assert($a === true);
  //$b = 'qwe';   $a = true; $a |= $b; assert($a === true);
  //$b = [1,2,3]; $a = true; $a |= $b; assert($a === false);

  $b = true;    $a = false; $a |= $b; assert($a === true);
  $b = 123;     $a = false; $a |= $b; assert($a === true);
  $b = 12.45;   $a = false; $a |= $b; assert($a === true);
}
$a = true;
check_assign_to_bool_02($a);








function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a |= 456;  assert($a === 507);
  $a |= -12;  assert($a === -1);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a |= null;  assert($a === 123);   // expected-warning{{implicit convertion from NULL to integer}}
  $a = 123; $a |= false; assert($a === 123);
  $a = 123; $a |= 123;   assert($a === 123);
  $a = 123; $a |= 12.34; assert($a === 127);   // expected-warning{{implicit convertion from double to integer}}
  $a = 123; $a |= "qwe"; assert($a === 123);   // expected-warning{{implicit convertion from string to integer}} 

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a |= get_void();   assert($a === 123);  // expected-warning{{implicit convertion from NULL to integer}}
  $a = 123; $a |= get_bool();   assert($a === 123);
  $a = 123; $a |= get_int();    assert($a === 123);
  $a = 123; $a |= get_double(); assert($a === 127);  // expected-warning{{implicit convertion from double to integer}}
  //$a = 123; $a |= get_string(); assert($a === 0);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a |= get_bool_ref();   assert($a === 123);
  $a = 123; $a |= get_int_ref();    assert($a === 123);
  $a = 123; $a |= get_double_ref(); assert($a === 127);  // expected-warning{{implicit convertion from double to integer}}
  //$a = 123; $a |= get_string_ref(); assert($a === 0);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null;  $a = 123; $a |= $b;
  $b = true;  $a = 123; $a |= $b; assert($a === 123);
  $b = 123;   $a = 123; $a |= $b; assert($a === 123);
  $b = 12.45; $a = 123; $a |= $b; assert($a === 127);
  //$b = 'qwe';   $a = 123; $a |= $b;
  //$b = [1,2,3]; $a = 123; $a |= $b;
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a |= 456;  assert($a === 507);
  $a |= -12;  assert($a === -1);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a |= null;  assert($a === 123);   // expected-warning{{implicit convertion from NULL to integer}}
  $a = 123; $a |= false; assert($a === 123);
  $a = 123; $a |= 123;   assert($a === 123);
  $a = 123; $a |= 12.34; assert($a === 127);   // expected-warning{{implicit convertion from double to integer}}
  $a = 123; $a |= "qwe"; assert($a === 123);   // expected-warning{{implicit convertion from string to integer}} 

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a |= get_void();   assert($a === 123);  // expected-warning{{implicit convertion from NULL to integer}}
  $a = 123; $a |= get_bool();   assert($a === 123);
  $a = 123; $a |= get_int();    assert($a === 123);
  $a = 123; $a |= get_double(); assert($a === 127);  // expected-warning{{implicit convertion from double to integer}}
  //$a = 123; $a |= get_string(); assert($a === 0);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a |= get_bool_ref();   assert($a === 123);
  $a = 123; $a |= get_int_ref();    assert($a === 123);
  $a = 123; $a |= get_double_ref(); assert($a === 127);  // expected-warning{{implicit convertion from double to integer}}
  //$a = 123; $a |= get_string_ref(); assert($a === 0);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null;  $a = 123; $a |= $b;
  $b = true;  $a = 123; $a |= $b; assert($a === 123);
  $b = 123;   $a = 123; $a |= $b; assert($a === 123);
  $b = 12.45; $a = 123; $a |= $b; assert($a === 127);
  //$b = 'qwe';   $a = 123; $a |= $b;
  //$b = [1,2,3]; $a = 123; $a |= $b;
}
$a = 123;
check_assign_to_int_02($a);







function check_assign_to_string_01(string $a) {
  assert($a === 'qwe');

  $a = 'qwe'; $a |= '223';   assert($a === 'sww');
  $a = 'qwe'; $a |= '';      assert($a === 'qwe');

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 'qwe'; $a |= "qwe";   assert($a === 'qwe');

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 'qwe'; $a |= get_string(); assert($a === 'qwe');

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 'qwe'; $a |= get_string_ref(); assert($a === 'qwe');

//-------------------------------------------------------------------
// assign box to string
//-------------------------------------------------------------------
  // $b = null;    $a = 'qwe'; $a |= $b; 
  // $b = true;    $a = 'qwe'; $a |= $b; 
  // $b = 123;     $a = 'qwe'; $a |= $b; 
  // $b = 12.45;   $a = 'qwe'; $a |= $b; 
  $b = 'qwe'; $a = 'qwe'; $a |= $b; assert($a === 'qwe');
  $b = '2';   $a = 'qwe'; $a |= $b; assert($a === 'swe');
  $b = '223456';   $a = 'qwe'; $a |= $b;   assert($a === 'sww456');
  
  $b = 'qqqqqqqqqqwwwwwwwwwweeeeeeeeee'; 
  $a = '222222222222222222223333333333'; 
  $a |= $b;   
  assert($a === 'sssssssssswwwwwwwwwwwwwwwwwwww');

  //$b = [1,2,3]; $a = 'qwe'; $a |= $b; 
}
check_assign_to_string_01('qwe');

function check_assign_to_string_02(string &$a) {
  assert($a === 'qwe');

  $a = 'qwe'; $a |= '223';   assert($a === 'sww');
  $a = 'qwe'; $a |= '';      assert($a === 'qwe');

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 'qwe'; $a |= "qwe";   assert($a === 'qwe');

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 'qwe'; $a |= get_string(); assert($a === 'qwe');

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 'qwe'; $a |= get_string_ref(); assert($a === 'qwe');

//-------------------------------------------------------------------
// assign box to string
//-------------------------------------------------------------------
  // $b = null;    $a = 'qwe'; $a |= $b; 
  // $b = true;    $a = 'qwe'; $a |= $b; 
  // $b = 123;     $a = 'qwe'; $a |= $b; 
  // $b = 12.45;   $a = 'qwe'; $a |= $b; 
  $b = 'qwe';   $a = 'qwe'; $a |= $b; assert($a === 'qwe');
  $b = '2';   $a = 'qwe'; $a |= $b;   assert($a === 'swe');
  $b = '223456';   $a = 'qwe'; $a |= $b;   assert($a === 'sww456');
  
  $b = 'qqqqqqqqqqwwwwwwwwwweeeeeeeeee'; 
  $a = '222222222222222222223333333333'; 
  $a |= $b;   
  assert($a === 'sssssssssswwwwwwwwwwwwwwwwwwww');

  //$b = [1,2,3]; $a = 'qwe'; $a |= $b; 
}
$a = 'qwe';
check_assign_to_string_02($a);







function check_assign_to_universal_01($a) {
  $a |= [4,5,6];

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  //$a |= null;                                     // 
  $a = 123; $a |= false;  assert($a === 123);
  $a = 123; $a |= 123;    assert($a === 123);
  $a = 123; $a |= 12.34;  assert($a === 127); // expected-warning{{implicit convertion from double to integer}}
  //$a |= "qwe";

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  //$a = 123; $a |= get_void();   // 
  $a = 123; $a |= get_bool();   assert($a === 123);
  $a = 123; $a |= get_int();    assert($a === 123);
  $a = 123; $a |= get_double(); assert($a === 127); // expected-warning{{implicit convertion from double to integer}}
  //$a = 123; $a |= get_string();
  //$a = 123; $a |= get_array();

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a |= get_bool_ref();   assert($a === 123);
  $a = 123; $a |= get_int_ref();    assert($a === 123);
  $a = 123; $a |= get_double_ref(); assert($a === 127); // expected-warning{{implicit convertion from double to integer}}
  //$a |= get_string_ref();
  //$a |= get_array_ref(); 

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;  $a = 123; $a |= $b; assert($a === 123);
  $b = true;  $a = 123; $a |= $b; assert($a === 123);
  $b = 123;   $a = 123; $a |= $b; assert($a === 123);
  $b = 12.45; $a = 123; $a |= $b; assert($a === 127);
  //$b = 'qwe';   $a |= $b;
  //$b = [1,2,3]; $a |= $b;
}
check_assign_to_universal_01(123);

?>