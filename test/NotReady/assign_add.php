<?

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }

function check_assign_to_bool_01(bool $a) {
  assert($a === false);

  $a += false; assert($a === false);
  $a += true;  assert($a === true);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;    assert($a === true); // expected-warning{{implicit convertion from NULL to boolean}}
  $a += 123;     assert($a === true);
  $a += 12.34;   assert($a === true);
  $a += "qwe";   assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === true); // expected-warning{{implicit convertion from NULL to boolean}}
  $a += get_bool();   assert($a === true);
  $a += get_int();    assert($a === true);
  $a += get_double(); assert($a === true);
  $a += get_string(); assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === true);
  $a += get_int_ref();    assert($a === true);
  $a += get_double_ref(); assert($a === true);
  $a += get_string_ref(); assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  //$b = null;    $a += $b; assert($a === true);
  $b = true;    $a += $b; assert($a === true);
  $b = 123;     $a += $b; assert($a === true);
  $b = 12.45;   $a += $b; assert($a === true);
  $b = 'qwe';   $a += $b; assert($a === true);
  //$b = [1,2,3]; $a += $b; assert($a === true);
}
check_assign_to_bool_01(false);


function check_assign_to_bool_02(bool &$a) {
  assert($a === false);

  $a += false; assert($a === false);
  $a += true;  assert($a === true);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;    assert($a === true); // expected-warning{{implicit convertion from NULL to boolean}}
  $a += 123;     assert($a === true);
  $a += 12.34;   assert($a === true);
  $a += "qwe";   assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === true); // expected-warning{{implicit convertion from NULL to boolean}}
  $a += get_bool();   assert($a === true);
  $a += get_int();    assert($a === true);
  $a += get_double(); assert($a === true);
  $a += get_string(); assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === true);
  $a += get_int_ref();    assert($a === true);
  $a += get_double_ref(); assert($a === true);
  $a += get_string_ref(); assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  //$b = null;    $a += $b; assert($a === true);
  $b = true;    $a += $b; assert($a === true);
  $b = 123;     $a += $b; assert($a === true);
  $b = 12.45;   $a += $b; assert($a === true);
  $b = 'qwe';   $a += $b; assert($a === true);
  //$b = [1,2,3]; $a += $b; assert($a === true);
}
$a = false;
check_assign_to_bool_02($a);








function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a += 456;  assert($a === 579);
  $a += -12;  assert($a === 567);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;  assert($a === 567);   // expected-warning{{implicit convertion from NULL to integer}}
  $a += false; assert($a === 567);
  $a += 123;   assert($a === 690);
  $a += 12.34; assert($a === 702);
  $a += "qwe"; assert($a === 702);   // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === 702);  // expected-warning{{implicit convertion from NULL to integer}}
  $a += get_bool();   assert($a === 703);
  $a += get_int();    assert($a === 826);
  $a += get_double(); assert($a === 838);
  $a += get_string(); assert($a === 838);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === 839);
  $a += get_int_ref();    assert($a === 962);
  $a += get_double_ref(); assert($a === 974);
  $a += get_string_ref(); assert($a === 974);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null; $a += $b; assert($a === 974);
  $b = true;  $a += $b; assert($a === 975);
  $b = 123;   $a += $b; assert($a === 1098);
  $b = 12.45; $a += $b; assert($a === 1110);
  //$b = 'qwe';   $a += $b; assert($a === 1110);
  //$b = [1,2,3]; $a += $b; assert($a === false);
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a += 456;  assert($a === 579);
  $a += -12;  assert($a === 567);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;  assert($a === 567);   // expected-warning{{implicit convertion from NULL to integer}}
  $a += false; assert($a === 567);
  $a += 123;   assert($a === 690);
  $a += 12.34; assert($a === 702);
  $a += "qwe"; assert($a === 702);   // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === 702);  // expected-warning{{implicit convertion from NULL to integer}}
  $a += get_bool();   assert($a === 703);
  $a += get_int();    assert($a === 826);
  $a += get_double(); assert($a === 838);
  $a += get_string(); assert($a === 838);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === 839);
  $a += get_int_ref();    assert($a === 962);
  $a += get_double_ref(); assert($a === 974);
  $a += get_string_ref(); assert($a === 974);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null; $a += $b; assert($a === 974);
  $b = true;  $a += $b; assert($a === 975);
  $b = 123;   $a += $b; assert($a === 1098);
  $b = 12.45; $a += $b; assert($a === 1110);
  //$b = 'qwe';   $a += $b; assert($a === 1110);
  //$b = [1,2,3]; $a += $b; assert($a === false);
}
$a = 123;
check_assign_to_int_02($a);








function eq(double $a, double $b) : bool { return abs($a - $b) < 1e-10; }

function check_assign_to_double_01(float $a) {
  assert($a === 123.456);

  $a += 456.5;   assert($a === 579.956);
  $a += -12.258; assert($a === 567.698);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;  assert($a === 567.698);   // expected-warning{{implicit convertion from NULL to double}}
  $a += false; assert($a === 567.698);
  $a += 123;   assert($a === 690.698);
  $a += 12.34; assert($a === 703.038);
  $a += "qwe"; assert($a === 703.038);   // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === 703.038); // expected-warning{{implicit convertion from NULL to double}}
  $a += get_bool();   assert($a === 704.038);
  $a += get_int();    assert($a === 827.038);
  $a += get_double(); assert($a === 839.378);
  $a += get_string(); assert($a === 839.378); // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === 840.378);
  $a += get_int_ref();    assert($a === 963.378);
  $a += get_double_ref(); assert(eq($a, 975.718));
  $a += get_string_ref(); assert(eq($a, 975.718));  // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  //$b = null;    $a += $b;
  $b = true;    $a += $b; assert(eq($a, 976.718));
  $b = 123;     $a += $b; assert(eq($a, 1099.718));
  $b = 12.45;   $a += $b; assert(eq($a, 1112.168));
  //$b = 'qwe';   $a += $b;
  //$b = [1,2,3]; $a += $b;
}

check_assign_to_double_01(123.456);

function check_assign_to_double_02(float &$a) {
  assert($a === 123.456);

  $a += 456.5;   assert($a === 579.956);
  $a += -12.258; assert($a === 567.698);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;  assert($a === 567.698);   // expected-warning{{implicit convertion from NULL to double}}
  $a += false; assert($a === 567.698);
  $a += 123;   assert($a === 690.698);
  $a += 12.34; assert($a === 703.038);
  $a += "qwe"; assert($a === 703.038);   // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === 703.038); // expected-warning{{implicit convertion from NULL to double}}
  $a += get_bool();   assert($a === 704.038);
  $a += get_int();    assert($a === 827.038);
  $a += get_double(); assert($a === 839.378);
  $a += get_string(); assert($a === 839.378); // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === 840.378);
  $a += get_int_ref();    assert($a === 963.378);
  $a += get_double_ref(); assert(eq($a, 975.718));
  $a += get_string_ref(); assert(eq($a, 975.718));  // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  //$b = null;    $a += $b;
  $b = true;    $a += $b; assert(eq($a, 976.718));
  $b = 123;     $a += $b; assert(eq($a, 1099.718));
  $b = 12.45;   $a += $b; assert(eq($a, 1112.168));
  //$b = 'qwe';   $a += $b;
  //$b = [1,2,3]; $a += $b;
}
$a = 123.456;
check_assign_to_double_02($a);








function check_assign_to_array_01(array $a) {
  assert($a === [1,2,3]);

  $a += [4,5,6]; assert($a === [4,5,6]);

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref();

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  //$b = null;    $a += $b;
  //$b = true;    $a += $b;
  //$b = 123;     $a += $b;
  //$b = 12.45;   $a += $b;
  //$b = 'qwe';   $a += $b;
  $b = [1,2,3]; $a += $b;
}
//TODO: implement array addition in runtime
//check_assign_to_array_01([1,2,3]);

function check_assign_to_array_02(array &$a) {
  assert($a === [1,2,3]);

  $a += [4,5,6]; assert($a === [1,2,3]);

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref();

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  //$b = null;    $a += $b;
  //$b = true;    $a += $b;
  //$b = 123;     $a += $b;
  //$b = 12.45;   $a += $b;
  //$b = 'qwe';   $a += $b;
  $b = [1,2,3]; $a += $b;
}
$a = [1,2,3];
//TODO: implement array addition in runtime
//check_assign_to_array_02($a);

?>