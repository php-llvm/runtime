<?

function static_assert($a) { assert($a); } // expected-warning{{static_assert has special treatment that cannot be redefined}}

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }

function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a =  123; $a >>= 456; assert($a ===  0);
  $a = -123; $a >>= 456; assert($a === -1);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a >>= null;  assert($a === 123);   // expected-warning{{implicit convertion from NULL to integer}}
  $a = 123; $a >>= false; assert($a === 123);
  $a = 123; $a >>= 2;     assert($a === 30);
  $a = 123; $a >>= 12.34; assert($a === 0);     // expected-warning{{implicit convertion from double to integer}}
  $a = 123; $a >>= "qwe"; assert($a === 123);   // expected-warning{{implicit convertion from string to integer}} 

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a >>= get_void();   assert($a === 123);  // expected-warning{{implicit convertion from NULL to integer}}
  $a = 123; $a >>= get_bool();   assert($a === 61);
  $a = 123; $a >>= get_int();    assert($a === 0);
  $a = 123; $a >>= get_double(); assert($a === 0);  // expected-warning{{implicit convertion from double to integer}}
  $a = 123; $a >>= get_string(); assert($a === 123);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a >>= get_bool_ref();   assert($a === 61);
  $a = 123; $a >>= get_int_ref();    assert($a === 0);
  //$a = 123; $a >>= get_double_ref(); // expected-warning{{implicit convertion from double to integer}}
  //$a = 123; $a >>= get_string_ref(); // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null;  $a = 123; $a >>= $b; assert($a === 123);
  $b = true;  $a = 123; $a >>= $b; assert($a === 61);
  $b = 123;   $a = 123; $a >>= $b; assert($a === 0);
  $b = 12.45; $a = 123; $a >>= $b; assert($a === 0);
  //$b = 'qwe';   $a = 123; $a >>= $b; assert($a === 123);
  //$b = [1,2,3]; $a = 123; $a >>= $b;
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a =  123; $a >>= 456; assert($a ===  0);
  $a = -123; $a >>= 456; assert($a === -1);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a >>= null;  assert($a === 123);   // expected-warning{{implicit convertion from NULL to integer}}
  $a = 123; $a >>= false; assert($a === 123);
  $a = 123; $a >>= 2;     assert($a === 30);
  $a = 123; $a >>= 12.34; assert($a === 0);     // expected-warning{{implicit convertion from double to integer}}
  $a = 123; $a >>= "qwe"; assert($a === 123);   // expected-warning{{implicit convertion from string to integer}} 

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a >>= get_void();   assert($a === 123);  // expected-warning{{implicit convertion from NULL to integer}}
  $a = 123; $a >>= get_bool();   assert($a === 61);
  $a = 123; $a >>= get_int();    assert($a === 0);
  $a = 123; $a >>= get_double(); assert($a === 0);  // expected-warning{{implicit convertion from double to integer}}
  $a = 123; $a >>= get_string(); assert($a === 123);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a >>= get_bool_ref();   assert($a === 61);
  $a = 123; $a >>= get_int_ref();    assert($a === 0);
  //$a = 123; $a >>= get_double_ref(); // expected-warning{{implicit convertion from double to integer}}
  //$a = 123; $a >>= get_string_ref(); // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null;  $a = 123; $a >>= $b; assert($a === 123);
  $b = true;  $a = 123; $a >>= $b; assert($a === 61);
  $b = 123;   $a = 123; $a >>= $b; assert($a === 0);
  $b = 12.45; $a = 123; $a >>= $b; assert($a === 0);
  //$b = 'qwe';   $a = 123; $a >>= $b; assert($a === 123);
  //$b = [1,2,3]; $a = 123; $a >>= $b;
}
$a = 123;
check_assign_to_int_02($a);







function check_assign_to_universal_01($a) {
  $a = 123; $a >>= [4,5,6];

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a >>= null;   assert($a === 123);
  $a = 123; $a >>= false;  assert($a === 123);
  $a = 123; $a >>= 123;    assert($a === 0);
  $a = 123; $a >>= 12.34;  assert($a === 0); // expected-warning{{implicit convertion from double to integer}}
  $a = 123; $a >>= "qwe";  assert($a === 123);

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a >>= get_void();   assert($a == 123);
  $a = 123; $a >>= get_bool();   assert($a === 61);
  $a = 123; $a >>= get_int();    assert($a === 0);
  $a = 123; $a >>= get_double(); assert($a === 0); // expected-warning{{implicit convertion from double to integer}}
  $a = 123; $a >>= get_string(); assert($a === 123);
  $a = 123; $a >>= get_array(); 

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a >>= get_bool_ref();   assert($a === 61);
  $a = 123; $a >>= get_int_ref();    assert($a === 0);
  $a = 123; $a >>= get_double_ref(); assert($a === 0); // expected-warning{{implicit convertion from double to integer}}
  $a = 123; $a >>= get_string_ref(); assert($a == 123);
  $a = 123; $a >>= get_array_ref(); 

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;  $a = 123; $a >>= $b; assert($a == 123);
  $b = true;  $a = 123; $a >>= $b; assert($a === 61);
  $b = 123;   $a = 123; $a >>= $b; assert($a === 0);
  $b = 12.45; $a = 123; $a >>= $b; assert($a === 0);
  $b = 'qwe';   $a = 123; $a >>= $b; assert($a == 123);
  //$b = [1,2,3]; $a = 123; $a >>= $b;
}
check_assign_to_universal_01(123);

?>