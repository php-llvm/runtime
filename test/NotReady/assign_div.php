<?

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_bool_01(bool $a) {
  assert($a === true);

  $a /= true;  assert($a === true);
  $a = false; $a /= true;  assert($a === false);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a /= 123;     assert($a === true);
  $a = true; $a /= 12.34;   assert($a === true);
  $a = true; $a /= "qwe";   assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a /= get_bool();   assert($a === true);
  $a = true; $a /= get_int();    assert($a === true);
  $a = true; $a /= get_double(); assert($a === true);
  $a = true; $a /= get_string(); assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a /= get_bool_ref();   assert($a === true);
  $a = true; $a /= get_int_ref();    assert($a === true);
  $a = true; $a /= get_double_ref(); assert($a === true);
  $a = true; $a /= get_string_ref(); assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  //$b = null;    $a = true; $a /= $b;
  $b = true;    $a = true; $a /= $b; assert($a === true);
  $b = 123;     $a = true; $a /= $b; assert($a === true);
  $b = 12.45;   $a = true; $a /= $b; assert($a === true);
  $b = 'qwe';   $a = true; $a /= $b; assert($a === true);
  //$b = [1,2,3]; $a = true; $a /= $b;
}
check_assign_to_bool_01(true);


function check_assign_to_bool_02(bool &$a) {
  assert($a === true);

  $a /= true;  assert($a === true);
  $a = false; $a /= true;  assert($a === false);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a /= 123;     assert($a === true);
  $a = true; $a /= 12.34;   assert($a === true);
  $a = true; $a /= "qwe";   assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a /= get_bool();   assert($a === true);
  $a = true; $a /= get_int();    assert($a === true);
  $a = true; $a /= get_double(); assert($a === true);
  $a = true; $a /= get_string(); assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a /= get_bool_ref();   assert($a === true);
  $a = true; $a /= get_int_ref();    assert($a === true);
  $a = true; $a /= get_double_ref(); assert($a === true);
  $a = true; $a /= get_string_ref(); assert($a === true); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  //$b = null;    $a = true; $a /= $b;
  $b = true;    $a = true; $a /= $b; assert($a === true);
  $b = 123;     $a = true; $a /= $b; assert($a === true);
  $b = 12.45;   $a = true; $a /= $b; assert($a === true);
  $b = 'qwe';   $a = true; $a /= $b; assert($a === true);
  //$b = [1,2,3]; $a = true; $a /= $b;
}
$a = true;
check_assign_to_bool_02($a);








function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a = 123; $a /= 456;  assert($a === 0);
  $a = 123; $a /= -12;  assert($a === -10);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a /= 123;   assert($a === 1);
  $a = 123; $a /= 12.34; assert($a === 10);   // expected-warning{{implicit convertion from double to integer}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool();   assert($a === 123);
  $a = 123; $a /= get_int();    assert($a === 1);
  $a = 123; $a /= get_double(); assert($a === 10);  // expected-warning{{implicit convertion from double to integer}}
  //$a = 123; $a /= get_string();                     // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool_ref();   assert($a === 123);
  $a = 123; $a /= get_int_ref();    assert($a === 1);
  $a = 123; $a /= get_double_ref(); assert($a === 10);  // expected-warning{{implicit convertion from double to integer}}
  //$a /= get_string_ref(); assert($a === -727);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null; $a /= $b;
  $b = true;  $a = 123; $a /= $b; assert($a === 123);
  $b = 123;   $a = 123; $a /= $b; assert($a === 1);
  $b = 12.45; $a = 123; $a /= $b; assert($a === 10);
  //$b = 'qwe';   $a /= $b;
  //$b = [1,2,3]; $a /= $b;
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a = 123; $a /= 456;  assert($a === 0);
  $a = 123; $a /= -12;  assert($a === -10);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a /= 123;   assert($a === 1);
  $a = 123; $a /= 12.34; assert($a === 10);   // expected-warning{{implicit convertion from double to integer}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool();   assert($a === 123);
  $a = 123; $a /= get_int();    assert($a === 1);
  $a = 123; $a /= get_double(); assert($a === 10);  // expected-warning{{implicit convertion from double to integer}}
  //$a = 123; $a /= get_string();                     // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool_ref();   assert($a === 123);
  $a = 123; $a /= get_int_ref();    assert($a === 1);
  $a = 123; $a /= get_double_ref(); assert($a === 10);  // expected-warning{{implicit convertion from double to integer}}
  //$a /= get_string_ref(); assert($a === -727);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null; $a /= $b;
  $b = true;  $a = 123; $a /= $b; assert($a === 123);
  $b = 123;   $a = 123; $a /= $b; assert($a === 1);
  $b = 12.45; $a = 123; $a /= $b; assert($a === 10);
  //$b = 'qwe';   $a /= $b;
  //$b = [1,2,3]; $a /= $b;
}
$a = 123;
check_assign_to_int_02($a);








function eq(double $a, double $b) : bool { return abs($a - $b) < 1e-10; }

function check_assign_to_double_01(float $a) {
  assert($a === 123.456);

  $a = 123; $a /= 456.5;   assert(eq($a,   0.26944140197));
  $a = 123; $a /= -12.258; assert(eq($a, -10.03426333822));

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a /= 123;   assert(eq($a, 1.0));
  $a = 123; $a /= 12.34; assert(eq($a, 9.96758508914));

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool();   assert(eq($a, 123.0));
  $a = 123; $a /= get_int();    assert(eq($a, 1.0));
  $a = 123; $a /= get_double(); assert(eq($a, 9.96758508914));
  //$a = 123; $a /= get_string(); // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool_ref();   assert(eq($a, 123.0));
  $a = 123; $a /= get_int_ref();    assert(eq($a, 1.0));
  $a = 123; $a /= get_double_ref(); assert(eq($a, 9.96758508914));
  //$a = 123; $a /= get_string_ref(); // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  //$b = null;    $a = 123; $a /= $b;
  $b = true;    $a = 123; $a /= $b; assert(eq($a, 123.0));
  $b = 123;     $a = 123; $a /= $b; assert(eq($a, 1.0));
  $b = 12.45;   $a = 123; $a /= $b; assert(eq($a, 9.87951807228));
  //$b = 'qwe';   $a = 123; $a /= $b;
  //$b = [1,2,3]; $a = 123; $a /= $b;
}

check_assign_to_double_01(123.456);

function check_assign_to_double_02(float &$a) {
  assert($a === 123.456);

  $a = 123; $a /= 456.5;   assert(eq($a,   0.26944140197));
  $a = 123; $a /= -12.258; assert(eq($a, -10.03426333822));

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a /= 123;   assert(eq($a, 1.0));
  $a = 123; $a /= 12.34; assert(eq($a, 9.96758508914));

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool();   assert(eq($a, 123.0));
  $a = 123; $a /= get_int();    assert(eq($a, 1.0));
  $a = 123; $a /= get_double(); assert(eq($a, 9.96758508914));
  //$a = 123; $a /= get_string(); // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool_ref();   assert(eq($a, 123.0));
  $a = 123; $a /= get_int_ref();    assert(eq($a, 1.0));
  $a = 123; $a /= get_double_ref(); assert(eq($a, 9.96758508914));
  //$a = 123; $a /= get_string_ref(); // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  //$b = null;    $a = 123; $a /= $b;
  $b = true;    $a = 123; $a /= $b; assert(eq($a, 123.0));
  $b = 123;     $a = 123; $a /= $b; assert(eq($a, 1.0));
  $b = 12.45;   $a = 123; $a /= $b; assert(eq($a, 9.87951807228));
  //$b = 'qwe';   $a = 123; $a /= $b;
  //$b = [1,2,3]; $a = 123; $a /= $b;
}
$a = 123.456;
check_assign_to_double_02($a);








function check_assign_to_universal_01($a) {
  //$a /= [4,5,6];

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a /= 123;    assert(eq($a, 1.0));
  $a = 123; $a /= 12.34;  assert(eq($a, 9.96758508914));

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool();   assert(eq($a, 123.0));
  $a = 123; $a /= get_int();    assert(eq($a, 1.0));
  $a = 123; $a /= get_double(); assert(eq($a, 9.96758508914));
  //$a = 123; $a /= get_string();
  //$a = 123; $a /= get_array();

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a /= get_bool_ref();   assert(eq($a, 123.0));
  $a = 123; $a /= get_int_ref();    assert(eq($a, 1.0));
  $a = 123; $a /= get_double_ref(); assert(eq($a, 9.96758508914));
  //$a /= get_string_ref();
  //$a /= get_array_ref(); 

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  //$b = null;    $a /= $b;
  $b = true;  $a = 123; $a /= $b; assert(eq($a, 123.0));
  $b = 123;   $a = 123; $a /= $b; assert(eq($a, 1.0));
  $b = 12.45; $a = 123; $a /= $b; assert(eq($a, 9.87951807228));
  //$b = 'qwe';   $a /= $b;
  //$b = [1,2,3]; $a /= $b;
}
check_assign_to_universal_01(123);

?>