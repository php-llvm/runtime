<?

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_bool_01(bool $a) {
  assert($a === true);

  $a -= false; assert($a === true);
  $a -= true;  assert($a === false);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a -= null;    assert($a === true);  // expected-warning{{implicit convertion from NULL to boolean}}
  $a = true; $a -= 123;     assert($a === false);
  $a = true; $a -= 12.34;   assert($a === false);
  $a = true; $a -= "qwe";   assert($a === false); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a -= get_void();   assert($a === true);  // expected-warning{{implicit convertion from NULL to boolean}}
  $a = true; $a -= get_bool();   assert($a === false);
  $a = true; $a -= get_int();    assert($a === false);
  $a = true; $a -= get_double(); assert($a === false);
  //$a = true; $a -= get_string(); assert($a === false); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a -= get_bool_ref();   assert($a === false);
  $a = true; $a -= get_int_ref();    assert($a === false);
  $a = true; $a -= get_double_ref(); assert($a === false);
  //$a = true; $a -= get_string_ref(); assert($a === false); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  //$b = null;    $a = true; $a -= $b; assert($a === true);
  $b = true;    $a = true; $a -= $b; assert($a === false);
  $b = 123;     $a = true; $a -= $b; assert($a === false);
  $b = 12.45;   $a = true; $a -= $b; assert($a === false);
  //$b = 'qwe';   $a = true; $a -= $b; assert($a === true);
  //$b = [1,2,3]; $a = true; $a -= $b; assert($a === false);
}
check_assign_to_bool_01(true);


function check_assign_to_bool_02(bool &$a) {
  assert($a === true);

  $a -= false; assert($a === true);
  $a -= true;  assert($a === false);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a -= null;    assert($a === true);  // expected-warning{{implicit convertion from NULL to boolean}}
  $a = true; $a -= 123;     assert($a === false);
  $a = true; $a -= 12.34;   assert($a === false);
  $a = true; $a -= "qwe";   assert($a === false); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a -= get_void();   assert($a === true);  // expected-warning{{implicit convertion from NULL to boolean}}
  $a = true; $a -= get_bool();   assert($a === false);
  $a = true; $a -= get_int();    assert($a === false);
  $a = true; $a -= get_double(); assert($a === false);
  //$a = true; $a -= get_string(); assert($a === false); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a -= get_bool_ref();   assert($a === false);
  $a = true; $a -= get_int_ref();    assert($a === false);
  $a = true; $a -= get_double_ref(); assert($a === false);
  //$a = true; $a -= get_string_ref(); assert($a === false); // expected-warning{{implicit convertion from string to boolean}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  //$b = null;    $a = true; $a -= $b; assert($a === true);
  $b = true;    $a = true; $a -= $b; assert($a === false);
  $b = 123;     $a = true; $a -= $b; assert($a === false);
  $b = 12.45;   $a = true; $a -= $b; assert($a === false);
  //$b = 'qwe';   $a = true; $a -= $b; assert($a === true);
  //$b = [1,2,3]; $a = true; $a -= $b; assert($a === false);
}
$a = true;
check_assign_to_bool_02($a);








function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a -= 456;  assert($a === -333);
  $a -= -12;  assert($a === -321);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;  assert($a === -321);   // expected-warning{{implicit convertion from NULL to integer}}
  $a -= false; assert($a === -321);
  $a -= 123;   assert($a === -444);
  $a -= 12.34; assert($a === -456);
  $a -= "qwe"; assert($a === -456);   // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   assert($a === -456);  // expected-warning{{implicit convertion from NULL to integer}}
  $a -= get_bool();   assert($a === -457);
  $a -= get_int();    assert($a === -580);
  $a -= get_double(); assert($a === -592);
  $a -= get_string(); assert($a === -592);    // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   assert($a === -593);
  $a -= get_int_ref();    assert($a === -716);
  $a -= get_double_ref(); assert($a === -728);
  //$a -= get_string_ref(); assert($a === -728);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null; $a -= $b;
  $b = true;  $a -= $b; assert($a === -729);
  $b = 123;   $a -= $b; assert($a === -852);
  $b = 12.45; $a -= $b; assert($a === -864);
  //$b = 'qwe';   $a -= $b;
  //$b = [1,2,3]; $a -= $b;
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a -= 456;  assert($a === -333);
  $a -= -12;  assert($a === -321);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;  assert($a === -321);   // expected-warning{{implicit convertion from NULL to integer}}
  $a -= false; assert($a === -321);
  $a -= 123;   assert($a === -444);
  $a -= 12.34; assert($a === -456);
  $a -= "qwe"; assert($a === -456);   // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   assert($a === -456);  // expected-warning{{implicit convertion from NULL to integer}}
  $a -= get_bool();   assert($a === -457);
  $a -= get_int();    assert($a === -580);
  $a -= get_double(); assert($a === -592);
  $a -= get_string(); assert($a === -592);    // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   assert($a === -593);
  $a -= get_int_ref();    assert($a === -716);
  $a -= get_double_ref(); assert($a === -728);
  //$a -= get_string_ref(); assert($a === -728);  // expected-warning{{implicit convertion from string to integer}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  //$b = null; $a -= $b;
  $b = true;  $a -= $b; assert($a === -729);
  $b = 123;   $a -= $b; assert($a === -852);
  $b = 12.45; $a -= $b; assert($a === -864);
  //$b = 'qwe';   $a -= $b;
  //$b = [1,2,3]; $a -= $b;
}
$a = 123;
check_assign_to_int_02($a);








function eq(double $a, double $b) : bool { return abs($a - $b) < 1e-10; }

function check_assign_to_double_01(float $a) {
  assert($a === 123.456);

  $a -= 456.5;   assert($a === -333.044);
  $a -= -12.258; assert($a === -320.786);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;  assert($a === -320.786);   // expected-warning{{implicit convertion from NULL to double}}
  $a -= false; assert($a === -320.786);
  $a -= 123;   assert($a === -443.786);
  $a -= 12.34; assert($a === -456.126);
  $a -= "qwe"; assert($a === -456.126);   // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   assert($a === -456.126); // expected-warning{{implicit convertion from NULL to double}}
  $a -= get_bool();   assert($a === -457.126);
  $a -= get_int();    assert($a === -580.126);
  $a -= get_double(); assert($a === -592.466);
  $a -= get_string(); assert($a === -592.466);     // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   assert($a === -593.466);
  $a -= get_int_ref();    assert($a === -716.466);
  $a -= get_double_ref(); assert(eq($a, -728.806));
  $a -= get_string_ref(); assert($a === -728.806);  // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  //$b = null;    $a -= $b;
  $b = true;    $a -= $b; assert(eq($a, -729.806));
  $b = 123;     $a -= $b; assert(eq($a, -852.806));
  $b = 12.45;   $a -= $b; assert(eq($a, -865.256));
  //$b = 'qwe';   $a -= $b;
  //$b = [1,2,3]; $a -= $b;
}

check_assign_to_double_01(123.456);

function check_assign_to_double_02(float &$a) {
  assert($a === 123.456);

  $a -= 456.5;   assert($a === -333.044);
  $a -= -12.258; assert($a === -320.786);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;  assert($a === -320.786);   // expected-warning{{implicit convertion from NULL to double}}
  $a -= false; assert($a === -320.786);
  $a -= 123;   assert($a === -443.786);
  $a -= 12.34; assert($a === -456.126);
  $a -= "qwe"; assert($a === -456.126);   // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   assert($a === -456.126); // expected-warning{{implicit convertion from NULL to double}}
  $a -= get_bool();   assert($a === -457.126);
  $a -= get_int();    assert($a === -580.126);
  $a -= get_double(); assert($a === -592.466);
  $a -= get_string(); assert($a === -592.466);     // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   assert($a === -593.466);
  $a -= get_int_ref();    assert($a === -716.466);
  $a -= get_double_ref(); assert(eq($a, -728.806));
  $a -= get_string_ref(); assert($a === -728.806);  // expected-warning{{implicit convertion from string to double}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  //$b = null;    $a -= $b;
  $b = true;    $a -= $b; assert(eq($a, -729.806));
  $b = 123;     $a -= $b; assert(eq($a, -852.806));
  $b = 12.45;   $a -= $b; assert(eq($a, -865.256));
  //$b = 'qwe';   $a -= $b;
  //$b = [1,2,3]; $a -= $b;
}
$a = 123.456;
check_assign_to_double_02($a);

?>