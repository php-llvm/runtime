// RUN: %clang_php %s -verify
<?php

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a = 123; $a **= 2;   assert($a === 15129);
  $a = 123; $a **= -3;  assert($a === 0);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a **= null;  assert($a === 1);  
  $a = 123; $a **= false; assert($a === 1);
  $a = -1;  $a **= 123;   assert($a === -1);
  $a = 2;   $a **= 12.34; var_dump($a);  assert($a === 5184);
  $a = 123; $a **= "qwe"; assert($a === 1);  

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a **= get_void();   assert($a === 1); 
  $a = 123; $a **= get_bool();   assert($a === 123);
  $a = -1;  $a **= get_int();    assert($a === -1);
  $a = 2;   $a **= get_double(); assert($a === 5184); 
  $a = 123; $a **= get_string(); assert($a === 1); 

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a **= get_bool_ref();   assert($a === 123);
  $a = -1;  $a **= get_int_ref();    assert($a === -1);
  $a = 2;   $a **= get_double_ref(); assert($a === 5184); 
  $a = 123; $a **= get_string_ref(); assert($a === 1); 

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null;    $a = 123; $a **= $b; assert($a === 1);
  $b = true;    $a = 123; $a **= $b; assert($a === 123);
  $b = 5;       $a = 2;   $a **= $b; assert($a === 32);
  $b = 12.45;   $a = 2;   $a **= $b; assert($a === 5595); 
  $b = 'qwe';   $a = 123; $a **= $b; assert($a === 1);
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a = 123; $a **= 2;   assert($a === 15129);
  $a = 123; $a **= -3;  assert($a === 0);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 123; $a **= null;  assert($a === 1);  
  $a = 123; $a **= false; assert($a === 1);
  $a = -1;  $a **= 123;   assert($a === -1);
  $a = 2;   $a **= 12.34; var_dump($a);  assert($a === 5184);
  $a = 123; $a **= "qwe"; assert($a === 1);  

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a **= get_void();   assert($a === 1); 
  $a = 123; $a **= get_bool();   assert($a === 123);
  $a = -1;  $a **= get_int();    assert($a === -1);
  $a = 2;   $a **= get_double(); assert($a === 5184); 
  $a = 123; $a **= get_string(); assert($a === 1); 

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a **= get_bool_ref();   assert($a === 123);
  $a = -1;  $a **= get_int_ref();    assert($a === -1);
  $a = 2;   $a **= get_double_ref(); assert($a === 5184); 
  $a = 123; $a **= get_string_ref(); assert($a === 1); 

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null;    $a = 123; $a **= $b; assert($a === 1);
  $b = true;    $a = 123; $a **= $b; assert($a === 123);
  $b = 5;       $a = 2;   $a **= $b; assert($a === 32);
  $b = 12.45;   $a = 2;   $a **= $b; assert($a === 5595); 
  $b = 'qwe';   $a = 123; $a **= $b; assert($a === 1);
}
$a = 123;
check_assign_to_int_02($a);








function eq(double $a, double $b) : bool { return abs($a - $b) < 1e-10; }

function check_assign_to_double_01(float $a) {
  assert($a === 123.456);

  $a = 2; $a **= 1.5;   assert(eq($a, 2.828427124746190));
  $a = 2; $a **= -12.258; assert(eq($a, 0.000204161718729));

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 2; $a **= null;  assert($a === 1.0);  
  $a = 2; $a **= false; assert($a === 1.0);
  $a = 2; $a **= 12;    assert($a === 4096.0);
  $a = 2; $a **= 12.34; assert(eq($a, 5184.539008902266));
  $a = 2; $a **= "qwe"; assert($a === 1.0);  

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 2;  $a **= get_void();   assert($a === 1.0);
  $a = 2;  $a **= get_bool();   assert($a === 2.0);
  $a = -1; $a **= get_int();    assert($a === -1.0);
  $a = 2;  $a **= get_double(); assert(eq($a, 5184.539008902266));
  $a = 2;  $a **= get_string(); assert($a === 1.0);

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 2;  $a **= get_bool_ref();   assert($a === 2.0);
  $a = -1; $a **= get_int_ref();    assert($a === -1.0);
  $a = 2;  $a **= get_double_ref(); assert(eq($a, 5184.539008902266));
  $a = 2;  $a **= get_string_ref(); assert($a === 1.0); 

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a = 2;  $a **= $b; assert($a === 1.0);
  $b = true;    $a = 2;  $a **= $b; assert($a === 2.0);
  $b = 123;     $a = -1; $a **= $b; assert($a === -1.0);
  $b = 12.45;   $a = 2;  $a **= $b; assert(eq($a, 5595.300891666001));
  $b = 'qwe';   $a = 2;  $a **= $b; assert($a === 1.0);
}

check_assign_to_double_01(123.456);

function check_assign_to_double_02(float &$a) {
  assert($a === 123.456);

  $a = 2; $a **= 1.5;   assert(eq($a, 2.828427124746190));
  $a = 2; $a **= -12.258; assert(eq($a, 0.000204161718729));

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = 2; $a **= null;  assert($a === 1.0);  
  $a = 2; $a **= false; assert($a === 1.0);
  $a = 2; $a **= 12;    assert($a === 4096.0);
  $a = 2; $a **= 12.34; assert(eq($a, 5184.539008902266));
  $a = 2; $a **= "qwe"; assert($a === 1.0);  

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 2;  $a **= get_void();   assert($a === 1.0);
  $a = 2;  $a **= get_bool();   assert($a === 2.0);
  $a = -1; $a **= get_int();    assert($a === -1.0);
  $a = 2;  $a **= get_double(); assert(eq($a, 5184.539008902266));
  $a = 2;  $a **= get_string(); assert($a === 1.0);

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 2;  $a **= get_bool_ref();   assert($a === 2.0);
  $a = -1; $a **= get_int_ref();    assert($a === -1.0);
  $a = 2;  $a **= get_double_ref(); assert(eq($a, 5184.539008902266));
  $a = 2;  $a **= get_string_ref(); assert($a === 1.0); 

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a = 2;  $a **= $b; assert($a === 1.0);
  $b = true;    $a = 2;  $a **= $b; assert($a === 2.0);
  $b = 123;     $a = -1; $a **= $b; assert($a === -1.0);
  $b = 12.45;   $a = 2;  $a **= $b; assert(eq($a, 5595.300891666001));
  $b = 'qwe';   $a = 2;  $a **= $b; assert($a === 1.0);
}
$a = 123.456;
check_assign_to_double_02($a);


?>