#!/usr/bin/env python

# ===----------------------------------------------------------------------=== #
#
# phlang - the open source PHP compiler based on llvm/clang.
#
# Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met :
#
# -Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# - Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and / or other materials provided with the distribution.
#
# - Neither the name "phlang" nor the names of its contributors may be used to
# endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# ===----------------------------------------------------------------------=== #
#
# Runtime test harness.
#
# Runs PHP runtime tests, which require building and running executable file.
# Each test (a PHP source file):
#  1) is compiled into executable file,
#  2) the resulted executable is run,
#  3) the result of execution is compared against expected.
#
# ===----------------------------------------------------------------------=== #

import os
import platform
import shutil
import subprocess
import sys
import tempfile
import threading
import time
import ConfigParser
from multiprocessing.pool import ThreadPool
from subprocess import call


# ------------------------------------------------------------------------------
# Harness parameters
# ------------------------------------------------------------------------------

if len(sys.argv) < 3:
    print("Insufficient arguments")
    print("Run: " + sys.argv[0] +
          "<test file directory> <path to compiler> [<compiler options>]")
    exit(1)

# Root directory of test files.
test_directory = sys.argv[1]

# Compiler used for tests.
compiler = sys.argv[2]
if not os.path.isfile(compiler):
    print("ERROR: compiler '" + compiler + "' is not found")
    exit(1)

# Compiler flags used to compile all tests.
common_compiler_flags = []
if len(sys.argv) > 3:
    common_compiler_flags = sys.argv[3:]


# ------------------------------------------------------------------------------
# Harness options.
# ------------------------------------------------------------------------------

# Allowed test file extensions.
allowed_files = ["php"]

# Directories skipped when test files is searched for.
excluded_dirs = ["include"]

# Maximum file name length. Used to format output.
max_file_len = 50

use_parallel_execution = True

# Directory for temporary output files.
tempfile.tempdir=os.curdir
output_dir = tempfile.mkdtemp()

# Path to shell tools on Windows
lit_path = None


# ------------------------------------------------------------------------------
# Harness state
# ------------------------------------------------------------------------------

# Any information related to a test (options, output, return code etc) is stored
# in dictionaries indexed by the test file path. Dictionaries are thread-safe in
# python, so they can be used to collect results from multiple threads.

# --- Options

expected_output = {}   # from .expect file if available
compiler_options = {}  # specified in '.options' file.
runtime_options = {}

# --- Results

compiler_errors = {}
compiler_retcodes = {}  # OS return code
compiler_output = {}

runtime_retcodes = {}   # OS return code
runtime_output = {}     # Captured stdout (for debugging)
runtime_errors = {}     # Captured stderr
output_mismatch = set() # Tests in which output mismatched with expected

num_failed_tests = 0    # Total number of failed tests
num_completed_tests = 0 # Total number of completed tests


# ------------------------------------------------------------------------------
# Working variables
# ------------------------------------------------------------------------------

# All test files.
all_test_files = []

config_file = None


# check if file has correct extension
def is_allowed(file):
    for ext in allowed_files:
        if file.endswith(ext):
            return True
    return False

# check if directory is allowed
def is_excluded(dir):
    for excl in excluded_dirs:
        if excl == dir:
            return True
    return False


# recursively list all files in directory
def list_files(dir):
    for root, dirs, files in os.walk(dir):
        if not is_excluded(root):
            for file in files:
                # consider only phpt files as test files
                # this done to avoid trying to tests files which are not meant to be executed on their own
                # (i.e. they should be only used as include by another tests)
                if is_allowed(file):
                    all_test_files.append(root + "/" + file)


# Taken from http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

# output string using system echo utility
def printer(str) :
    if platform.system() == 'Windows':
        tool = "echo.exe"
    else:
        tool = "echo"
    if not which(tool):
        print("Executable '" + tool + "' was not found in:")
        for path in os.environ["PATH"].split(os.pathsep):
            if path:
                print("    " + path)
        sys.exit()
    call([tool, str])
#    print(str)

# writes file containing output directory into current directory
# this file is needed to perform cleanup if script execution is interrupted for some reason and
# directory is not cleaned
def interrupt_protection(output_directory):
    tmp = open("..tmpDir","w")
    tmp.write(output_directory)
    tmp.close()

# if compiler relies on dynamic runtime we need to export path
# into PATH or LD_LIBRARY_PATH enviromental variables
def export_lib_path(path):
    lib_env_var = ""
    path_separator = ""
    if(platform.system() == "Windows"):
        lib_env_var="PATH"
        path_separator = ";"
    else:
        lib_env_var="LD_LIBRARY_PATH"
        path_separator = ":"
    if(os.environ.get(lib_env_var)):
        os.environ[lib_env_var] = libdir + path_separator + os.environ[lib_env_var]
    else:
        os.environ[lib_env_var] = libdir

# converts file name into string which can be used to name output exe file
# essientially, we replace all characters which are illiegal on windows or linux (/,\,:)
# so /home/file becomes .home.file
def legalize(filename):
    filename = filename.replace("/",".")
    filename = filename.replace("\\",".")
    filename = filename.replace(":","..")
    return filename

# returns status for completed test
# COMPILE FAIL - if test does not compile
# FAIL - if test compiles but fails at runtime
# OK - if test is successful
def get_status(test_source):
    if compiler_success(test_source):
       if runtime_success(test_source) and output_matches(test_source):
           return "OK"
       else:
           return "FAIL"
    else:
           return "COMPILE FAIL"

# returns shortened name for test
# internally we store absolute paths to test files to ease acessing them from any directory
# however such filenames are too long to display efficiently, so instead we display only
# filenames relative to test directory
def shorten(test_source):
    global test_directory
    if test_source.startswith(test_directory):
       return ("{:"+str(max_file_len)+"s}").format(test_source[len(test_directory)+1:])
    else:
       return ("{:"+str(max_file_len)+"s}").format(test_source)


def report_test_progress(completed):
    # to make output more pretty we format it according to maximum length of progress string
    # the lognest progress string is [%FILE_NUM%/%FILE_NUM]: 
    # so it can be computed as 2 length of file number string + 4 symbols (for [,/,],:) + extra space
    max_progress_len = len(str(len(all_test_files))) * 2 + 5
    progress = ("{:"+ str(max_progress_len)+"s}").format("[" + str(completed) + "/" + str(len(all_test_files)) + "]: ")
    return progress


# reports success or failure of particular test
# this function contains critical section to avoid threads messing with each other output
# printing output relies on calling system echo utility 
# this is done to prevent MSVS or other IDE capturing stdout and
# not printing anything until test is done

sync = threading.Lock()
def report(test_source):
    global num_failed_tests
    global num_completed_tests
    sync.acquire()
    num_completed_tests = num_completed_tests + 1
    status = get_status(test_source)
    if status != "OK":
        num_failed_tests = num_failed_tests + 1
    printer(report_test_progress(num_completed_tests) + shorten(test_source) + " " + status)
    sync.release()

def print_separator():
    printer("===============================================================================")


def start_report():
    printer("Runtime PHP test suite")
    printer("Using compiler: " + compiler)
    printer("Compiler flags: " + str(common_compiler_flags))
    printer("Test files directory: " + test_directory)
    printer("Temporary directory: " + output_dir)
    printer(str(len(all_test_files)) + " tests to run")
    print_separator()

# prints final report on the test run including total number of successful and failing tests
# as well as detailed information on failing tests
# currently that includes:
# If tests does not compile:
#
# 1) compiler return code 
# 2) compiler stderr output 
# 3) compiler stdout output

# If tests compiles, but fails at runtime
#
# 1) runtime return code
# 2) rutime stderr output
# 3) runtime stdout output
# 4) runtime expected output (if output does not match it)


def final_report():
    global elapsed_time
    print_separator()
    printer("Sucessfully run " + str(len(all_test_files) - num_failed_tests) + " tests, failed " + str(num_failed_tests))
    if num_failed_tests != 0:
        for file in all_test_files:
            if not compiler_success(file):
                print_separator()
                printer("Test " + file + " failed at compile time.")
                printer("Compiler return code: " + str(compiler_retcodes[file]))
                printer("Compiler error output:")
                printer(compiler_errors[file])
                printer("Compiler output:")
                printer(compiler_output[file])
                if file in compiler_options:
                    printer("Compiler options:")
                    printer(compiler_options[file])
                if file in runtime_options:
                    printer("Runtime options:")
                    printer(runtime_options[file])
                print_separator()
            else:
                if not runtime_success(file):
                    print_separator()
                    printer("Test " + file + " failed at runtime.")
                    printer("Runtime return code: " + str(runtime_retcodes[file]))
                    printer("Runtime error output:")
                    printer(runtime_errors[file].decode("utf-8"))
                    printer("Runtime output:")
                    printer(runtime_output[file].decode("utf-8"))
                    if not output_matches(file):
                        printer("Expected output:")
                        printer(expected_output[file])
                    print_separator()
    printer("Time elapsed " + str(elapsed_time) + "s")

# check saved retcodes to determine success of compilation/execution
def check_success(dictionary, test_source):
    if not (test_source in dictionary):
        return False
    if dictionary[test_source] != 0:
        return False
    return True


def output_matches(test_source):
    if not test_source in output_mismatch:
        return True
    return False


def compiler_success(test_source):
    return check_success(compiler_retcodes, test_source)


def runtime_success(test_source):
    return check_success(runtime_retcodes, test_source) and output_matches(test_source)


# Packs all popen arguments into flat list to avoid issues on linux
def pack_arguments(*args):
    output = []
    for arg in args:
        if type(arg) is list:
            output += arg
        else:
            output.append(str(arg))
    return output


# Compiles provided file to executable.
# if compilation fails error output and return code is saved
def compile(test_source):
    global compiler
    global compiler_errors
    global compiler_retcodes
    global output_dir
    global compiler_options
    output = output_dir + "/" + legalize(test_source) + ".exe"
    extra_flags = compiler_options.get(test_source, '')
    args = pack_arguments(compiler, common_compiler_flags, extra_flags, test_source, "-o", output)
    compile_call = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    io = compile_call.communicate()
    compiler_retcodes[test_source] = compile_call.returncode
    if not compiler_success(test_source):
        compiler_errors[test_source] = io[1] # stderr
        compiler_output[test_source] = io[0] # stdout
    return output


# runs previously compiled executable and saves return code and any error output
# test_source parameter is needed to update correct entry in test files database
def run_exe(test_source, executable):
    global runtime_errors
    global runtime_retcodes
    global runtime_output
    global runtime_options
    extra_options = runtime_options.get(test_source, '')
    args = pack_arguments([executable], extra_options)
    executor = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # we use communicate instead of wair because otherwise process may hang due to piped output
    io = executor.communicate()
    runtime_retcodes[test_source] = executor.returncode
    # is there was an error save stderr
    runtime_errors[test_source] = io[1] # stderr
    runtime_output[test_source] = io[0] #stdout
    # if we have .expect file we must also check test output against it
    if os.path.isfile(test_source + ".expect"):
        exp_file = open(test_source + ".expect", "r")
        expected_output[test_source] = exp_file.read()
        # mark test as failing output check
        if expected_output[test_source].encode('UTF-8') != runtime_output[test_source]:
            output_mismatch.add(test_source)
        exp_file.close()


# Reads content of option file if it is found.
def read_options(test_source):
    global compiler_options
    global runtime_options
    if os.path.isfile(test_source + ".options"):
        compile_opts_read = False
        runtime_opts_read = False
        with open(test_source + ".options", "r") as opts_file:
            for line in opts_file:
                line = line.strip()
                if line:
                    if line[0] != '#':
                        if not compile_opts_read:
                            if line != '-':
                                compiler_options[test_source] = line
                            compile_opts_read = True
                            continue
                        if not runtime_opts_read:
                            if line != '-':
                                runtime_options[test_source] = line
                            runtime_opts_read = True
                            continue
                        break
            opts_file.close()


# runs compiled tests executable, saves return code and any error output
def run_test(test_source):
    read_options(test_source)
    executable = compile(test_source)
    if compiler_success(test_source):
        run_exe(test_source,executable)
    report(test_source)


# display compiler version
def display_version():
    printer("Compiler version:")
    print_separator()
    print(compiler)
    call([compiler, "-v"])
    print_separator()


def read_config_file():
    global config_file
    global lit_path
    config_file = ConfigParser.ConfigParser()
    cfgfile = os.path.join(os.path.curdir, 'tester.cfg')
    cfgfile = os.path.abspath(cfgfile)
    if os.path.isfile(cfgfile):
        print("Config file found: " + cfgfile)
        config_file.read(cfgfile)
        lit_path = config_file.get('path', 'lit_tools', '')
        if lit_path:
            lit_path = os.path.abspath(lit_path)
            print("Tools directory: " + lit_path)
            if not os.path.isdir(lit_path):
                sys.exit("Directory " + lit_path + " not found")
            if not lit_path in os.environ["PATH"].split(os.pathsep):
                os.environ["PATH"] += os.pathsep + lit_path

# Checks if there are no remnants of previous runs, remove them if found.
def prerun_cleanup():
    if os.path.isfile("..tmpDir"):
        printer("Warning: previous test run did not finish correctly. Performing cleanup.")
        f = open("..tmpDir","r")
        oldDir = f.readline() 
        f.close()
        if os.path.isdir(oldDir) :
            shutil.rmtree(oldDir)
            printer("Removed " + oldDir)

def postrun_cleanup(): 
    shutil.rmtree(output_dir, ignore_errors=True)
    os.remove("..tmpDir")


# ------------------------------------------------------------------------------
# Execution start point.
# ------------------------------------------------------------------------------

read_config_file()

# perform initial cleaunup
prerun_cleanup() 

# Display version info.
display_version()

# setup protection again interruption
interrupt_protection(output_dir)

list_files(test_directory)

start_report()
# start parallel execution using ThreadPool
start_time = time.time()
if use_parallel_execution:
    ThreadRunner = ThreadPool()
    ThreadRunner.map(run_test, all_test_files)
else:
    for test in all_test_files:
        run_test(test)
end_time = time.time()
elapsed_time = end_time - start_time

final_report()

# testing is complete. Remove temporary directory
postrun_cleanup()